import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DialogService } from '@coyoapp/ui/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent {
  items = [
    { id: 1, name: 'Volvo' },
    { id: 2, name: 'Saab' },
    { id: 3, name: 'Opel' },
    { id: 4, name: 'Audi' },
    { id: 5, name: 'Saab' },
    { id: 6, name: 'Opel' },
    { id: 7, name: 'Audi' },
    { id: 8, name: 'Saab' },
    { id: 9, name: 'Opel' },
    { id: 10, name: 'Audi' },
    { id: 11, name: 'Saab' },
    { id: 12, name: 'Opel' },
    { id: 13, name: 'Audi' },
    { id: 14, name: 'Saab' },
    { id: 15, name: 'Opel' },
    { id: 16, name: 'Audi' }
  ];

  test?: { id: string, name: string };

  constructor(
    private readonly dialogService: DialogService
  ) {}

  openDialog(): void {
    this.dialogService.open(DialogComponent);
  }
}
