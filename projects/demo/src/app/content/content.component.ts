import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentComponent implements OnInit, OnDestroy {

  @Input()
  name = 'content';

  ngOnInit(): void {
    console.log(`Initializing ${this.name}`);
  }

  ngOnDestroy(): void {
    console.log(`Destroying ${this.name}`);
  }
}
