import { A11yModule } from '@angular/cdk/a11y';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { CardModule } from '@coyoapp/ui/card';
import { CheckboxModule } from '@coyoapp/ui/checkbox';
import { CollapseModule } from '@coyoapp/ui/collapse/collapse.module';
import { DialogModule } from '@coyoapp/ui/dialog';
import { FileModule } from '@coyoapp/ui/file';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import {
  FormlyCheckboxComponent,
  FormlyInputDatepickerComponent,
  FormlyInputNumberComponent,
  FormlyInputTextareaComponent,
  FormlyInputTextComponent,
  FormlySelectComponent
} from '@coyoapp/ui/formly-field';
import { IconModule } from '@coyoapp/ui/icon';
import { InputDatepickerModule } from '@coyoapp/ui/input-datepicker';
import { InputNumberModule } from '@coyoapp/ui/input-number';
import { InputPasswordModule } from '@coyoapp/ui/input-password';
import { InputTextModule } from '@coyoapp/ui/input-text';
import { InputTextareaModule } from '@coyoapp/ui/input-textarea';
import { InputTimepickerModule } from '@coyoapp/ui/input-timepicker';
import { MenuModule } from '@coyoapp/ui/menu';
import { PopoverModule } from '@coyoapp/ui/popover';
import { RadioModule } from '@coyoapp/ui/radio';
import { ScrollingModule } from '@coyoapp/ui/scrolling';
import { SelectModule } from '@coyoapp/ui/select';
import { SidebarModule } from '@coyoapp/ui/sidebar';
import { TabsModule } from '@coyoapp/ui/tabs';
import { ThemeModule } from '@coyoapp/ui/theme';
import { ToggleModule } from '@coyoapp/ui/toggle';
import { TooltipModule } from '@coyoapp/ui/tooltip';
import { FormlyModule } from '@ngx-formly/core';
import { AppComponent } from './app.component';
import { ContentComponent } from './content/content.component';
import { DialogComponent } from './dialog/dialog.component';
import { NavigationCardDemoComponent } from './navigation-card-demo/navigation-card-demo.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    IconModule.forRoot(),
    ButtonModule,
    CheckboxModule,
    DialogModule,
    FileModule,
    FormFieldModule,
    InputDatepickerModule.forRoot(),
    InputNumberModule,
    InputPasswordModule,
    InputTextareaModule,
    InputTextModule,
    InputTimepickerModule.forRoot(),
    MenuModule.forRoot(),
    RadioModule,
    SelectModule,
    TabsModule,
    ThemeModule,
    ToggleModule,
    CollapseModule,
    SidebarModule,
    PopoverModule.forRoot(),
    TooltipModule.forRoot(),
    ScrollingModule,
    CardModule,
    A11yModule,
    FormlyModule.forRoot({
      types: [
        { name: 'cui-number', component: FormlyInputNumberComponent },
        { name: 'cui-checkbox', component: FormlyCheckboxComponent },
        { name: 'cui-text', component: FormlyInputTextComponent },
        { name: 'cui-textarea', component: FormlyInputTextareaComponent },
        { name: 'cui-date', component: FormlyInputDatepickerComponent },
        { name: 'cui-select', component: FormlySelectComponent }
      ]
    })
  ],
  declarations: [AppComponent, DialogComponent, ContentComponent, NavigationCardDemoComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
