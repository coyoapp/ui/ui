import { Component } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { CheckboxChangeEvent } from '@coyoapp/ui/checkbox';
import { DialogWidth } from '@coyoapp/ui/dialog';
import { DialogService } from '@coyoapp/ui/dialog/dialog/dialog.service';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, delay, switchMap, tap } from 'rxjs/internal/operators';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  model = {
    number: 5
  };
  form = new UntypedFormGroup({});
  fields: FormlyFieldConfig[] = [
    {
      id: '1',
      key: 'number',
      type: 'cui-number',
      name: 'Number',
      templateOptions: {
        max: 10,
        min: 5,
        placeholder: '10',
        required: true,
        label: 'number label',
        description: 'number hint'
      }
    },
    {
      id: '2',
      key: 'text',
      type: 'cui-text',
      name: 'Text',
      defaultValue: 'Initial text',
      templateOptions: {
        maxLength: 20,
        minLength: 3,
        placeholder: 'placeholder text',
        required: true,
        label: 'text label',
        description: 'text hint'
      }
    },
    {
      id: '3',
      key: 'textarea',
      type: 'cui-textarea',
      name: 'Textarea',
      defaultValue: 'Initial textarea',
      templateOptions: {
        maxLength: 20,
        minLength: 3,
        placeholder: 'placeholder textarea',
        required: true,
        label: 'textarea label',
        description: 'textarea hint'
      }
    },
    {
      id: '4a',
      key: 'checkbox',
      type: 'cui-checkbox',
      name: 'Checkbox',
      defaultValue: true,
      templateOptions: {
        required: true,
        label: 'checkbox label 1',
        description: 'checkbox hint 1'
      }
    },
    {
      id: '4b',
      key: 'checkbox',
      type: 'cui-checkbox',
      name: 'Checkbox',
      defaultValue: false,
      templateOptions: {
        required: false,
        label: 'checkbox label 2',
        description: 'checkbox hint 2'
      }
    },
    {
      id: '5',
      key: 'datepicker',
      type: 'cui-date',
      name: 'Datepicker',
      defaultValue: '10/10/2021',
      templateOptions: {
        placeholder: 'mm/dd/yyyy',
        required: true,
        label: 'date label',
        description: 'date hint'
      }
    },
    {
      id: '6',
      key: 'select',
      type: 'cui-select',
      name: 'Select',
      defaultValue: [1, 2],
      templateOptions: {
        required: true,
        label: 'select label',
        description: 'select hint',
        multiple: true,
        bindValue: 'key',
        bindLabel: 'label',
        options: [
          { key: 1, label: 'Volvo1' },
          { key: 2, label: 'Saab2' },
          { key: 3, label: 'Opel3' }]
      }
    }
  ];

  count = 1;
  active = false;
  str = 'http://www.coyoapp.com';

  items = [
    { id: 1, name: 'Volvo1' },
    { id: 2, name: 'Saab2' },
    { id: 3, name: 'Opel3' },
    { id: 4, name: 'Audi4' },
    { id: 5, name: 'Saab5' },
    { id: 6, name: 'Opel6' },
    { id: 7, name: 'Audi7' },
    { id: 8, name: 'Saab8' },
    { id: 9, name: 'Opel9' },
    { id: 10, name: 'Audi10' },
    { id: 11, name: 'Saab11' },
    { id: 12, name: 'Opel12' },
    { id: 13, name: 'Audi13' },
    { id: 14, name: 'Saab14' },
    { id: 15, name: 'Opel15' },
    { id: 16, name: 'Audi16' }
  ];

  name1 = new UntypedFormControl('');
  name2 = new UntypedFormControl(42);
  name3 = new UntypedFormControl('');
  name4 = new UntypedFormControl('');
  name5 = new UntypedFormControl();
  name6 = new UntypedFormControl();
  name7 = new UntypedFormControl();
  name8 = new UntypedFormControl();
  radioControl = new UntypedFormControl('ok');

  formGroup = new UntypedFormGroup({
    name1: new UntypedFormControl('test', [Validators.required])
  });

  selfSavingControl = new UntypedFormControl('');
  disabledFieldValue = 'Hello World';

  bool = new UntypedFormControl(true);

  asyncItems$: Observable<{ id: number, name: string }[]>;

  search$: BehaviorSubject<string> = new BehaviorSubject<string>('');

  searchFn: (term: string, item: unknown) => boolean;

  submitKeys: string[] = [' ', ':', ','];

  loading$ = new Subject<boolean>();

  collapsed = false;

  backendResponseSubjects: { [key: string]: Subject<boolean> } = {
    text: new Subject(),
    number: new Subject(),
    password: new Subject(),
    textarea: new Subject(),
    timepicker: new Subject(),
    datepicker: new Subject()
  };

  get tooltipHTML(): string {
    const names = ['Fynn', 'Mark', 'Tim', 'Manuel', 'Daniel', 'Oliver', 'Lukas', 'Cedrik', 'Marko'];
    return names.join('<br/>');
  }

  constructor(
    private readonly dialogService: DialogService
  ) {
    this.asyncItems$ = this.search$
      .pipe(debounceTime(300),
        tap(() => this.loading$.next(true)),
        switchMap(term => of(this.items.filter(item => item.name.includes(term)))
          .pipe(delay(300))),
        tap(() => this.loading$.next(false)));
    this.searchFn = (term, item) => true;
  }

  openDialog(): void {
    this.dialogService.open(DialogComponent, {
      width: DialogWidth.LARGE,
      closeOnBackdropClick: true
    });
  }

  onChange(event: CheckboxChangeEvent): void {
    console.log('change', event);
  }

  onIndeterminateChange(event: boolean): void {
    console.log('indeterminateChange', event);
  }

  backendSaveSimulation(type: string) {
    return () => {
      setTimeout(() => {
        this.backendResponseSubjects[type].next(true);
      }, 3000);

      return this.backendResponseSubjects[type].asObservable();
    };
  }

  collapsableHidden() {
    console.log('collapsable hidden');
  }

  collapsableShown() {
    console.log('collapsable shown');
  }

  log(event: any) {
    console.log(event);
  }

  onSubmit(model: any) {
    console.log(model);
  }
}
