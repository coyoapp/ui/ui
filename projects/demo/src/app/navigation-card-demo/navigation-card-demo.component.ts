import { ChangeDetectionStrategy, Component } from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

@Component({
  selector: 'cui-navigation-card-demo',
  templateUrl: './navigation-card-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationCardDemoComponent {
  active = 0;
  items: { name: string, icon: coyoIcon }[] = [{
    name: 'Timeline',
    icon: 'timeline-app'
  }, {
    name: 'Wiki',
    icon: 'wiki-app'
  }, {
    name: 'Blog',
    icon: 'blog-app'
  }];
}
