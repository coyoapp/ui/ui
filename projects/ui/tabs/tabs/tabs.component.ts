import { FocusKeyManager } from '@angular/cdk/a11y';
import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnDestroy,
  Output,
  QueryList,
  ViewChildren,
  ViewEncapsulation
} from '@angular/core';
import { ButtonComponent } from '@coyoapp/ui/button';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { merge, Subscription } from 'rxjs';
import { CUI_TAB_HANDLE, TabHandle } from '../tab-handle/tab-handle';
import { TabComponent } from '../tab/tab.component';
import { borderAnimation } from './tabs.animations';

/**
 * COYO-UI Tabs component
 *
 * @description
 * Tabs component is similar in functionality to material UI tabs. The
 * `cui-tabs component` can contain multiple `cui-tab` components.
 *
 * @example
 * <cui-tabs>
 *   <cui-tab label="First" prefixIcon="globe">This is the <b>first</b> tab.</cui-tab>
 *   <cui-tab label="Second">This is the <b>second</b> tab.</cui-tab>
 *   <cui-tab label="Third">This is the <b>third</b> tab.</cui-tab>
 *   <cui-tab label="Fourth" [disabled]="true">This is the <b>fourth</b> tab.</cui-tab>
 *   <cui-tab label="Fifth" suffixIcon="shield">This is the <b>fifth</b> tab.</cui-tab>
 *   <cui-tab label="URL" url="ui/router" uiSrefStatus #srs="uiSrefStatus" [urlActive]="srs.status.active"></cui-tab>
 *   <cui-tab label="URL" url="angular/router" routerLinkActive #rla="routerLinkActive" [urlActive]="rla.isActive"></cui-tab>
 * </cui-tabs>
 */
@Component({
  selector: 'cui-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  exportAs: 'tabs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-tabs'
  },
  animations: [
    borderAnimation
  ],
  providers: [{
    provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
    useExisting: forwardRef(() => TabsComponent)
  }]
})
export class TabsComponent implements AfterViewInit, AfterContentInit, OnDestroy {
  private stateSubscription?: Subscription;

  private _activeTabId: string | undefined;
  private _activeMenuId: string | undefined;
  private _activeUrlTabId: string | undefined;
  private _activeUrlMenuId: string | undefined;

  /**
   * The ID of the active tab.
   */
  @Input()
  set active(active: string) {
    const oldActive = this.active;
    const oldActiveMenu = this.activeMenu;
    const tab = [...this.tabs].find(tab => tab.id === active);
    this._activeTabId = tab?.id;
    this._activeMenuId = this.findActiveMenuId(tab);
    if (oldActive !== this.active) {
      this.tabChange.emit(this.active);
    }
    if (oldActive !== this.active || oldActiveMenu !== this.activeMenu) {
      this.changeDetectorRef.markForCheck();
    }
  }

  get active(): string { return this._activeUrlTabId || this._activeTabId || ''; }
  get activeMenu(): string { return this._activeUrlMenuId || this._activeMenuId || ''; }
  get activeContent(): TabComponent | undefined { return this.tabs.find(tab => tab.id === this._activeTabId); }

  /**
   * Tabs align
   */
  @Input()
  @HostBinding('attr.data-cui-align')
  tabAlign: 'left' | 'justify' | 'center' = 'left';

  /**
   * Event emitter for the current tab ID.
   */
  @Output()
  readonly tabChange = new EventEmitter<string>();

  @ContentChildren(TabComponent, { descendants: true })
  tabs!: QueryList<TabComponent>;

  @ContentChildren(CUI_TAB_HANDLE, { descendants: true })
  tabHandles!: QueryList<TabHandle>;

  @ViewChildren('tabLabel', { read: ButtonComponent })
  labels!: QueryList<ButtonComponent>;

  keyManager!: FocusKeyManager<ButtonComponent>;

  constructor(
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngAfterViewInit(): void {
    this.keyManager = new FocusKeyManager<ButtonComponent>(this.labels)
      .withHorizontalOrientation('ltr')
      .withWrap();
  }

  ngAfterContentInit(): void {
    this.stateSubscription = merge(...this.tabs.map(tab => tab.stateChanges$))
      .subscribe(changes => {
        this.changeDetectorRef.markForCheck();
        if (changes.urlActive) {
          const oldActive = this.active;
          const tab = this.tabs.find(tab => tab.urlActive);
          this._activeUrlTabId = tab?.id;
          this._activeUrlMenuId = this.findActiveMenuId(tab);
          if (oldActive !== this.active) {
            this.tabChange.emit(this.active);
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.stateSubscription?.unsubscribe();
  }

  clickTab(tab: TabComponent): void {
    const label = this.labels.find(label => label.elementRef.nativeElement.id === tab.labelId);
    if (label) {
      label?.elementRef.nativeElement.click();
    } else {
      this.active = tab.id;
    }
  }

  private findActiveMenuId(tab?: TabComponent): string | undefined {
    if (tab) {
      const tabHandle = this.tabHandles.find(handle => handle.tab === tab);
      const tabMenu = tabHandle?.menuItem.menu;
      return this.tabs.find(tab => !!tabMenu && tab.menuTrigger?.menu === tabMenu)?.id;
    }
    return undefined;
  }
}
