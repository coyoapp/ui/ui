import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { IconModule } from '@coyoapp/ui/icon';
import { MenuModule } from '@coyoapp/ui/menu';
import { PaginationComponent } from '@coyoapp/ui/pagination';
import { ChangeDetectorRef, QueryList } from '@angular/core';
import { TabsComponent } from '../index';

describe('TabsComponent', () => {
  let component: TabsComponent;
  let fixture: ComponentFixture<TabsComponent>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    await TestBed.configureTestingModule({
      declarations: [TabsComponent],
      imports: [MenuModule.forRoot(), IconModule.forRoot()],
      providers: [
        {
          provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
          useValue: PaginationComponent
        }]
    }).overrideTemplate(TabsComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsComponent);
    component = fixture.componentInstance;
    (component as any).changeDetectorRef = changeDetectorRef;
    component.tabHandles = new QueryList();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get correct CUI_FOCUS_KEY_MANAGER_PROVIDER provider of type MenuComponent', () => {
    expect(TestBed.inject(CUI_FOCUS_KEY_MANAGER_PROVIDER)).toBeDefined();
    const paginationDecorators = (<any>TabsComponent).__annotations__;
    const useExistingClass = paginationDecorators[0].providers[0].useExisting();

    expect(paginationDecorators[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(TabsComponent);
  });
});
