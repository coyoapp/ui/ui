export * from './tabs.module';
export * from './tabs/tabs.component';
export * from './tabs/tabs.animations';
export * from './tab/tab.component';
export * from './tab-handle/tab-handle.directive';
