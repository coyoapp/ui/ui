import { InjectionToken } from '@angular/core';
import { MenuItemComponent } from '@coyoapp/ui/menu';
import { TabHandleDirective } from '..';
import { TabComponent } from '../tab/tab.component';

export const CUI_TAB_HANDLE = new InjectionToken<TabHandleDirective>('cuiTabHandle');

export interface TabHandle {
  tab: TabComponent;
  menuItem: MenuItemComponent
}
