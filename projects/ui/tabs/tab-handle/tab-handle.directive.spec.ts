import { fakeAsync, tick } from '@angular/core/testing';
import { MenuItemComponent } from '@coyoapp/ui/menu';
import { Subject } from 'rxjs';
import { TabComponent } from '../tab/tab.component';
import { TabsComponent } from '../tabs/tabs.component';
import { TabHandleDirective } from './tab-handle.directive';

describe('TabHandleDirective', () => {
  let tabChange$: Subject<string>;
  let menuItem: MenuItemComponent;
  let tabs: jasmine.SpyObj<TabsComponent>;

  beforeEach(() => {
    menuItem = { active: false } as MenuItemComponent;
    tabs = jasmine.createSpyObj('TabsComponent', ['clickTab']);
    tabChange$ = new Subject<string>();
    (tabs as any).tabChange = tabChange$.asObservable();
  });

  it('should create an instance', () => {
    const directive = new TabHandleDirective(menuItem, tabs);

    expect(directive).toBeTruthy();
  });

  it('should update active state', fakeAsync(() => {
    const tab = { id: 'tabId' } as TabComponent;
    const directive = new TabHandleDirective(menuItem, tabs);
    directive.tab = tab;

    tabChange$.next('tabId');
    tick();

    expect(menuItem.active).toBe(true);
  }));

  it('should activate', () => {
    const tab = { id: 'tabId' } as TabComponent;
    const directive = new TabHandleDirective(menuItem, tabs);
    directive.tab = tab;
    directive.activate();

    expect(tabs.clickTab).toHaveBeenCalledWith(tab);
  });
});
