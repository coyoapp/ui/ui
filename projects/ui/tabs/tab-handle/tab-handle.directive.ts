import { Directive, Input, OnDestroy, Self } from '@angular/core';
import { MenuItemComponent } from '@coyoapp/ui/menu';
import { Subscription } from 'rxjs';
import { TabComponent } from '../tab/tab.component';
import { TabsComponent } from '../tabs/tabs.component';
import { CUI_TAB_HANDLE, TabHandle } from './tab-handle';

@Directive({
  selector: '[cui-menu-item][cui-tab-handle]',
  providers: [{
    provide: CUI_TAB_HANDLE,
    useExisting: TabHandleDirective
  }],
  host: {
    '(click)': 'activate($event)'
  }
})
export class TabHandleDirective implements TabHandle, OnDestroy {
  private changeSubscription?: Subscription;

  /**
   * The corresponding tab.
   */
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('cui-tab-handle')
  tab!: TabComponent;

  /**
   * Wether the tab handle is disabled.
   */
  @Input()
  disabled = false;

  constructor(
    @Self() readonly menuItem: MenuItemComponent,
    private readonly tabs: TabsComponent
  ) {
    this.changeSubscription = this.tabs.tabChange.subscribe(id => {
      this.menuItem.active = this.tab.id === id;
    });
  }

  ngOnDestroy(): void {
    this.changeSubscription?.unsubscribe();
  }

  /**
   * Activate the tab (i.e. trigger a click action to also account for links).
   */
  activate(): void {
    this.tabs.clickTab(this.tab);
  }
}
