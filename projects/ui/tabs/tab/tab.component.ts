import {
  ChangeDetectionStrategy,
  Component,
  Host,
  Input,
  OnChanges,
  OnDestroy,
  Optional,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { MenuTriggerDirective } from '@coyoapp/ui/menu';
import { Subject } from 'rxjs';

let nextUniqueId = 0;

/**
 * COYO-UI Tab component
 *
 * @description
 * The tab component is used in combination with the parent tabs component and
 * it's similar in functionality with material UI tabs. You have to wrap the
 * content of each tab inside the `cui-tab` tag.
 *
 * @example
 * <cui-tabs>
 *   <cui-tab label="First" prefixIcon="globe">This is the <b>first</b> tab.</cui-tab>
 *   <cui-tab label="Second">This is the <b>second</b> tab.</cui-tab>
 *   <cui-tab label="Third" suffixIcon="shield">This is the <b>third</b> tab.</cui-tab>
 * </cui-tabs>
 */
@Component({
  selector: 'cui-tab',
  templateUrl: './tab.component.html',
  exportAs: 'tab',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-tab'
  }
})
export class TabComponent implements OnChanges, OnDestroy {
  private readonly _uid = nextUniqueId++;
  private readonly _uidTab = `cui-tab-${this._uid}`;
  private readonly _uidLabel = `cui-tab-label-${this._uid}`;
  private readonly _stateChanges = new Subject<SimpleChanges>();

  readonly stateChanges$ = this._stateChanges.asObservable();

  /**
   * The ID of this tab.
   */
  @Input()
  get id(): string { return this._id; }
  set id(value: string) { this._id = value || this._uidTab; }
  private _id: string = this._uidTab;

  /**
   * The ID of this tab label.
   */
  @Input()
  get labelId(): string { return this._labelId; }
  set labelId(value: string) { this._labelId = value || this._uidLabel; }
  private _labelId: string = this._uidLabel;

  @ViewChild(TemplateRef)
  content?: TemplateRef<any>;

  /**
   * A template reference to be used instead of the component's content.
   * Used to implement lazy initialization for the tab content. Takes precedence
   * over the component's content.
   */
  @Input()
  template?: TemplateRef<any>;

  /**
   * The tab label.
   */
  @Input()
  label = '';

  /**
   * A URL to link to instead of showing tab content.
   */
  @Input()
  url?: string;

  /**
   * Specifies where to open the content linked via the `url` property.
   */
  @Input()
  urlTarget?: '_blank' | '_self';

  /**
   * Wether this URL tab label is active.
   */
  @Input()
  urlActive = false;

  /**
   * Wether this tab label is disabled.
   */
  @Input()
  disabled = false;

  /**
   * The name of an icon to prefix the tab label.
   */
  @Input()
  prefixIcon?: coyoIcon;

  /**
   * The name of an icon to suffix the tab label.
   */
  @Input()
  suffixIcon?: coyoIcon;

  /**
   * Optional CSS classes to be applied to the tab label.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  class?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * Optional CSS classes to be applied to the textual content of the tab label.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  textClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * Optional CSS classes to be applied to the icons of the tab label.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  iconClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * The actually used template reference.
   */
  get templateRef(): TemplateRef<any> | undefined {
    return this.template || this.content;
  }

  constructor(
    @Optional() @Host() public readonly menuTrigger?: MenuTriggerDirective
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    this._stateChanges.next(changes);
  }

  ngOnDestroy(): void {
    this._stateChanges.complete();
  }
}
