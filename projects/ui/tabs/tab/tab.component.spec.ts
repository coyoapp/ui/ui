import { ChangeDetectorRef, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabComponent } from './tab.component';

describe('TabComponent', () => {
  let component: TabComponent;
  let fixture: ComponentFixture<TabComponent>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);

    await TestBed.configureTestingModule({
      declarations: [TabComponent]
    }).overrideTemplate(TabComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabComponent);
    component = fixture.componentInstance;
    (component as any).changeDetectorRef = changeDetectorRef;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the ID', () => {
    expect(component.id).toMatch(/^cui-tab-\d+$/);
  });

  it('should set the ID', () => {
    component.id = 'some-id';

    expect(component.id).toEqual('some-id');
    component.id = '';

    expect(component.id).toMatch(/^cui-tab-\d+$/);
  });

  it('should get the label ID', () => {
    expect(component.labelId).toMatch(/^cui-tab-label-\d+$/);
  });

  it('should set the label ID', () => {
    component.labelId = 'some-id';

    expect(component.labelId).toEqual('some-id');
    component.labelId = '';

    expect(component.labelId).toMatch(/^cui-tab-label-\d+$/);
  });

  it('should call stateChanges.next on ngOnChanges', () => {
    const changes = { label: new SimpleChange(undefined, 'label', true) };
    const { _stateChanges } = component as any;
    spyOn(_stateChanges, 'next');

    component.ngOnChanges(changes);

    expect(_stateChanges.next).toHaveBeenCalledWith(changes);
  });

  it('should call stateChanges.complete on ngOnDestroy', () => {
    const { _stateChanges } = component as any;
    spyOn(_stateChanges, 'complete');

    component.ngOnDestroy();

    expect(_stateChanges.complete).toHaveBeenCalled();
  });
});
