import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { MenuModule } from '@coyoapp/ui/menu';
import { TabHandleDirective } from './tab-handle/tab-handle.directive';
import { TabComponent } from './tab/tab.component';
import { TabsComponent } from './tabs/tabs.component';

@NgModule({
  imports: [CommonModule, ButtonModule, MenuModule],
  declarations: [TabsComponent, TabComponent, TabHandleDirective],
  exports: [TabsComponent, TabComponent, TabHandleDirective]
})
export class TabsModule {}
