import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';

@Component({
  selector: 'cui-input-textarea',
  templateUrl: './input-textarea.component.html',
  styleUrls: ['./input-textarea.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputTextareaComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-textarea'
  }
})
export class InputTextareaComponent extends FormFieldControlComponent<string> {

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  /**
   * The minimum height of this input.
   */
  @Input()
  minRows = 3;

  /**
   * The maximum height of this input.
   */
  @Input()
  maxRows = 0;

  /**
   * The minimum number of characters for this input.
   */
  @Input()
  minlength: number | null = null;

  /**
   * The maximum number of characters for this input.
   */
  @Input()
  maxlength: number | null = null;

  /**
   * A regular expression that this input's value is checked against.
   */
  @Input()
  pattern: string | RegExp = '';

  /**
   * Flag to set spellcheck for input
   */
  @Input()
  spellcheck = false;

  constructor(injector: Injector) {
    super(injector);
  }
}
