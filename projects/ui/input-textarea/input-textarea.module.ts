import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AutosizeModule } from 'ngx-autosize';
import { InputTextareaComponent } from './input-textarea/input-textarea.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, AutosizeModule],
  declarations: [InputTextareaComponent],
  exports: [InputTextareaComponent]
})
export class InputTextareaModule {}
