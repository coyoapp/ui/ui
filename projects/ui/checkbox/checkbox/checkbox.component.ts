import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import {
  AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, HostBinding, Input, OnDestroy, Output, ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@coyoapp/ui/cdk';
import { BehaviorSubject, Observable } from 'rxjs';

let nextUniqueId = 0;

export enum CheckboxState {
  CHECKED = 'true',
  UNCHECKED = 'false',
  MIXED = 'mixed'
}

export interface CheckboxChangeEvent {
  source: CheckboxComponent;
  checked: boolean;
  indeterminate: boolean;
  state: CheckboxState;
}

/**
 * The cui-checkbox component extends the visual style of a checkbox.
 */
@Component({
  selector: 'cui-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CheckboxComponent),
    multi: true
  }],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-checkbox',
    '[class.cui-checkbox-indeterminate]': 'indeterminate',
    '[class.cui-checkbox-checked]': 'checked',
    '[class.cui-checkbox-disabled]': 'disabled',
    '[class.cui-checkbox-spread]': 'align === "left-spread" || align === "right-spread"'
  }
})
export class CheckboxComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {
  private readonly _uid = `cui-checkbox-${nextUniqueId++}`;
  private _id = this._uid;
  private _indeterminate = false;
  private _checked = false;
  private _changeFn: (value: boolean) => void = noop;
  private _touchFn: () => void = noop;

  private readonly _ariaChecked$: BehaviorSubject<string>;
  readonly ariaChecked$: Observable<string>;

  /**
   * The appearance mode of the checkbox.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  mode: 'primary' | 'secondary' = 'primary';

  /**
   * The size of the checkbox.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 'm';

  /**
   * The position of the label in relation to the checkbox.
   */
  @Input()
  align: 'left' | 'right' | 'left-spread' | 'right-spread' = 'left';

  /**
   * The ID of this checkbox. If you want to add an external Label suffix the id used in the FOR attribute with '-input'
   */
  @Input()
  get id(): string { return this._id + '-input'; }
  set id(value: string) { this._id = value || this._uid; }

  /**
   * Whether the checkbox is indeterminate.
   */
  @Input()
  get indeterminate(): boolean { return this._indeterminate; }
  set indeterminate(value: boolean) {
    if (value !== this._indeterminate) {
      this._indeterminate = value;
      this.handleStateChange();
    }
  }

  /**
   * Whether the checkbox is checked.
   */
  @Input()
  get checked(): boolean { return this._checked; }
  set checked(value: boolean) {
    if (value !== this._checked) {
      this._checked = value;
      this.handleStateChange();
    }
  }

  /**
   * Whether the checkbox is disabled.
   */
  @Input()
  disabled = false;

  /**
   * Whether the checkbox is required.
   */
  @Input()
  required = false;

  /**
   * Name value will be applied to the input element if present
   */
  @Input() name?: string;

  /**
   * The value attribute of the native input element
   */
  @Input() value?: string;

  /**
   * The tab index of the input.
   */
  @Input()
  tabIndex?: number;

  /**
   * The `aria-label` attribute of this element.
   */
  @Input('aria-label')
  ariaLabel?: string;

  /**
   * The `aria-labelledby` attribute of this element.
   */
  @Input('aria-labelledby')
  ariaLabelledby?: string;

  /**
   * The `aria-describedby` attribute of this element. It is read after the
   * element's label and field type.
   */
  @Input('aria-describedby')
  ariaDescribedby?: string;

  /**
   * Event emitted when the checkbox's `checked` value changes.
   */
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() readonly change: EventEmitter<CheckboxChangeEvent> = new EventEmitter<CheckboxChangeEvent>();

  /**
   * Event emitted when the checkbox's `indeterminate` value changes.
   */
  @Output()
  readonly indeterminateChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  /**
   * The current state of this checkbox.
   */
  get state(): CheckboxState {
    if (this.checked) {
      if (this.indeterminate) {
        return CheckboxState.MIXED;
      }
      return CheckboxState.CHECKED;
    }
    return CheckboxState.UNCHECKED;
  }

  constructor(
    private readonly elementRef: ElementRef,
    private readonly focusMonitor: FocusMonitor,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    this._ariaChecked$ = new BehaviorSubject<string>(this.state);
    this.ariaChecked$ = this._ariaChecked$.asObservable();
  }

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef, true).subscribe(focusOrigin => {
      if (!focusOrigin) {
        // When a focused element becomes disabled, the browser *immediately* fires a blur event.
        // Angular does not expect events to be raised during change detection, so any state change
        // (such as a form control's 'ng-touched') will cause a changed-after-checked error.
        // See https://github.com/angular/angular/issues/17793. To work around this, we defer
        // telling the form control it has been touched until the next tick.
        // @see https://github.com/angular/components/blob/master/src/material/checkbox/checkbox.ts
        Promise.resolve().then(() => {
          this._touchFn();
          this.changeDetectorRef.markForCheck();
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.focusMonitor.stopMonitoring(this.elementRef);
  }

  registerOnChange(fn: (value: boolean) => void): void {
    this._changeFn = fn;
  }

  registerOnTouched(fn: () => void): void {
    this._touchFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  changed(event: Event): void {
    event.stopPropagation();
  }

  clicked(event: Event): void {
    event.stopPropagation();
    if (!this.disabled) {
      if (this.indeterminate) {
        Promise.resolve().then(() => {
          this._indeterminate = false;
          this.indeterminateChange.emit(this._indeterminate);
        });
      }

      this._checked = !this._checked;
      this.handleStateChange(true);
    }
  }

  clickFocused(): void {
    this.focusMonitor.focusVia(this.elementRef, 'mouse');
  }

  focus(origin?: FocusOrigin, options?: FocusOptions): void {
    if (origin) {
      this.focusMonitor.focusVia(this.elementRef, origin, options);
    } else {
      this.elementRef.nativeElement.focus(options);
    }
  }

  writeValue(value: boolean): void {
    this.checked = !!value;
    this.handleStateChange();
  }

  private handleStateChange(emit = false): void {
    this._ariaChecked$.next(this.state);
    if (emit) {
      this._changeFn?.(this.checked);
      this.change.emit({
        source: this,
        checked: this.checked,
        indeterminate: this.indeterminate,
        state: this.state
      });
    }
  }
}
