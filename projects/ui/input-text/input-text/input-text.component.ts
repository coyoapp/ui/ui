import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';

@Component({
  selector: 'cui-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputTextComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-text'
  }
})
export class InputTextComponent extends FormFieldControlComponent<string> {

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  /**
   * The minimum number of characters for this input.
   */
  @Input()
  minlength: number | null = null;

  /**
   * The maximum number of characters for this input.
   */
  @Input()
  maxlength: number | null = null;

  /**
   * A regular expression that this input's value is checked against.
   */
  @Input()
  pattern: string | RegExp = '';

  /**
   * Flag to set clear button for input
   */
  @Input()
  clearable = false;

  constructor(injector: Injector) {
    super(injector);
  }

  clear(): void {
    this.value = '';
  }
}
