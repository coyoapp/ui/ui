import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from '@coyoapp/ui/button';
import { InputTextComponent } from './input-text/input-text.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, ButtonModule],
  declarations: [InputTextComponent],
  exports: [InputTextComponent]
})
export class InputTextModule {}
