import { FocusKeyManager } from '@angular/cdk/a11y';
import {
  AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Inject, Input, Output, QueryList, ViewChildren, ViewEncapsulation
} from '@angular/core';
import { ButtonComponent } from '@coyoapp/ui/button';
import { clamp, CUI_FOCUS_KEY_MANAGER_PROVIDER, FocusKeyManagerProvider } from '@coyoapp/ui/cdk';
import { CUI_PAGINATION_CONFIG, PaginationConfig } from '../pagination.config';

/**
 * A navigation component to switch between different pages of paged chunks of
 * data such as a table. Pagination is built with list HTML elements and a
 * wrapping `<nav>` element to identify it as a navigation section to screen
 * readers and other assistive technologies. Furthermore, the current page is
 * correctly highlighted using `aria-current`. The ARIA labels for the
 * pagination component can be customized by providing your own translations in
 * the modules `forRoot()` method. This will allow you to change the following:
 *
 * - The label for the individual page buttons.
 * - The label for the first and last button.
 * - The label for the pagination navigation itself.
 */
@Component({
  selector: 'cui-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
    useExisting: forwardRef(() => PaginationComponent)
  }],
  host: {
    class: 'cui-pagination'
  }
})
export class PaginationComponent implements AfterViewInit, FocusKeyManagerProvider<ButtonComponent> {
  private _page = 0;

  keyManager!: FocusKeyManager<ButtonComponent>;

  /**
   * The current page.
   */
  @Input()
  get page(): number { return this._page; }
  set page(page: number) {
    const newPage = clamp(0, page, this.pageCount - 1);
    if (this._page !== newPage) {
      this._page = newPage;
      this.pageChange.emit(this._page);
    }
  }

  /**
   * The total number of pages.
   */
  @Input()
  pageCount = 1;

  /**
   * The number of pages to be shown around the current page.
   */
  @Input()
  activePadding = 1;

  /**
   * The number of pages to be shown at the edges.
   */
  @Input()
  sidePadding = 1;

  /**
   * The size of the buttons.
   */
  @Input()
  size: 's' | 'm' | 'l' = 's';

  /**
   * The look of the buttons.
   */
  @Input()
  look: 'button' | 'link' = 'button';

  /**
   * Event emitter for the current page.
   */
  @Output()
  readonly pageChange = new EventEmitter<number>(true);

  @ViewChildren(ButtonComponent)
  buttons!: QueryList<ButtonComponent>;

  get isFirst(): boolean {
    return this.page === 0;
  }

  get isLast(): boolean {
    return this.page === this.pageCount - 1;
  }

  get pages(): number[] {
    if (!this.sidePadding && !this.activePadding) {
      return [this.page];
    }

    const result = new Set<number>();
    const minPage = this.page <= this.sidePadding + this.activePadding + 1;
    const minActivepage = minPage
      ? this.sidePadding + 2 * this.activePadding + 2
      : this.sidePadding;
    const maxPage = this.page >= this.pageCount - this.sidePadding - this.activePadding - 2;
    const maxActivepage = maxPage
      ? this.pageCount - this.sidePadding - 2 * this.activePadding - 2
      : this.pageCount - this.sidePadding;

    this.addSeq(result, 0, minActivepage);
    if (!minPage && !maxPage) {
      this.addSeq(result,
        this.page - this.activePadding,
        this.page + this.activePadding + 1);
    }
    this.addSeq(result, maxActivepage, this.pageCount);

    return [...result];
  }

  constructor(
    @Inject(CUI_PAGINATION_CONFIG) readonly config: PaginationConfig
  ) {}

  ngAfterViewInit(): void {
    this.keyManager = new FocusKeyManager<ButtonComponent>(this.buttons)
      .withHorizontalOrientation('ltr')
      .withHomeAndEnd()
      .withWrap();
  }

  private addSeq(set: Set<number>, start: number, end: number): void {
    const _start = clamp(start, 0, this.pageCount);
    const _end = clamp(end, 0, this.pageCount);
    Array(_end - _start).fill(0).forEach((_, i) => set.add(_start + i));
  }
}
