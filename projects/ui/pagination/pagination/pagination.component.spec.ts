import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { CUI_PAGINATION_CONFIG } from '../pagination.config';
import { PaginationComponent } from './pagination.component';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PaginationComponent],
      providers: [{
        provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
        useValue: PaginationComponent
      },
      {
        provide: CUI_PAGINATION_CONFIG,
        useValue: {
          i18n: {
            pagination: 'Pagination',
            first: 'First',
            last: 'Last',
            page: (page: number) => `Page ${page}`
          }
        }
      }]
    }).overrideTemplate(PaginationComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check for the first page', () => {
    // given
    component.page = 0;
    component.pageCount = 10;

    // when
    fixture.detectChanges();

    // then
    expect(component.isFirst).toBe(true);
    expect(component.isLast).toBe(false);
  });

  it('should check for the last page', () => {
    // given
    component.page = 9;
    component.pageCount = 10;

    // when
    fixture.detectChanges();

    // then
    expect(component.isFirst).toBe(false);
    expect(component.isLast).toBe(true);
  });

  testIndexes(1, 0, 0, 0, [0]);
  testIndexes(1, 0, 1, 1, [0]);
  testIndexes(1, 0, 2, 2, [0]);
  testIndexes(5, 0, 0, 0, [0]);
  testIndexes(5, 2, 0, 0, [2]);
  testIndexes(5, 0, 0, 1, [0, 1, 2, 4]);
  testIndexes(5, 0, 1, 1, [0, 1, 2, 3, 4]);
  testIndexes(5, 0, 0, 2, [0, 1, 2, 3, 4]);
  testIndexes(5, 0, 2, 2, [0, 1, 2, 3, 4]);
  testIndexes(10, 9, 0, 0, [9]);
  testIndexes(10, 5, 1, 1, [0, 4, 5, 6, 9]);
  testIndexes(10, 3, 1, 1, [0, 1, 2, 3, 4, 9]);
  testIndexes(10, 6, 1, 1, [0, 5, 6, 7, 8, 9]);
  testIndexes(20, 10, 2, 2, [0, 1, 8, 9, 10, 11, 12, 18, 19]);
  testIndexes(20, 5, 2, 2, [0, 1, 2, 3, 4, 5, 6, 7, 18, 19]);
  testIndexes(20, 16, 2, 2, [0, 1, 12, 13, 14, 15, 16, 17, 18, 19]);

  function testIndexes(len: number, idx: number, padAct: number, padSide: number, expected: number[]) {
    it(`should calculate pages for len=${len}, idx=${idx}, padAct=${padAct}, padSide=${padSide}`, () => {
      // given
      component.page = idx;
      component.pageCount = len;
      component.activePadding = padAct;
      component.sidePadding = padSide;

      // when
      fixture.detectChanges();

      // then
      expect(component.pages).toEqual(expected);
    });
  }

  it('should get correct CUI_FOCUS_KEY_MANAGER_PROVIDER provider of type MenuComponent', () => {
    expect(TestBed.inject(CUI_FOCUS_KEY_MANAGER_PROVIDER)).toBeDefined();
    const paginationDecorators = (<any>PaginationComponent).__annotations__;
    const useExistingClass = paginationDecorators[0].providers[0].useExisting();

    expect(paginationDecorators[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(PaginationComponent);
  });
});
