import { inject } from '@angular/core/testing';
import { CUI_PAGINATION_CONFIG, CUI_PAGINATION_DEFAULT_CONFIG, PaginationConfig } from './pagination.config';

describe('TabsConfig', () => {
  it('should provide a default config', inject([CUI_PAGINATION_CONFIG], (config: PaginationConfig) => {
    expect(config).toBe(CUI_PAGINATION_DEFAULT_CONFIG);
  }));

  it('should generate a page i18n message', inject([CUI_PAGINATION_CONFIG], (config: PaginationConfig) => {
    // when
    const result1 = config.i18n.page(1, true);
    const result2 = config.i18n.page(1, false);

    // then
    expect(result1).toBe('Current page, page 1');
    expect(result2).toBe('Go to page 1');
  }));
});
