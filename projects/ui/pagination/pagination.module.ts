import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  imports: [CommonModule, IconModule, ButtonModule],
  declarations: [PaginationComponent],
  exports: [PaginationComponent]
})
export class PaginationModule {}
