import { InjectionToken } from '@angular/core';

export interface PaginationMessages {
  pagination: string;
  first: string;
  last: string;
  page: (page: number, current: boolean) => string;
}

export interface PaginationConfig {
  readonly i18n: Readonly<PaginationMessages>
}

export const CUI_PAGINATION_DEFAULT_CONFIG: PaginationConfig = {
  i18n: {
    pagination: 'Pagination',
    first: 'Go to first page',
    last: 'Go to last page',
    page: (n, c) => c ? `Current page, page ${n}` : `Go to page ${n}`
  }
};

export const CUI_PAGINATION_CONFIG = new InjectionToken<PaginationConfig>('CUI_PAGINATION_CONFIG', {
  factory: () => CUI_PAGINATION_DEFAULT_CONFIG
});
