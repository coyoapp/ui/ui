import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '@coyoapp/ui/icon';
import { LoadingModule } from '@coyoapp/ui/loading';
import { AnchorButtonComponent, ButtonComponent } from './button/button.component';
import { IconButtonDirective } from './icon-button/icon-button.directive';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    LoadingModule
  ],
  declarations: [
    ButtonComponent,
    AnchorButtonComponent,
    IconButtonDirective
  ],
  exports: [
    ButtonComponent,
    AnchorButtonComponent,
    IconButtonDirective
  ]
})
export class ButtonModule {}
