import {
  ChangeDetectorRef, Directive, forwardRef, InjectionToken, Input
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

export const CUI_ICON_BUTTON = new InjectionToken<IconButtonDirective>('cuiIconButton');

/**
 * Directive to turn a COYO button into an icon button. Buttons or links
 * containing only an icon should be given a meaningful label via `aria-label`
 * or `aria-labelledby`.
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'button[cui-icon-button], a[cui-icon-button]',
  providers: [{
    provide: CUI_ICON_BUTTON,
    useExisting: forwardRef(() => IconButtonDirective)
  }]
})
export class IconButtonDirective {

  /**
   * The name of the icon.
   */
  @Input()
  get icon(): coyoIcon { return this._icon; }
  set icon(icon: coyoIcon) { this._icon = icon; this.cdRef.markForCheck(); }
  private _icon: coyoIcon = 'emblem-coyo';

  constructor(
    private readonly cdRef: ChangeDetectorRef
  ) {}
}
