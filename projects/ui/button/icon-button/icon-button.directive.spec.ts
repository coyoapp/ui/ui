import { ChangeDetectorRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import 'reflect-metadata';
import { CUI_ICON_BUTTON, IconButtonDirective } from './icon-button.directive';

describe('IconButtonDirective', () => {
  let directive: IconButtonDirective;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    directive = new IconButtonDirective(changeDetectorRef);

    TestBed.configureTestingModule({
      providers: [{
        provide: CUI_ICON_BUTTON,
        useValue: IconButtonDirective
      }]
    });
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should run change detection when icon changes', () => {
    // when
    directive.icon = 'globe';

    // then
    expect(directive.icon).toBe('globe');
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should have injected CUI_ICON_BUTTON provider', () => {
    expect(TestBed.inject(CUI_ICON_BUTTON)).toBeDefined();
  });

  it('should get correct CUI_ICON_BUTTON provider of type IconBUttonDirective', () => {
    expect(TestBed.inject(CUI_ICON_BUTTON)).toBeDefined();
    const directiveDecoraters = (<any>IconButtonDirective).__annotations__;
    const useExistingClass = directiveDecoraters[0].providers[0].useExisting();

    expect(directiveDecoraters[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(IconButtonDirective);
  });

  it('should get correct selector', () => {
    const directiveDecoraters = (<any>IconButtonDirective).__annotations__;

    expect(directiveDecoraters[0].selector).toBe('button[cui-icon-button], a[cui-icon-button]');
  });
});
