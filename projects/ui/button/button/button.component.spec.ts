import { FocusMonitor } from '@angular/cdk/a11y';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER, noop } from '@coyoapp/ui/cdk';
import { of } from 'rxjs';
import { CUI_ICON_BUTTON, IconButtonDirective } from '../icon-button/icon-button.directive';
import { AnchorButtonComponent, ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let focusMonitor: jasmine.SpyObj<FocusMonitor>;
  let focusKeyManager: {
    keyManager: {
      onKeydown: any
    }
  };

  beforeEach(async () => {
    focusMonitor = jasmine.createSpyObj('FocusMonitor', ['monitor', 'stopMonitoring', 'focusVia']);
    focusMonitor.monitor.and.returnValue(of(null));
    focusKeyManager = {
      keyManager: {
        onKeydown: noop
      }
    };
    await TestBed.configureTestingModule({
      declarations: [ButtonComponent],
      providers: [{
        provide: FocusMonitor,
        useValue: focusMonitor
      }, {
        provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
        useValue: focusKeyManager
      }]
    }).overrideTemplate(ButtonComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the icon for the icon button', () => {
    expect(component.isIconButton).toBe(false);
    expect(component.icon).toBe('emblem-coyo');
  });

  it('should monitor the element after view init', () => {
    expect(focusMonitor.monitor).toHaveBeenCalledWith(fixture.elementRef, true);
  });

  it('should stop monitoring the element on destroy', () => {
    // when
    component.ngOnDestroy();

    // then
    expect(focusMonitor.stopMonitoring).toHaveBeenCalledWith(fixture.elementRef);
  });

  it('should call focusKeyManager.keyManager.onKeydown on keydown event', () => {
    const element = fixture.nativeElement;
    spyOn(focusKeyManager.keyManager, 'onKeydown');
    const event = new Event('keydown', { bubbles: true });
    element.dispatchEvent(event);

    expect(focusKeyManager.keyManager.onKeydown).toHaveBeenCalled();
  });

  it('should focus correct target', () => {
    const element = fixture.nativeElement;
    spyOn(element, 'focus');
    component.focus('keyboard');
    fixture.detectChanges();

    expect(focusMonitor.focusVia).toHaveBeenCalled();
    component.focus();
    fixture.detectChanges();

    expect(element.focus).toHaveBeenCalled();
  });
});

describe('AnchorButtonComponent', () => {
  let component: AnchorButtonComponent;
  let fixture: ComponentFixture<AnchorButtonComponent>;
  let focusMonitor: jasmine.SpyObj<FocusMonitor>;

  beforeEach(async () => {
    focusMonitor = jasmine.createSpyObj('FocusMonitor', ['monitor', 'stopMonitoring', 'focusVia']);
    focusMonitor.monitor.and.returnValue(of(null));
    await TestBed.configureTestingModule({
      declarations: [AnchorButtonComponent],
      providers: [{
        provide: FocusMonitor,
        useValue: focusMonitor
      }]
    }).overrideTemplate(AnchorButtonComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnchorButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not halt events by default', () => {
    // given
    const event = jasmine.createSpyObj('Event', ['preventDefault', 'stopImmediatePropagation']);

    // when
    component._haltDisabledEvents(event);

    // then
    expect(event.preventDefault).not.toHaveBeenCalled();
    expect(event.stopImmediatePropagation).not.toHaveBeenCalled();
  });

  it('should halt events if disabled', () => {
    // given
    const event = jasmine.createSpyObj('Event', ['preventDefault', 'stopImmediatePropagation']);
    component.disabled = true;
    fixture.detectChanges();

    // when
    component._haltDisabledEvents(event);

    // then
    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopImmediatePropagation).toHaveBeenCalled();
  });
});

describe('ButtonComponent with IconButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let focusMonitor: jasmine.SpyObj<FocusMonitor>;
  let iconButton: IconButtonDirective | undefined;

  beforeEach(async () => {
    focusMonitor = jasmine.createSpyObj('FocusMonitor', ['monitor', 'stopMonitoring']);
    focusMonitor.monitor.and.returnValue(of(null));
    iconButton = { icon: 'globe' } as IconButtonDirective;
    await TestBed.configureTestingModule({
      declarations: [ButtonComponent],
      providers: [{
        provide: FocusMonitor,
        useValue: focusMonitor
      }, {
        provide: CUI_ICON_BUTTON,
        useValue: iconButton
      }]
    }).overrideTemplate(ButtonComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get the icon for the icon button', () => {
    expect(component.isIconButton).toBe(true);
    expect(component.icon).toBe('globe');
  });

  it('should set mode', () => {
    component.mode = 'test' as any;

    expect((component as any)._mode).toEqual('test');

  });
});
