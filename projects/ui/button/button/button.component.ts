import { FocusableOption, FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  Optional,
  ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER, FocusKeyManagerProvider } from '@coyoapp/ui/cdk';
import { CustomPaletteDirective } from '@coyoapp/ui/theme';
import { CUI_ICON_BUTTON, IconButtonDirective } from '../icon-button/icon-button.directive';

type ButtonMode = 'primary' | 'secondary' | 'ghost' | 'success' | 'error' | 'custom';

/**
 * Buttons are native `<button>` or `<a>` elements enhanced with styling. Native
 * `<button>` and `<a>` elements are always used in order to provide the most
 * straightforward and accessible experience for users. A `<button>` element
 * should be used whenever some **action** is performed. An `<a>` element should
 * be used whenever the user will **navigate** to another view.
 *
 * Buttons can be colored using the `mode` property to set the color to
 * *primary*, *secondary*, *ghost*, *success* or *error*.
 */
@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'button[cui-button], button[cui-icon-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-button',
    '[class.cui-icon-button]': 'isIconButton',
    '[class.cui-round]': 'round && !isIconButton',
    '[class.cui-loading]': 'loading',
    '[attr.disabled]': 'disabled || null'
  }
})
export class ButtonComponent implements AfterViewInit, OnDestroy, FocusableOption {

  /**
   * The appearance mode of the button. The mode defaults to "secondary" unless
   * a {@link CustomPaletteDirective} is active on the context in which case it
   * defaults to "custom".
   *
   * @see {@link CustomPaletteDirective} to set a custom color palette.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  get mode(): ButtonMode { return this._mode; }
  set mode(value: ButtonMode) { this._mode = value; }
  private _mode: ButtonMode = this.customPalette ? 'custom' : 'secondary';

  /**
   * The look of the button.
   */
  @Input()
  @HostBinding('attr.data-cui-look')
  look: 'button' | 'link' = 'button';

  /**
   * The size of the button.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 'xs' | 's' | 'm' | 'l' | 'xl' = 'm';

  /**
   * Whether the button has round corners.
   */
  @Input()
  round = false;

  /**
   * Whether the button is in a loading state.
   */
  @Input()
  loading = false;

  /**
   * Whether the button is disabled.
   */
  @Input()
  disabled = false;

  /**
   * The name of an icon to prefix the button text.
   */
  @Input()
  prefixIcon?: coyoIcon;

  /**
   * The name of an icon to suffix the button text.
   */
  @Input()
  suffixIcon?: coyoIcon;

  /**
   * Optional CSS classes to be applied to the textual content of the button.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  textClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * Optional CSS classes to be applied to the icons of the button.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  iconClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * The icon size based on the button size.
   */
  get iconSize(): 'xs' | 's' | 'm' | 'l' | 'xl' {
    switch (this.size) {
      case 'xs': return 's';
      case 'xl': return 'l';
      default: return 'm';
    }
  }

  /**
   * Whether the button is an icon button.
   */
  get isIconButton(): boolean {
    return !!this.iconButton;
  }

  /**
   * The name of the icon for the icon button.
   */
  get icon(): coyoIcon {
    return this.iconButton?.icon || 'emblem-coyo';
  }

  constructor(
    readonly elementRef: ElementRef<HTMLElement>,
    private readonly focusMonitor: FocusMonitor,
    @Optional() @Inject(CustomPaletteDirective) private readonly customPalette?: CustomPaletteDirective,
    @Optional() @Inject(CUI_ICON_BUTTON) private readonly iconButton?: IconButtonDirective,
    @Optional() @Inject(CUI_FOCUS_KEY_MANAGER_PROVIDER) private readonly focusKeyManager?: FocusKeyManagerProvider<ButtonComponent>
  ) {}

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef, true).subscribe();
  }

  ngOnDestroy(): void {
    this.focusMonitor.stopMonitoring(this.elementRef);
  }

  @HostListener('keydown', ['$event'])
  manage(event: KeyboardEvent): void {
    this.focusKeyManager?.keyManager?.onKeydown(event);
  }

  focus(origin?: FocusOrigin, options?: FocusOptions): void {
    if (origin) {
      this.focusMonitor.focusVia(this.elementRef.nativeElement, origin, options);
    } else {
      this.elementRef.nativeElement.focus(options);
    }
  }
}

/**
 * A button based on a native `<a>` element.
 *
 * @see ButtonComponent
 */
@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'a[cui-button], a[cui-icon-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{ provide: ButtonComponent, useExisting: AnchorButtonComponent }],
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  host: {
    'class': 'cui-button',
    '[class.cui-icon-button]': 'isIconButton',
    '[class.cui-round]': 'round && !isIconButton',
    '[class.cui-loading]': 'loading',
    '[attr.tabindex]': 'disabled ? -1 : (tabIndex || 0)',
    '[attr.disabled]': 'disabled || null',
    '[attr.aria-disabled]': 'disabled ? disabled.toString() : null',
    '(click)': '_haltDisabledEvents($event)'
  }
})
export class AnchorButtonComponent extends ButtonComponent {

  @Input() tabIndex?: number;

  constructor(
    elementRef: ElementRef,
    focusMonitor: FocusMonitor,
    @Optional() @Inject(CustomPaletteDirective) customPalette?: CustomPaletteDirective,
    @Optional() @Inject(CUI_ICON_BUTTON) iconButton?: IconButtonDirective,
    @Optional() @Inject(CUI_FOCUS_KEY_MANAGER_PROVIDER) focusKeyManager?: FocusKeyManagerProvider<ButtonComponent>
  ) {
    super(elementRef, focusMonitor, customPalette, iconButton, focusKeyManager);
  }

  _haltDisabledEvents(event: Event): void {
    // A disabled button shouldn't apply any actions
    if (this.disabled) {
      event.preventDefault();
      event.stopImmediatePropagation();
    }
  }
}
