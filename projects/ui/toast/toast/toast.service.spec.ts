import { TestBed } from '@angular/core/testing';
import { ActiveToast, ToastRef, ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { ToastService } from './toast.service';
import { ToastButtonLink, ToastOptions } from '../toast.config';

describe('ToastService', () => {
  let service: ToastService;
  let toastrService: jasmine.SpyObj<ToastrService>;

  beforeEach(() => {
    toastrService = jasmine.createSpyObj('ToastrService', ['show', 'success', 'error', 'clear']);
    TestBed.configureTestingModule({
      providers: [{
        provide: ToastrService,
        useValue: toastrService
      }]
    });
    service = TestBed.inject(ToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should show a default toast with a persistent configuration', () => {
    // given
    const config = { closeButton: false, disableTimeOut: true };
    const button: ToastButtonLink = { id: 'undo', title: 'Undo this', ariaLabel: 'Undo this action' };
    const toastOptions: ToastOptions = { button };
    toastrService.show.and.returnValue({ toastRef: { afterClosed: () => of({}) } as ToastRef<any> } as ActiveToast<any>);

    // when
    const result = service.show('message', 'title', true, toastOptions);

    // then
    expect(result).toBeTruthy();
    expect(toastrService.show).toHaveBeenCalledWith('message', 'title', { ...config, ...toastOptions });
  });

  it('should show a success toast', () => {
    // given
    const link: ToastButtonLink = { id: 'link', title: 'This is a link' };
    const toastOptions: ToastOptions = { link };
    toastrService.success.and.returnValue({ toastRef: { afterClosed: () => of({}) } as ToastRef<any> } as ActiveToast<any>);

    // when
    const result = service.success('message', 'title', undefined, toastOptions);

    // then
    expect(result).toBeTruthy();
    expect(toastrService.success).toHaveBeenCalledWith('message', 'title', toastOptions);
  });

  it('should show an error toast', () => {
    // given
    const button: ToastButtonLink = { id: 'undo', title: 'Undo this', ariaLabel: 'Undo this action' };
    const toastOptions: ToastOptions = { button };
    toastrService.error.and.returnValue({ toastRef: { afterClosed: () => of({}) } as ToastRef<any> } as ActiveToast<any>);

    // when
    const result = service.error('message', 'title', undefined, toastOptions);

    // then
    expect(result).toBeTruthy();
    expect(toastrService.error).toHaveBeenCalledWith('message', 'title', toastOptions);
  });

  it('should clear', () => {
    // when
    service.clear();

    // then
    expect(toastrService.clear).toHaveBeenCalledWith();
  });
});
