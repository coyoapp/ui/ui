import {
  animate, state, style, transition, trigger
} from '@angular/animations';
import {
  ChangeDetectionStrategy, Component, Inject, ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { Toast, ToastPackage, ToastrService } from 'ngx-toastr';
import { CUI_TOAST_CONFIG, ToastButtonLink, ToastConfig, ToastOptions } from '../toast.config';

@Component({
  selector: 'cui-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-toast'
  },
  animations: [
    trigger('flyInOut', [
      state('inactive', style({ opacity: 0, transform: 'translate3d(0, 80px, 0) scale(0.6)' })),
      state('active', style({ opacity: 1, transform: 'translate3d(0, 0, 0) scale(1)' })),
      state('removed', style({ opacity: 0, transform: 'translate3d(0, 130px, -1px) scale(0.5)' })),
      transition('inactive => active', animate('{{ easeTime }}ms {{ easing }}')),
      transition('active => removed', animate('{{ easeTime }}ms {{ easing }}'))
    ])
  ],
  preserveWhitespaces: false
})
export class ToastComponent extends Toast {
  private static readonly ICONS: { [mode: string]: coyoIcon } = {
    'toast-success': 'check-circle',
    'toast-error': 'stop'
  };

  get icon(): coyoIcon {
    return ToastComponent.ICONS[this.toastPackage.toastType] || 'info';
  }

  toastOptions: ToastOptions;

  constructor(
    readonly toastrService: ToastrService,
    readonly toastPackage: ToastPackage,
    @Inject(CUI_TOAST_CONFIG) readonly config: ToastConfig
  ) {
    super(toastrService, toastPackage);
    this.toastOptions = toastPackage.config as unknown as ToastOptions;
  }

  action(event:Event, linkButton: ToastButtonLink): void {
    event.stopPropagation();
    this.toastPackage.triggerAction(linkButton);
    this.toastrService.clear();
  }
}
