import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  IndividualConfig, ToastPackage, ToastRef, ToastrService
} from 'ngx-toastr';
import { of } from 'rxjs';
import { CUI_TOAST_CONFIG, ToastButtonLink } from '../toast.config';
import { ToastComponent } from './toast.component';

describe('ToastComponent', () => {
  let component: ToastComponent;
  let fixture: ComponentFixture<ToastComponent>;
  let toastRef: jasmine.SpyObj<ToastRef<any>>;
  let toastrService: jasmine.SpyObj<ToastrService>;
  let toastPackage: ToastPackage;

  beforeEach(async () => {
    toastRef = jasmine.createSpyObj('ToastRef', ['afterActivate', 'afterClosed', 'manualClose', 'manualClosed', 'timeoutReset', 'countDuplicate']);
    toastRef.afterActivate.and.returnValue(of(void 0));
    toastRef.afterClosed.and.returnValue(of(void 0));
    toastRef.manualClosed.and.returnValue(of(void 0));
    toastRef.timeoutReset.and.returnValue(of(void 0));
    toastRef.countDuplicate.and.returnValue(of(0));
    toastrService = jasmine.createSpyObj('ToastrService', ['remove', 'clear']);
    toastPackage = new ToastPackage(1, {} as IndividualConfig, undefined, undefined, 'toast-type', toastRef);
    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [ToastComponent],
      providers: [{
        provide: ToastrService,
        useValue: toastrService
      }, {
        provide: ToastPackage,
        useValue: toastPackage
      }, {
        provide: CUI_TOAST_CONFIG,
        useValue: {
          close: 'Close'
        }
      }]
    }).overrideTemplate(ToastComponent, '<div></div>')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.toastOptions).toBeDefined();
  });

  it('should get default icon', () => {
    expect(component.icon).toBe('info');
  });

  it('should get success icon', () => {
    component.toastPackage.toastType = 'toast-success';

    expect(component.icon).toBe('check-circle');
  });

  it('should get error icon', () => {
    component.toastPackage.toastType = 'toast-error';

    expect(component.icon).toBe('stop');
  });

  it('should trigger action', () => {
    // given
    const btn = { id: 'btn-link' } as ToastButtonLink;
    const event = new Event('click');
    spyOn(event, 'stopPropagation');
    spyOn(toastPackage, 'triggerAction');

    // when
    component.action(event, btn);

    // then
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(toastPackage.triggerAction).toHaveBeenCalledWith(btn);
    expect(toastrService.clear).toHaveBeenCalled();
  });
});
