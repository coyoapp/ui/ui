import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ToastReference } from '../toast-reference';
import { ToastOptions } from '../toast.config';

/**
 * A service to show toast messages.
 */
@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private readonly PERSISTENCE_CONFIG = { closeButton: false, disableTimeOut: true };

  constructor(
    private toastr: ToastrService
  ) {}

  /**
   * Show a default toast.
   *
   * @param message the message of the toast
   * @param title the title of the toast
   * @param persistence set to true so that the toast does not disappear after the default timeout.
   * @param options the optional toast options.
   * @returns a toast reference
   */
  show(message: string, title?: string, persistence?: boolean, options?: ToastOptions): ToastReference {
    const toast = this.toastr.show(message, title, this.getToastOptions(persistence, options));
    return new ToastReference(
      toast.toastRef,
      toast.onAction
    );
  }

  /**
   * Show a success toast.
   *
   * @param message the message of the toast
   * @param title the title of the toast
   * @param persistence set to true so that the toast does not disappear after the default timeout.
   * @param options the optional toast options.
   * @returns a toast reference
   */
  success(message: string, title?: string, persistence?: boolean, options?: ToastOptions): ToastReference {
    const toast = this.toastr.success(message, title, this.getToastOptions(persistence, options));
    return new ToastReference(
      toast.toastRef,
      toast.onAction
    );
  }

  /**
   * Show an error toast.
   *
   * @param message the message of the toast
   * @param title the title of the toast
   * @param persistence set to true so that the toast does not disappear after the default timeout.
   * @param options the optional toast options.
   * @returns a toast reference
   */
  error(message: string, title?: string, persistence?: boolean, options?: ToastOptions): ToastReference {
    const toast = this.toastr.error(message, title, this.getToastOptions(persistence, options));
    return new ToastReference(
      toast.toastRef,
      toast.onAction
    );
  }

  /**
   * Clear all toasts.
   */
  clear(): void {
    this.toastr.clear();
  }

  private getToastOptions(persistence?: boolean, options?: ToastOptions): Partial<ToastOptions> {
    options = !options ? {} : options;
    return persistence ? { ...this.PERSISTENCE_CONFIG, ...options } : options;
  }
}
