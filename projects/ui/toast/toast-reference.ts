import { ToastRef } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastButtonLink } from './toast.config';

/**
 * Reference to the currently opened toast.
 */
export class ToastReference {

  constructor(
    private readonly toastRef: ToastRef<unknown>,
    private readonly actionRef: Observable<ToastButtonLink>
  ) {}

  /**
   * Observable which notified when the Toast is finished closing
   */
  get afterClosed(): Observable<void> {
    return this.toastRef.afterClosed().pipe(map(() => void 0));
  }

  /**
   * Close the toast.
   */
  close(): void {
    this.toastRef.close();
  }

  /**
   * Observable which notifies when the link/button is clicked.
   *
   * @returns the action reference Observable
   */
  onAction(): Observable<ToastButtonLink> {
    return this.actionRef;
  }
}
