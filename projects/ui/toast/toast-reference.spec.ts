import { ToastReference } from '@coyoapp/ui/toast/toast-reference';
import { ToastButtonLink } from '@coyoapp/ui/toast/toast.config';
import { of } from 'rxjs';
import { ToastRef } from 'ngx-toastr';

describe('ToastRef', () => {
  it('should create an instance', () => {
    // given
    const refSpy = jasmine.createSpyObj('toastRef', ['']);
    const actionRef = jasmine.createSpyObj('actionRef', of({}));
    // when
    const toastRef = new ToastReference(refSpy, actionRef);
    // then
    expect(toastRef).toBeTruthy();
  });

  it('should get the afterClosed call', () => {
    // given
    const refSpy = jasmine.createSpyObj('toastRef', ['afterClosed']);
    refSpy.afterClosed.and.returnValue(of({}));

    const toastRef = new ToastReference(refSpy, of({} as ToastButtonLink));
    // when
    const retObs = toastRef.afterClosed;

    // then
    expect(refSpy.afterClosed).toHaveBeenCalled();
    expect(retObs).toBeTruthy();
  });

  it('should close toast', () => {
    // given
    const refSpy = jasmine.createSpyObj('toastRef', ['afterClosed', 'close']);
    refSpy.afterClosed.and.returnValue(of({}));

    const toastRef = new ToastReference(refSpy, of({} as ToastButtonLink));
    // when
    toastRef.close();

    // then
    expect(refSpy.close).toHaveBeenCalled();
  });

  it('should return void 0 observable value to afterClosed method call', () => {
    const toastRef = jasmine.createSpyObj('ToastRef', ['close', 'afterClosed']);
    toastRef.afterClosed.and.returnValue(of(void 0));
    const ref = new ToastReference(toastRef as unknown as ToastRef<unknown>, of({} as ToastButtonLink));
    const { afterClosed } = ref;

    expect(afterClosed).toBeTruthy();
    afterClosed.subscribe(val => {
      expect(val).toBeFalsy();
    });
  });

  it('should return an observable when call action', () => {
    // given
    const refSpy = jasmine.createSpyObj('toastRef', ['']);
    const actionRef = of({} as ToastButtonLink);
    const toastRef = new ToastReference(refSpy, actionRef);
    // when
    const result = toastRef.onAction();
    // then
    expect(result).toBe(actionRef);
  });
});
