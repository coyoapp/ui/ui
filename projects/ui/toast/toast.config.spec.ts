import { inject } from '@angular/core/testing';
import { CUI_TOAST_CONFIG, CUI_TOAST_DEFAULT_CONFIG, ToastConfig } from './toast.config';

describe('ToastConfig', () => {
  it('should provide a default config', inject([CUI_TOAST_CONFIG], (config: ToastConfig) => {
    expect(config).toBe(CUI_TOAST_DEFAULT_CONFIG);
  }));
});
