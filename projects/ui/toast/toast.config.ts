import { InjectionToken } from '@angular/core';

export interface ToastMessages {
  close: string;
}

export interface ToastConfig {
  readonly i18n: Readonly<ToastMessages>;
}

export interface ToastButtonLink {
  readonly ariaLabel?: string;
  readonly id: string;
  readonly title: string;
}

export interface ToastOptions {
  readonly enableHtml?: boolean;
  readonly link?: ToastButtonLink;
  readonly button?: ToastButtonLink;
}

export const CUI_TOAST_DEFAULT_CONFIG: ToastConfig = {
  i18n: {
    close: 'Close'
  }
};

export const CUI_TOAST_CONFIG = new InjectionToken<ToastConfig>('CUI_TOAST_CONFIG', {
  factory: () => CUI_TOAST_DEFAULT_CONFIG
});
