import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { ToastrModule } from 'ngx-toastr';
import { ToastComponent } from './toast/toast.component';

export const toastModuleProviders = ToastrModule.forRoot({
  toastComponent: ToastComponent,
  toastClass: 'ngx-toastr cui-toast',
  titleClass: 'cui-toast-title',
  messageClass: 'cui-toast-message',
  timeOut: 8000,
  closeButton: true,
  extendedTimeOut: 4000,
  positionClass: 'toast-bottom-left',
  easing: 'cubic-bezier(0.21, 1.02, 0.73, 1)',
  easeTime: 120,
  tapToDismiss: false
}).providers || [];

@NgModule({
  imports: [
    CommonModule,
    ToastrModule,
    IconModule,
    ButtonModule
  ],
  declarations: [
    ToastComponent
  ]
})
export class ToastModule {

  static forRoot(): ModuleWithProviders<ToastModule> {
    return {
      ngModule: ToastModule,
      providers: toastModuleProviders
    };
  }
}
