import { InjectionToken } from '@angular/core';

export interface TableMessages {
  ASC: string;
  DESC: string;
  NONE: string;
}

export interface TableConfig {
  readonly i18n: Readonly<TableMessages>;
}

export const CUI_TABLE_DEFAULT_CONFIG: TableConfig = {
  i18n: {
    ASC: 'Sorted in ascending order',
    DESC: 'Sorted in descending order',
    NONE: 'Not sorted'
  }
};

export const CUI_TABLE_CONFIG = new InjectionToken<TableConfig>('CUI_TABLE_CONFIG', {
  factory: () => CUI_TABLE_DEFAULT_CONFIG
});
