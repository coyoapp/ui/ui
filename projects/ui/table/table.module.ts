import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { SortableComponent } from './sortable/sortable.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule
  ],
  declarations: [
    SortableComponent
  ],
  exports: [
    SortableComponent
  ]
})
export class TableModule {}
