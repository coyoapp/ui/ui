import {
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Inject,
  Input,
  Output,
  ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { CUI_TABLE_CONFIG, TableConfig } from '../table.config';

export type SortDirection = 'ASC' | 'DESC' | 'NONE';

/**
 * The sort component adds sorting functionality to table headers.
 */
@Component({
  selector: 'th[cuiSortable]',
  template: `
    <ng-content></ng-content>
    <button cui-button cui-icon-button mode="ghost" size="xs"
      [icon]="iconNameMapping[cuiSortable]"
      [attr.aria-label]="globalConfig.i18n[cuiSortable]"
    ></button>`,
  styleUrls: ['./sortable.component.scss'],
  host: {
    class: 'cui-sortable'
  },
  encapsulation: ViewEncapsulation.None
})
export class SortableComponent {
  readonly iconNameMapping: { [key in SortDirection]: coyoIcon } = {
    ASC: 'chevron-up',
    DESC: 'chevron-down',
    NONE: 'chevron-select'
  };

  /** Specify the sort direction. */
  @Input()
  @HostBinding('attr.data-cui-sort')
  set cuiSortable(value: SortDirection) {
    // needed to apply default sorting when used as <... cuiSortable></...>
    this._cuiSortable = value || 'NONE';
  }
  get cuiSortable(): SortDirection {
    return this._cuiSortable;
  }
  _cuiSortable: SortDirection = 'NONE';

  /** Emits when sorting direction changed. */
  @Output()
  readonly cuiSortChange = new EventEmitter<SortDirection>();

  @HostListener('click')
  toggle() {
    switch (this.cuiSortable) {
      case 'ASC': this.cuiSortable = 'DESC'; break;
      case 'DESC': this.cuiSortable = 'NONE'; break;
      case 'NONE': this.cuiSortable = 'ASC'; break;
    }
    this.cuiSortChange.next(this.cuiSortable);
  }

  constructor(
    @Inject(CUI_TABLE_CONFIG) readonly globalConfig: TableConfig
  ) {}
}
