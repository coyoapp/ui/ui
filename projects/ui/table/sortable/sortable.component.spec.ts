import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SortableComponent } from './sortable.component';

describe('CollapseComponent', () => {
  let component: SortableComponent;
  let fixture: ComponentFixture<SortableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [SortableComponent]
    }).overrideTemplate(SortableComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change sorting on toggle', () => {
    component.toggle();

    expect(component.cuiSortable).toEqual('ASC');
    component.toggle();

    expect(component.cuiSortable).toEqual('DESC');
    component.toggle();

    expect(component.cuiSortable).toEqual('NONE');
  });
});
