import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@coyoapp/ui/cdk';
import { ToggleComponent } from '@coyoapp/ui/toggle';
import { Subject } from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

describe('ToggleComponent', () => {
  let component: ToggleComponent;
  let fixture: ComponentFixture<ToggleComponent>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);

    await TestBed.configureTestingModule({
      declarations: [ToggleComponent],
      providers: [{
        provide: NG_VALUE_ACCESSOR,
        useValue: ToggleComponent
      }]
    }).overrideTemplate(ToggleComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleComponent);
    component = fixture.componentInstance;
    (component as any).changeDetectorRef = changeDetectorRef;
    fixture.detectChanges();
    component.ngAfterViewInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.checked).toBeFalse();
    expect(component.disabled).toBeFalse();
  });

  it('should set the id', () => {
    // given
    const value = 'some-id';

    // when
    component.id = value;

    // then
    expect(component.id).toEqual(value);
  });

  it('should set checked state', () => {
    // given
    const value = true;

    // when
    component.checked = value;

    // then
    expect(component.checked).toEqual(value);
  });

  it('should set disabled state', () => {
    // given
    const value = true;

    // when
    component.disabled = value;

    // then
    expect(component.disabled).toEqual(value);
  });

  it('should emit checked state on click', done => {
    // given
    const event = {
      stopPropagation: noop
    } as Event;

    component.registerOnChange(state => {
      expect(state).toEqual(true);
      done();
    });

    // when
    component.clicked(event);
  });

  it('should write the checked state', () => {
    // when
    component.writeValue(true);

    // then
    expect(component.checked).toBeTrue();
  });

  it('should write the unchecked state', () => {
    // given
    component.checked = true;

    // when
    component.writeValue(false);

    // then
    expect(component.checked).toBeFalse();
  });

  it('should get correct NG_VALUE_ACCESSOR provider of type ToggleComponent', () => {
    expect(TestBed.inject(NG_VALUE_ACCESSOR)).toBeDefined();
    const toggleDecorators = (<any>ToggleComponent).__annotations__;
    const useExistingClass = toggleDecorators[0].providers[0].useExisting();

    expect(toggleDecorators[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(ToggleComponent);
  });

  it('should register correct function when registerOnTouched method is called', () => {
    const fn = {
      callback: noop
    };
    spyOn(fn, 'callback');
    component.registerOnTouched(fn.callback);
    (component as any)._touchFn();

    expect(fn.callback).toHaveBeenCalled();
  });

  it('should set component disabled when setDisabledState is called', () => {
    component.setDisabledState(false);

    expect(component.disabled).toBeFalse();
  });

  it('should have default touchFn method that returns empty object', () => {
    expect((component as any)._touchFn()).toBeFalsy();
  });

  it('should have default changeFn method that returns empty object', () => {
    expect((component as any)._touchFn()).toBeFalsy();
  });

  it('should call stopPropagation when calling changed method', () => {
    const event = new Event('keydown', { bubbles: true });
    spyOn(event, 'stopPropagation');
    component.changed(event);

    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('should call focusVia when calling focus method', () => {
    (component as any).elementRef = {
      nativeElement: {
        focus: noop
      }
    };
    spyOn((component as any).elementRef.nativeElement, 'focus');
    (component as any).focusMonitor = createSpyObj('FocusMonitor', ['focusVia', 'stopMonitoring']);
    component.focus();

    expect((component as any).elementRef.nativeElement.focus).toHaveBeenCalled();
    component.focus('keyboard');

    expect((component as any).focusMonitor.focusVia).toHaveBeenCalled();
  });

  it('should call focusVia when calling clickFocused method', () => {
    (component as any).focusMonitor = createSpyObj('FocusMonitor', ['focusVia', 'stopMonitoring']);
    component.clickFocused();

    expect((component as any).focusMonitor.focusVia).toHaveBeenCalled();
  });

  it('should start monitoring after view init', fakeAsync(() => {
    let touchFnCalled = false;
    (component as any)._touchFn = () => {
      touchFnCalled = true;
    };
    const subj = new Subject();
    let monitorWasCalled = false;
    (component as any).focusMonitor = {
      monitor: () => {
        monitorWasCalled = true;
        return subj.asObservable();
      },
      stopMonitoring: noop
    };
    component.ngAfterViewInit();
    subj.next(false);
    tick();

    expect(monitorWasCalled).toBeTruthy();
    expect(touchFnCalled).toBeTruthy();
    expect((component as any).changeDetectorRef.markForCheck).toHaveBeenCalled();
  }));
});
