import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import {
  AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, HostBinding, Input, OnDestroy, Output, ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@coyoapp/ui/cdk';

let nextUniqueId = 0;

/**
 * The cui-toggle component
 */
@Component({
  selector: 'cui-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ToggleComponent),
    multi: true
  }],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-toggle',
    '[class.cui-toggle-checked]': 'checked',
    '[class.cui-toggle-disabled]': 'disabled',
    '[class.cui-toggle-spread]': 'align === "left-spread" || align === "right-spread"'
  }
})
export class ToggleComponent implements AfterViewInit, OnDestroy, ControlValueAccessor {
  private readonly _uid = `cui-toggle-${nextUniqueId++}`;
  private _id = this._uid;
  private _checked = false;
  private _changeFn: (value: boolean) => void = noop;
  private _touchFn: () => void = noop;

  /**
   * The size of the toggle.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 'm';

  /**
   * The position of the label in relation to the toggle.
   */
  @Input()
  align: 'left' | 'right' | 'left-spread' | 'right-spread' = 'left';

  /**
   * The ID of this toggle.
   */
  @Input()
  get id(): string { return this._id; }
  set id(value: string) { this._id = value || this._uid; }

  /**
   * ID of the native input element inside `<cui-toggle>`, in order to attach an external Label
   * you have to suffix the ID used in the FOR attribute with '-input'
   * */
  get inputId(): string {
    return `${this.id || this._uid}-input`;
  }

  /**
   * Whether the toggle is checked.
   */
  @Input()
  get checked(): boolean { return this._checked; }
  set checked(value: boolean) {
    if (value !== this._checked) {
      this._checked = value;
      this.handleStateChange();
    }
  }

  /**
   * Whether the toggle is disabled.
   */
  @Input()
  disabled = false;

  /**
   * Whether the toggle is required.
   */
  @Input()
  required = false;

  /**
   * Name value will be applied to the input element if present
   */
  @Input()
  name?: string;

  /**
   * The value attribute of the native input element
   */
  @Input()
  value?: string;

  /**
   * The tab index of the input.
   */
  @Input()
  tabIndex?: number;

  /**
   * The `aria-label` attribute of this element.
   */
  @Input('aria-label')
  ariaLabel?: string;

  /**
   * The `aria-labelledby` attribute of this element.
   */
  @Input('aria-labelledby')
  ariaLabelledby?: string;

  /**
   * The `aria-describedby` attribute of this element. It is read after the
   * element's label and field type.
   */
  @Input('aria-describedby')
  ariaDescribedby?: string;

  /**
   * Event emitted when the toggle's `checked` value changes.
   */
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() readonly change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private readonly elementRef: ElementRef,
    private readonly focusMonitor: FocusMonitor,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef, true).subscribe(focusOrigin => {
      if (!focusOrigin) {
        // When a focused element becomes disabled, the browser *immediately* fires a blur event.
        // Angular does not expect events to be raised during change detection, so any state change
        // (such as a form control's 'ng-touched') will cause a changed-after-checked error.
        // See https://github.com/angular/angular/issues/17793. To work around this, we defer
        // telling the form control it has been touched until the next tick.
        // @see https://github.com/angular/components/blob/master/src/material/checkbox/checkbox.ts
        Promise.resolve().then(() => {
          this._touchFn();
          this.changeDetectorRef.markForCheck();
        });
      }
    });
  }

  ngOnDestroy(): void {
    this.focusMonitor.stopMonitoring(this.elementRef);
  }

  registerOnChange(fn: (value: boolean) => void): void {
    this._changeFn = fn;
  }

  registerOnTouched(fn: () => void): void {
    this._touchFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  changed(event: Event): void {
    event.stopPropagation();
  }

  clicked(event: Event): void {
    event.stopPropagation();
    if (!this.disabled) {
      this._checked = !this._checked;
      this.handleStateChange(true);
    }
  }

  clickFocused(): void {
    this.focusMonitor.focusVia(this.elementRef, 'mouse');
  }

  focus(origin?: FocusOrigin, options?: FocusOptions): void {
    if (origin) {
      this.focusMonitor.focusVia(this.elementRef, origin, options);
    } else {
      this.elementRef.nativeElement.focus(options);
    }
  }

  writeValue(value: boolean): void {
    this.checked = value;
    this.handleStateChange();
  }

  private handleStateChange(emit = false): void {
    if (emit) {
      this._changeFn?.(this.checked);
      this.change.emit(this.checked);
    }
  }
}
