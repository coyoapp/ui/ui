import {
  ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

/**
 * Use badges to give users **additional information** or **small status
 * updates** for specific UI elements. Badges scale to match the size of the
 * immediate parent element by using relative font sizing and `em` units.
 *
 * Note that depending on how they are used, badges may be confusing for users
 * of screen readers and similar assistive technologies. While the styling of
 * badges provides a visual cue as to their purpose, these users will simply be
 * presented with the content of the badge. Depending on the specific situation,
 * these badges may seem like random additional words or numbers at the end of
 * a sentence, link, or button.
 *
 * Unless the context is clear consider including additional context with a
 * visually hidden piece of additional text.
 *
 * ```html
Profile <cui-badge>9<span class="cui-visually-hidden">unread messages</span></cui-badge>
```
 */
@Component({
  selector: 'cui-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-badge',
    '[class.cui-round]': 'round'
  }
})
export class BadgeComponent {

  /**
   * The appearance mode of the badge.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  mode: 'primary' | 'secondary' | 'ghost' | 'note' | 'info' | 'success' | 'warning' | 'error' = 'primary';

  /**
   * The name of an icon to prefix the badge.
   */
  @Input()
  prefixIcon?: coyoIcon;

  /**
   * The name of an icon to suffix the badge.
   */
  @Input()
  suffixIcon?: coyoIcon;

  /**
   * Optional CSS classes to be applied to the icons of the badge.
   */
  @Input()
  iconClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * Whether the badge has round corners.
   */
  @Input()
  round = false;
}
