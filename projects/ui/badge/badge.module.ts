import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '@coyoapp/ui/icon';
import { BadgeComponent } from './badge/badge.component';

@NgModule({
  imports: [CommonModule, IconModule],
  declarations: [BadgeComponent],
  exports: [BadgeComponent]
})
export class BadgeModule {}
