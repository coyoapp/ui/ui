import { InjectionToken } from '@angular/core';

export interface AlertMessages {
  close: string;
}

export interface AlertConfig {
  readonly i18n: Readonly<AlertMessages>;
}

export const CUI_ALERT_DEFAULT_CONFIG: AlertConfig = {
  i18n: {
    close: 'Close'
  }
};

export const CUI_ALERT_CONFIG = new InjectionToken<AlertConfig>('CUI_ALERT_CONFIG', {
  factory: () => CUI_ALERT_DEFAULT_CONFIG
});
