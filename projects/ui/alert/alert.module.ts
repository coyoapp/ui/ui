import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { AlertIconPipe } from './alert-icon/alert-icon.pipe';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  imports: [CommonModule, IconModule, ButtonModule],
  declarations: [AlertComponent, AlertIconPipe],
  exports: [AlertComponent]
})
export class AlertModule {}
