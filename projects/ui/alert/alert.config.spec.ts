import { inject } from '@angular/core/testing';
import { AlertConfig, CUI_ALERT_CONFIG, CUI_ALERT_DEFAULT_CONFIG } from './alert.config';

describe('AlertConfig', () => {
  it('should provide a default config', inject([CUI_ALERT_CONFIG], (config: AlertConfig) => {
    expect(config).toBe(CUI_ALERT_DEFAULT_CONFIG);
  }));
});
