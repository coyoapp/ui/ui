import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CUI_ALERT_CONFIG } from '../alert.config';
import { AlertComponent } from './alert.component';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [AlertComponent],
      providers: [{
        provide: CUI_ALERT_CONFIG,
        useValue: { i18n: { close: 'Close' } }
      }]
    }).overrideTemplate(AlertComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit on close', done => {
    // given
    component.closed.subscribe(() => {
      // avoid empty spec warning
      expect(component).toBeTruthy();
      // then
      done();
    });

    // when
    component.close();
  });
});
