import {
  animate, style, transition, trigger
} from '@angular/animations';
import {
  ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Inject, Input, Output, ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { AlertConfig, CUI_ALERT_CONFIG } from '../alert.config';

/**
 * Provide contextual feedback messages for typical user actions with the
 * handful of available and flexible alert messages.
 *
 * **Please note:** Using color to add meaning only provides a visual indication,
 * which will not be conveyed to users of assistive technologies – such as
 * screen readers. Ensure that information denoted by the color is either
 * obvious from the content itself (e.g. the visible text), or is included
 * through alternative means, such as additional text hidden with the
 * `.cui-visually-hidden` class.
 *
 * Closing the alert can be done via the `closeable` input and by listening to
 * the `(closed)` event emitter and dismissing the alert in the parent
 * component. It is currently not possible to destroy a component from within
 * itself. A closing transition can be applied via the `transition` input. Thus,
 * it is required to either include `NoopAnimationsModule` or the
 * `BrowserAnimationsModule`.
 */
@Component({
  selector: 'cui-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-alert',
    '[class.cui-icon-alert]': 'icon',
    '[class.cui-dismissable-alert]': 'dismissable',
    '[@.disabled]': '!transition',
    '[@animateInOut]': 'transition'
  },
  animations: [
    trigger('animateInOut', [
      transition(':enter', [
        style({ opacity: 0, transform: 'rotateX(90deg)' }),
        animate('120ms', style({ opacity: 1, transform: 'rotateX(0)' }))
      ]),
      transition(':leave', [
        animate('120ms', style({ opacity: 0, transform: 'rotateX(90deg)' }))
      ])
    ])
  ]
})
export class AlertComponent {

  /**
   * The appearance mode of the alert.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  mode: 'primary' | 'secondary' | 'ghost' | 'note' | 'info' | 'success' | 'warning' | 'error' = 'primary';

  /**
   * The size of the button.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 'm';

  /**
   * An optional icon to show in the alert.
   */
  @Input()
  icon: coyoIcon | false | null = null;

  /**
   * Whether the alert can be closed.
   */
  @Input()
  closeable = false;

  /**
   * Whether the alert should fade in/out.
   */
  @Input()
  transition = false;

  /**
   * Event emitted when the close button is clicked.
   */
  @Output()
  readonly closed: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    @Inject(CUI_ALERT_CONFIG) readonly config: AlertConfig
  ) {}

  /**
   * Emits a close event for the alert.
   */
  close(): void {
    this.closed.emit(void 0);
  }
}
