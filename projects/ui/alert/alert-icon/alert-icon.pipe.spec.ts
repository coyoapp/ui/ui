import { AlertIconPipe } from './alert-icon.pipe';

describe('AlertIconPipe', () => {
  let pipe: AlertIconPipe;

  beforeEach(() => {
    pipe = new AlertIconPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('retrieve the correct icons', () => {
    // when
    const result1 = pipe.transform('globe', 'primary');
    const result2 = pipe.transform(null, 'primary');
    const result3 = pipe.transform(null, 'secondary');
    const result4 = pipe.transform(null, 'ghost');
    const result5 = pipe.transform(null, 'note');
    const result6 = pipe.transform(null, 'info');
    const result7 = pipe.transform(null, 'success');
    const result8 = pipe.transform(null, 'warning');
    const result9 = pipe.transform(null, 'error');
    const result10 = pipe.transform(null, 'unknown' as any);

    // then
    expect(result1).toBe('globe');
    expect(result2).toBe('info');
    expect(result3).toBe('info');
    expect(result4).toBe('info');
    expect(result5).toBe('note');
    expect(result6).toBe('info');
    expect(result7).toBe('check-circle');
    expect(result8).toBe('warning-triangle');
    expect(result9).toBe('stop');
    expect(result10).toBe('info');
  });
});
