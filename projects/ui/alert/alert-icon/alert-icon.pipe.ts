import { Pipe, PipeTransform } from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

/**
 * Internally used pipe to retrieve the most appropriate icon for an alert.
 */
@Pipe({
  name: 'cuiAlertIcon'
})
export class AlertIconPipe implements PipeTransform {
  private static readonly ICONS: { [mode: string]: coyoIcon } = {
    primary: 'info',
    secondary: 'info',
    ghost: 'info',
    note: 'note',
    info: 'info',
    success: 'check-circle',
    warning: 'warning-triangle',
    error: 'stop'
  };

  transform(icon: coyoIcon | null, mode: 'primary' | 'secondary' | 'ghost' | 'note' | 'info' | 'success' | 'warning' | 'error'): coyoIcon {
    return icon || AlertIconPipe.ICONS[mode] || 'info';
  }
}
