import { AfterContentInit, Component } from '@angular/core';
import { isObservable } from 'rxjs';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-select',
  host: {
    class: 'cui-formly-select'
  },
  template: `
    <cui-form-field [label]="to.label || ''">
      <cui-select [required]="!!to.required"
                  [compareWith]="compareItems"
                  [items]="items"
                  [multiple]="!!to.multiple"
                  [bindValue]="to.bindValue || 'id'"
                  [bindLabel]="to.bindLabel || 'label'"
                  [class.is-invalid]="showError"
                  [formControl]="toControl(formControl)"
      ></cui-select>
      <p *ngIf="to.description" cuiFormHint>{{to.description}}</p>
    </cui-form-field>`
})
export class FormlySelectComponent extends CuiFieldType implements AfterContentInit {
  items: any[] = [];

  compareItems = (option: any, model: string) => option['id'] === model;

  ngAfterContentInit() {
    if (isObservable(this.to.options)) {
      this.to.options.subscribe(values => {
        this.items = values;
      });
    } else {
      this.items = this.to.options ?? [];
    }
    if (this.to.bindValue) {
      this.compareItems = (option: any, model: string) => option[this.to.bindValue] === model;
    }
  }
}
