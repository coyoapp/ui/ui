import { FieldType } from '@ngx-formly/core';
import { AbstractControl, UntypedFormControl } from '@angular/forms';

/**
 * Base class for all CUI formly wrappers.
 */
export abstract class CuiFieldType extends FieldType {

  toControl(absCtrl: AbstractControl): UntypedFormControl {
    return absCtrl as UntypedFormControl;
  }
}
