import { Component } from '@angular/core';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-datepicker',
  host: {
    class: 'cui-formly-datepicker'
  },
  template: `
    <cui-form-field [label]="to.label || ''">
      <cui-input-datepicker [required]="!!to.required"
                            [placeholder]="to.placeholder || ''"
                            [class.is-invalid]="showError"
                            [formControl]="toControl(formControl)"
      ></cui-input-datepicker>
      <p *ngIf="to.description" cuiFormHint>{{to.description}}</p>
    </cui-form-field>`
})
export class FormlyInputDatepickerComponent extends CuiFieldType {}
