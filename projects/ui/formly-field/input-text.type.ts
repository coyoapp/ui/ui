import { Component } from '@angular/core';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-text',
  host: {
    class: 'cui-formly-text'
  },
  template: `
    <cui-form-field [label]="to.label || ''">
      <cui-input-text [required]="!!to.required"
                      [placeholder]="to.placeholder || ''"
                      [minlength]="to.minLength || null"
                      [maxlength]="to.maxLength || null"
                      [class.is-invalid]="showError"
                      [formControl]="toControl(formControl)"
      ></cui-input-text>
      <p *ngIf="to.description" cuiFormHint>{{to.description}}</p>
    </cui-form-field>`
})
export class FormlyInputTextComponent extends CuiFieldType {}
