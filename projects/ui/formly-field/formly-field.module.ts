import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from '@coyoapp/ui/checkbox';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { InputDatepickerModule } from '@coyoapp/ui/input-datepicker';
import { InputNumberModule } from '@coyoapp/ui/input-number';
import { InputTextModule } from '@coyoapp/ui/input-text';
import { InputTextareaModule } from '@coyoapp/ui/input-textarea';
import { SelectModule } from '@coyoapp/ui/select';
import { FormlyCheckboxComponent } from './checkbox.type';
import { FormlyInputDatepickerComponent } from './input-datepicker.type';
import { FormlyInputNumberComponent } from './input-number.type';
import { FormlyInputTextComponent } from './input-text.type';
import { FormlyInputTextareaComponent } from './input-textarea.type';
import { FormlySelectComponent } from './select.type';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    FormFieldModule,
    CheckboxModule,
    InputNumberModule,
    InputDatepickerModule,
    SelectModule
  ],
  declarations: [
    FormlyCheckboxComponent,
    FormlyInputNumberComponent,
    FormlyInputTextComponent,
    FormlyInputTextareaComponent,
    FormlyInputDatepickerComponent,
    FormlySelectComponent
  ],
  exports: [
    FormlyCheckboxComponent,
    FormlyInputNumberComponent,
    FormlyInputTextComponent,
    FormlyInputTextareaComponent,
    FormlyInputDatepickerComponent,
    FormlySelectComponent
  ]
})
export class FormlyFieldModule {}
