import { Component } from '@angular/core';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-number',
  host: {
    class: 'cui-formly-number'
  },
  template: `
    <cui-form-field [label]="to.label || ''">
      <cui-input-number [required]="!!to.required"
                        [placeholder]="to.placeholder || ''"
                        [min]="to.min || null"
                        [max]="to.max || null"
                        [class.is-invalid]="showError"
                        [formControl]="toControl(formControl)"
      ></cui-input-number>
      <p *ngIf="to.description" cuiFormHint>{{to.description}}</p>
    </cui-form-field>`
})
export class FormlyInputNumberComponent extends CuiFieldType {}
