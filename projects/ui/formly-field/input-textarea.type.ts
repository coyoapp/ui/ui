import { Component } from '@angular/core';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-textarea',
  host: {
    class: 'cui-formly-textarea'
  },
  template: `
    <cui-form-field [label]="to.label || ''">
      <cui-input-textarea [required]="!!to.required"
                          [placeholder]="to.placeholder || ''"
                          [minlength]="to.minLength || null"
                          [maxlength]="to.maxLength || null"
                          [class.is-invalid]="showError"
                          [formControl]="toControl(formControl)"
      ></cui-input-textarea>
      <p *ngIf="to.description" cuiFormHint>{{to.description}}</p>
    </cui-form-field>`
})
export class FormlyInputTextareaComponent extends CuiFieldType {}
