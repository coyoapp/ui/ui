import { Component } from '@angular/core';
import { CuiFieldType } from './cui-field-type';

@Component({
  selector: 'cui-formly-checkbox',
  host: {
    class: 'cui-formly-checkbox'
  },
  styleUrls: ['./checkbox.type.scss'],
  template: `
    <cui-checkbox [formControl]="toControl(formControl)" [required]="!!to.required">{{to.label}}</cui-checkbox>
    <div class="cui-input-hints" *ngIf="to.description">
      <p>{{to.description}}</p>
    </div>`
})
export class FormlyCheckboxComponent extends CuiFieldType {}
