import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  InjectionToken,
  Input,
  OnDestroy,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { TippyInstance, TippyService } from '@ngneat/helipopper';

export const CUI_POPOVER = new InjectionToken<PopoverComponent>('cuiPopover');

export type PopoverPlacement =
  'auto' | 'auto-start' | 'auto-end' |
  'top' | 'top-start' | 'top-end' |
  'left' | 'left-start' | 'left-end' |
  'right' | 'right-start' | 'right-end' |
  'bottom' | 'bottom-start' | 'bottom-end';

@Component({
  selector: 'cui-popover',
  exportAs: 'popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: CUI_POPOVER,
    useExisting: PopoverComponent
  }],
  host: {
    class: 'cui-popover'
  }
})
export class PopoverComponent implements OnDestroy {
  private host?: ElementRef;
  private tippy?: TippyInstance;

  @ViewChild(TemplateRef)
  content?: TemplateRef<any>;

  /**
   * A template reference to be used instead of the component's content.
   * Used to implement lazy initialization for the popover content. Takes
   * precedence over the component's content.
   */
  @Input()
  template?: TemplateRef<any>;

  /**
   * The placement of the popover.
   */
  @Input()
  placement: PopoverPlacement = 'bottom-start';

  /**
   * The actually used template reference.
   */
  get templateRef(): TemplateRef<any> | undefined {
    return this.template || this.content;
  }

  constructor(
    private readonly tippyService: TippyService
  ) {}

  ngOnDestroy(): void {
    this.tearDown();
  }

  show(host: ElementRef): void {
    if (this.host !== host) {
      this.tearDown();
    }

    if (!this.tippy && this.templateRef) {
      this.host = host;
      this.tippy = this.tippyService.create(this.host.nativeElement, this.templateRef, {
        variation: 'popover',
        theme: 'popover',
        placement: this.placement
      });
    }

    this.tippy?.show();
  }

  hide(refocus = true): void {
    this.tippy?.hide();
    if (refocus) {
      this.host?.nativeElement.focus();
    }
  }

  private tearDown(): void {
    this.tippy?.destroy();
    this.tippy = undefined;
    this.host = undefined;
  }
}
