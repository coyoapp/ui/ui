import { ElementRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExtendedTippyInstance, TippyService } from '@ngneat/helipopper';
import { PopoverComponent } from './popover.component';

describe('PopoverComponent', () => {
  let component: PopoverComponent;
  let fixture: ComponentFixture<PopoverComponent>;
  let nativeElement: HTMLElement;
  let elementRef: ElementRef;
  let tippyService: jasmine.SpyObj<TippyService>;
  let tippyInstance: jasmine.SpyObj<ExtendedTippyInstance<any>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PopoverComponent]
    }).overrideTemplate(PopoverComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoverComponent);
    component = fixture.componentInstance;
    nativeElement = { focus: () => {} } as HTMLElement;
    elementRef = { nativeElement } as ElementRef;
    tippyService = jasmine.createSpyObj('TippyService', ['create']);
    tippyInstance = jasmine.createSpyObj('TippyInstance', ['show', 'enable', 'disable', 'setContent', 'setProps', 'destroy']);
    tippyService.create.and.returnValue(tippyInstance);
    (component as any).tippyService = tippyService;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call tippyService on show', () => {
    spyOn((component as any), 'tearDown');
    (component as any).host = null;
    (component as any).content = 'test';
    component.show(elementRef);

    expect((component as any).tearDown).toHaveBeenCalled();
    expect((component as any).host).toEqual(elementRef);
    expect((component as any).tippy).toEqual(tippyInstance);
    expect(tippyInstance.show).toHaveBeenCalled();
  });

  it('should hide to call appropriate methods', () => {
    spyOn(elementRef.nativeElement, 'focus');
    (component as any).host = elementRef;
    component.hide(true);

    expect(elementRef.nativeElement.focus).toHaveBeenCalled();
  });
});
