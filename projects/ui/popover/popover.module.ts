import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { popperVariation, provideTippyConfig, tooltipVariation } from '@ngneat/helipopper';
import { PopoverTriggerDirective } from './popover-trigger/popover-trigger.directive';
import { PopoverComponent } from './popover/popover.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PopoverTriggerDirective,
    PopoverComponent
  ],
  exports: [
    PopoverTriggerDirective,
    PopoverComponent
  ]
})
export class PopoverModule {

  static forRoot(): ModuleWithProviders<PopoverModule> {
    return {
      ngModule: PopoverModule,
      providers: [provideTippyConfig({
        variations: {
          tooltip: tooltipVariation,
          popover: {
            ...popperVariation,
            placement: 'auto',
            appendTo: 'parent',
            arrow: false,
            animation: 'shift-away-subtle',
            offset: [0, 8],
            maxWidth: 'unset'
          }
        }
      })]
    };
  }
}
