import { Directive, ElementRef, HostListener, Input, OnDestroy } from '@angular/core';
import { PopoverComponent } from '../popover/popover.component';

@Directive({
  selector: '[cui-popover-trigger]'
})
export class PopoverTriggerDirective implements OnDestroy {

  @Input('cui-popover-trigger')
  popover?: PopoverComponent;

  @Input()
  disabled = false;

  constructor(
    private readonly elementRef: ElementRef
  ) {}

  ngOnDestroy(): void {
    this.popover?.hide();
  }

  @HostListener('click')
  click(): void {
    if (!this.disabled) {
      this.popover?.show(this.elementRef);
    }
  }
}
