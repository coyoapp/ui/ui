import { ElementRef } from '@angular/core';
import { PopoverComponent } from '@coyoapp/ui/popover';
import { PopoverTriggerDirective } from './popover-trigger.directive';

describe('PopoverTriggerDirective', () => {
  let directive: PopoverTriggerDirective;
  let elementRef: ElementRef;
  let popover: jasmine.SpyObj<PopoverComponent>;

  beforeEach(() => {
    elementRef = {} as any;
    popover = jasmine.createSpyObj('PopoverComponent', ['show', 'hide']);
    directive = new PopoverTriggerDirective(elementRef);
    directive.popover = popover;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should hide popover on destroy', () => {
    directive.ngOnDestroy();

    expect(directive.popover?.hide).toHaveBeenCalled();
  });

  it('should show on click', () => {
    directive.click();

    expect(directive.popover?.show).toHaveBeenCalled();
  });
});
