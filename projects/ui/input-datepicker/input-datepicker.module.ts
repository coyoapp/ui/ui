import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { ButtonModule } from '@coyoapp/ui/button';
import { AngularMyDatePickerModule } from '@nodro7/angular-mydatepicker';
import { MenuModule } from '@coyoapp/ui/menu';
import { InputDatepickerComponent } from './input-datepicker/input-datepicker.component';
import { InputDatepickerInlineComponent } from './input-datepicker-inline/input-datepicker-inline.component';

@NgModule({
  imports: [CommonModule, MenuModule, FormsModule, ReactiveFormsModule, ButtonModule, AngularMyDatePickerModule,
            NgxMaskPipe, NgxMaskDirective],
  declarations: [InputDatepickerComponent, InputDatepickerInlineComponent],
  exports: [InputDatepickerComponent, InputDatepickerInlineComponent]
})
export class InputDatepickerModule {

  static forRoot(): ModuleWithProviders<InputDatepickerModule> {
    return {
      ngModule: InputDatepickerModule,
      providers: [provideNgxMask()]
    };
  }
}
