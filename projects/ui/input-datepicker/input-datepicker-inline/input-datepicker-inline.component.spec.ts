import {
  ComponentFixture, fakeAsync, TestBed, tick
} from '@angular/core/testing';
import { IMyDateModel } from '@nodro7/angular-mydatepicker';
import { ChangeDetectorRef } from '@angular/core';
import { InputDatepickerInlineComponent } from './input-datepicker-inline.component';

describe('InputDatepickerComponent', () => {
  let component: InputDatepickerInlineComponent;
  let fixture: ComponentFixture<InputDatepickerInlineComponent>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    await TestBed.configureTestingModule({
      declarations: [InputDatepickerInlineComponent]
    }).overrideTemplate(InputDatepickerInlineComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDatepickerInlineComponent);
    component = fixture.componentInstance;
    (component as any).changeDetectorRef = changeDetectorRef;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the date dropdown when onDateChanged() is called', () => {
    // given
    spyOn(component.dateChanged, 'emit');
    const date: IMyDateModel = {
      isRange: true
    };
    // when
    component.onDateChanged(date);
    fixture.detectChanges();

    // then
    expect(component.dateChanged.emit).toHaveBeenCalled();
  });

  it('should setDate on onModelValueChanged method call', () => {
    // given
    spyOn(component, 'setDate');

    // when
    component.onModelValueChanged();
    fixture.detectChanges();

    // then
    expect(component.setDate).toHaveBeenCalled();
  });

  it('should have correct ngModel value', fakeAsync(() => {
    fixture = TestBed.createComponent(InputDatepickerInlineComponent);
    fixture.componentInstance.value = '22/02/2021';
    fixture.detectChanges();
    tick();

    expect(fixture.componentInstance.value).toEqual('22/02/2021');
  }));

  it('should set correct config values', fakeAsync(() => {
    // given
    fixture = TestBed.createComponent(InputDatepickerInlineComponent);
    fixture.componentInstance.dateFormat = 'dd/mm/yyyy';
    fixture.componentInstance.isRange = false;
    Object.defineProperty(fixture.componentInstance, 'config', {
      selectorHeight: '20rem',
      selectorWidth: '20rem',
      dateFormats: {
        'dd/mm/yyyy': {
          delimiter: '/',
          mask: 'd0/M0/0000',
          dateFormat: 'dd/mm/yyyy',
          rangeMask: 'd0/M0/0000 - d0/M0/0000'
        }
      }
    } as any);

    // when
    tick();
    fixture.detectChanges();
    component.setDate();

    // then
    expect(fixture.componentInstance.selectedDateFormatObject).toEqual({
      mask: 'd0/M0/0000',
      rangeMask: 'd0/M0/0000 - d0/M0/0000',
      dateFormat: 'dd/mm/yyyy',
      delimiter: '/'
    });
  }));

  it('should set default single date model when setDate is called', fakeAsync(() => {
    // given
    component.dateFormat = 'dd/mm/yyyy';
    component.value = '22/12/2021';
    component.isRange = true;

    // when
    tick();
    fixture.detectChanges();
    component.setRangeOfDatesModel();

    // then
    expect(component.hasValue).toBeFalse();
    expect(component.model).toEqual({
      isRange: true,
      singleDate: undefined,
      dateRange: undefined
    });
  }));

  it('should set default range dates model when setRangeOfDatesModel is called', () => {
    // given
    component.dateFormat = 'dd/mm/yyyy';
    component.isRange = true;

    // when
    fixture.detectChanges();
    component.setRangeOfDatesModel();

    // then
    expect(component.hasValue).toBeFalse();
    expect(component.model).toEqual({
      isRange: true,
      singleDate: undefined,
      dateRange: undefined
    });
  });

  it('should extract correct my IMyDate object when calling getDayMonthYearFormatFromDateAndFormat', () => {
    // given
    component.dateFormat = 'dd/mm/yyyy';
    component.isRange = false;

    // when
    fixture.detectChanges();
    const date = component.getDayMonthYearFormatFromDateAndFormat('20/02/2022', 'dd/mm/yyyy');

    // then
    expect(date).toEqual({
      year: 2022,
      month: 2,
      day: 20
    });
  });

  it('should set correct dateFormat values', fakeAsync(() => {
    spyOn(component, 'setDate');
    component.dateFormat = 'mm/dd/yyyy';

    expect(component.showDatepicker).toBeFalse();
    expect(component.value).toBeFalsy();
    expect(component.myDatePickerOptions.dateFormat).toEqual('mm/dd/yyyy');
    expect(component.setDate).toHaveBeenCalled();
    tick();
    fixture.detectChanges();

    expect(component.showDatepicker).toBeTrue();
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  }));

  it('should set correct date value on onDateChanged() method based on date type', () => {
    const date: IMyDateModel = {
      isRange: true,
      singleDate: undefined,
      dateRange: {
        formatted: '12.22.2222 - 12.22.2222'
      }
    };
    spyOn(component.dateChanged, 'emit');
    component.onDateChanged(date);
    fixture.detectChanges();

    expect(component.dateChanged.emit).toHaveBeenCalled();
    expect(component.value).toEqual('12.22.2222 - 12.22.2222');
    date.singleDate = {
      formatted: '12.22.2222'
    };
    component.onDateChanged(date);

    expect(component.value).toEqual('12.22.2222');
  });

  it('should set correct date on setDate method call', () => {
    spyOn(component, 'setRangeOfDatesModel');
    spyOn(component, 'getDayMonthYearFormatFromDateAndFormat').and.returnValue({ year: 2222, month: 12, day: 11 });
    // check with range date
    component.dateFormat = 'dd.mm.yyyy';
    component.isRange = true;
    component.value = '12.12.2222 - 13.12.2222';
    fixture.detectChanges();
    component.setDate();

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component.setRangeOfDatesModel).toHaveBeenCalled();

    // check with single date
    component.isRange = false;
    component.value = '11.11.2222';
    component.setDate();

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component.getDayMonthYearFormatFromDateAndFormat).toHaveBeenCalledWith('11.11.2222', 'dd.mm.yyyy');
    expect(component.model).toEqual({
      isRange: false,
      singleDate: { date: { year: 2222, month: 12, day: 11 } },
      dateRange: undefined
    });
  });

  it('should set correct model on setRangeOfDatesModel method call', () => {
    spyOn(component, 'getDayMonthYearFormatFromDateAndFormat').and.returnValue({ year: 2222, month: 12, day: 11 });
    component.isRange = true;
    fixture.detectChanges();
    component.value = '12.12.2222 - 13.12.2222';
    component.setRangeOfDatesModel();

    expect(component.getDayMonthYearFormatFromDateAndFormat).toHaveBeenCalled();
    expect(component.model).toEqual({
      isRange: true,
      singleDate: undefined,
      dateRange: { beginDate: { year: 2222, month: 12, day: 11 }, endDate: { year: 2222, month: 12, day: 11 } }
    });
    // on invalid date should set default model
    component.value = '12.12.2222';
    component.setRangeOfDatesModel();

    expect(component.model).toEqual({ isRange: true, singleDate: undefined, dateRange: undefined });
  });

});
