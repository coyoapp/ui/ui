import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Injector,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation,
  LOCALE_ID
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';
import { IAngularMyDpOptions, IMyDate, IMyDateModel } from '@nodro7/angular-mydatepicker';
import {
  CUI_INPUT_DATEPICKER_INLINE_CONFIG, DateFormatObject,
  DatePickerConfig
} from '../input-datepicker.config';

/**
 * COYO-UI Inline DatePicker Component
 *
 * @description
 * This component allows single date and range of dates selection. It will display an inline datepicker.
 * The component has the following inputs:
 * isRange - specifies if we want to select a single date or a range of dates
 * dateFormat - specifies the date format that will be used for the datepicker librayr component
 *
 * @example
 *    <cui-input-datepicker-inline [(ngModel)]="value">
 *   </cui-input-datepicker-inline>
 */

@Component({
  selector: 'cui-input-datepicker-inline',
  templateUrl: './input-datepicker-inline.component.html',
  styleUrls: ['./input-datepicker-inline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [provideFormFieldControl(InputDatepickerInlineComponent)],
  host: {
    class: 'cui-datepicker-inline'
  }
})
export class InputDatepickerInlineComponent extends FormFieldControlComponent<string> {
  /**
   * Locale input for angular-mydatepicker component.
   */
  @Input()
  get locale(): string | undefined {
    return this._locale;
  }

  set locale(value: string | undefined) {
    this._locale = value;
  }

  private _locale?: string;

  /**
   * isRange input to toggle date range selection
   */
  @Input()
  get isRange(): boolean {
    return this._isRange;
  }

  set isRange(value: boolean) {
    if (this._isRange !== value) {
      this.showDatepicker = false;
      this.value = null;
      this._isRange = value;
      this.myDatePickerOptions.dateRange = value;
      this.setDate();
      setTimeout(() => {
        this.showDatepicker = true;
        this.changeDetectorRef.markForCheck();
      }, 0);
    }
  }

  private _isRange = false;

  /**
   * datepicker width
   */
  @Input() get width(): string | undefined {
    return this._width;
  }

  set width(value: string | undefined) {
    this._width = value;
    this.myDatePickerOptions.selectorWidth = this._width ? this._height : this.config.selectorHeight;
  }

  _width?: string;

  /**
   * datepicker height
   */
  @Input() get height(): string | undefined {
    return this._height;
  }

  set height(value: string | undefined) {
    this._height = value;
    this.myDatePickerOptions.selectorHeight = this._height ? this._width : this.config.selectorWidth;
  }

  _height?: string;

  /**
   * dateFormat input
   */
  @Input()
  get dateFormat(): string {
    return this._dateFormat;
  }

  set dateFormat(value: string) {
    if (this._dateFormat !== value) {
      this.showDatepicker = false;
      this.value = null;
      this._dateFormat = value;
      this.myDatePickerOptions.dateFormat = value;
      this.setDate();
      setTimeout(() => {
        this.showDatepicker = true;
        this.changeDetectorRef.markForCheck();
      }, 0);
    }
  }

  private _dateFormat = 'dd/mm/yyyy';

  // this getter extracts the active config based on the selected dateFormat input
  get selectedDateFormatObject(): DateFormatObject {
    return this.config.dateFormats[this.dateFormat] ? this.config.dateFormats[this.dateFormat] : this.config.dateFormats['default'];
  }

  public showDatepicker = true;

  // this defines the datepicker library component options, that we extract from the config file
  public myDatePickerOptions: IAngularMyDpOptions = {
    dateRange: this.isRange,
    inline: true,
    dateFormat: this.selectedDateFormatObject.dateFormat,
    showSelectorArrow: false,
    selectorWidth: this.config.selectorWidth,
    selectorHeight: this.config.selectorHeight,
    markWeekends: { marked: false, color: '' }
  };

  @Output()
  readonly dateChanged: EventEmitter<IMyDateModel> = new EventEmitter<IMyDateModel>();

  model: IMyDateModel | null = null;

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  constructor(
    injector: Injector,
    @Inject(CUI_INPUT_DATEPICKER_INLINE_CONFIG) readonly config: DatePickerConfig,
    @Inject(LOCALE_ID) readonly angularLocale: string,
    readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
    this.setDate();
  }

  //
  /**
   *  onDateChanged is a called when the model changes and allows us to emit the dateChanged event and set the date value
   */
  onDateChanged(event: IMyDateModel): void {
    this.value = event.singleDate ? event.singleDate?.formatted as string : event.dateRange?.formatted as string;
    this.dateChanged.emit(event);
  }

  /**
   * getDayMonthYearFormatFromDateAndFormat method extracts the IMyDate date object based on specified date and format
   */
  getDayMonthYearFormatFromDateAndFormat(date: string, dateFormat: string): IMyDate {
    const dateObject: IMyDate = {
      year: 0,
      month: 0,
      day: 0
    };
    // we extract the date parts using the delimiter in settings file
    const dateParts = date.split(this.selectedDateFormatObject.delimiter);
    const dateFormatParts = dateFormat.split(this.selectedDateFormatObject.delimiter);
    // the date components are extracted into an IMyDate interface
    dateFormatParts.forEach((val, index) => {
      if (val.includes('y')) {
        dateObject.year = Number.parseInt(dateParts[index], 10);
      }
      if (val.includes('m')) {
        dateObject.month = Number.parseInt(dateParts[index], 10);
      }
      if (val.includes('d')) {
        dateObject.day = Number.parseInt(dateParts[index], 10);
      }
    });
    return dateObject;
  }

  /**
   * setDate method sets the initial date values for the datePicker component
   */
  setDate(): void {
    if (this.value) {
      if (this.isRange) {
        // if we have a date range we initiate the datepicker library model for it
        this.setRangeOfDatesModel();
      } else {
        // if it's not range we just initiate the single date model
        const dayMonthYearDate = this.getDayMonthYearFormatFromDateAndFormat(this.value, this.selectedDateFormatObject.dateFormat);
        this.model = { isRange: this.isRange, singleDate: { date: dayMonthYearDate }, dateRange: undefined };
      }
    } else {
      // if we have no existing date value we set the datepicker model to default settings
      this.model = { isRange: this.isRange, singleDate: undefined, dateRange: undefined };
    }
    this.myDatePickerOptions.dateFormat = this.selectedDateFormatObject.dateFormat;
    this.changeDetectorRef.markForCheck();
  }

  /**
   * setRangeOfDatesModel will set the initial datepicker model for a range of dates
   */
  setRangeOfDatesModel(): void {
    if (!this.value) {
      return;
    }
    // we split the range date string into two parts
    const days = this.value.split('-');
    if (days.length > 1) {
      // if both date ranges were set we initiate the datepicker model with the two dates
      const beginDayMonthYearDate = this.getDayMonthYearFormatFromDateAndFormat(days[0], this.selectedDateFormatObject.dateFormat);
      const endDayMonthYearDate = this.getDayMonthYearFormatFromDateAndFormat(days[1], this.selectedDateFormatObject.dateFormat);
      this.model = {
        isRange: this.isRange,
        singleDate: undefined,
        dateRange: { beginDate: beginDayMonthYearDate, endDate: endDayMonthYearDate }
      };
    } else {
      // if we don't have existing range values we set the datepicker model to default values
      this.model = { isRange: this.isRange, singleDate: undefined, dateRange: undefined };
    }
  }

  onModelValueChanged(): void {
    this.setDate();
    setTimeout(() => {
      this.setDate();
    }, 0);
  }
}
