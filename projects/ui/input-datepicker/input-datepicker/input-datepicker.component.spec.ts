import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MenuComponent } from '@coyoapp/ui/menu';
import { TippyService } from '@ngneat/helipopper';

import { InputDatepickerComponent } from './input-datepicker.component';

describe('InputDatepickerComponent', () => {
  let component: InputDatepickerComponent;
  let fixture: ComponentFixture<InputDatepickerComponent>;
  let tippyService: TippyService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InputDatepickerComponent]
    }).overrideTemplate(InputDatepickerComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDatepickerComponent);
    component = fixture.componentInstance;
    tippyService = TestBed.inject(TippyService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear the date value when clearValue() method is called', () => {
    // given
    component.value = '12/02/2022';

    // when
    component.clearValue();
    fixture.detectChanges();

    // then
    expect(component.value).toBeNull();
  });

  it('should close the date dropdown when onDateChanged() is called', () => {
    // given
    component.menu = new MenuComponent(tippyService);
    spyOn(component.menu, 'hide').and.callThrough();

    // when
    component.onDateChanged();
    fixture.detectChanges();

    // then
    expect(component.menu.hide).toHaveBeenCalled();
  });

  it('should return correct isRange value', fakeAsync(() => {
    (component as any)._isRange = false;
    component.isRange = true;
    fixture.detectChanges();
    tick();

    expect(component.isRange).toEqual(true);
  }));

  it('should return correct dateFormat value', () => {
    (component as any)._dateFormat = 'dd/mm/yyyy';
    component.dateFormat = 'mm/dd/yyyy';
    fixture.detectChanges();

    expect(component.dateFormat).toEqual('mm/dd/yyyy');
  });

  it('should return correct selectedDateFormatObject value', () => {
    component.dateFormat = 'mm/dd/yyyy';
    fixture.detectChanges();
    const selectedDateFormat = component.selectedDateFormatObject;

    expect(selectedDateFormat).toEqual({
      delimiter: '/',
      mask: 'M0/d0/0000',
      dateFormat: 'mm/dd/yyyy',
      rangeMask: 'M0/d0/0000 - M0/d0/0000'
    });
  });

  it('should clear the value when clearIfNotMatch() is called and form is invalid', () => {
    // given
    spyOnProperty(component.formControl, 'invalid', 'get').and.returnValue(true);
    component.value = '12/02/202';

    // when
    component.clearIfNotMatch();
    fixture.detectChanges();

    // then
    expect(component.value).toBeNull();
  });

  it('should not clear the value when clearIfNotMatch() is called and form is valid', () => {
    // given
    spyOnProperty(component.formControl, 'invalid', 'get').and.returnValue(false);
    component.value = '12/02/2022';

    // when
    component.clearIfNotMatch();
    fixture.detectChanges();

    // then
    expect(component.value).not.toBeNull();
  });
});
