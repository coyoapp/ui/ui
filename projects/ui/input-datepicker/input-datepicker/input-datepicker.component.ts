import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';
import { MenuComponent } from '@coyoapp/ui/menu';
import { CUI_INPUT_DATEPICKER_INLINE_CONFIG, DateFormatObject, DatePickerConfig } from '../input-datepicker.config';

/**
 * COYO-UI DatePicker Component
 *
 * @description
 * This component allows single date and range of dates selection or direct user input. This component
 * can be wrapped around and can be wrapped inside a cui-form-field component.
 * The user can select a date from the datepicker or type in a date that is validated using a mask.
 *
 * The component has the following inputs:
 * isRange - specifies if we want a single date or a range of dates
 * dateFormat - specifies the format used by the ngx-mask component and the datepicker library component
 *
 * @example
 * <cui-form-field>
 *   <cui-input-datepicker [(ngModel)]="value">
 *   </cui-input-datepicker>
 * </cui-form-field>
 */
@Component({
  selector: 'cui-input-datepicker',
  templateUrl: './input-datepicker.component.html',
  styleUrls: ['./input-datepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputDatepickerComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-datepicker'
  }
})
export class InputDatepickerComponent extends FormFieldControlComponent<string> {
  @ViewChild(MenuComponent)
  menu!: MenuComponent;
  showDatepicker = true;
  _clearButtonLabel = '';
  _calendarButtonLabel = '';

  /**
   * isRange input to toggle date range selection
   */
  @Input()
  get isRange(): boolean {
    return this._isRange;
  }

  set isRange(value: boolean) {
    this._isRange = value;
  }

  /**
   * clearButtonLabel input to set clear button aria-label attribute
   */
  @Input()
  get clearButtonLabel(): string {
    return this._clearButtonLabel;
  }

  set clearButtonLabel(value: string) {
    this._clearButtonLabel = value;
  }
  /**
   * calendarButtonLabel input to set clear button aria-label attribute
   */
  @Input()
  get calendarButtonLabel(): string {
    return this._calendarButtonLabel;
  }

  set calendarButtonLabel(value: string) {
    this._calendarButtonLabel = value;
  }

  private _isRange = false;

  /**
   * dateFormat input
   */
  @Input()
  get dateFormat(): string {
    return this._dateFormat;
  }

  set dateFormat(value: string) {
    this._dateFormat = value;
  }

  private _dateFormat = 'dd/mm/yyyy';

  /**
   * Locale input for angular-mydatepicker component.
   */
  @Input()
  get locale(): string | undefined {
    return this._locale;
  }

  set locale(value: string | undefined) {
    this._locale = value;
  }

  private _locale?: string;

  get selectedDateFormatObject(): DateFormatObject {
    return this.config.dateFormats[this.dateFormat] ? this.config.dateFormats[this.dateFormat] : this.config.dateFormats['default'];
  }

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  constructor(
    injector: Injector,
    @Inject(CUI_INPUT_DATEPICKER_INLINE_CONFIG) readonly config: DatePickerConfig,
    readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(injector);
  }

  onDateChanged(): void {
    this.menu.hide();
  }

  clearValue(): void {
    this.value = null;
  }

  clearIfNotMatch(): void {
    if (this.formControl.invalid) {
      this.clearValue();
    }
  }

}
