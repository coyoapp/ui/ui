import { InjectionToken } from '@angular/core';

/**
 * CDateFormatObject interface
 *
 * @description
 * This is used to define the config values for the ng-aask library and the datepicker library.
 * The properties are:
 * mask - this is the mask used for a single date by the ngx-mask library to validate user input
 * rangeMask - this is the mask used for a range of dates by the ngx-mask library
 * dateFormat - this is the format exported by the datePicker library that has to match the mask
 * delimiter - this is the delimiter that separates the date parts, ex: 12.03.2022, 12/03/2022
 */

export interface DateFormatObject {
  mask: string,
  rangeMask: string,
  dateFormat: string,
  delimiter: string
}

/**
 * DateFormat interface
 *
 * @description
 * This interfaces acts like a map that allows us to specify different formats we support and define
 * the config options for each library
 */
export interface DateFormat {
  [index: string]: DateFormatObject
}

/**
 * DatePickerConfig interface
 *
 * @description
 * this is the config interface used to initiate the datePicker library component
 */
export interface DatePickerConfig {
  dateFormats: DateFormat,
  selectorHeight: string,
  selectorWidth: string
}

/**
 * CUI_INPUT_DATEPICKER_INLINE_DEFAULT_CONFIG config
 *
 * @description
 * this is the default config that is used by the datePicker component that wraps
 * together all the previous interfaces
 */
export const CUI_INPUT_DATEPICKER_INLINE_DEFAULT_CONFIG: DatePickerConfig = {
  selectorHeight: '20rem',
  selectorWidth: '20rem',
  dateFormats: {
    'dd/mm/yyyy': {
      delimiter: '/',
      mask: 'd0/M0/0000',
      dateFormat: 'dd/mm/yyyy',
      rangeMask: 'd0/M0/0000 - d0/M0/0000'
    },
    'mm/dd/yyyy': {
      delimiter: '/',
      mask: 'M0/d0/0000',
      dateFormat: 'mm/dd/yyyy',
      rangeMask: 'M0/d0/0000 - M0/d0/0000'
    },
    'dd.mm.yyyy': {
      delimiter: '.',
      mask: 'd0.M0.0000',
      dateFormat: 'dd.mm.yyyy',
      rangeMask: 'd0.M0.0000 - d0.M0.0000'
    },
    'mm.dd.yyyy': {
      delimiter: '.',
      mask: 'M0.d0.0000',
      dateFormat: 'mm.dd.yyyy',
      rangeMask: 'M0.d0.0000 - M0.d0.0000'
    },
    'default': {
      delimiter: '/',
      mask: 'd0/M0/0000',
      dateFormat: 'dd/mm/yyyy',
      rangeMask: 'd0/M0/0000 - d0/M0/0000'
    }
  }
};

export const CUI_INPUT_DATEPICKER_INLINE_CONFIG = new InjectionToken<DatePickerConfig>('CUI_INPUT_DATEPICKER_INLINE_CONFIG', {
  factory: () => CUI_INPUT_DATEPICKER_INLINE_DEFAULT_CONFIG
});
