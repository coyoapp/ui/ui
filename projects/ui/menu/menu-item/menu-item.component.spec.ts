import { FocusKeyManager, FocusMonitor } from '@angular/cdk/a11y';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER, FocusKeyManagerProvider } from '@coyoapp/ui/cdk';
import { of } from 'rxjs';
import { CUI_MENU, Menu } from '@coyoapp/ui/menu/menu/menu';
import { MenuItemComponent } from './menu-item.component';

describe('MenuItemComponent', () => {
  let component: MenuItemComponent;
  let fixture: ComponentFixture<MenuItemComponent>;
  let focusMonitor: jasmine.SpyObj<FocusMonitor>;
  let keyManager: jasmine.SpyObj<FocusKeyManager<MenuItemComponent>>;
  let keyManagerProvider: jasmine.SpyObj<FocusKeyManagerProvider<any>>;
  let menu: jasmine.SpyObj<Menu>;

  beforeEach(async () => {
    focusMonitor = jasmine.createSpyObj('FocusMonitor', ['monitor', 'stopMonitoring', 'focusVia']);
    keyManager = jasmine.createSpyObj('FocusKeyManager', ['onKeydown']);
    keyManagerProvider = jasmine.createSpyObj('MenuComponent', ['hide']);
    keyManagerProvider.keyManager = keyManager;
    keyManager = jasmine.createSpyObj('FocusKeyManager', ['onKeydown']);
    menu = jasmine.createSpyObj('Menu', ['show', 'hide', 'addItem', 'removeItem']);
    menu.keyManager = keyManager;
    menu.parent = true as any;

    focusMonitor.monitor.and.returnValue(of(null));

    await TestBed.configureTestingModule({
      declarations: [MenuItemComponent],
      providers: [{
        provide: FocusMonitor,
        useValue: focusMonitor
      }, {
        provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
        useValue: keyManagerProvider
      }, {
        provide: CUI_MENU,
        useValue: {}
      }
      ]
    }).overrideTemplate(MenuItemComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemComponent);
    component = fixture.componentInstance;
    (component as any).menu = menu;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should focus element', () => {
    component.focus('keyboard');
    fixture.detectChanges();

    expect(focusMonitor.focusVia).toHaveBeenCalled();
    component.focus();
    fixture.detectChanges();
  });

  it('should call correct event methods on _closeAndHaltDisabledEvents call when component disabled', () => {
    component.disabled = true;
    const event = new MouseEvent('click', { detail: 0 });
    spyOn(event, 'preventDefault');
    spyOn(event, 'stopImmediatePropagation');
    component._closeAndHaltDisabledEvents(event);

    expect(event.preventDefault).toHaveBeenCalled();
    expect(event.stopImmediatePropagation).toHaveBeenCalled();
  });

  it('should set the suffixIcon icon on setSubmenuTrigger call', () => {
    (component as any).isSubmenuTrigger = true;
    (component as any).setSubmenuTrigger();

    expect(component.suffixIcon).toEqual((MenuItemComponent as any).SUBMENU_SUFFIX);
  });

  it('should forward key events', () => {
    const event = { code: 'ArrowRight' } as KeyboardEvent;
    (component as any).elementRef = {
      nativeElement: {
        click: () => {}
      }
    };
    spyOn((component as any).elementRef.nativeElement, 'click');
    (component as any).isSubmenuTrigger = true;
    component.manage(event);

    expect(keyManager.onKeydown).toHaveBeenCalledWith(event);
    expect((component as any).elementRef.nativeElement.click).toHaveBeenCalled();

    (event as any).code = 'ArrowLeft';
    component.manage(event);

    expect((component as any).elementRef.nativeElement.click).toHaveBeenCalled();
    (event as any).code = 'Escape';
    component.manage(event);

    expect(menu.hide).toHaveBeenCalled();
  });
});
