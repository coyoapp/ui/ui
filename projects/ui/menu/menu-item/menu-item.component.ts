import { FocusableOption, FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { CUI_MENU_TRIGGER } from '../menu-trigger/menu-trigger';
import { CUI_MENU, Menu } from '../menu/menu';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[cui-menu-item]',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-menu-item',
    '(click)': '_closeAndHaltDisabledEvents($event)'
  }
})
export class MenuItemComponent implements OnInit, AfterViewInit, OnDestroy, FocusableOption {
  private static readonly SUBMENU_SUFFIX = 'chevron-right';
  private readonly isSubmenuTrigger: boolean;

  /**
   * Wether this item is disabled.
   */
  @Input()
  @HostBinding('class.cui-menu-item-disabled')
  disabled = false;

  /**
   * Wether this item is in active state.
   */
  @Input()
  @HostBinding('class.cui-active')
  active = false;

  /**
   * Wether this item should close the menu on click.
   */
  @Input()
  closeOnClick = true;

  /**
   * The name of an icon to prefix the item text.
   */
  @Input()
  prefixIcon?: coyoIcon;

  /**
   * The name of an icon to suffix the item text.
   */
  @Input()
  suffixIcon?: coyoIcon;

  /**
   * Optional CSS classes to be applied to the textual content of the button.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  textClass?: string | string[] | Set<string> | { [klass: string]: any; };

  /**
   * Optional CSS classes to be applied to the icons of the button.
   */
  @Input() // eslint-disable-next-line @typescript-eslint/no-explicit-any
  iconClass?: string | string[] | Set<string> | { [klass: string]: any; };

  constructor(
    readonly elementRef: ElementRef,
    private readonly focusMonitor: FocusMonitor,
    @Inject(CUI_MENU) readonly menu: Menu,
    @Inject(CUI_MENU_TRIGGER) @Optional() readonly menuTrigger: boolean
  ) {
    this.isSubmenuTrigger = !!menuTrigger;
    this.setSubmenuTrigger();
  }

  ngOnInit(): void {
    this.menu.addItem(this);
  }

  ngAfterViewInit(): void {
    this.focusMonitor.monitor(this.elementRef, true).subscribe();
  }

  ngOnDestroy(): void {
    this.focusMonitor.stopMonitoring(this.elementRef);
    this.menu.removeItem(this);
  }

  setSubmenuTrigger() {
    if (this.isSubmenuTrigger) {
      this.suffixIcon = MenuItemComponent.SUBMENU_SUFFIX;
    }
  }

  focus(origin?: FocusOrigin): void {
    this.focusMonitor.focusVia(this.elementRef.nativeElement, origin || null);
  }

  @HostListener('keydown', ['$event'])
  manage(event: KeyboardEvent): void {
    this.menu.keyManager?.onKeydown(event);
    if (event.code === 'ArrowRight' && this.isSubmenuTrigger) {
      this.elementRef.nativeElement.click();
    } else if (event.code === 'ArrowLeft' && this.menu.parent) {
      this.menu.hide(false, true);
    } else if (event.code === 'Escape') {
      this.menu.hide(true, true);
    }
  }

  _closeAndHaltDisabledEvents(event: MouseEvent): void {
    // a disabled item shouldn't apply any actions
    // submenu triggers will be called programmatically
    if (this.disabled || this.isSubmenuTrigger) {
      event.preventDefault();
      event.stopImmediatePropagation();
    } else if (this.closeOnClick) {
      const refocus = event.detail === 0;
      this.menu.hide(true, refocus);
    }
  }
}
