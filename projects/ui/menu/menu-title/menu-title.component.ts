import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[cui-menu-title]',
  templateUrl: './menu-title.component.html',
  styleUrls: ['./menu-title.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'cui-menu-title'
  }
})
export class MenuTitleComponent {}
