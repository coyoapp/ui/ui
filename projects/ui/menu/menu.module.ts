import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { IconModule } from '@coyoapp/ui/icon';
import { popperVariation, provideTippyConfig, TIPPY_CONFIG, tooltipVariation } from '@ngneat/helipopper';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuTitleComponent } from './menu-title/menu-title.component';
import { MenuTriggerDirective } from './menu-trigger/menu-trigger.directive';
import { MenuComponent } from './menu/menu.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule
  ],
  declarations: [
    MenuComponent,
    MenuItemComponent,
    MenuTitleComponent,
    MenuTriggerDirective
  ],
  exports: [
    MenuComponent,
    MenuItemComponent,
    MenuTitleComponent,
    MenuTriggerDirective
  ]
})
export class MenuModule {

  static forRoot(): ModuleWithProviders<MenuModule> {
    const providers = provideTippyConfig({
      defaultVariation: 'tooltip',
      variations: {
        tooltip: tooltipVariation,
        popover: {
          ...popperVariation,
          placement: 'auto',
          appendTo: 'parent',
          arrow: false,
          animation: 'shift-away-subtle',
          offset: [0, 8],
          maxWidth: 'unset'
        }
      }
    });
    return {
      ngModule: MenuModule,
      providers: [providers, {provide: TIPPY_CONFIG, useValue: {
          variations: {
            tooltip: tooltipVariation,
            popover: {
              ...popperVariation,
              placement: 'auto',
              appendTo: 'parent',
              arrow: false,
              animation: 'shift-away-subtle',
              offset: [0, 8],
              maxWidth: 'unset'
            }
          }
        }}]
    };
  }
}
