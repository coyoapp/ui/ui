import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { MenuComponent } from '../menu/menu.component';
import { CUI_MENU_TRIGGER } from './menu-trigger';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[cui-menu-trigger]',
  providers: [{
    provide: CUI_MENU_TRIGGER,
    useValue: true
  }]
})
export class MenuTriggerDirective implements OnDestroy {

  @Input('cui-menu-trigger')
  menu?: MenuComponent;

  @Input()
  disabled = false;

  constructor(
    private readonly elementRef: ElementRef,
    @Self() @Optional() private readonly menuItemComponent?: MenuItemComponent
  ) {}

  ngOnDestroy(): void {
    this.menu?.hide();
  }

  @HostListener('click', ['$event'])
  click(event: MouseEvent): void {
    if (!this.disabled) {
      const focusFirst = event.detail === 0;
      this.menu?.show(this.elementRef, this.menuItemComponent?.menu, focusFirst);
    }
  }
}
