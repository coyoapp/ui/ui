import { InjectionToken } from '@angular/core';

export const CUI_MENU_TRIGGER = new InjectionToken<boolean>('CUI_MENU_TRIGGER');
