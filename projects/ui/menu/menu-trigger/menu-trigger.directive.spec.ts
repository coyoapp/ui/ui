import { FocusKeyManager } from '@angular/cdk/a11y';
import { ElementRef } from '@angular/core';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { MenuComponent } from '../menu/menu.component';
import { MenuTriggerDirective } from './menu-trigger.directive';

describe('MenuTriggerDirective', () => {
  let directive: MenuTriggerDirective;
  let elementRef: ElementRef;
  let menu: jasmine.SpyObj<MenuComponent>;
  let keyManager: jasmine.SpyObj<FocusKeyManager<MenuItemComponent>>;

  beforeEach(() => {
    keyManager = jasmine.createSpyObj('FocusKeyManager', ['onKeydown']);
    menu = jasmine.createSpyObj('MenuComponent', ['show', 'hide']);
    menu.keyManager = keyManager;
    elementRef = {} as any;
    directive = new MenuTriggerDirective(elementRef);
    directive.menu = menu;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should show menu on click', () => {
    // when
    const event = new MouseEvent('click', { detail: 0 });
    directive.click(event);

    // then
    expect(menu.show).toHaveBeenCalledWith(elementRef, undefined, true);
  });

  it('hide on destroy', () => {
    // when
    directive.ngOnDestroy();

    // then
    expect(menu.hide).toHaveBeenCalled();
  });

  it('should gracefully fail without menu', () => {
    // given
    const event = new MouseEvent('click', { detail: 0 });
    directive.menu = undefined;

    // when
    directive.click(event);
    directive.ngOnDestroy();

    // then
    expect(true).toBeTrue();
  });
});
