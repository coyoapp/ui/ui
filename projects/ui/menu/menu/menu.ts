import { InjectionToken } from '@angular/core';
import { FocusKeyManagerProvider } from '@coyoapp/ui/cdk';
import { MenuItemComponent } from '../menu-item/menu-item.component';

export const CUI_MENU = new InjectionToken<Menu>('CUI_MENU');

export interface Menu extends FocusKeyManagerProvider<MenuItemComponent> {
  parent?: Menu;
  addItem: (item: MenuItemComponent) => void;
  removeItem: (item: MenuItemComponent) => void;
  hide: (propagate: boolean, refocus: boolean) => void;
}
