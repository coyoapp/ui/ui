import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TippyService } from '@ngneat/helipopper';
import { ElementRef } from '@angular/core';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { MenuComponent } from './menu.component';
import createSpyObj = jasmine.createSpyObj;

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  let tippyService: jasmine.SpyObj<TippyService>;

  beforeEach(async () => {
    tippyService = jasmine.createSpyObj('TippyService', ['create', 'show']);

    await TestBed.configureTestingModule({
      declarations: [MenuComponent],
      providers: [{
        provide: TippyService,
        useValue: tippyService
      },
      {
        provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
        useValue: MenuComponent
      }]
    }).overrideTemplate(MenuComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call tearDown on show() call with different host', () => {
    spyOn(component as any, 'tearDown');
    const elRef: ElementRef<unknown> = { nativeElement: {} };
    (component as any).host = null;
    component.show(elRef);

    expect((component as any).tearDown).toHaveBeenCalled();
  });

  it('should call correct methods on show', () => {
    const elRef: ElementRef<unknown> = { nativeElement: {} };
    spyOn((component as any), 'getSortedItems');
    (component as any).itemsDirty = true;
    (component as any).host = null;
    component.show(elRef, {} as any, false);

    expect((component as any).keyManager).toBeTruthy();
    expect((component as any).itemsDirty).toBeFalse();
    expect((component as any).getSortedItems).toHaveBeenCalled();

    spyOn((component as any).keyManager, 'setActiveItem');
    component.show(elRef, {} as any, true);

    expect((component as any).keyManager.setActiveItem).toHaveBeenCalledWith(0);
  });

  it('should create tippy service when no tippy service exists and content exists', () => {
    (component as any).content = { nativeElement: {} };
    spyOn(component as any, 'tearDown');
    const elRef: ElementRef<unknown> = { nativeElement: {} };
    (component as any).host = null;
    component.show(elRef);

    expect((component as any).host).toEqual(elRef);
    expect(tippyService.create).toHaveBeenCalled();
  });

  it('should get correct CUI_FOCUS_KEY_MANAGER_PROVIDER provider of type MenuComponent', () => {
    expect(TestBed.inject(CUI_FOCUS_KEY_MANAGER_PROVIDER)).toBeDefined();
    const menuDecorators = (<any>MenuComponent).__annotations__;
    const useExistingClass = menuDecorators[0].providers[0].useExisting();
    const useExistingClass2 = menuDecorators[0].providers[1].useExisting();

    expect(menuDecorators[0].providers[0].useExisting).toBeTruthy();
    expect(menuDecorators[0].providers[1].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(MenuComponent);
    expect(useExistingClass2).toEqual(MenuComponent);
  });

  it('should call tippy.hide() on hide() call', () => {
    (component as any)._parent = createSpyObj('MenuComponent', ['hide']);
    (component as any).tippy = jasmine.createSpyObj('TippyInstance', ['hide', 'destroy']);
    component.hide(true, true);

    expect((component as any).tippy.hide).toHaveBeenCalled();
    expect((component as any).parent.hide).toHaveBeenCalled();
  });

  it('should push new items on addItem call', () => {
    spyOn((component as any).items, 'push');
    component.addItem({} as any);

    expect((component as any).items.push).toHaveBeenCalled();
    expect((component as any).itemsDirty).toBeTrue();
  });

  it('should remove items on removeItem call', () => {
    spyOn((component as any).items, 'splice');
    component.removeItem({} as any);

    expect((component as any).items.splice).toHaveBeenCalled();
    expect((component as any).itemsDirty).toBeTrue();
  });

  it('should return sorted items', () => {
    const tippy = {
      popper: createSpyObj('TippyInstance', ['querySelectorAll'])
    };
    (component as any).items = [{ elementRef: { nativeElement: true } }, { elementRef: { nativeElement: true } }];
    tippy.popper.querySelectorAll.and.returnValue([true, true]);
    const results = (component as any).getSortedItems(tippy);

    expect(results).toEqual([{ elementRef: { nativeElement: true } }, { elementRef: { nativeElement: true } }]);
  });

});
