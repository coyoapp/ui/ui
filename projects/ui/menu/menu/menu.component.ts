import { FocusKeyManager } from '@angular/cdk/a11y';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy, Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { TippyInstance, TippyService } from '@ngneat/helipopper';
import { MenuItemComponent } from '../menu-item/menu-item.component';
import { CUI_MENU, Menu } from './menu';

export type MenuPlacement =
  'auto' | 'auto-start' | 'auto-end' |
  'top' | 'top-start' | 'top-end' |
  'left' | 'left-start' | 'left-end' |
  'right' | 'right-start' | 'right-end' |
  'bottom' | 'bottom-start' | 'bottom-end';

@Component({
  selector: 'cui-menu',
  exportAs: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
    useExisting: forwardRef(() => MenuComponent)
  }, {
    provide: CUI_MENU,
    useExisting: forwardRef(() => MenuComponent)
  }],
  host: {
    class: 'cui-menu'
  }
})
export class MenuComponent implements OnDestroy, Menu {
  private static readonly SUBMENU_PLACEMENT = 'auto-end';
  private items: MenuItemComponent[] = [];
  private itemsDirty = false;
  private tippy?: TippyInstance;
  private host?: ElementRef;
  private _parent?: Menu;

  get parent(): Menu | undefined {
    return this._parent;
  }

  keyManager: FocusKeyManager<MenuItemComponent> = new FocusKeyManager([]).withWrap();

  @ViewChild(TemplateRef)
  content?: TemplateRef<HTMLElement>;

  /**
   * The placement of the menu.
   */
  @Input()
  placement: MenuPlacement = 'bottom-start';

  /**
   * Event emitter for menu hiding.
   */
  @Output()
  readonly onHidden = new EventEmitter<void>();

  /**
   * Event emitter for menu shown.
   */
  @Output()
  readonly onShown = new EventEmitter<void>();

  /**
   * The append to
   */
  @Input()
  get appendTo(): 'parent' | Element | ((ref: Element) => Element) {
    return this._appendTo;
  }

  set appendTo(value: 'parent' | 'body' | Element | ((ref: Element) => Element)) {
    this._appendTo = value === 'body' ? document.body : value;
  }

  private _appendTo: 'parent' | Element | ((ref: Element) => Element) = 'parent';

  constructor(
    private readonly tippyService: TippyService
  ) {}

  ngOnDestroy(): void {
    this.tearDown();
  }

  addItem(item: MenuItemComponent): void {
    this.items.push(item);
    this.itemsDirty = true;
  }

  removeItem(item: MenuItemComponent): void {
    this.items.splice(this.items.indexOf(item), 1);
    this.itemsDirty = true;
  }

  show(host: ElementRef, parent?: Menu, focusFirst = false): void {
    if (this.host !== host) {
      this.tearDown();
    }

    if (!this.tippy && this.content) {
      this.host = host;
      this._parent = parent;
      this.tippy = this.tippyService.create(this.host.nativeElement, this.content, {
        variation: 'popover',
        appendTo: this.appendTo ?? 'parent',
        theme: 'menu',
        placement: this.parent ? MenuComponent.SUBMENU_PLACEMENT : this.placement,
        onHidden: () => this.onHidden.emit(),
        onShow: () => this.onShown.emit()
      });
    }

    this.tippy?.show();
    if (this.itemsDirty) {
      this.keyManager = new FocusKeyManager(this.getSortedItems(this.tippy)).withWrap();
      this.itemsDirty = false;
    }
    if (focusFirst) {
      this.keyManager.setActiveItem(0);
    }
  }

  hide(propagate = false, refocus = true): void {
    this.tippy?.hide();
    if (refocus) {
      this.host?.nativeElement.focus();
    }
    if (propagate) {
      this.parent?.hide(propagate, refocus);
    }
  }

  private getSortedItems(tippy?: TippyInstance): MenuItemComponent[] {
    const result: (MenuItemComponent | undefined)[] = [];
    tippy?.popper.querySelectorAll('[cui-menu-item]').forEach(elem =>
      result.push(this.items.find(item => item.elementRef.nativeElement === elem)));
    return result.filter(item => item) as MenuItemComponent[];
  }

  private tearDown(): void {
    this.tippy?.destroy();
    this.tippy = undefined;
    this.host = undefined;
  }
}
