import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseComponent } from './collapse.component';

describe('CollapseComponent', () => {
  let component: CollapseComponent;
  let fixture: ComponentFixture<CollapseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NoopAnimationsModule],
      declarations: [CollapseComponent]
    }).overrideTemplate(CollapseComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change collapsed value on toggle', () => {
    spyOn((component as any).cuiCollapseChange, 'next');

    expect(component.collapsed).toBeFalse();
    component.toggle();

    expect(component.collapsed).toBeTrue();
    expect(component.cuiCollapseChange.next).toHaveBeenCalled();
  });

  it('should call updateClassesAndEmitEvents on changes', () => {
    spyOn(component as any, 'updateClassesAndEmitEvents');
    component.ngOnChanges({ collapsed: { firstChange: false } } as any);

    expect((component as any).updateClassesAndEmitEvents).toHaveBeenCalled();
  });

  it('should call updateClassesAndEmitEvents on changes', () => {
    spyOn(component.hidden, 'emit');
    spyOn(component.shown, 'emit');
    (component as any).elementRef = {
      nativeElement: {
        classList: jasmine.createSpyObj('ClassList', ['add', 'remove'])
      }
    };

    (component as any).updateClassesAndEmitEvents();

    expect(component.shown.emit).toHaveBeenCalled();
    expect((component as any).elementRef.nativeElement.classList.add).toHaveBeenCalled();

    component.collapsed = true;
    (component as any).updateClassesAndEmitEvents();

    expect((component as any).elementRef.nativeElement.classList.remove).toHaveBeenCalled();
    expect(component.hidden.emit).toHaveBeenCalled();
  });
});
