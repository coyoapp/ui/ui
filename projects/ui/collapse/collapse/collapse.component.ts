import {
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';
import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';

const DEFAULT_DURATION = 150;

@Component({
  selector: '[cuiCollapse]',
  template: '<ng-content></ng-content>',
  styleUrls: ['./collapse.component.scss'],
  exportAs: 'cuiCollapse',
  animations: [
    trigger('collapseAnimation', [
      state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
      state('true', style({ height: '0', visibility: 'collapse' })),
      transition('false => true', animate(DEFAULT_DURATION + 'ms ease-in')),
      transition('true => false', animate(DEFAULT_DURATION + 'ms ease-out'))
    ])
  ],
  host: {
    class: 'cui-collapse'
  },
  encapsulation: ViewEncapsulation.None
})
export class CollapseComponent implements OnChanges, OnInit {

  @HostBinding('@collapseAnimation')
  @Input('cuiCollapse')
  collapsed = false;

  @Output()
  readonly cuiCollapseChange = new EventEmitter<boolean>();

  @Output()
  readonly shown = new EventEmitter<void>();

  @Output()
  readonly hidden = new EventEmitter<void>();

  constructor(
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    this.updateClassesAndEmitEvents();
  }

  ngOnChanges({ collapsed }: SimpleChanges) {
    if (!collapsed.firstChange) {
      this.updateClassesAndEmitEvents();
    }
  }

  toggle(open: boolean = this.collapsed) {
    this.collapsed = !open;
    this.cuiCollapseChange.next(this.collapsed);
  }

  private updateClassesAndEmitEvents() {
    const { classList } = this.elementRef.nativeElement;
    if (this.collapsed) {
      classList.remove('show');
      this.hidden.emit();
    } else {
      classList.add('show');
      this.shown.emit();
    }
  }
}
