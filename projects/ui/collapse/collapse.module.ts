import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CollapseComponent } from './collapse/collapse.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CollapseComponent
  ],
  exports: [
    CollapseComponent
  ]
})
export class CollapseModule {}
