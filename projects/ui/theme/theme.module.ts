import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CustomPaletteDirective } from './custom-palette/custom-palette.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [CustomPaletteDirective],
  exports: [CustomPaletteDirective]
})
export class ThemeModule {}
