import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core';
import { HEX } from '../color/color';
import { Palette } from '../palette/palette';
import { PaletteService } from '../palette/palette.service';

/**
 * Directive to set the CSS custom properties for the custom color palette.
 */
@Directive({
  selector: '[cuiCustomPalette]'
})
export class CustomPaletteDirective implements OnChanges {
  private static readonly HEX = /^#[\dA-Fa-f]{6}$/;

  /**
   * The custom base color (in HEX notation) to be used as the base color for
   * the color palette, e.g. `"#dd350d"`.
   */
  @Input()
  cuiCustomPalette: HEX = '';

  /**
   * A partial set of override colors to further adjust the outcome of the
   * custom color palette.
   */
  @Input()
  cuiCustomPaletteOverrides?: Partial<Palette>;

  /**
   * The light fill color (in HEX notation) to be used (defaults to white).
   */
  @Input()
  cuiCustomLightFill: HEX = PaletteService.LIGHT;

  /**
   * The dark fill color (in HEX notation) to be used (defaults to black).
   */
  @Input()
  cuiCustomDarkFill: HEX = PaletteService.DARK;

  constructor(
    private readonly renderer: Renderer2,
    private readonly elementRef: ElementRef,
    private readonly paletteService: PaletteService
  ) {}

  ngOnChanges(): void {
    if (CustomPaletteDirective.HEX.test(this.cuiCustomPalette)) {
      const palette = this.paletteService.build(this.cuiCustomPalette, this.cuiCustomLightFill, this.cuiCustomDarkFill);
      Object.entries({ ...palette, ...this.cuiCustomPaletteOverrides }).forEach(([key, value]) => {
        this.renderer.setStyle(this.elementRef.nativeElement, `--cui-custom-${key}`, value, 2);
      });
    } else {
      ['bg', 'fill', 'text'].forEach(key => {
        ['', '-hover', '-active'].forEach(mod => {
          this.renderer.removeStyle(this.elementRef.nativeElement, `--cui-custom-${key}${mod}`, 2);
        });
      });
    }
  }
}
