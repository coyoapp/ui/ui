import { ElementRef, Renderer2 } from '@angular/core';
import { PaletteService } from '../palette/palette.service';
import { CustomPaletteDirective } from './custom-palette.directive';

describe('CustomPaletteDirective', () => {
  let renderer: jasmine.SpyObj<Renderer2>;
  let paletteService: jasmine.SpyObj<PaletteService>;
  let nativeElement: HTMLElement;
  let elementRef: ElementRef;
  let directive: CustomPaletteDirective;

  beforeEach(() => {
    renderer = jasmine.createSpyObj('Renderer2', ['setStyle', 'removeStyle']);
    paletteService = jasmine.createSpyObj('PaletteService', ['build']);
    paletteService.build.and.returnValue({ color: '#222222' } as any);
    nativeElement = {} as HTMLElement;
    elementRef = { nativeElement } as ElementRef;
    directive = new CustomPaletteDirective(renderer, elementRef, paletteService);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should correctly update palette onChanges', () => {
    directive.cuiCustomPalette = '#000000';
    directive.ngOnChanges();

    expect(paletteService.build).toHaveBeenCalledWith('#000000', '#ffffff', '#000000');
    expect(renderer.setStyle).toHaveBeenCalled();

    directive.cuiCustomPalette = 'incorrectColorCode';
    directive.ngOnChanges();

    expect(renderer.removeStyle).toHaveBeenCalled();

  });
});
