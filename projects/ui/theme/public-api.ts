export * from './theme.module';
export * from './color/color';
export * from './color/color.service';
export * from './custom-palette/custom-palette.directive';
export * from './palette/palette';
export * from './palette/palette.service';
