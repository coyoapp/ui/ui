import { TestBed } from '@angular/core/testing';
import { PaletteService } from './palette.service';

describe('PaletteService', () => {
  let service: PaletteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaletteService);
  });

  it('should build a dark palette', () => {
    const color = '#0073e6';
    const palette = service.build(color);

    expect(palette).toEqual({
      'bg': '#0073e6',
      'bg-hover': '#0059b3',
      'bg-active': '#004c99',
      'fill': '#ffffff',
      'fill-hover': '#ffffff',
      'fill-active': '#ffffff',
      'text': '#0073e6',
      'text-hover': '#004c99',
      'text-active': '#003366'
    });
  });

  it('should build a light palette', () => {
    const color = '#6bf2c9';
    const palette = service.build(color);

    expect(palette).toEqual({
      'bg': '#6bf2c9',
      'bg-hover': '#aff8e2',
      'bg-active': '#defcf3',
      'fill': '#000000',
      'fill-hover': '#000000',
      'fill-active': '#000000',
      'text': '#0b8460',
      'text-hover': '#053d2c',
      'text-active': '#010e0a'
    });
  });
});
