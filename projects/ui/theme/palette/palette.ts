import type { HEX } from '../color/color';

export interface Palette {
  'bg': HEX,
  'bg-hover': HEX,
  'bg-active': HEX,
  'fill': HEX,
  'fill-hover': HEX,
  'fill-active': HEX,
  'text': HEX,
  'text-hover': HEX,
  'text-active': HEX
}
