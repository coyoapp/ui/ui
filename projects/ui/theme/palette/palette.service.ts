import { Injectable } from '@angular/core';
import { HEX, HSL } from '../color/color';
import { ColorService } from '../color/color.service';
import { Palette } from './palette';

/**
 * Service to generate custom color palettes.
 */
@Injectable({
  providedIn: 'root'
})
export class PaletteService {
  /* The reference color white. */
  private static readonly WHITE = '#ffffff';
  /* The default dark fill color for light buttons. */
  static readonly DARK = '#000000';
  /* The default light fill color for dark buttons. */
  static readonly LIGHT = '#ffffff';
  /* The default minimum WCAG contrast ratio. */
  static readonly CONTRAST = 4.5;

  constructor(
    private colorService: ColorService
  ) {}

  /**
   * Builds a new color palette based on the given color.
   *
   * @param color the base color for this palette
   * @param lightFill the light fill color for dark buttons
   * @param darkFill the dark fill color for light buttons
   * @returns a color palette
   */
  build(color: HEX, lightFill = PaletteService.LIGHT, darkFill = PaletteService.DARK): Palette {
    const isDark = this.colorService.contrast(color, lightFill) >= PaletteService.CONTRAST;
    const fillColor = isDark ? lightFill : darkFill;
    const textColor = this.assertContrast(color, PaletteService.CONTRAST);
    return {
      'bg': color,
      'bg-hover': this.adjustHex(color, { l: isDark ? -10 : 15 }),
      'bg-active': this.adjustHex(color, { l: isDark ? -15 : 25 }),
      'fill': fillColor,
      'fill-hover': fillColor,
      'fill-active': fillColor,
      'text': textColor,
      'text-hover': this.adjustHex(textColor, { l: -15 }),
      'text-active': this.adjustHex(textColor, { l: -25 })
    };
  }

  private adjustHex(color: HEX, delta: Partial<HSL>) {
    const rgb = this.colorService.hexToRgb(color);
    const hsl = this.colorService.rgbToHsl(rgb);
    const hsl2 = this.colorService.adjust(hsl, delta);
    const rgb2 = this.colorService.hslToRgb(hsl2);
    return this.colorService.rgbToHex(rgb2);
  }

  private assertContrast(color: HEX, contrast: number): HEX {
    let result = color;
    while (this.colorService.contrast(result, PaletteService.WHITE) < contrast) {
      result = this.adjustHex(result, { l: -5 });
    }
    return result;
  }
}
