import { Injectable } from '@angular/core';
import { clamp } from '@coyoapp/ui/cdk';
import type { HEX } from './color';
import { HSL, RGB } from './color';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  private static readonly RC = 0.2126;
  private static readonly GC = 0.7152;
  private static readonly BC = 0.0722;
  private static readonly LC = 1 / 12.92;

  /**
   * Converts a HEX color value to RGB.
   * Returns r, g, and b in the set [0, 255].
   *
   * @param hex the color in HEX
   * @returns the RGB representation or `null` if invalid
   * @throws error if the color is invalid
   */
  hexToRgb(hex: HEX): RGB {
    const result = /^#?([\da-f]{2})([\da-f]{2})([\da-f]{2})$/i.exec(hex);
    if (!result) {
      throw new Error(`Invalid color ${hex}`);
    }

    return {
      r: Number.parseInt(result[1], 16),
      g: Number.parseInt(result[2], 16),
      b: Number.parseInt(result[3], 16)
    };
  }

  /**
   * Converts an RGB color value to HEX.
   * Assumes r, g, and b are contained in the set [0, 255].
   *
   * @param rgb the color in RGB
   * @returns the HEX representation
   * @throws error if the color is invalid
   */
  rgbToHex(rgb: RGB): HEX {
    if (!this.isValidRGB(rgb)) {
      throw new Error(`Invalid color rgb(${rgb.r},${rgb.g},${rgb.b})`);
    }

    return '#' + ((1 << 24) + (rgb.r << 16) + (rgb.g << 8) + rgb.b).toString(16).slice(1);
  }

  /**
   * Converts an RGB color value to HSL. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes r, g, and b are contained in the set [0, 255] and
   * returns h, s, and l in the sets h = [0, 360] and {s, l} = [0, 100].
   *
   * @param rgb the color in RGB
   * @returns the HSL representation or `null` if invalid
   * @throws error if the color is invalid
   */
  rgbToHsl(rgb: RGB): HSL {
    if (!this.isValidRGB(rgb)) {
      throw new Error(`Invalid color rgb(${rgb.r},${rgb.g},${rgb.b})`);
    }

    const r = rgb.r / 255;
    const g = rgb.g / 255;
    const b = rgb.b / 255;
    const min = Math.min(r, g, b);
    const max = Math.max(r, g, b);
    const delta = max - min;
    const l = (min + max) / 2;

    let h = 0;
    switch (max) {
      case min: h = 0; break;
      case r: h = (g - b) / delta; break;
      case g: h = (b - r) / delta + 2; break;
      case b: h = (r - g) / delta + 4; break;
    }

    h = Math.min(h * 60, 360);
    if (h < 0) {
      h += 360;
    }

    let s = 0;
    if (max === min) {
      s = 0;
    } else if (l <= 0.5) {
      s = delta / (max + min);
    } else {
      s = delta / (2 - max - min);
    }

    return {
      h: this.round(h),
      s: this.round(s * 100),
      l: this.round(l * 100)
    };
  }

  /**
   * Converts an HSL color value to RGB. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes h, s, and l are contained in the sets = [0, 360]
   * and {s, l} = [0, 100] and returns r, g, and b in the set [0, 255].
   *
   * @param hsl the color in HSL
   * @returns the RGB representation or `null` if invalid
   * @throws error if the color is invalid
   */
  hslToRgb(hsl: HSL): RGB {
    if (!this.isValidHSL(hsl)) {
      throw new Error(`Invalid color hsl(${hsl.h},${hsl.s},${hsl.l})`);
    }

    const h = hsl.h / 360;
    const s = hsl.s / 100;
    const l = hsl.l / 100;

    let r = l;
    let g = l;
    let b = l;
    if (s !== 0) {
      const hue2rgb = (p: number, q: number, t: number) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };

      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;

      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }

    return {
      r: this.round(r * 255),
      g: this.round(g * 255),
      b: this.round(b * 255)
    };
  }

  /**
   * Increases or decreases one or more properties of `color` by fixed amounts.
   *
   * @param color the color to be changed
   * @param delta one or more HSL delta values
   * @returns the updated color
   * @throws error if the color is invalid
   */
  adjust(color: HSL, delta: Partial<HSL>): HSL {
    return {
      h: clamp(color.h + (delta.h || 0), 0, 360),
      s: clamp(color.s + (delta.s || 0), 0, 100),
      l: clamp(color.l + (delta.l || 0), 0, 100)
    };
  }

  /**
   * Calculates the contrast value between too  Formula
   * adapted from https://www.accessibility-developer-guide.com/knowledge/colours-and-contrast/how-to-calculate.
   * Assumes h, s, and l are contained in the set [0, 1] and
   * returns a value ranging from 1:1 (no contrast at all)
   * to 21:1 (the highest possible contrast).
   *
   * @param color1 the color in HSL
   * @param color2 the other color in HSL
   * @returns the contrast value
   */
  contrast(color1: HEX, color2: HEX): number {
    const rl1 = this.relativeLuminance(this.hexToRgb(color1));
    const rl2 = this.relativeLuminance(this.hexToRgb(color2));
    const min = Math.min(rl1, rl2);
    const max = Math.max(rl1, rl2);
    return this.round((max + 0.05) / (min + 0.05), 2);
  }

  private adjustGamma(n: number) {
    return Math.pow((n + 0.055) / 1.055, 2.4);
  }

  private relativeLuminance(color: RGB) {
    const rsrgb = color.r / 255;
    const gsrgb = color.g / 255;
    const bsrgb = color.b / 255;

    const r = rsrgb <= 0.03928 ? rsrgb * ColorService.LC : this.adjustGamma(rsrgb);
    const g = gsrgb <= 0.03928 ? gsrgb * ColorService.LC : this.adjustGamma(gsrgb);
    const b = bsrgb <= 0.03928 ? bsrgb * ColorService.LC : this.adjustGamma(bsrgb);

    return r * ColorService.RC + g * ColorService.GC + b * ColorService.BC;
  }

  private isValidRGB(rgb: RGB): boolean {
    return this.isBetween(rgb.r, 0, 255)
      && this.isBetween(rgb.g, 0, 255)
      && this.isBetween(rgb.b, 0, 255);
  }

  private isValidHSL(hsl: HSL): boolean {
    return this.isBetween(hsl.h, 0, 360)
      && this.isBetween(hsl.s, 0, 100)
      && this.isBetween(hsl.l, 0, 100);
  }

  private isBetween(value: number, min: number, max: number): boolean {
    return value >= min && value <= max;
  }

  private round(value: number, decimals = 0): number {
    return Math.round((value + Number.EPSILON) * Math.pow(10, decimals)) / Math.pow(10, decimals);
  }
}
