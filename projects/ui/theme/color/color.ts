export type Color = HEX | RGB | HSL;

export type HEX = string;

export interface RGB { r: number; g: number; b: number; }

export interface HSL { h: number; s: number; l: number; }
