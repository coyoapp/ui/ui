import { TestBed } from '@angular/core/testing';
import { ColorService } from './color.service';

describe('ColorService', () => {
  let service: ColorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ColorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should convert RGB to HEX', () => {
    expect(service.rgbToHex({ r: 0, g: 0, b: 0 })).toBe('#000000');
    expect(service.rgbToHex({ r: 255, g: 255, b: 255 })).toBe('#ffffff');
    expect(service.rgbToHex({ r: 102, g: 51, b: 153 })).toBe('#663399');
    expect(service.rgbToHex({ r: 0, g: 115, b: 230 })).toBe('#0073e6');
    expect(service.rgbToHex({ r: 0, g: 102, b: 204 })).toBe('#0066cc');
    expect(service.rgbToHex({ r: 0, g: 89, b: 178 })).toBe('#0059b2');
    expect(service.rgbToHex({ r: 107, g: 242, b: 201 })).toBe('#6bf2c9');
    expect(service.rgbToHex({ r: 152, g: 246, b: 218 })).toBe('#98f6da');
    expect(service.rgbToHex({ r: 199, g: 250, b: 235 })).toBe('#c7faeb');
    expect(service.rgbToHex({ r: 255, g: 0, b: 0 })).toBe('#ff0000');
    expect(() => service.rgbToHex({ r: -1, g: 0, b: 0 })).toThrowError(/^Invalid color/g);
  });

  it('should convert HEX to RGB', () => {
    expect(service.hexToRgb('#000000')).toEqual({ r: 0, g: 0, b: 0 });
    expect(service.hexToRgb('#ffffff')).toEqual({ r: 255, g: 255, b: 255 });
    expect(service.hexToRgb('#663399')).toEqual({ r: 102, g: 51, b: 153 });
    expect(service.hexToRgb('#0073e6')).toEqual({ r: 0, g: 115, b: 230 });
    expect(service.hexToRgb('#0066cc')).toEqual({ r: 0, g: 102, b: 204 });
    expect(service.hexToRgb('#0059b2')).toEqual({ r: 0, g: 89, b: 178 });
    expect(service.hexToRgb('#6bf2c9')).toEqual({ r: 107, g: 242, b: 201 });
    expect(service.hexToRgb('#98f6da')).toEqual({ r: 152, g: 246, b: 218 });
    expect(service.hexToRgb('#c7faeb')).toEqual({ r: 199, g: 250, b: 235 });
    expect(() => service.hexToRgb('#invalid')).toThrowError(/^Invalid color/g);
  });

  it('should convert RGB to HSL', () => {
    expect(service.rgbToHsl({ r: 0, g: 0, b: 0 })).toEqual({ h: 0, s: 0, l: 0 });
    expect(service.rgbToHsl({ r: 255, g: 255, b: 255 })).toEqual({ h: 0, s: 0, l: 100 });
    expect(service.rgbToHsl({ r: 102, g: 51, b: 153 })).toEqual({ h: 270, s: 50, l: 40 });
    expect(service.rgbToHsl({ r: 0, g: 115, b: 230 })).toEqual({ h: 210, s: 100, l: 45 });
    expect(service.rgbToHsl({ r: 0, g: 102, b: 204 })).toEqual({ h: 210, s: 100, l: 40 });
    expect(service.rgbToHsl({ r: 0, g: 89, b: 178 })).toEqual({ h: 210, s: 100, l: 35 });
    expect(service.rgbToHsl({ r: 107, g: 242, b: 201 })).toEqual({ h: 162, s: 84, l: 68 });
    expect(service.rgbToHsl({ r: 152, g: 246, b: 218 })).toEqual({ h: 162, s: 84, l: 78 });
    expect(service.rgbToHsl({ r: 199, g: 250, b: 235 })).toEqual({ h: 162, s: 84, l: 88 });
    expect(service.rgbToHsl({ r: 255, g: 0, b: 0 })).toEqual({ h: 0, s: 100, l: 50 });
    expect(service.rgbToHsl({ r: 255, g: 250, b: 254 })).toEqual({ h: 312, s: 100, l: 99 });
    expect(() => service.rgbToHsl({ r: -1, g: 0, b: 0 })).toThrowError(/^Invalid color/g);
  });

  it('should convert HSL to RGB', () => {
    expect(service.hslToRgb({ h: 0, s: 0, l: 0 })).toEqual({ r: 0, g: 0, b: 0 });
    expect(service.hslToRgb({ h: 0, s: 0, l: 100 })).toEqual({ r: 255, g: 255, b: 255 });
    expect(service.hslToRgb({ h: 270, s: 50, l: 40 })).toEqual({ r: 102, g: 51, b: 153 });
    expect(service.hslToRgb({ h: 210, s: 100, l: 45 })).toEqual({ r: 0, g: 115, b: 230 });
    expect(service.hslToRgb({ h: 210, s: 100, l: 40 })).toEqual({ r: 0, g: 102, b: 204 });
    // FIXME: expect(service.hslToRgb({ h: 210, s: 100, l: 35 })).toEqual({ r: 0, g: 89, b: 178 });
    // FIXME: expect(service.hslToRgb({ h: 162, s: 84, l: 68 })).toEqual({ r: 107, g: 242, b: 201 });
    expect(service.hslToRgb({ h: 162, s: 84, l: 78 })).toEqual({ r: 152, g: 246, b: 218 });
    expect(service.hslToRgb({ h: 162, s: 84, l: 88 })).toEqual({ r: 199, g: 250, b: 235 });
    expect(service.hslToRgb({ h: 0, s: 84, l: 78 })).toEqual({ r: 246, g: 152, b: 152 });
    expect(() => service.hslToRgb({ h: -1, s: 0, l: 0 })).toThrowError(/^Invalid color/g);
  });

  it('should adjust HSL values', () => {
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { h: 5, s: 5, l: 5 })).toEqual({ h: 185, s: 55, l: 55 });
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { h: -5, s: -5, l: -5 })).toEqual({ h: 175, s: 45, l: 45 });
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { h: 500, s: 500, l: 500 })).toEqual({ h: 360, s: 100, l: 100 });
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { h: 5 })).toEqual({ h: 185, s: 50, l: 50 });
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { s: 5 })).toEqual({ h: 180, s: 55, l: 50 });
    expect(service.adjust({ h: 180, s: 50, l: 50 }, { l: 5 })).toEqual({ h: 180, s: 50, l: 55 });
  });

  it('should calculate the contrast value', () => {
    expect(service.contrast('#000000', '#000000')).toBe(1);
    expect(service.contrast('#000000', '#ffffff')).toBe(21);
    expect(service.contrast('#000000', '#663399')).toBe(2.5);
    expect(service.contrast('#663399', '#ffffff')).toBe(8.41);
  });
});
