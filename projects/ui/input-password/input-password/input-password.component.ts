import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';

@Component({
  selector: 'cui-input-password',
  templateUrl: './input-password.component.html',
  styleUrls: ['./input-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputPasswordComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-password'
  }
})
export class InputPasswordComponent extends FormFieldControlComponent<string> {
  showPassword = false;

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  /**
   * The minimum number of characters for this input.
   */
  @Input()
  minlength: number | null = null;

  /**
   * The maximum number of characters for this input.
   */
  @Input()
  maxlength: number | null = null;

  /**
   * A regular expression that this input's value is checked against.
   */
  @Input()
  pattern: string | RegExp = '';

  constructor(injector: Injector) {
    super(injector);
  }

  toggle(): void {
    this.showPassword = !this.showPassword;
  }
}
