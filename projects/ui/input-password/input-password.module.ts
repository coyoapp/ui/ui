import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from '@coyoapp/ui/button';
import { InputPasswordComponent } from './input-password/input-password.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, ButtonModule],
  declarations: [InputPasswordComponent],
  exports: [InputPasswordComponent]
})
export class InputPasswordModule {}
