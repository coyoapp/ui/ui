import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SidebarComponent } from './sidebar.component';

describe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarComponent]
    }).overrideTemplate(SidebarComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show sidebar', () => {
    spyOn((component as any).isVisible, 'next');
    component.show();

    expect((component as any).isVisible.next).toHaveBeenCalledWith(true);
  });

  it('should hide sidebar', () => {
    spyOn((component as any).isVisible, 'next');
    component.hide();

    expect((component as any).isVisible.next).toHaveBeenCalledWith(false);
  });

  it('should close and open sidebar on keypress', () => {
    spyOn(component, 'hide');
    component.manage({ code: 'Escape' } as KeyboardEvent);

    expect(component.hide).toHaveBeenCalled();
  });
});
