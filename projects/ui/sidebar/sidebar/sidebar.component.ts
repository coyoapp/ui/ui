import { Component, HostListener, Input, OnDestroy, TemplateRef, ViewEncapsulation } from '@angular/core';
import { BehaviorSubject, Observable, timer } from 'rxjs';
import { delayWhen, throttleTime } from 'rxjs/operators';

/**
 * CUI Sidebar component
 */
@Component({
  selector: 'cui-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  exportAs: 'cuiSidebar'
})
export class SidebarComponent implements OnDestroy {
  private static readonly ANIMATION_SPEED = 300;
  private readonly isVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  readonly isVisible$: Observable<boolean> = this.isVisible.asObservable()
    .pipe(throttleTime(SidebarComponent.ANIMATION_SPEED));
  readonly isVisibleDelay$: Observable<boolean> = this.isVisible$
    .pipe(delayWhen(isVisible => timer(isVisible ? 0 : SidebarComponent.ANIMATION_SPEED)));

  autoFocus = false;

  /**
   * A template reference to be used instead of the component's content.
   * Used to implement lazy initialization for the sidebar content. Takes
   * precedence over the component's content.
   */
  @Input()
  sidebarTemplateRef?: TemplateRef<any>;

  /** The position of the sidebar. */
  @Input()
  direction: 'left' | 'right' = 'left';

  /** The width of the sidebar in `vw`. */
  @Input()
  barWidth = 25;

  @HostListener('keydown', ['$event'])
  manage(event: KeyboardEvent): void {
    if (event.code === 'Escape') {
      this.hide();
    }
  }

  ngOnDestroy(): void {
    this.isVisible.complete();
  }

  /**
   * Shows the sidebar.
   */
  show(autoFocus = false): void {
    this.autoFocus = autoFocus;
    this.isVisible.next(true);
  }

  /**
   * Hides the sidebar.
   */
  hide(): void {
    this.isVisible.next(false);
  }
}
