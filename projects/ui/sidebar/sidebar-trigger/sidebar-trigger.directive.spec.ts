import { FocusMonitor } from '@angular/cdk/a11y';
import { ElementRef } from '@angular/core';
import { EMPTY } from 'rxjs';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { SidebarTriggerDirective } from './sidebar-trigger.directive';

describe('SidebarTriggerDirective', () => {
  let directive: SidebarTriggerDirective;
  let sidebar: jasmine.SpyObj<SidebarComponent>;
  let focusMonitor: jasmine.SpyObj<FocusMonitor>;

  beforeEach(() => {
    sidebar = jasmine.createSpyObj('SidebarComponent', ['show', 'hide']);
    focusMonitor = jasmine.createSpyObj('FocusMonitor', ['monitor']);
    focusMonitor.monitor.and.returnValue(EMPTY);
    directive = new SidebarTriggerDirective({} as ElementRef<any>, focusMonitor);
    directive.sidebar = sidebar;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should show menu on click', () => {
    // when
    directive.click();

    // then
    expect(sidebar.show).toHaveBeenCalled();
  });

  it('hide on destroy', () => {
    // when
    directive.ngOnDestroy();

    // then
    expect(sidebar.hide).toHaveBeenCalled();
  });
});
