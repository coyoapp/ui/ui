import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import { Directive, ElementRef, HostListener, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SidebarComponent } from '../sidebar/sidebar.component';

/** Directive used to toggle sidebar component. */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[cui-sidebar-trigger]'
})
export class SidebarTriggerDirective implements OnDestroy {
  private subscription?: Subscription;
  private lastOrigin: FocusOrigin = null;

  /** Input to sidebar component */
  @Input('cui-sidebar-trigger')
  sidebar?: SidebarComponent;

  constructor(
    elementRef: ElementRef,
    focusMonitor: FocusMonitor
  ) {
    this.subscription = focusMonitor.monitor(elementRef)
      .subscribe(origin => { this.lastOrigin = origin; });
  }

  ngOnDestroy(): void {
    this.sidebar?.hide();
    this.subscription?.unsubscribe();
  }

  @HostListener('click')
  click(): void {
    this.sidebar?.show(this.lastOrigin === 'keyboard');
  }
}
