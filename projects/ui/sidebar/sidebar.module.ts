import { A11yModule } from '@angular/cdk/a11y';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconModule } from '@coyoapp/ui/icon';
import { SidebarTriggerDirective } from './sidebar-trigger/sidebar-trigger.directive';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    A11yModule
  ],
  declarations: [
    SidebarComponent,
    SidebarTriggerDirective
  ],
  exports: [
    SidebarComponent,
    SidebarTriggerDirective
  ]
})
export class SidebarModule {}
