import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER, noop } from '@coyoapp/ui/cdk';
import { MenuItemComponent } from '@coyoapp/ui/menu';
import { CardMenuComponent } from './card-menu.component';

describe('CardMenuComponent', () => {
  let component: CardMenuComponent;
  let fixture: ComponentFixture<CardMenuComponent>;
  let focusKeyManager: {
    keyManager: {
      onKeydown: any
    }
  };

  beforeEach(async () => {
    focusKeyManager = {
      keyManager: {
        onKeydown: noop
      }
    };
    await TestBed.configureTestingModule({
      providers: [{
        provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
        useValue: focusKeyManager
      }],
      declarations: [CardMenuComponent]
    }).overrideTemplate(CardMenuComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the keyManager on get keyManager() call ', () => {
    (component.menu as any) = { keyManager: true };
    const { keyManager } = component;

    expect(keyManager).toBeTruthy();
  });

  it('should get correct CUI_FOCUS_KEY_MANAGER_PROVIDER provider of type CardMenuComponent', () => {
    expect(TestBed.inject(CUI_FOCUS_KEY_MANAGER_PROVIDER)).toBeDefined();
    const directiveDecoraters = (<any>CardMenuComponent).__annotations__;
    const useExistingClass = directiveDecoraters[0].providers[0].useExisting();

    expect(directiveDecoraters[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(CardMenuComponent);
  });

  it('should get correct CUI_MENU provider of type CardMenuComponent', () => {
    expect(TestBed.inject(CUI_FOCUS_KEY_MANAGER_PROVIDER)).toBeDefined();
    const directiveDecoraters = (<any>CardMenuComponent).__annotations__;
    const useExistingClass = directiveDecoraters[0].providers[1].useExisting();

    expect(directiveDecoraters[0].providers[1].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(CardMenuComponent);
  });

  it('should get menu parent', () => {
    (component as any).menu = { parent: 'parent' as any };

    expect(component.parent).toEqual('parent' as any);
  });

  it('should hide the menu', () => {
    (component as any).menu = {
      hide: () => {}
    };
    spyOn((component as any).menu, 'hide');
    component.hide(true, true);

    expect(component.menu?.hide).toHaveBeenCalledWith(true, true);
  });

  it('should buffer items until menu is available', () => {
    // given
    const item = {} as MenuItemComponent;
    const addItemSpy = jasmine.createSpy('addItem');

    // when
    component.addItem(item);
    (component as any).menu = { addItem: addItemSpy };

    // then
    expect(addItemSpy).not.toHaveBeenCalled();

    // when again
    component.ngAfterViewChecked();

    // then
    expect(addItemSpy).toHaveBeenCalledWith(item);
  });

  it('should buffer and remove items until menu is available', () => {
    // given
    const item = {} as MenuItemComponent;
    const addItemSpy = jasmine.createSpy('addItem');

    // when
    component.addItem(item);
    component.removeItem(item);
    (component as any).menu = { addItem: addItemSpy };
    component.ngAfterViewChecked();

    // then
    expect(addItemSpy).not.toHaveBeenCalled();
  });
});
