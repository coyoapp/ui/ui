import { FocusKeyManager } from '@angular/cdk/a11y';
import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { CUI_FOCUS_KEY_MANAGER_PROVIDER } from '@coyoapp/ui/cdk';
import { CUI_MENU, Menu, MenuComponent, MenuItemComponent, MenuPlacement } from '@coyoapp/ui/menu';

/**
 * COYO-UI card menu component
 *
 * @description
 * Shows card menu items.
 *
 * @example
 * <cui-card-header>
 *   <h2>...</h2>
 *   <cui-card-menu>
 *     <a href="#" cui-menu-item>Action</a>
 *   </cui-card-menu>
 * <cui-card-header>
 */
@Component({
  selector: 'cui-card-menu',
  templateUrl: './card-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: CUI_FOCUS_KEY_MANAGER_PROVIDER,
    useExisting: forwardRef(() => CardMenuComponent)
  }, {
    provide: CUI_MENU,
    useExisting: forwardRef(() => CardMenuComponent)
  }],
  host: {
    class: 'cui-card-menu'
  }
})
export class CardMenuComponent implements Menu, AfterViewChecked {
  @ViewChild(MenuComponent)
  menu?: MenuComponent;

  /**
   * The placement of the menu.
   */
  @Input()
  placement: MenuPlacement = 'bottom-end';

  /**
   * The aria label for the trigger
   */
  @Input()
  ariaLabel?: string;

  private itemBuffer: MenuItemComponent[] = [];

  get keyManager(): FocusKeyManager<MenuItemComponent> | undefined {
    return this.menu?.keyManager;
  }

  get parent(): Menu | undefined {
    return this.menu?.parent;
  }

  ngAfterViewChecked(): void {
    if (this.menu) {
      this.itemBuffer.forEach(item => this.menu?.addItem(item));
      this.itemBuffer = [];
    }
  }

  addItem(item: MenuItemComponent): void {
    if (this.menu) {
      this.menu.addItem(item);
    } else {
      this.itemBuffer.push(item);
    }
  }

  removeItem(item: MenuItemComponent): void {
    if (this.menu) {
      this.menu.removeItem(item);
    } else if (this.itemBuffer.includes(item)) {
      const index = this.itemBuffer.indexOf(item);
      this.itemBuffer.splice(index, 1);
    }
  }

  hide(propagate: boolean, refocus: boolean): void {
    this.menu?.hide(propagate, refocus);
  }
}
