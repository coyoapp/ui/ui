import {
  ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation
} from '@angular/core';

/**
 * COYO-UI card header component
 *
 * @description
 * Wraps the header content.
 *
 * @example
 * <cui-card-header>
 *   <h2>...</h2>
 * <cui-card-header>
 */
@Component({
  selector: 'cui-card-header',
  templateUrl: './card-header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-card-content cui-card-header'
  }
})
export class CardHeaderComponent {

  /**
   * The size of the card title.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 's';
}
