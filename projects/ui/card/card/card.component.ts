import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

/**
 * COYO-UI card component.
 *
 * @description
 * Content container element for all other card components.
 * Provides properties to adjust the card size and his border radius.
 *
 * @example
 * <cui-card>
 *   <cui-card-header>...</cui-card-header>
 *   <cui-card-body>...</cui-card-body>
 *   <cui-card-footer>...</cui-card-footer>
 * </cui-card>
 */
@Component({
  selector: 'cui-card',
  template: '<ng-content></ng-content>',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-card'
  }
})
export class CardComponent {}
