import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { MenuModule } from '@coyoapp/ui/menu';
import { CardBodyComponent } from './card-body/card-body.component';
import { CardFooterComponent } from './card-footer/card-footer.component';
import { CardHeaderComponent } from './card-header/card-header.component';
import { CardMenuComponent } from './card-menu/card-menu.component';
import { CardComponent } from './card/card.component';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    MenuModule
  ],
  declarations: [
    CardComponent,
    CardHeaderComponent,
    CardMenuComponent,
    CardBodyComponent,
    CardFooterComponent
  ],
  exports: [
    CardComponent,
    CardHeaderComponent,
    CardMenuComponent,
    CardBodyComponent,
    CardFooterComponent
  ]
})
export class CardModule {}
