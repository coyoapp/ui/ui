import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

/**
 * COYO-UI card body component
 *
 * @description
 * Wraps the body content.
 *
 * @example
 * <cui-card-body>
 *   <p>...</p>
 * </cui-card-body>
 */
@Component({
  selector: 'cui-card-body',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-card-content cui-card-body'
  }
})
export class CardBodyComponent {}
