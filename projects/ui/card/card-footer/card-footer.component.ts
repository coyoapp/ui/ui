import { Component, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';

/**
 * COYO-UI card footer component
 *
 * @description
 * Wraps the footer content.
 *
 * @example
 * <cui-card-footer>
 *   <button cui-button>Action 1</button>
 *   <button cui-button>Action 2</button>
 *   <button cui-button>Action 3</button>
 * <cui-card-footer>
 */
@Component({
  selector: 'cui-card-footer',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-card-content cui-card-footer'
  }
})
export class CardFooterComponent {}
