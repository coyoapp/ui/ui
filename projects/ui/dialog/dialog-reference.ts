import { InjectionToken } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { OverlayRef } from '@angular/cdk/overlay';

export const DialogReference = new InjectionToken<CuiDialogReference<unknown, unknown>>('cui-dialog-ref');

/**
 * Reference to the currently opened dialog. Can be injected by the dialog component via {@link DialogReference}.
 */
export class CuiDialogReference<Data, Result> {
  readonly result: Observable<Result>;

  constructor(
    private readonly closingSubject: Subject<Result>,
    private readonly overlayRef: OverlayRef,
    public readonly data?: Data
  ) {
    this.result = closingSubject.asObservable();
  }

  /**
   * Closes the dialog with the given result. Result is optional, if not given the result observable will just complete.
   *
   * @param result - the result of the dialog
   */
  close(result?: Result): void {
    this.overlayRef.dispose();
    if (result !== undefined) {
      this.closingSubject.next(result);
    }
    this.closingSubject.complete();
  }
}
