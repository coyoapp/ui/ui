import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { IconModule } from '@coyoapp/ui/icon';
import { ButtonModule } from '@coyoapp/ui/button';
import { A11yModule } from '@angular/cdk/a11y';
import { DialogPortalComponent } from './dialog-portal/dialog-portal.component';
import { DialogTitleWrapperComponent } from './dialog-title-wrapper/dialog-title-wrapper.component';
import { DialogActionsComponent } from './dialog-actions/dialog-actions.component';
import { DialogComponent } from './dialog/dialog.component';
import { DialogService } from './dialog/dialog.service';
import { DialogTitleComponent } from './dialog-title/dialog-title.component';

@NgModule({
  declarations: [
    DialogPortalComponent,
    DialogTitleWrapperComponent,
    DialogActionsComponent,
    DialogComponent,
    DialogTitleComponent
  ],
  imports: [
    CommonModule,
    OverlayModule,
    PortalModule,
    IconModule,
    ButtonModule,
    A11yModule
  ],
  exports: [
    DialogActionsComponent,
    DialogComponent,
    DialogTitleComponent
  ],
  providers: [{
    provide: DialogService, useClass: DialogService
  }]
})
export class DialogModule {}
