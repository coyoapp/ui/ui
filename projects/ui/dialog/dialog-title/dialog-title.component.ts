import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

/**
 * Component for rendering additional content to the dialog title
 */
@Component({
  selector: 'cui-dialog-title',
  templateUrl: './dialog-title.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DialogTitleComponent {}
