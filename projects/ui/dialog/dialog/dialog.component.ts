import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { CUI_SELECT_CONTAINER_CONFIG } from '@coyoapp/ui/select';

let nextUniqueId = 0;

/**
 * Dialog Component should wrap the the title, content and actions of a dialog.
 *
 * @example
 * Here is a simple example of a confirmation dialog
 * ```
 * <cui-dialog>
 *  <cui-dialog-title title="Test title" [closable]="true"></cui-dialog-title>
 *  Are you sure you want to do this?
 *  <cui-dialog-actions>
 *    <button cui-button>No</button>
 *    <button cui-button>Yes</button>
 *  </cui-dialog-actions>
 * </cui-dialog>
 * ```
 */
@Component({
  selector: 'cui-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: CUI_SELECT_CONTAINER_CONFIG,
    useValue: {
      containerSelector: '.cui-dialog-portal'
    }
  }],
  host: {
    class: 'cui-dialog'
  }
})
export class DialogComponent implements AfterViewInit {

  @ViewChild('scrollContainer', { static: true })
  scrollContainerElementRef!: ElementRef<HTMLElement>;

  readonly titleId = `dialog-title-${nextUniqueId++}`;

  /**
   * The title of the dialog.
   */
  @Input() title = '';

  /**
   * Flag if the dialog should be closable.
   */
  @Input() closable = true;

  calculateElevations(): void {
    const scrollContainer = this.scrollContainerElementRef.nativeElement;

    if (scrollContainer.scrollTop) {
      scrollContainer.classList.add('elevation-top');
    } else {
      scrollContainer.classList.remove('elevation-top');
    }

    if (scrollContainer.scrollTop + scrollContainer.clientHeight < scrollContainer.scrollHeight) {
      scrollContainer.classList.add('elevation-bottom');
    } else {
      scrollContainer.classList.remove('elevation-bottom');
    }
  }

  ngAfterViewInit(): void {
    this.calculateElevations();
  }
}
