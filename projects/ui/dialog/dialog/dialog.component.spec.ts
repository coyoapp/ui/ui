import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { DialogComponent } from './dialog.component';

describe('DialogComponent', () => {
  let component: DialogComponent;
  let fixture: ComponentFixture<DialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogComponent, MockDialogTitleWrapperComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the elevations for top and bottom', () => {
    // given
    const spy = jasmine.createSpyObj('classList', ['add', 'remove']);
    component.scrollContainerElementRef = {
      nativeElement: {
        scrollTop: 100,
        clientHeight: 200,
        scrollHeight: 400,
        classList: spy
      } as HTMLElement
    };

    // when
    component.calculateElevations();

    // then
    expect(spy.add).toHaveBeenCalledWith('elevation-top');
    expect(spy.add).toHaveBeenCalledWith('elevation-bottom');
    expect(spy.remove).not.toHaveBeenCalled();
  });

  it('should calculate the elevations for top and bottom', () => {
    // given
    const spy = jasmine.createSpyObj('classList', ['add', 'remove']);
    component.scrollContainerElementRef = {
      nativeElement: {
        scrollTop: 200,
        clientHeight: 200,
        scrollHeight: 400,
        classList: spy
      } as HTMLElement
    };

    // when
    component.calculateElevations();

    // then
    expect(spy.add).toHaveBeenCalledWith('elevation-top');
    expect(spy.remove).toHaveBeenCalledWith('elevation-bottom');
  });

  it('should calculate the elevations for top and bottom', () => {
    // given
    const spy = jasmine.createSpyObj('classList', ['add', 'remove']);
    component.scrollContainerElementRef = {
      nativeElement: {
        scrollTop: 0,
        clientHeight: 200,
        scrollHeight: 400,
        classList: spy
      } as HTMLElement
    };

    // when
    component.calculateElevations();

    // then
    expect(spy.remove).toHaveBeenCalledWith('elevation-top');
    expect(spy.add).toHaveBeenCalledWith('elevation-bottom');
  });
});

@Component({
  selector: 'cui-dialog-title-wrapper',
  template: ''
})
class MockDialogTitleWrapperComponent {

  @Input() title = '';

  @Input() closable = true;

  @Input() dialogTitleId = '';
}
