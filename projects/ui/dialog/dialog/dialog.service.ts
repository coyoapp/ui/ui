import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, ComponentType } from '@angular/cdk/portal';
import { Inject, Injectable, Injector } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { CuiDialogOptions, DialogOptions } from '../dialog-options';
import { DialogPortalComponent } from '../dialog-portal/dialog-portal.component';
import { CuiDialogReference, DialogReference } from '../dialog-reference';
import { CUI_DIALOG_CONFIG, DialogConfig } from '../dialog.config';
import { OverlayReference } from '../overlay-reference';

/**
 * Dialog service is responsible for every dialog related operation.
 */
@Injectable()
export class DialogService {

  constructor(
    private readonly overlay: Overlay,
    private readonly injector: Injector,
    @Inject(CUI_DIALOG_CONFIG) readonly config: DialogConfig
  ) {}

  /**
   * Opens a cui dialog. For operations like closing the dialog please inject {@link DialogReference} in your dialog
   * component. Overlay will close on backdrop click and escape key.
   *
   * @param componentType - The component type to render inside the dialog.The component must use
   * {@link DialogComponent} for consistent and correct rendering of the {@link DialogActionsComponent} and
   * {@link DialogTitleComponent}.
   * @param dialogOptions - The {@link DialogOptions} for the dialog. You can inject the data given into your component
   * via the {@link DialogReference} injection token.
   *
   * @returns An observable with the result of the dialog. It will emit one value and completes after the dialog was
   * closed. If the dialog has no result it will complete without any emit
   */
  open<Result, Data>(componentType: ComponentType<unknown>, dialogOptions?: CuiDialogOptions<Data>): Observable<Result> {
    const dialogRef = this.openWithRef(componentType, dialogOptions);
    return dialogRef.result;
  }

  /**
   * Opens a cui dialog and return the dialog. For operations like closing the dialog, you can inject {@link DialogReference} in your dialog
   * component, although it will also return the reference of the dialog. Overlay will close on backdrop click and escape key.
   *
   * @param componentType - The component type to render inside the dialog.The component must use
   * {@link DialogComponent} for consistent and correct rendering of the {@link DialogActionsComponent} and
   * {@link DialogTitleComponent}.
   * @param dialogOptions - The {@link DialogOptions} for the dialog. You can inject the data given into your component
   * via the {@link DialogReference} injection token.
   *
   * @returns The {@link CuiDialogReference} of the dialog. With it, you can perform operations like closing the dialog.
   * Also found in it, an observable with the result of the dialog. It will emit one value and completes after the dialog was
   * closed. If the dialog has no result it will complete without any emit
   */
  openWithRef<Result, Data>(componentType: ComponentType<unknown>, dialogOptions?: CuiDialogOptions<Data>): CuiDialogReference<Data, Result | any> {
    const overlayRef = this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true,
      panelClass: dialogOptions?.panelClass
    });

    const closingSubject = new Subject<Result>();
    const dialogRef = new CuiDialogReference<Data, Result>(closingSubject, overlayRef, dialogOptions?.data);
    const dialogInjector = Injector.create({
      parent: this.injector,
      providers: [{
        provide: OverlayReference,
        useValue: overlayRef
      }, {
        provide: DialogOptions,
        useValue: { ...this.config.defaults, ...dialogOptions }
      }, {
        provide: DialogReference,
        useValue: dialogRef
      }]
    });
    const dialogPortal = new ComponentPortal(DialogPortalComponent);
    dialogPortal.injector = dialogInjector;
    const componentRef = overlayRef.attach(dialogPortal);
    const componentPortal = new ComponentPortal(componentType);
    componentPortal.injector = dialogInjector;
    componentRef.instance.attachComponentPortal(componentPortal);
    if (dialogOptions?.closeOnBackdropClick) {
      overlayRef.backdropClick().subscribe(() => dialogRef.close());
    }
    return dialogRef;
  }
}
