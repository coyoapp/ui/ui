import { GlobalPositionStrategy, Overlay } from '@angular/cdk/overlay';
import { ComponentType } from '@angular/cdk/portal';
import { TestBed } from '@angular/core/testing';
import { DialogReference, DialogWidth } from '@coyoapp/ui/dialog';
import { DialogService } from './dialog.service';

describe('DialogService', () => {
  let service: DialogService;
  let overlay: jasmine.SpyObj<Overlay>;

  beforeEach(() => {
    overlay = jasmine.createSpyObj('overlay', ['create', 'position']);

    TestBed.configureTestingModule({
      providers: [{
        provide: DialogService,
        useClass: DialogService
      }, {
        provide: Overlay,
        useValue: overlay
      }]
    });

    service = TestBed.inject(DialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open a dialog', () => {
    // given
    const positionStrategy = {} as GlobalPositionStrategy;
    const spy = jasmine.createSpyObj(['global', 'centerHorizontally', 'centerVertically']);
    const overlayRef = jasmine.createSpyObj('overlayRef', ['attach']);
    const dialogContainer = { instance: jasmine.createSpyObj('dialogContainerInstance', ['attachComponentPortal']) };
    spy.global.and.returnValue(spy);
    spy.centerHorizontally.and.returnValue(spy);
    spy.centerVertically.and.returnValue(positionStrategy);
    overlay.position.and.returnValue(spy);
    overlay.create.and.returnValue(overlayRef);
    overlayRef.attach.and.returnValue(dialogContainer);

    const dialogOptions = { width: DialogWidth.LARGE, data: 'someData', panelClass: 'panel' };

    // when
    const result = service.open({} as ComponentType<unknown>, dialogOptions);

    // then
    expect(overlay.create).toHaveBeenCalledWith({
      positionStrategy,
      hasBackdrop: true,
      panelClass: 'panel'
    });
    const componentPortal = dialogContainer.instance.attachComponentPortal.calls.mostRecent().args[0];
    const dialogReference = componentPortal.injector.get(DialogReference);

    expect(dialogReference.data).toEqual(dialogOptions.data);
    expect(dialogReference.result).toBe(result);
  });

  it('should open and return dialog ref', () => {
    // given
    const positionStrategy = {} as GlobalPositionStrategy;
    const spy = jasmine.createSpyObj(['global', 'centerHorizontally', 'centerVertically']);
    const overlayRef = jasmine.createSpyObj('overlayRef', ['attach']);
    const dialogContainer = { instance: jasmine.createSpyObj('dialogContainerInstance', ['attachComponentPortal']) };
    spy.global.and.returnValue(spy);
    spy.centerHorizontally.and.returnValue(spy);
    spy.centerVertically.and.returnValue(positionStrategy);
    overlay.position.and.returnValue(spy);
    overlay.create.and.returnValue(overlayRef);
    overlayRef.attach.and.returnValue(dialogContainer);

    const dialogOptions = { width: DialogWidth.LARGE, data: 'someData', panelClass: 'panel' };

    // when
    const result = service.openWithRef({} as ComponentType<unknown>, dialogOptions);

    // then
    expect(overlay.create).toHaveBeenCalledWith({
      positionStrategy,
      hasBackdrop: true,
      panelClass: 'panel'
    });
    const componentPortal = dialogContainer.instance.attachComponentPortal.calls.mostRecent().args[0];
    const dialogReference = componentPortal.injector.get(DialogReference);

    expect(dialogReference).toBe(result);
  });
});
