import { BasePortalOutlet, CdkPortalOutlet, ComponentPortal, TemplatePortal } from '@angular/cdk/portal';
import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  EmbeddedViewRef,
  HostBinding,
  HostListener,
  Inject,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { CuiDialogOptions, DialogOptions } from '../dialog-options';
import { CuiDialogReference, DialogReference } from '../dialog-reference';
import { DialogWidth } from '../dialog-width';

@Component({
  selector: 'cui-dialog-portal',
  templateUrl: './dialog-portal.component.html',
  styleUrls: ['./dialog-portal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DialogPortalComponent extends BasePortalOutlet {

  @ViewChild(CdkPortalOutlet, { static: true }) _portalOutlet!: CdkPortalOutlet;

  @HostBinding('class') sizeClass: DialogWidth = DialogWidth.MEDIUM;

  @HostBinding('class.cui-dialog-portal') hostClass = true;

  constructor(
    @Inject(DialogOptions) private readonly dialogOptions: CuiDialogOptions<unknown>,
    @Inject(DialogReference) readonly dialogReference: CuiDialogReference<unknown, unknown>
  ) {
    super();
    this.sizeClass = this.dialogOptions.width || DialogWidth.MEDIUM;
  }

  attachComponentPortal<T>(portal: ComponentPortal<T>): ComponentRef<T> {
    return this._portalOutlet.attachComponentPortal(portal);
  }

  attachTemplatePortal<C>(portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    return this._portalOutlet.attachTemplatePortal(portal);
  }

  @HostListener('keydown.esc', ['$event'])
  close(event: Event): void {
    this.dialogReference.close();
    event.stopPropagation();
  }
}
