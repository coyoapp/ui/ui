import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogOptions, DialogReference, DialogWidth } from '@coyoapp/ui/dialog';
import { CdkPortalOutlet, ComponentPortal } from '@angular/cdk/portal';
import { DialogPortalComponent } from './dialog-portal.component';
import SpyObj = jasmine.SpyObj;

describe('DialogPortalComponent', () => {
  let component: DialogPortalComponent;
  let fixture: ComponentFixture<DialogPortalComponent>;
  let cdkPortalSpy: SpyObj<CdkPortalOutlet>;

  beforeEach(async () => {
    cdkPortalSpy = jasmine.createSpyObj('CdkPortalOutlet', ['attachComponentPortal', 'attachTemplatePortal']);
    await TestBed.configureTestingModule({
      declarations: [DialogPortalComponent],
      providers: [{
        provide: DialogOptions,
        useValue: { width: DialogWidth.LARGE }
      }, {
        provide: DialogReference,
        useValue: {}
      }]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPortalComponent);
    component = fixture.componentInstance;
    (component as any)._portalOutlet = cdkPortalSpy;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.sizeClass).toBe(DialogWidth.LARGE);
  });

  it('should call attachComponentPortal when calling attachComponentPortal method', () => {
    const portal = new ComponentPortal(DialogPortalComponent);
    component.attachComponentPortal(portal);

    expect((component as any)._portalOutlet.attachComponentPortal).toHaveBeenCalled();
  });

  it('should call attachComponentPortal when calling attachComponentPortal method', () => {
    component.attachTemplatePortal(jasmine.createSpyObj('TemplatePortal', ['attachComponentPortal', 'attachTemplatePortal']));

    expect((component as any)._portalOutlet.attachTemplatePortal).toHaveBeenCalled();
  });

  it('should call close on the dialogReference when calling close method', () => {
    (component as any).dialogReference = jasmine.createSpyObj('CuiDialogReference', ['close']);
    const event = jasmine.createSpyObj('Event', ['stopPropagation']);
    component.close(event);

    expect((component as any).dialogReference.close).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  });
});
