import { InjectionToken } from '@angular/core';
import { DialogWidth } from './dialog-width';

export interface DialogMessages {
  close: string;
}

export interface DialogDefaults {
  width: DialogWidth;
}

export interface DialogConfig {
  readonly i18n: Readonly<DialogMessages>;
  readonly defaults: Readonly<DialogDefaults>
}

export const CUI_TOAST_DEFAULT_CONFIG: DialogConfig = {
  i18n: {
    close: 'Close'
  },
  defaults: {
    width: DialogWidth.MEDIUM
  }
};

export const CUI_DIALOG_CONFIG = new InjectionToken<DialogConfig>('CUI_DIALOG_CONFIG', {
  factory: () => CUI_TOAST_DEFAULT_CONFIG
});
