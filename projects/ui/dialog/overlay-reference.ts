import { InjectionToken } from '@angular/core';

export const OverlayReference = new InjectionToken('cui-overlay-ref');
