import { InjectionToken } from '@angular/core';
import { DialogWidth } from './dialog-width';

export const DialogOptions = new InjectionToken<CuiDialogOptions<unknown>>('cui-dialog-options');

/**
 * Options for the dialogs
 */
export interface CuiDialogOptions<Data> {
  /**
   * The width of the dialog.
   *
   * Results in the following widths of the dialog:
   * {@link DialogWidth.SMALL} -> 400px
   * {@link DialogWidth.MEDIUM} -> 640px
   * {@link DialogWidth.LARGE} -> 960px
   *
   * @default {@link DialogWidth.MEDIUM}
   */
  width?: DialogWidth;

  /**
   * Data that will be delivered to your dialog implementation via injection token {@link DialogReference}
   */
  data?: Data;

  /** Custom class to add to the overlay pane. */
  panelClass?: string | string[];

  /**
   * Close the dialog when the user clicks on the backdrop.
   */
  closeOnBackdropClick?: boolean;
}
