import { OverlayRef } from '@angular/cdk/overlay';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CuiDialogReference, DialogReference } from '@coyoapp/ui/dialog';
import { Subject } from 'rxjs';
import { DialogTitleWrapperComponent } from './dialog-title-wrapper.component';

describe('DialogTitleWrapperComponent', () => {
  let component: DialogTitleWrapperComponent;
  let fixture: ComponentFixture<DialogTitleWrapperComponent>;
  let overlayRef: jasmine.SpyObj<OverlayRef>;
  let closingSubject: Subject<unknown>;

  beforeEach(async () => {
    overlayRef = jasmine.createSpyObj('overlayRef', ['dispose']);
    closingSubject = new Subject();
    await TestBed.configureTestingModule({
      declarations: [DialogTitleWrapperComponent],
      providers: [{
        provide: DialogReference,
        useValue: new CuiDialogReference(closingSubject, overlayRef)
      }]
    }).overrideTemplate(DialogTitleWrapperComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTitleWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call close on the dialogReference when calling onClick method', () => {
    (component as any).dialogReference = jasmine.createSpyObj('CuiDialogReference', ['close']);
    component.onClick();

    expect((component as any).dialogReference.close).toHaveBeenCalled();
  });
});
