import {
  ChangeDetectionStrategy, Component, Inject, Input, ViewEncapsulation
} from '@angular/core';
import { CuiDialogReference, DialogReference } from '../dialog-reference';
import { CUI_DIALOG_CONFIG, DialogConfig } from '../dialog.config';

/**
 * Component for rendering a title of a dialog
 */
@Component({
  selector: 'cui-dialog-title-wrapper',
  templateUrl: './dialog-title-wrapper.component.html',
  styleUrls: ['./dialog-title-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-dialog-title'
  }
})
export class DialogTitleWrapperComponent {

  /**
   * The title of the dialog
   */
  @Input() title = '';

  /**
   * Flag if the dialog should be closable
   */
  @Input() closable = true;

  /**
   * The id of the dialog which is attached to the title for a11y labeling
   */
  @Input() dialogTitleId = '';

  closeAriaLabel: string;

  constructor(
    @Inject(DialogReference) private readonly dialogReference: CuiDialogReference<unknown, unknown>,
    @Inject(CUI_DIALOG_CONFIG) readonly config: DialogConfig
  ) {
    this.closeAriaLabel = config.i18n.close;
  }

  onClick(): void {
    this.dialogReference.close();
  }
}
