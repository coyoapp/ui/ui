import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

/**
 * Component for rendering dialog action buttons.
 *
 * @example
 * ```
 * <cui-dialog-actions>
 *   <button cui-button>No</button>
 *   <button cui-button>Yes</button>
 * </cui-dialog-actions>
 * ```
 */
@Component({
  selector: 'cui-dialog-actions',
  templateUrl: './dialog-actions.component.html',
  styleUrls: ['./dialog-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-dialog-actions'
  }
})
export class DialogActionsComponent {}
