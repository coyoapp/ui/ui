import { TestBed } from '@angular/core/testing';
import { CuiDialogReference } from '@coyoapp/ui/dialog/dialog-reference';
import { Subject } from 'rxjs';
import { OverlayRef } from '@angular/cdk/overlay';

describe('DialogReference', () => {
  let dialogReference: CuiDialogReference<unknown, boolean>;
  let result: Subject<boolean>;
  let overlayRef: jasmine.SpyObj<OverlayRef>;

  beforeEach(async () => {
    result = new Subject();
    overlayRef = jasmine.createSpyObj('OverlayRef', ['dispose']);
    dialogReference = new CuiDialogReference(result, overlayRef);
    TestBed.configureTestingModule({});
  });

  it('should create an instance', () => {
    expect(dialogReference).toBeTruthy();
  });

  it('should call the appropriate method on closingSubject when calling close', () => {
    (dialogReference as any).result = result.asObservable();
    spyOn(result, 'next');
    spyOn(result, 'complete');
    dialogReference.close(true);

    expect(overlayRef.dispose).toHaveBeenCalled();
    expect(result.complete).toHaveBeenCalled();
    expect(result.next).toHaveBeenCalledWith(true);
  });

  it('should call the appropriate method on closingSubject when calling close when the result is false', () => {
    (dialogReference as any).result = result.asObservable();
    spyOn(result, 'next');
    spyOn(result, 'complete');
    dialogReference.close(false);

    expect(overlayRef.dispose).toHaveBeenCalled();
    expect(result.complete).toHaveBeenCalled();
    expect(result.next).toHaveBeenCalledWith(false);
  });

  it('should not call next on result subject for undefined result', () => {
    (dialogReference as any).result = result.asObservable();
    spyOn(result, 'next');
    spyOn(result, 'complete');
    dialogReference.close();

    expect(overlayRef.dispose).toHaveBeenCalled();
    expect(result.complete).toHaveBeenCalled();
    expect(result.next).not.toHaveBeenCalled();
  });
});
