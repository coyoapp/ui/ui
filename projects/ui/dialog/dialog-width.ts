export enum DialogWidth {
  SMALL = 'sm',
  MEDIUM = 'md',
  LARGE = 'lg'
}
