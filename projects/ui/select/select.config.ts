import { InjectionToken } from '@angular/core';

export interface SelectMessages {
  addTag: string;
  clearAll: string;
  clearItem: string;
  loading: string;
  notFound: string;
}

export interface SelectConfig {
  readonly i18n: Readonly<SelectMessages>;
}

export const CUI_SELECT_DEFAULT_CONFIG: SelectConfig = {
  i18n: {
    addTag: 'Add item',
    clearAll: 'Clear all',
    clearItem: 'Clear item',
    loading: 'Loading…',
    notFound: 'No items found'
  }
};

export const CUI_SELECT_CONFIG = new InjectionToken<SelectConfig>('CUI_SELECT_CONFIG', {
  factory: () => CUI_SELECT_DEFAULT_CONFIG
});

export interface SelectContainerConfig {
  readonly containerSelector: string;
}

export const CUI_SELECT_CONTAINER_CONFIG = new InjectionToken<SelectContainerConfig>('CUI_SELECT_CONTAINER_CONFIG');
