import { Directive, TemplateRef } from '@angular/core';

/**
 * Marker directive to use a custom label for the item(s) in the select
 * dropdown. The marked template will be used for both single- and multi-select
 * components.
 *
 * @example
 * ```
 * <cui-select ...>
 *   <ng-template cuiSelectOption let-item="item">
 *     Option: {{ item | json }}
 *   </ng-template>
 * </cui-select>
 * ```
 */
@Directive({
  selector: '[cuiSelectOption]'
})
export class SelectOptionDirective {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public template: TemplateRef<any>) {}
}
