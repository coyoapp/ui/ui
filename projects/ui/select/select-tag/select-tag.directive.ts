import { Directive, TemplateRef } from '@angular/core';

/**
 * Marker directive to use a custom label for the tag item(s) in the select
 * dropdown. The marked template will be used for both single- and multi-select
 * components.
 *
 * @example
 * ```
 * <cui-select ...>
 *   <ng-template cuiSelectTag let-item="item">
 *     Option: {{ item | json }}
 *   </ng-template>
 * </cui-select>
 * ```
 */
@Directive({
  selector: '[cuiSelectTag]'
})
export class SelectTagDirective {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public template: TemplateRef<any>) {}
}
