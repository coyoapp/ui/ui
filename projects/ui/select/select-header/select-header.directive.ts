import { Directive, TemplateRef } from '@angular/core';

/**
 * Marker directive to use a header in the select
 * dropdown. The marked template will be used for both single- and multi-select
 * components.
 *
 * @example
 * ```
 * <cui-select ...>
 *   <ng-template cuiSelectHeader>
 *     <h5>This is a header</h5>
 *   </ng-template>
 * </cui-select>
 * ```
 */
@Directive({
  selector: '[cuiSelectHeader]'
})
export class SelectHeaderDirective {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public template: TemplateRef<any>) {}
}
