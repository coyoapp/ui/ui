import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from '@coyoapp/ui/button';
import { CheckboxModule } from '@coyoapp/ui/checkbox';
import { IconModule } from '@coyoapp/ui/icon';
import { LoadingModule } from '@coyoapp/ui/loading';
import { NgSelectModule } from '@ng-select/ng-select';
import { SelectHeaderDirective } from './select-header/select-header.directive';
import { SelectLabelDirective } from './select-label/select-label.directive';
import { SelectOptionDirective } from './select-option/select-option.directive';
import { SelectTagDirective } from './select-tag/select-tag.directive';
import { SelectComponent } from './select/select.component';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgSelectModule, LoadingModule, IconModule, ButtonModule, CheckboxModule],
  declarations: [SelectComponent, SelectTagDirective, SelectOptionDirective, SelectLabelDirective, SelectHeaderDirective],
  exports: [SelectComponent, SelectTagDirective, SelectOptionDirective, SelectLabelDirective, SelectHeaderDirective]
})
export class SelectModule {}
