import { Directive, TemplateRef } from '@angular/core';

/**
 * Marker directive to use a custom label for the selected item(s). The marked
 * template will be used for both single- and multi-select components.
 *
 * @example
 * ```
 * <cui-select ...>
 *   <ng-template cuiSelectLabel let-item="item">
 *     Label: {{ item | json }}
 *   </ng-template>
 * </cui-select>
 * ```
 */
@Directive({
  selector: '[cuiSelectLabel]'
})
export class SelectLabelDirective {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(public template: TemplateRef<any>) {}
}
