import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostListener,
  Inject,
  Injector,
  Input,
  OnInit,
  Optional,
  Output,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subject } from 'rxjs';
import { SelectHeaderDirective } from '../select-header/select-header.directive';
import { SelectLabelDirective } from '../select-label/select-label.directive';
import { SelectOptionDirective } from '../select-option/select-option.directive';
import { SelectTagDirective } from '../select-tag/select-tag.directive';
import { CUI_SELECT_CONFIG, CUI_SELECT_CONTAINER_CONFIG, SelectConfig, SelectContainerConfig } from '../select.config';

/**
 * Select elements are used instead of native `<select>` elements to provide a
 * consistent cross-browser UI / UX. They replace single-selects, multi-selects
 * as well as tag-inputs.
 *
 * @example
 * ```
 * <cui-form-field label="Select form field">
 *   <cui-select [formControl]="control" [items]="items" [multiple]="true"></cui-select>
 * </cui-form-field>
 * ```
 *
 * @example
 * Templates can be customized using the marker directives `cuiSelectLabel` and
 * `cuiSelectOption` respectively.
 *
 * ```
 * <cui-form-field label="Select form field">
 *   <cui-select [formControl]="control" [items]="items" [multiple]="true">
 *     <ng-template cuiSelectLabel let-item="item">
 *       Label: {{ item | json }}
 *     </ng-template>
 *     <ng-template cuiSelectOption let-item="item">
 *       Option: {{ item | json }}
 *     </ng-template>
 *   </cui-select>
 * </cui-form-field>
 * ```
 */
@Component({
  selector: 'cui-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [provideFormFieldControl(SelectComponent)],
  host: {
    class: 'cui-select'
  }
})
export class SelectComponent<T> extends FormFieldControlComponent<T> implements OnInit {

  /**
   * Allows to create custom options.
   */
  @Input()
  addTag: boolean | ((term: string) => T | Promise<T>) = false;

  /**
   * Object property to use for selected model. By default binds to whole object.
   */
  @Input()
  bindValue = '';

  /**
   * Object property to use for label.
   */
  @Input()
  bindLabel = 'label';

  /**
   * Allow to group items by key or function expression
   * */
  @Input()
  groupBy: string | ((value: any) => any) = '';

  /**
   * Select marked dropdown item using tab.
   */
  @Input()
  selectOnTab = false;

  /**
   * Marks first item as focused when opening/filtering.
   */
  @Input()
  markFirst = false;

  /**
   * Allows to hide selected items.
   */
  @Input()
  hideSelected = false;

  /**
   * Allow to clear selected value.
   */
  @Input()
  clearable = false;

  /**
   * Custom autocomplete or advanced filter.
   */
  @Input()
  typeahead: Subject<string> = new Subject<string>();

  /**
   * Submit keys **additionally** to the enter key. When multiselect and addTags is configured the select will
   * add tags on the given keydown events. It compares the `key` value of the keydown event with the array items.
   */
  @Input()
  submitKeys?: string[];

  /**
   * This prefix is added prior to an entered search term.
   */
  @Input()
  searchPrefix?: string;

  /**
   * The list of items to display.
   */
  @Input()
  items: Array<T> = [];

  /**
   * You can set the loading state from the outside (e.g. async items loading).
   */
  @Input()
  loading = false;

  /**
   * When multiple = true, allows to set a limit number of selection.
   */
  @Input()
  maxSelectedItems = Number.MAX_SAFE_INTEGER;

  /**
   * Allows to select multiple items.
   */
  @Input()
  multiple = false;

  /**
   * Allow to search for value.
   */
  @Input()
  searchable = true;

  /**
   * Virtual scroll for better performance when rendering a lot of data.
   */
  @Input()
  virtualScroll = true;

  /**
   * Allow to filter by custom search function.
   */
  @Input()
  searchFn: ((term: string, item: T) => boolean) | null = null;

  /**
   * Provide custom trackBy function
   */
  @Input()
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  trackByFn: ((item: T) => any) | null = null;

  // eslint-disable-next-line @angular-eslint/no-output-native, @angular-eslint/no-output-rename
  @Output('change')
  changeEvent = new EventEmitter<T>();

  // eslint-disable-next-line @angular-eslint/no-output-native, @angular-eslint/no-output-rename
  @Output('clear')
  readonly clearEvent = new EventEmitter<void>();

  // eslint-disable-next-line @angular-eslint/no-output-native, @angular-eslint/no-output-rename
  @Output('scroll')
  scrollEvent = new EventEmitter<{ start: number, end: number }>();

  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('scrollToEnd')
  readonly scrollToEndEvent = new EventEmitter<void>();

  /**
   * Fired when item is added while [multiple]="true". Outputs added item
   */
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('add')
  readonly addEvent = new EventEmitter<void>();

  /**
   * Fired when item is removed while [multiple]="true"
   */
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('remove')
  readonly removeEvent = new EventEmitter<void>();

  /**
   * Fired on select blur
   */
  // eslint-disable-next-line @angular-eslint/no-output-rename
  @Output('blur')
  readonly blurEvent = new EventEmitter<void>();

  @Output()
  readonly search = new EventEmitter<string>();

  @ContentChild(SelectOptionDirective, { read: TemplateRef })
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  optionTemplate?: TemplateRef<any>;

  @ContentChild(SelectLabelDirective, { read: TemplateRef })
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  labelTemplate?: TemplateRef<any>;

  @ContentChild(SelectTagDirective, { read: TemplateRef })
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  tagTemplate?: TemplateRef<any>;

  @ContentChild(SelectHeaderDirective, { read: TemplateRef })
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  headerTemplate?: TemplateRef<any>;

  @ViewChild('select', { static: true, read: NgSelectComponent })
  private selectRef!: NgSelectComponent;

  constructor(
    injector: Injector,
    @Inject(CUI_SELECT_CONFIG) readonly globalConfig: SelectConfig,
    @Optional() @Inject(CUI_SELECT_CONTAINER_CONFIG) readonly selectContainerConfig?: SelectContainerConfig
  ) {
    super(injector);
  }

  private _config: SelectConfig = this.globalConfig;

  /**
   * custom attributes to add on input.
   */
  inputAttrs: { [key: string]: any } = {};

  /**
   * Custom select configuration overrides.
   */
  @Input()
  get config(): SelectConfig {
    return this._config;
  }

  set config(config: SelectConfig) {
    this._config = { ...this.globalConfig, ...config };
  }

  protected get focusElementRef(): ElementRef<HTMLElement> | undefined {
    return this.selectRef?.searchInput;
  }

  /**
   * A function to compare the option values with the selected values.
   *
   * @param option a value from an option
   * @param model a value from the selection
   * @returns the result of the comparison
   */
  @Input()
  compareWith: (option: T, model: T) => boolean = (a, b) => a === b;

  ngOnInit() {
    if (this.placeholder) {
      // Adding passed placeholder in inputAttrs
      this.inputAttrs.placeholder = this.placeholder;
    }
  }

  /**
   * React on the key down event of the ng-select as it needs to react to
   * certain keys when it is used as a tag input.
   *
   * @param event the keyboard event
   */
  onKeyDown(event: KeyboardEvent) {
    const eventKey = event.key;
    if (eventKey.length === 1
      && this.searchable
      && !this.selectRef.searchTerm
      && this.searchPrefix
      && event.key !== this.searchPrefix) {
      this.selectRef.searchTerm = this.searchPrefix;
    }

    if (this.multiple && this.addTag && (this.submitKeys?.indexOf(event.key) ?? -1) > -1) {
      this.selectTag();
      event.preventDefault();
    }
  }

  /**
   * Emits the onChange event.
   * Adjusts the dropdownPanel positioning after onChange
   *
   * @param event the onChange event
   */
  onChange(event: T | undefined): void {
    this.changeEvent.emit(event);
    setTimeout(() => {
      const ngSelect = this.selectRef;
      if (ngSelect?.isOpen && ngSelect.dropdownPanel) {
        ngSelect.dropdownPanel.adjustPosition();
      }
    }, 0);
  }

  /**
   * Emits the option add event.
   *
   * @param $event
   */
  tagAdded($event: any) {
    this.addEvent.emit($event);
  }

  /**
   * Emits the option remove event.
   *
   * @param $event
   */
  tagRemoved($event: any) {
    this.removeEvent.emit($event);
  }

  /**
   * Emits when focus changed from ng-select
   */
  focusChanged() {
    this.blurEvent.emit();
  }

  /**
   * Delegate the on touched function to the ng-select reference.
   *
   * @param fn the touch function
   */
  registerOnTouched(fn: () => void): void {
    super.registerOnTouched(fn);
    setTimeout(() => this.selectRef.registerOnTouched(fn));
  }

  /**
   * Opens the select dropdown panel.
   */
  open(): void {
    this.selectRef.open();
  }

  /**
   * Closes the select dropdown panel.
   */
  close(): void {
    this.selectRef.close();
  }

  /**
   * This scroll handler updates the location of the dropdown panel when the
   * host is scrolling. Required because the dropdown panel is attached to a
   * fixed container and thus not informed about scrolling events of its host
   * component.
   *
   * @param event The scroll event
   */
  @HostListener('window:scroll', ['$event'])
  onGlobalScroll(event: Event): void {
    const ngSelect = this.selectRef;
    if (ngSelect?.isOpen && ngSelect.dropdownPanel && this.selectContainerConfig?.containerSelector) {
      if (event.target !== ngSelect.dropdownPanel.scrollElementRef.nativeElement) {
        ngSelect.dropdownPanel.adjustPosition();
      }
    }
  }

  private selectTag(): void {
    if (!this.selectRef.itemsList.markedItem) {
      this.selectRef.selectTag();
    }
  }

  onCheckBoxClick($event: Event) {
    $event.preventDefault();
  }

  /**
   * Patch method for ng-select lib
   *
   * Scope:
   * - adds aria-multiselectable="true" for ng-select dropdown popup
   * - helps screen reader tell correct selection number
   */
  setupDropdown(): void {
    // setTimout needed because ng-select library emits open dropdown event but only after calls detectChanges
    setTimeout(() => {
      if (this.multiple) {
        const ngSelectDropdownId = `#${this.selectRef.dropdownId}`;
        const dropdownElement = document.querySelector(ngSelectDropdownId);

        if (dropdownElement) {
          dropdownElement.setAttribute('aria-multiselectable', 'true');
        }
      }
    });
  }
}
