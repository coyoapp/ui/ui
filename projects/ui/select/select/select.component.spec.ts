import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormFieldControlComponent } from '@coyoapp/ui/form-field';
import { SelectConfig } from '@coyoapp/ui/select';
import { SelectComponent } from './select.component';

describe('SelectComponent', () => {
  let component: SelectComponent<any>;
  let fixture: ComponentFixture<SelectComponent<any>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SelectComponent]
    }).overrideTemplate(SelectComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct compare function', () => {
    expect(component.compareWith(true, true)).toBeTrue();
    expect(component.compareWith(true, false)).toBeFalse();
  });

  it('should set correct config', () => {
    // check default values of config
    expect(component.config).toEqual({
      i18n: {
        addTag: 'Add item',
        clearAll: 'Clear all',
        clearItem: 'Clear item',
        loading: 'Loading…',
        notFound: 'No items found'
      }
    } as SelectConfig);

    component.config = {
      i18n: {
        addTag: 'test',
        clearAll: 'test',
        clearItem: 'test',
        loading: 'test',
        notFound: 'test'
      }
    };

    expect(component.config.i18n.addTag).toEqual('test');
  });

  it('should call open() on selectRef when calling open method', () => {
    (component as any).selectRef = jasmine.createSpyObj('NgSelectComponent', ['open']);
    component.open();

    expect((component as any).selectRef.open).toHaveBeenCalled();
  });

  it('should call close() on selectRef when calling close method', () => {
    (component as any).selectRef = jasmine.createSpyObj('NgSelectComponent', ['close']);
    component.close();

    expect((component as any).selectRef.close).toHaveBeenCalled();
  });

  it('should submit on submit keys and prevent default', () => {
    // given
    const selectRef = {
      itemsList: {
        markedItem: false
      },
      selectTag: jasmine.createSpy('select tag spy')
    };

    (component as any).selectRef = selectRef;
    component.addTag = true;
    component.multiple = true;
    component.submitKeys = [' '];

    // when
    const event = {
      code: 'space',
      key: ' ',
      preventDefault: jasmine.createSpy('prevent default spy')
    } as any as KeyboardEvent;
    component.onKeyDown(event);

    // then
    expect(selectRef.selectTag).toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not submit on not given submit key', () => {
    // given
    const selectRef = {
      itemsList: {
        markedItem: false
      },
      selectTag: jasmine.createSpy('select tag spy')
    };

    (component as any).selectRef = selectRef;
    component.addTag = true;
    component.multiple = true;
    component.submitKeys = [' '];

    // when
    component.onKeyDown({ code: 'a', key: 'a' } as KeyboardEvent);

    // then
    expect(selectRef.selectTag).not.toHaveBeenCalled();
  });

  it('should not submit when list is open', () => {
    // given
    const selectRef = {
      itemsList: {
        markedItem: true
      },
      selectTag: jasmine.createSpy('select tag spy')
    };

    (component as any).selectRef = selectRef;
    component.addTag = true;
    component.multiple = true;
    component.submitKeys = [' '];

    // when
    const event = {
      code: 'space',
      key: ' ',
      preventDefault: jasmine.createSpy('prevent default spy')
    } as any as KeyboardEvent;
    component.onKeyDown(event);

    // then
    expect(selectRef.selectTag).not.toHaveBeenCalled();
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should add default prefix on first keystroke', () => {
    // given
    const selectRef = {
      searchTerm: ''
    };

    (component as any).selectRef = selectRef;
    component.searchPrefix = '#';

    // when
    component.onKeyDown({ code: 'a', key: 'a' } as KeyboardEvent);

    // then
    expect(selectRef.searchTerm).toBe('#');
  });

  it('should register on touched', fakeAsync(() => {
    (component as any).selectRef = {
      registerOnTouched: () => {}
    };
    spyOn((component as any).selectRef, 'registerOnTouched');
    spyOn(FormFieldControlComponent.prototype, 'registerOnTouched');
    component.registerOnTouched(() => {});
    tick();

    expect(FormFieldControlComponent.prototype.registerOnTouched).toHaveBeenCalled();
    expect((component as any).selectRef.registerOnTouched).toHaveBeenCalled();
  }));

  it('should call appropriate methods onGlobalScroll call', () => {
    (component as any).selectRef = {
      registerOnTouched: () => {},
      isOpen: true,
      dropdownPanel: {
        adjustPosition: () => {},
        scrollElementRef: {
          nativeElement: {}
        }
      }
    };
    (component as any).selectContainerConfig = { containerSelector: true };
    spyOn((component as any).selectRef.dropdownPanel, 'adjustPosition');
    const event = new Event('scroll');
    component.onGlobalScroll(event);

    expect((component as any).selectRef.dropdownPanel.adjustPosition).toHaveBeenCalled();
  });

  it('should emit event and call appropriate methods onChange', fakeAsync(() => {
    (component as any).selectRef = {
      registerOnTouched: () => {},
      isOpen: true,
      dropdownPanel: {
        adjustPosition: () => {}
      }
    };
    spyOn(component.changeEvent, 'emit');
    spyOn((component as any).selectRef.dropdownPanel, 'adjustPosition');
    const event = [{ key: 1, label: 'Volvo1' }, { key: 3, label: 'Opel3' }, { key: 2, label: 'Saab2' }];
    component.onChange(event);
    tick();

    expect(component.changeEvent.emit).toHaveBeenCalledWith(event);
    expect((component as any).selectRef.dropdownPanel.adjustPosition).toHaveBeenCalled();
  }));

  it('should call preventDefault when calling onCheckBoxClick method', () => {
    const event = new Event('click', { bubbles: true });
    spyOn(event, 'preventDefault');
    component.onCheckBoxClick(event);

    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should add aria-multiselectable="true" attribute for ng-select dropdown popup', fakeAsync(() => {
    const test = 'test';
    component.multiple = true;
    (component as any).selectRef = {
      dropdownId: test
    };
    const setAttributeSpy = jasmine.createSpy().and.callFake(() => {});
    spyOn(document, 'querySelector').and.returnValue({
      setAttribute: setAttributeSpy
    } as any);
    component.setupDropdown();

    tick();
    fixture.detectChanges();

    const ngSelectDropdownId = `#${test}`;

    expect(document.querySelector).toHaveBeenCalledWith(ngSelectDropdownId);
    expect(setAttributeSpy).toHaveBeenCalledWith('aria-multiselectable', 'true');
  }));

  it('should not add aria-multiselectable="true" attribute when multiple is not set', fakeAsync(() => {
    component.multiple = false;
    spyOn(document, 'querySelector').and.callFake(() => {});
    component.setupDropdown();

    tick();
    fixture.detectChanges();

    expect(document.querySelector).not.toHaveBeenCalled();
  }));

  it('should emit tagAdded when option is added', () => {
    // given
    const items:string[] = ['1', '2', '3'];
    (component as any).selectRef = {
      select: jasmine.createSpy('select tag spy').and.callFake(() =>
        spyOn(fixture.componentInstance, 'tagAdded')).call(items[0])
    };
    component.multiple = true;
    component.items = items;

    // when
    (component as any).selectRef.select(component.items[0]);

    // then
    expect(component.tagAdded).toHaveBeenCalledWith(items[0]);
    expect((component as any).selectRef.select).toHaveBeenCalled();
  });

  it('should emit tagRemoved when option is removed', () => {
    // given
    const items:string[] = ['1', '2', '3'];
    (component as any).selectRef = {
      unselect: jasmine.createSpy('remove tag spy').and.callFake(() =>
        spyOn(fixture.componentInstance, 'tagRemoved')).call(items[0])
    };
    component.multiple = true;
    component.items = items;

    // when
    (component as any).selectRef.unselect(component.items[0]);

    // then
    expect(component.tagRemoved).toHaveBeenCalledWith(items[0]);
    expect((component as any).selectRef.unselect).toHaveBeenCalled();
  });

  it('should emit focusChanged', () => {
    // given
    const items:string[] = ['1', '2', '3'];
    (component as any).selectRef = {
      blur: jasmine.createSpy('select blur spy').and.callFake(() =>
        spyOn(fixture.componentInstance, 'focusChanged')).call(items[0])
    };
    component.items = items;

    // when
    (component as any).selectRef.blur();

    // then
    expect(component.focusChanged).toHaveBeenCalled();
    expect((component as any).selectRef.blur).toHaveBeenCalled();
  });
});
