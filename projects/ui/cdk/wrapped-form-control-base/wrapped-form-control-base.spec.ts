import { ElementRef, Injector } from '@angular/core';
import { FormControlBase, WrappedFormControlBase } from '@coyoapp/ui/cdk';
import createSpyObj = jasmine.createSpyObj;

class WrappedFormControlBaseTest<T> extends WrappedFormControlBase<T> {
  get focusElementRef(): ElementRef<HTMLElement> {
    return { nativeElement: {} } as ElementRef;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}

describe('WrappedFormControlBase', () => {
  let component: WrappedFormControlBaseTest<HTMLElement>;

  beforeEach(async () => {
    component = new WrappedFormControlBaseTest(createSpyObj('Injector', ['get']));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set value on form control when incomingValues changes', () => {
    spyOn(component.formControl, 'setValue');

    expect((component as any).incomingValues).toBeTruthy();
    (component as any).incomingValues.next('test');

    expect(component.formControl.setValue).toHaveBeenCalled();
  });

  it('should emit value for outgoing changes', () => {
    spyOn(component, 'emitOutgoingValue');
    (component.formControl as any).valueChanges.next('ok');

    expect(component.emitOutgoingValue).toHaveBeenCalled();
  });

  it('should call onTouched when markAsTouched emits', () => {
    spyOn(component, 'onTouched');
    spyOn((component as any).touched, 'next');
    (component.formControl as any).markAsTouched();

    expect(component.onTouched).toHaveBeenCalledWith();
    spyOn(component, 'emitOutgoingValue');
  });

  it('should emit value when calling handleIncomingValue method', () => {
    spyOn((component as any).incomingValues, 'next');
    component.handleIncomingValue({} as HTMLElement);

    expect((component as any).incomingValues.next).toHaveBeenCalled();
  });

  it('should disable form control when calling setDisabledState method', () => {
    spyOn(component.formControl, 'disable');
    spyOn(component.formControl, 'enable');
    spyOn(FormControlBase.prototype, 'setDisabledState');
    component.setDisabledState(false);

    expect(component.formControl.enable).toHaveBeenCalled();
    component.setDisabledState(true);

    expect(component.formControl.disable).toHaveBeenCalled();
    expect(FormControlBase.prototype.setDisabledState).toHaveBeenCalled();
  });
});
