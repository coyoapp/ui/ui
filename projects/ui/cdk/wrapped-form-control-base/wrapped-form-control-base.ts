import { Injector } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControlBase } from '../form-control-base/form-control-base';

export abstract class WrappedFormControlBase<OuterType, InnerType = OuterType>
  extends FormControlBase<OuterType> {
  private incomingValues = new Subject<OuterType>();
  private touched = new Subject<boolean>();

  /** Bind this to your inner form control to make all the magic happen. */
  formControl = new UntypedFormControl();

  /** An observable that emits an event every time the control is touched. */
  touched$: Observable<boolean>;

  constructor(injector: Injector) {
    super(injector);
    this.touched$ = this.touched.asObservable()
      .pipe(startWith(this.formControl.touched));
    this.setUpOuterToInner$(this.incomingValues)
      .subscribe(inner => this.formControl.setValue(inner, { emitEvent: false }));
    this.setUpInnerToOuter$(this.formControl.valueChanges)
      .subscribe(outer => this.emitOutgoingValue(outer));
    this.formControl.markAsTouched = () => {
      this.touched.next(true);
      this.onTouched();
    };
  }

  /** Called as angular propagates values changes to this `ControlValueAccessor`. You normally do not need to use it. */
  handleIncomingValue(outer: OuterType): void {
    this.incomingValues.next(outer);
  }

  /** Called as angular propagates disabled changes to this `ControlValueAccessor`. You normally do not need to use it. */
  setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.formControl.disable({ emitEvent: false });
    } else {
      this.formControl.enable({ emitEvent: false });
    }
    super.setDisabledState(this.isDisabled);
  }

  /**
   * Override this to modify a value coming from the outside to the format needed within this component.
   *
   * For more complex needs, see {@link #setUpOuterToInner$} instead.
   */
  protected outerToInner(outer: OuterType): InnerType {
    return outer as unknown as InnerType;
  }

  /**
   * Override this to for full control over the stream of values passed in to your subclass.
   *
   * In this example, incoming values are debounced before being passed through to the inner form control
   * ```ts
   * setUpOuterToInner$(outer$: Observable<OuterType>): Observable<InnerType> {
   *   return values$.pipe(
   *     debounce(300),
   *     map((outer) => doExpensiveTransformToInnerValue(outer))
   *   );
   * }
   * ```
   *
   * For a simple transformation, see {@link #outerToInner} instead.
   */
  protected setUpOuterToInner$(outer$: Observable<OuterType>): Observable<InnerType> {
    return outer$.pipe(map(outer => this.outerToInner(outer)));
  }

  /**
   * Override this to modify a value coming from within this component to the format expected on the outside.
   *
   * For more complex needs, see {@link #setUpInnerToOuter$} instead.
   */
  protected innerToOuter(inner: InnerType): OuterType {
    return inner as unknown as OuterType;
  }

  /**
   * Override this to for full control over the stream of values emitted from your subclass.
   *
   * In this example, illegal values are not emitted
   * ```ts
   * setUpInnerToOuter$(inner$: Observable<InnerType>): Observable<OuterType> {
   *   return values$.pipe(
   *     debounce(300),
   *     filter((inner) => isLegalValue(outer))
   *   );
   * }
   * ```
   *
   * For a simple transformation, see {@link #innerToOuter} instead.
   */
  protected setUpInnerToOuter$(inner$: Observable<InnerType>): Observable<OuterType> {
    return inner$.pipe(map(inner => this.innerToOuter(inner)));
  }
}
