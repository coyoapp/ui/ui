import { FocusKeyManager } from '@angular/cdk/a11y';
import { InjectionToken } from '@angular/core';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const CUI_FOCUS_KEY_MANAGER_PROVIDER = new InjectionToken<FocusKeyManagerProvider<any>>('cuiFocusKeyManagerProvider');

export interface FocusKeyManagerProvider<T> {
  keyManager?: FocusKeyManager<T>;
}
