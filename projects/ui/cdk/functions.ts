// eslint-disable-next-line @typescript-eslint/no-empty-function
export function noop(): void {}

export function isNil<T>(value: T | null | undefined): boolean {
  return value == null;
}

export function clamp(number: number, lower?: number | null, upper?: number | null): number {
  if (upper != null) {
    number = number <= upper ? number : upper;
  }
  if (lower != null) {
    number = number >= lower ? number : lower;
  }
  return number;
}
