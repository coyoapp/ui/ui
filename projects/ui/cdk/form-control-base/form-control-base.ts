import { ChangeDetectorRef, Injector } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { noop } from '../functions';

export abstract class FormControlBase<T> implements ControlValueAccessor {

  protected readonly changeDetectorRef: ChangeDetectorRef;

  readonly ngControl: NgControl | null;

  /** Call this to emit a new value when it changes. */
  emitOutgoingValue: (value: T) => void = noop;

  /** Call this to "commit" a change, traditionally done e.g. on blur. */
  onTouched = noop;

  /** You can bind to this in your template as needed. */
  isDisabled = false;

  /** Implement this to handle a new value coming in from outside. */
  abstract handleIncomingValue(value: T): void;

  constructor(injector: Injector) {
    this.changeDetectorRef = injector.get(ChangeDetectorRef);
    this.ngControl = injector.get(NgControl, null);
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  /** Called as angular sets up the binding to this `ControlValueAccessor`. You normally do not need to use it. */
  registerOnChange(fn: (value: T) => void): void {
    this.emitOutgoingValue = fn;
  }

  /** Called as angular sets up the binding to this `ControlValueAccessor`. You normally do not need to use it. */
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  /** Called as angular propagates value changes to this `ControlValueAccessor`. You normally do not need to use it. */
  writeValue(value: T): void {
    this.handleIncomingValue(value);
    this.changeDetectorRef.markForCheck();
  }

  /** Called as angular propagates disabled changes to this `ControlValueAccessor`. You normally do not need to use it. */
  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.changeDetectorRef.markForCheck();
  }
}
