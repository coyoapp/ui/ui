import { ChangeDetectorRef, ElementRef, Injector } from '@angular/core';
import { FormControlBase, noop } from '@coyoapp/ui/cdk';

class FormControlBaseTest<T> extends FormControlBase<T> {
  get focusElementRef(): ElementRef<HTMLElement> {
    return { nativeElement: {} } as ElementRef;
  }

  handleIncomingValue() {
    // empty stub
  }

  constructor() {
    super({
      get: () => ({ valueAccessor: false })
    } as Injector);
  }
}

describe('FormControlBase', () => {
  let component: FormControlBaseTest<HTMLElement>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    component = new FormControlBaseTest();
    (component as any).changeDetectorRef = changeDetectorRef;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the ngControl value set', () => {
    expect(component.ngControl).toBeTruthy();
  });

  it('should add emitOutgoingValue callback when registerOnChange method is called', () => {
    component.registerOnChange(noop);

    expect(component.emitOutgoingValue).toBeTruthy();
  });

  it('should add emitOutgoingValue callback when registerOnTouched method is called', () => {
    component.registerOnTouched(noop);

    expect(component.onTouched).toBeTruthy();
  });

  it('should call appropriate methods when writeValue method is called', () => {
    spyOn(component, 'handleIncomingValue');
    component.writeValue({} as HTMLElement);

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component.handleIncomingValue).toHaveBeenCalled();
  });

  it('should set disabled state when setDisabledState method is called', () => {
    component.setDisabledState(true);

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    expect(component.isDisabled).toBeTrue();
  });
});
