import {
  ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation
} from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

/**
 * An SVG icon.
 *
 * `cui-icon` makes it easier to use vector-based icons. All icons must be
 * registered vie the `SvgIconRegistry`. The COYO icon set is registered by
 * default.
 *
 * Icons inherit the current <b>font color</b> and <b>size</b> by default.
 * Thus, they can be styled using the `font-size` and `color` properties.
 * The `size` input can also be used to quickly adjust the size of an icon.
 */
@Component({
  selector: 'cui-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-icon'
  }
})
export class IconComponent {

  /**
   * The name of the icon.
   */
  @Input()
  name: coyoIcon = 'emblem-coyo';

  /**
   * The size of the icon.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 'xs' | 's' | 'm' | 'l' | 'xl' | 'inline' = 'm';
}
