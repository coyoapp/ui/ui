import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ci } from '@coyoapp/icons';
import { provideSvgIconsConfig, SvgIconComponent } from '@ngneat/svg-icon';
import { IconComponent } from './icon/icon.component';

export function filter([name, data]: [string, string]): { name: string, data: string } {
  return ({ name, data });
}

@NgModule({
  imports: [
    CommonModule,
    SvgIconComponent
  ],
  declarations: [
    IconComponent
  ],
  exports: [
    IconComponent
  ]
})
export class IconModule {

  static forRoot(): ModuleWithProviders<IconModule> {
    return {
      ngModule: IconModule,
      providers: [provideSvgIconsConfig({
        icons: Object.entries(ci).map(filter),
        defaultSize: 'md',
        sizes: { md: 'inherit' }
      })]
    };
  }
}
