import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PortalModule } from '@angular/cdk/portal';
import { LoadingModule } from '@coyoapp/ui/loading';
import { IconModule } from '@coyoapp/ui/icon';
import { FormFieldComponent } from './form-field/form-field.component';
import { FormHintDirective } from './form-hint/form-hint.directive';
import { FormPrefixDirective } from './form-prefix/form-prefix.directive';
import { FormSuffixDirective } from './form-suffix/form-suffix.directive';
import { FormFieldErrorPipe } from './form-field-error/form-field-error.pipe';
import { FormInfoDirective } from './form-info/form-info.directive';
import { FormFieldLabelComponent } from './form-field-label/form-field-label.component';
import {
  FormSelfSaveDirective,
  PortalErrorComponent,
  PortalLoadingComponent,
  PortalSuccessComponent
} from './form-self-save/form-self-save.directive';

@NgModule({
  imports: [
    CommonModule,
    PortalModule,
    LoadingModule,
    IconModule
  ],
  declarations: [
    FormFieldComponent,
    FormHintDirective,
    FormPrefixDirective,
    FormSuffixDirective,
    FormInfoDirective,
    FormFieldErrorPipe,
    FormFieldLabelComponent,
    FormSelfSaveDirective,
    PortalLoadingComponent,
    PortalSuccessComponent,
    PortalErrorComponent
  ],
  exports: [
    FormFieldComponent,
    FormHintDirective,
    FormPrefixDirective,
    FormSuffixDirective,
    FormInfoDirective,
    FormFieldLabelComponent,
    FormSelfSaveDirective
  ]
})
export class FormFieldModule {}
