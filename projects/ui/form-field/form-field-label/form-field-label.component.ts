import {
  ChangeDetectionStrategy, Component, Input, ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'cui-form-field-label',
  templateUrl: './form-field-label.component.html',
  styleUrls: ['./form-field-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-form-field-label'
  }
})
export class FormFieldLabelComponent {

  /**
   * Specifies the id of the form element the label should be bound to
   */
  @Input()
  for?: string;

  /**
   * Specifies which form the label belongs to
   */
  @Input()
  form?: string;
}
