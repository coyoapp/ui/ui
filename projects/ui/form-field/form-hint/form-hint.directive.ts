import { Directive, InjectionToken } from '@angular/core';

export const CUI_FORM_HINT = new InjectionToken<FormHintDirective>('FormHint');

@Directive({
  selector: '[cuiFormHint]',
  providers: [{ provide: CUI_FORM_HINT, useExisting: FormHintDirective }]
})
export class FormHintDirective {}
