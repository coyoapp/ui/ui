import { Directive, InjectionToken } from '@angular/core';

export const CUI_FORM_SUFFIX = new InjectionToken<FormSuffixDirective>('FormSuffix');

@Directive({
  selector: '[cuiFormSuffix]',
  providers: [{ provide: CUI_FORM_SUFFIX, useExisting: FormSuffixDirective }]
})
export class FormSuffixDirective {}
