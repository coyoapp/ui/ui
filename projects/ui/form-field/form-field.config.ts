import { InjectionToken } from '@angular/core';

export interface FormErrorMessages {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [index: string]: string | ((context: any) => string)
}

export interface FormFieldConfig {
  readonly i18n: Readonly<FormErrorMessages>
}

export const CUI_FORM_FIELD_DEFAULT_CONFIG: FormFieldConfig = {
  i18n: {
    min: context => `This value must be bigger than ${context.min}.`,
    max: context => `This value must be smaller than ${context.max}.`,
    required: 'This is required.',
    requiredtrue: 'This must be true.',
    email: 'This must be a valid e-mail address.',
    minlength: context => `This field requires at least ${context.requiredLength} characters.`,
    maxlength: context => `This field must be shorter than ${context.requiredLength} characters.`,
    pattern: 'This field contains an invalid format.'
  }
};

export const CUI_FORM_FIELD_CONFIG = new InjectionToken<FormFieldConfig>('CUI_FORM_FIELD_OPTIONS', {
  factory: () => CUI_FORM_FIELD_DEFAULT_CONFIG
});
