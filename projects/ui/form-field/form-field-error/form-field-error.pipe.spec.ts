import { FormFieldErrorPipe } from './form-field-error.pipe';

describe('FormFieldErrorPipe', () => {
  let pipe: FormFieldErrorPipe;
  beforeEach(() => {
    pipe = new FormFieldErrorPipe(null as any);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should show the correct error message', () => {
    const errors = {
      test: 'context error'
    };
    (pipe as any).options = {
      i18n: {}
    };
    const result1 = pipe.transform('test', 'context', errors);

    expect(result1).toBe('context error');

  });

  it('should show the correct error message when sending function', () => {
    (pipe as any).options = {
      i18n: {
        test: () => 'function error'
      }
    };
    const result1 = pipe.transform('test', 'context');

    expect(result1).toBe('function error');

  });

  it('return Error when no other error message exists', () => {
    (pipe as any).options = {
      i18n: {}
    };
    spyOn(console, 'warn');
    const result1 = pipe.transform('test', 'context');

    expect(result1).toBe('Error');
    expect(console.warn).toHaveBeenCalled();

  });
});
