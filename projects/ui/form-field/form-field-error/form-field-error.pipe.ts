import { Inject, Pipe, PipeTransform } from '@angular/core';
import { CUI_FORM_FIELD_CONFIG, FormErrorMessages, FormFieldConfig } from '../form-field.config';

@Pipe({
  name: 'formFieldError'
})
export class FormFieldErrorPipe implements PipeTransform {

  constructor(
    @Inject(CUI_FORM_FIELD_CONFIG) private readonly options: FormFieldConfig
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/explicit-module-boundary-types
  transform(validator: string, context: any, errors?: FormErrorMessages): string {
    const message = errors?.[validator] || this.options.i18n[validator];
    if (!message) {
      console.warn(`[FormFieldErrorPipe] Could not find message for "${validator}"`);
      return 'Error';
    }

    return message instanceof Function
      ? message(context)
      : message;
  }
}
