import { inject } from '@angular/core/testing';
import {
  CUI_FORM_FIELD_CONFIG,
  CUI_FORM_FIELD_DEFAULT_CONFIG,
  FormFieldConfig
} from '@coyoapp/ui/form-field/form-field.config';

describe('FormFieldConfig', () => {
  it('should provide a default config', inject([CUI_FORM_FIELD_CONFIG], (config: FormFieldConfig) => {
    expect(config).toBe(CUI_FORM_FIELD_DEFAULT_CONFIG);
  }));

  it('should generate correct min i18n messages', inject([CUI_FORM_FIELD_CONFIG], (config: FormFieldConfig) => {
    const func = config.i18n.min as any;
    const result = func({ min: 'test' });

    expect(result).toBe('This value must be bigger than test.');
  }));

  it('should generate correct max i18n messages', inject([CUI_FORM_FIELD_CONFIG], (config: FormFieldConfig) => {
    const func = config.i18n.max as any;
    const result = func({ max: 'test' });

    expect(result).toBe('This value must be smaller than test.');
  }));

  it('should generate correct minlength i18n messages', inject([CUI_FORM_FIELD_CONFIG], (config: FormFieldConfig) => {
    const func = config.i18n.minlength as any;
    const result = func({ requiredLength: 'test' });

    expect(result).toBe('This field requires at least test characters.');
  }));

  it('should generate correct maxlength i18n messages', inject([CUI_FORM_FIELD_CONFIG], (config: FormFieldConfig) => {
    const func = config.i18n.maxlength as any;
    const result = func({ requiredLength: 'test' });

    expect(result).toBe('This field must be shorter than test characters.');
  }));
});
