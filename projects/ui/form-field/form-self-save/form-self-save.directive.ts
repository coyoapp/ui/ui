import {
  AfterViewInit,
  ApplicationRef,
  Component,
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Inject,
  InjectionToken,
  Injector,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { ComponentPortal, DomPortalOutlet, PortalOutlet } from '@angular/cdk/portal';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { coyoIcon } from '@coyoapp/icons';

/**
 * COYO-UI SelfSave Directive
 *
 * @description
 * This directive allows the form field control to auto save on value change, given an input save function
 * that returns an Observable monitors the save backend call and returns the result.
 *
 * Directive Inputs
 * saveFn - the save callback function that has to return an observable
 * saveDebounceTime - debounce time before saving new value
 * successIcon - Icon to show on successful save
 * errorIcon - Icon to show when there is an error
 *
 * @example
 * <cui-form-field
 *   label="Self Saving Number Field"
 *   cuiFormSelfSave
 *   [successIcon]="'chevron-left'"
 *   [errorIcon]="'chevron-right'"
 *   [saveDebounceTime]="3000"
 *   [saveFn]="backendSaveSimulation('number')">
 *     <cui-input-number [formControl]="selfSavingControl"></cui-input-number>
 * </cui-form-field>
 */

export const CUI_FORM_SELF_SAVE = new InjectionToken<FormSelfSaveDirective>('FormSelfSave');

@Directive({
  selector: '[cuiFormSelfSave]',
  providers: [{ provide: CUI_FORM_SELF_SAVE, useExisting: FormSelfSaveDirective }]
})
export class FormSelfSaveDirective implements OnDestroy, OnInit, AfterViewInit {
  /**
   * Save callback function
   */
  @Input() saveFn!: () => Observable<unknown>;

  /**
   * Debounce time before calling save function
   */
  @Input() saveDebounceTime = 1000;

  /**
   * Success Icon
   */
  @Input() successIcon: coyoIcon = 'check';

  /**
   * Error Icon
   */
  @Input() errorIcon: coyoIcon = 'close';

  portalHost?: PortalOutlet;
  inputElement?: HTMLInputElement;
  inputValue: unknown;
  componentPortalLoading: ComponentPortal<PortalLoadingComponent>;
  componentPortalSuccess: ComponentPortal<PortalSuccessComponent>;
  componentPortalError: ComponentPortal<PortalErrorComponent>;
  valueChangedSubject: Subject<unknown> = new Subject();
  changeValueSubscription?: Subscription;

  constructor(
    private elRef: ElementRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef,
    @Inject(DOCUMENT) private document: Document
  ) {
    this.componentPortalSuccess = new ComponentPortal(PortalSuccessComponent);
    this.componentPortalLoading = new ComponentPortal(PortalLoadingComponent);
    this.componentPortalError = new ComponentPortal(PortalErrorComponent);
  }

  ngOnInit(): void {
    // subscribe to value changes coming from the form control inside the form field component
    this.valueChangedSubject.asObservable()
      .pipe(debounceTime(this.saveDebounceTime))
      .pipe(distinctUntilChanged())
      .subscribe(() => {
        if (this.saveFn) {
          this.addLoader();
          const changeObservable: Observable<unknown> = this.saveFn();
          this.changeValueSubscription = changeObservable.subscribe(
            () => this.handleSuccessCase(),
            () => this.handleErrorCase()
          );
        }
      });
  }

  ngAfterViewInit(): void {
    this.inputElement = this.inputElement ?? <HTMLInputElement> this.elRef.nativeElement.querySelector('input') ?? <HTMLInputElement> this.elRef.nativeElement.querySelector('textarea');
    if (this.inputElement) {
      const inputID = this.inputElement.id;
      // register on change event on the input field
      this.inputElement.addEventListener('change', (event: Event) => {
        this.inputChanged(event);
      });

      // get portal element from within form-field based on ID
      const portalElement = this.document.querySelector(`#portal-${inputID}`);

      // create portal host
      if (!this.portalHost && portalElement) {
        this.portalHost = new DomPortalOutlet(
          portalElement,
          this.componentFactoryResolver,
          this.appRef,
          this.injector
        );
      }
    }
  }

  inputChanged(event: Event): void {
    if (event?.target) {
      const inputValue = (event.target as HTMLInputElement).value;
      if (inputValue !== this.inputValue) {
        this.inputValue = inputValue;
        this.valueChangedSubject.next(this.inputValue);
      }
    }
  }

  ngOnDestroy(): void {
    this.removeAll();
    this.changeValueSubscription?.unsubscribe();
    this.inputElement?.removeEventListener('change', (event: Event) => {
      this.inputChanged(event);
    });
  }

  /**
   * On successful save show the success component inside the portal
   */
  handleSuccessCase(): void {
    this.addSuccess();
    setTimeout(() => {
      this.removeAll();
    }, 1000);
  }

  /**
   * Show the loading animation
   */
  addLoader(): void {
    this.removeAll();
    if (this.componentPortalLoading) {
      this.portalHost?.attach(this.componentPortalLoading);
    }
  }

  /**
   * Adds the success component to the portal
   */
  addSuccess(): void {
    this.removeAll();
    if (this.componentPortalSuccess) {
      const componentRef = this.portalHost?.attach(this.componentPortalSuccess);
      componentRef.instance.iconName = this.successIcon;
    }
  }

  /**
   * Removes the attached components from the portal
   */
  removeAll(): void {
    this.portalHost?.detach();
  }

  /**
   * Show the error component on error
   */
  handleErrorCase(): void {
    this.removeAll();
    if (this.componentPortalError) {
      const componentRef = this.portalHost?.attach(this.componentPortalError);
      componentRef.instance.iconName = this.errorIcon;
    }
    setTimeout(() => {
      this.removeAll();
    }, 1000);
  }
}

@Component({
  selector: 'cui-loading-component-portal',
  template: '<cui-spinner class="cui-form-portal-loading" size="s"></cui-spinner>'
})
export class PortalLoadingComponent {}

@Component({
  selector: 'cui-success-component-portal',
  template: '<cui-icon class="cui-form-portal-loading" [name]="iconName"></cui-icon>'
})
export class PortalSuccessComponent {
  @Input() iconName: coyoIcon = 'check';
}

@Component({
  selector: 'cui-error-component-portal',
  template: '<cui-icon class="cui-form-portal-error" [name]="iconName"></cui-icon>'
})
export class PortalErrorComponent {
  @Input() iconName: coyoIcon = 'close';
}
