import {
  ApplicationRef, ComponentFactoryResolver, ElementRef, Injector
} from '@angular/core';
import { Subject } from 'rxjs';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TabsComponent } from '@coyoapp/ui/tabs';
import { IconModule } from '@coyoapp/ui/icon';
import { LoadingModule } from '@coyoapp/ui/loading';
import {
  FormSelfSaveDirective,
  PortalErrorComponent,
  PortalLoadingComponent,
  PortalSuccessComponent
} from './form-self-save.directive';
import createSpyObj = jasmine.createSpyObj;

describe('SelfSaveDirective', () => {
  let directive: FormSelfSaveDirective;
  let nativeElement: HTMLElement;
  let elementRef: ElementRef;
  let componentResolver: jasmine.SpyObj<ComponentFactoryResolver>;
  let applicationRef: jasmine.SpyObj<ApplicationRef>;
  let injector: jasmine.SpyObj<Injector>;
  let document: jasmine.SpyObj<Document>;
  let componentLoading: PortalLoadingComponent;
  let componentSuccess: PortalLoadingComponent;
  let componentError: PortalLoadingComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PortalLoadingComponent, PortalSuccessComponent, PortalErrorComponent],
      imports: [IconModule.forRoot(), LoadingModule]
    }).overrideTemplate(TabsComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    nativeElement = {} as HTMLElement;
    elementRef = { nativeElement } as ElementRef;
    componentResolver = jasmine.createSpyObj('ComponentFactoryResolver', ['create']);
    applicationRef = jasmine.createSpyObj('ApplicationRef', ['create']);
    injector = jasmine.createSpyObj('Injector', ['get']);
    document = jasmine.createSpyObj('Document', ['querySelector']);
    directive = new FormSelfSaveDirective(elementRef, componentResolver, injector, applicationRef, document);
    componentLoading = TestBed.createComponent(PortalLoadingComponent).componentInstance;
    componentSuccess = TestBed.createComponent(PortalSuccessComponent).componentInstance;
    componentError = TestBed.createComponent(PortalErrorComponent).componentInstance;
  });

  it('should create an instance of the directive and components', () => {
    expect(directive).toBeTruthy();
    expect(componentLoading).toBeTruthy();
    expect(componentSuccess).toBeTruthy();
    expect(componentError).toBeTruthy();
  });

  it('should register for value changes on ngOnInit', fakeAsync(() => {
    const valueSubj = new Subject();
    const saveFnSub = new Subject();
    directive.saveFn = () => saveFnSub.asObservable();
    spyOn(directive.valueChangedSubject, 'asObservable').and.returnValue(valueSubj.asObservable());
    spyOn(directive, 'addLoader');
    spyOn(directive, 'handleSuccessCase');
    spyOn(directive, 'handleErrorCase');
    directive.ngOnInit();
    valueSubj.next(true);
    tick(directive.saveDebounceTime);

    expect(directive.saveFn).toBeTruthy();
    expect(directive.addLoader).toHaveBeenCalled();
    expect(directive.valueChangedSubject.asObservable).toHaveBeenCalled();
    saveFnSub.next(true);
    tick();

    expect(directive.handleSuccessCase).toHaveBeenCalled();
    expect(directive.handleErrorCase).not.toHaveBeenCalled();
  }));

  it('should register change event on ngAfterViewInit', () => {
    directive.inputElement = createSpyObj('HTMLInputElement', ['addEventListener']);
    directive.ngAfterViewInit();

    expect(directive.inputElement?.addEventListener).toHaveBeenCalled();
    expect((directive as any).document.querySelector).toHaveBeenCalled();
  });

  it('should change input value on inputChanged', () => {
    const event = { target: { value: true } as unknown } as Event;
    directive.inputChanged(event);

    expect(directive.inputValue).toEqual(true);
  });

  it('should unsubscribe and remove portal on ngOnDestroy', () => {
    spyOn(directive, 'removeAll');
    directive.changeValueSubscription = createSpyObj('Subscription', ['unsubscribe']);
    directive.ngOnDestroy();

    expect(directive.changeValueSubscription?.unsubscribe).toHaveBeenCalled();
    expect(directive.removeAll).toHaveBeenCalled();
  });

  it('should call success method on handleSuccessCase', fakeAsync(() => {
    spyOn(directive, 'removeAll');
    spyOn(directive, 'addSuccess');
    directive.handleSuccessCase();

    expect(directive.addSuccess).toHaveBeenCalled();
    tick(1000);

    expect(directive.removeAll).toHaveBeenCalled();
  }));

  it('should add loader component on addLoader call', () => {
    spyOn(directive, 'removeAll');
    directive.portalHost = createSpyObj('PortalOutlet', ['attach']);
    directive.addLoader();

    expect(directive.portalHost?.attach).toHaveBeenCalled();
    expect(directive.removeAll).toHaveBeenCalled();
  });

  it('should add success component on addSuccess call', () => {
    const portalHost = jasmine.createSpyObj('PortalOutlet', ['attach']);
    const componentRef = { instance: { iconName: '' } };
    portalHost.attach.and.returnValue(componentRef);
    spyOn(directive, 'removeAll');
    directive.portalHost = portalHost;
    directive.addSuccess();

    expect(directive.portalHost?.attach).toHaveBeenCalled();
    expect(directive.removeAll).toHaveBeenCalled();
    expect(componentRef.instance.iconName).toEqual('check');
  });

  it('should detach portal on removeAll', () => {
    const portalHost = jasmine.createSpyObj('PortalOutlet', ['detach']);
    directive.portalHost = portalHost;
    directive.removeAll();

    expect(directive.portalHost?.detach).toHaveBeenCalled();
  });

  it('should handle error case on handleErrorCase call', fakeAsync(() => {
    const portalHost = jasmine.createSpyObj('PortalOutlet', ['attach']);
    const componentRef = { instance: { iconName: '' } };
    portalHost.attach.and.returnValue(componentRef);
    spyOn(directive, 'removeAll');
    directive.portalHost = portalHost;
    directive.handleErrorCase();

    expect(directive.portalHost?.attach).toHaveBeenCalled();
    expect(directive.removeAll).toHaveBeenCalled();
    expect(componentRef.instance.iconName).toEqual('close');
    tick(1000);

    expect(directive.removeAll).toHaveBeenCalled();
  }));

  it('should have correct default icons in components', () => {
    expect((componentSuccess as any).iconName).toEqual('check');
    expect((componentError as any).iconName).toEqual('close');
  });

});
