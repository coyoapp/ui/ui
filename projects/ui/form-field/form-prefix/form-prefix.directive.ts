import { Directive, InjectionToken } from '@angular/core';

export const CUI_FORM_PREFIX = new InjectionToken<FormPrefixDirective>('FormPrefix');

@Directive({
  selector: '[cuiFormPrefix]',
  providers: [{ provide: CUI_FORM_PREFIX, useExisting: FormPrefixDirective }]
})
export class FormPrefixDirective {}
