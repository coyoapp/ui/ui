import {
  AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ContentChildren, HostBinding, Input, QueryList, ViewEncapsulation
} from '@angular/core';
import { EMPTY, merge, Observable } from 'rxjs';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { FormFieldControl, FORM_FIELD } from '../form-field-control/form-field-control';
import { FormErrorMessages } from '../form-field.config';
import { CUI_FORM_HINT, FormHintDirective } from '../form-hint/form-hint.directive';
import { CUI_FORM_INFO } from '../form-info/form-info.directive';
import { CUI_FORM_PREFIX, FormPrefixDirective } from '../form-prefix/form-prefix.directive';
import { CUI_FORM_SUFFIX, FormSuffixDirective } from '../form-suffix/form-suffix.directive';

@Component({
  selector: 'cui-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    'class': 'cui-form-field',
    '[class.disabled]': 'control?.disabled'
  }
})
export class FormFieldComponent implements AfterContentInit {

  /**
   * The (auto-connected) label of the form field.
   */
  @Input()
  label = '';

  /**
   * The size of the form field.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 'm';

  /**
   * Wether to show an input count.
   */
  @Input()
  count = false;

  /**
   * The appearance mode of the form field.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  mode: 'primary' | 'secondary' | 'ghost' = 'primary';

  /**
   * Error message overrides.
   */
  @Input()
  errors?: FormErrorMessages;

  @ContentChildren(CUI_FORM_INFO, { descendants: true })
  infoChildren!: QueryList<FormPrefixDirective>;
  @ContentChildren(CUI_FORM_PREFIX, { descendants: true })
  prefixChildren!: QueryList<FormPrefixDirective>;
  @ContentChildren(CUI_FORM_SUFFIX, { descendants: true })
  suffixChildren!: QueryList<FormSuffixDirective>;
  @ContentChildren(CUI_FORM_HINT, { descendants: true })
  hintChildren!: QueryList<FormHintDirective>;

  @HostBinding('class.cui-form-field-touched')
  get touched(): boolean { return !!this.control?.ngControl?.touched; }
  @HostBinding('class.cui-form-field-invalid')
  get invalid(): boolean { return !!this.control?.ngControl?.invalid; }

  @HostBinding('class.cdk-focused')
  get cdkFocused(): boolean { return this.control?.focusOrigin != null; }
  @HostBinding('class.cdk-touch-focused')
  get cdkTouchFocused(): boolean { return this.control?.focusOrigin === 'touch'; }
  @HostBinding('class.cdk-mouse-focused')
  get cdkMouseFocused(): boolean { return this.control?.focusOrigin === 'mouse'; }
  @HostBinding('class.cdk-keyboard-focused')
  get cdkKeyboardFocused(): boolean { return this.control?.focusOrigin === 'keyboard'; }
  @HostBinding('class.cdk-program-focused')
  get cdkProgramFocused(): boolean { return this.control?.focusOrigin === 'program'; }

  @ContentChild(FORM_FIELD)
  private controlNonStatic: FormFieldControl | null = null;
  @ContentChild(FORM_FIELD, { static: true })
  private controlStatic: FormFieldControl | null = null;
  get control(): FormFieldControl | null {
    return this.controlNonStatic || this.controlStatic;
  }

  get count$(): Observable<number> {
    return (this.control?.ngControl?.valueChanges || EMPTY)
      .pipe(startWith(this.control?.ngControl?.value))
      .pipe(map(value => ('' + (value || '')).length))
      .pipe(distinctUntilChanged());
  }

  constructor(
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngAfterContentInit(): void {
    merge(
      this.control?.touched$ || EMPTY,
      this.control?.ngControl?.statusChanges || EMPTY,
      this.infoChildren.changes,
      this.prefixChildren.changes,
      this.suffixChildren.changes,
      this.hintChildren.changes
    ).subscribe(() => {
      this.changeDetectorRef.markForCheck();
    });
  }

  focus($event?: MouseEvent): void {
    $event?.preventDefault();
    this.control?.focus('mouse', { preventScroll: true });
  }
}
