import {
  ComponentFixture, fakeAsync, TestBed, tick
} from '@angular/core/testing';
import { Subject } from 'rxjs';
import { ChangeDetectorRef } from '@angular/core';
import { FormFieldComponent } from './form-field.component';
import createSpyObj = jasmine.createSpyObj;

describe('FormFieldComponent', () => {
  let component: FormFieldComponent;
  let fixture: ComponentFixture<FormFieldComponent>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    await TestBed.configureTestingModule({
      declarations: [FormFieldComponent]
    }).overrideTemplate(FormFieldComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFieldComponent);
    component = fixture.componentInstance;
    (component as any).changeDetectorRef = changeDetectorRef;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should run markForCheck control changes or control status changes', fakeAsync(() => {
    const subject = new Subject();
    (component as any).controlStatic = {
      touched$: subject.asObservable(),
      ngControl: {
        statusChanges: subject.asObservable()
      }
    };

    expect(component.control).toBeTruthy();
    component.ngAfterContentInit();
    subject.next('newEvent');
    tick();

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  }));

  it('should call appropriate functions when focus() is called', () => {
    const event = new MouseEvent('click', { bubbles: true });
    spyOn(event, 'preventDefault');
    (component as any).controlStatic = createSpyObj('FormFieldControl', ['focus']);

    expect(component.control).toBeTruthy();
    component.focus(event);

    expect(event.preventDefault).toHaveBeenCalled();
    expect((component as any).control.focus).toHaveBeenCalled();
  });

  it('should return count observable on count$ getter', fakeAsync(() => {
    const subject = new Subject();
    (component as any).controlStatic = {
      ngControl: {
        valueChanges: subject.asObservable(),
        value: 'test'
      }
    };
    const { count$ } = component;

    expect(count$).toBeTruthy();
    const countSub = count$.subscribe(val => {
      expect(val).toEqual(4);
    });
    tick();
    countSub.unsubscribe();
  }));
});
