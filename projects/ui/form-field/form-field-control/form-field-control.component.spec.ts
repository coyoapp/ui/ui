import { ChangeDetectorRef, ElementRef, Injector } from '@angular/core';
import { fakeAsync, tick } from '@angular/core/testing';
import { FormFieldControlComponent } from '@coyoapp/ui/form-field';
import { Subject } from 'rxjs';
import createSpyObj = jasmine.createSpyObj;

class FormFieldControlTest<T> extends FormFieldControlComponent<T> {
  get focusElementRef(): ElementRef<HTMLElement> {
    return { nativeElement: {} } as ElementRef;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}

describe('FormFieldControlComponent', () => {
  let component: FormFieldControlTest<HTMLElement>;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    component = new FormFieldControlTest(createSpyObj('Injector', ['get']));
    (component as any).cdRef = changeDetectorRef;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct id', () => {
    component.id = '1';

    expect((component as any)._id).toEqual('1');
    expect(component.id).toEqual((component as any)._id);
  });

  it('should disable form on disable method call', () => {
    (component as any).formControl = { disabled: false };

    expect(component.disabled).toEqual(false);
  });

  it('should return correct focus origin', () => {
    (component as any)._focusOrigin = createSpyObj('FocusOrigin', ['focus']);

    expect(component.focusOrigin).toBeTruthy();
  });

  it('should stop monitoring when onDestroy method is called', () => {
    (component as any).focusMonitor = createSpyObj('FocusMonitor', ['stopMonitoring']);
    component.ngOnDestroy();

    expect((component as any).focusMonitor.stopMonitoring).toHaveBeenCalled();
  });

  it('should set value to null on clear', () => {
    (component as any).value = true;
    component.clear();

    expect((component as any).value).toBeFalsy();
  });

  it('should call focusVia when calling focus method', () => {
    (component as any).focusMonitor = createSpyObj('FocusMonitor', ['focusVia']);
    component.focus();

    expect((component as any).focusMonitor.focusVia).toHaveBeenCalled();
  });

  it('should start monitoring after view init', fakeAsync(() => {
    const subj = new Subject();
    let runWasCalled = false;
    (component as any).ngZone = {
      run: (param: any) => {
        runWasCalled = true;
        if (typeof param === 'function') {
          param();
        }
      }
    };
    let monitorWasCalled = false;
    (component as any).focusMonitor = {
      monitor: () => {
        monitorWasCalled = true;
        return subj.asObservable();
      }
    };
    component.ngAfterViewInit();
    subj.next('ok');
    tick();

    expect(monitorWasCalled).toBeTruthy();
    expect(runWasCalled).toBeTruthy();

    expect((component as any)._focusOrigin).toBeTruthy();
    expect((component as any).cdRef.markForCheck).toHaveBeenCalled();
  }));

});
