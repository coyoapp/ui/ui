import { FocusOrigin } from '@angular/cdk/a11y';
import { InjectionToken, Provider, Type } from '@angular/core';
import { UntypedFormControl, NgControl } from '@angular/forms';
import { Observable } from 'rxjs';

export const FORM_FIELD = new InjectionToken<FormFieldControl>('FORM_FIELD');

/**
 * Use in the `providers` of a component that implements `FormFieldControl`
 * to reduce some boilerplate.
 *
 * @param type the type of the provided control
 * @returns the form field provider
 *
 * @example
 * ```ts
 * @Component({ providers: [provideFormFieldControl(MyFormControl)] }
 * class MyFormControl implements FormFieldControl {
 *   // ...
 * }
 * ```
 */
export function provideFormFieldControl(type: Type<FormFieldControl>): Provider {
  return {
    provide: FORM_FIELD,
    useExisting: type,
    multi: true
  };
}

export interface FormFieldControl {

  /** The unique ID of the control. */
  id: string;

  /** The origin of the current focus state. */
  focusOrigin: FocusOrigin;

  formControl: UntypedFormControl;

  ngControl: NgControl | null;

  touched$: Observable<boolean>;

  /**
   * A max length for the field.
   *
   * Unfortunately, there is no way to retrieve a list of registered validators
   * for a form field. Thus, we need to use this workaround to retrieve the max.
   * length defined for the field. A corresponding feature request is tracked at
   * https://github.com/angular/angular/issues/13461.
   */
  maxlength: number | null;

  /**
   *
   * @param origin
   * @param options
   */
  focus(origin: FocusOrigin, options?: FocusOptions): void;
}
