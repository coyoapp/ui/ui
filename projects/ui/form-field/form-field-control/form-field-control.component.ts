/* eslint-disable @angular-eslint/directive-class-suffix */
import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import {
  AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Injector, Input, NgZone, OnDestroy
} from '@angular/core';
import { isNil, WrappedFormControlBase } from '@coyoapp/ui/cdk';
import { FormFieldControl } from './form-field-control';

let nextUniqueId = 0;

@Directive()
export abstract class FormFieldControlComponent<T> extends WrappedFormControlBase<T> implements FormFieldControl, AfterViewInit, OnDestroy {
  private readonly _uid = `cui-control-${nextUniqueId++}`;
  protected readonly cdRef: ChangeDetectorRef;
  protected readonly ngZone: NgZone;
  protected readonly focusMonitor: FocusMonitor;

  /**
   * The native HTML element that receives focus upon interaction.
   */
  protected abstract get focusElementRef(): ElementRef<HTMLElement> | undefined;

  /**
   * The ID of this form element.
   */
  @Input()
  get id(): string { return this._id; }
  set id(value: string) { this._id = value || this._uid; }
  private _id: string = this._uid;

  /**
   * Whether this form element is required.
   */
  @Input()
  required = false;

  /**
   * Specifies a short hint that describes the expected value of this form element.
   */
  @Input()
  placeholder = '';

  /**
   * The autocomplete attribute of the input.
   */
  @Input()
  autocomplete?: string;

  /**
   * The tab index of the input.
   */
  @Input()
  tabIndex?: number;

  /**
   * Id of the describing element
   */
  @Input()
  describedBy?: string;

  /**
   * The current value of this form element.
   */
  get value(): T | null { return this.formControl.value; }
  set value(value: T | null) { this.formControl.setValue(value); }

  /**
   * Whether this form element has a value.
   */
  get hasValue(): boolean {
    return !isNil(this.value);
  }

  /**
   * Whether this form element is disabled.
   */
  get disabled(): boolean {
    return this.formControl.disabled;
  }

  /**
   * The focus origin of the native HTML element.
   *
   * @see https://material.angular.io/cdk/a11y/overview#focusmonitor
   */
  get focusOrigin(): FocusOrigin { return this._focusOrigin; }
  private _focusOrigin: FocusOrigin = null;

  /**
   * Unfortunately, there is no way to retrieve a list of registered validators
   * for a form field. Thus, we need to use this workaround to retrieve the max.
   * length defined for the field. A corresponding feature request is tracked at
   * https://github.com/angular/angular/issues/13461.
   */
  maxlength: number | null = null;

  autofilled = false;

  constructor(injector: Injector) {
    super(injector);
    this.cdRef = injector.get(ChangeDetectorRef);
    this.ngZone = injector.get(NgZone);
    this.focusMonitor = injector.get(FocusMonitor);
  }

  ngAfterViewInit(): void {
    if (this.focusElementRef) {
      this.focusMonitor.monitor(this.focusElementRef)
        .subscribe(origin => this.ngZone.run(() => {
          this._focusOrigin = origin;
          this.cdRef.markForCheck();
        }));
    }
  }

  ngOnDestroy(): void {
    if (this.focusElementRef) {
      this.focusMonitor.stopMonitoring(this.focusElementRef);
    }
  }

  /**
   * Focus the native form element.
   *
   * @param origin Focus origin.
   * @param options Options that can be used to configure the focus behavior.
   */
  focus(origin: FocusOrigin = 'program', options?: FocusOptions): void {
    if (this.focusElementRef) {
      this.focusMonitor.focusVia(this.focusElementRef.nativeElement, origin, options);
    }
  }

  /**
   * Clears the form field.
   */
  clear(): void {
    this.value = null;
  }
}
