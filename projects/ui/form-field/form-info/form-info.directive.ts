import { Directive, InjectionToken } from '@angular/core';

export const CUI_FORM_INFO = new InjectionToken<FormInfoDirective>('FormInfo');

@Directive({
  selector: '[cuiFormInfo]',
  providers: [{ provide: CUI_FORM_INFO, useExisting: FormInfoDirective }]
})
export class FormInfoDirective {}
