import {
  AfterContentInit,
  ChangeDetectorRef,
  ContentChildren,
  Directive,
  EventEmitter,
  forwardRef, InjectionToken,
  Input,
  Output,
  QueryList
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@coyoapp/ui/cdk';
import {
  CuiRadioChange,
  RadioButtonComponent, _CuiRadioButtonBase
} from '../radio-button/radio-button.component';

let nextUniqueId = 0;

/**
 * Provider Expression that allows mat-radio-group to register as a ControlValueAccessor. This
 * allows it to support [(ngModel)] and ngControl.
 */
export const CUI_RADIO_GROUP_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RadioGroupDirective),
  multi: true
};

/**
 * Injection token that can be used to inject instances of `MatRadioGroup`. It serves as
 * alternative token to the actual `MatRadioGroup` class which could cause unnecessary
 * retention of the class and its component metadata.
 */
export const CUI_RADIO_GROUP = new InjectionToken<_CuiRadioGroupBase<_CuiRadioButtonBase>>('CuiRadioGroup');

/**
 * Base class with all of the `MatRadioGroup` functionality.
 */
@Directive()
export abstract class _CuiRadioGroupBase<T extends _CuiRadioButtonBase> implements AfterContentInit, ControlValueAccessor {
  /** Selected value for the radio group. */
  private _value: unknown = null;

  /** The HTML name attribute applied to radio buttons in this group. */
  private _name = `cui-radio-group-${nextUniqueId++}`;

  /** The currently selected radio button. Should match value. */
  private _selected: T | null = null;

  /** Whether the `value` has been set to its initial value. */
  private _isInitialized = false;

  /** Whether the labels should appear after or before the radio-buttons. Defaults to 'left' */
  private _align: 'left' | 'right' = 'left';

  /** Whether the radio group is disabled. */
  private _disabled = false;

  /** Whether the radio group is required. */
  private _required = false;

  /** The method to be called in order to update ngModel */
  _controlValueAccessorChangeFn: (value: unknown) => void = noop;

  /**
   * onTouch function registered via registerOnTouch (ControlValueAccessor).
   */
  onTouched: () => unknown = noop;

  /**
   * Event emitted when the group value changes.
   * Change events are only emitted when the value changes due to user interaction with
   * a radio button (the same behavior as `<input type-"radio">`).
   */
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() readonly change: EventEmitter<CuiRadioChange> = new EventEmitter<CuiRadioChange>();

  /** Child radio buttons. */
  abstract _radios: QueryList<T>;

  /** Name of the radio button group. All radio buttons inside this group will use this name. */
  @Input()
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
    this._updateRadioButtonNames();
  }

  /** Whether the labels should appear to the right or to the left of the radio-buttons. Defaults to 'right' */
  @Input()
  get align(): 'left' | 'right' {
    return this._align;
  }

  set align(value: 'left' | 'right') {
    this._align = value === 'left' ? 'left' : 'right';
    this._markRadiosForCheck();
  }

  /**
   * Value for the radio-group. Should equal the value of the selected radio button if there is
   * a corresponding radio button with a matching value. If there is not such a corresponding
   * radio button, this value persists to be applied in case a new radio button is added with a
   * matching value.
   */
  @Input()
  get value(): unknown {
    return this._value;
  }

  set value(newValue: unknown) {
    if (this._value !== newValue) {
      // Set this before proceeding to ensure no circular loop occurs with selection.
      this._value = newValue;

      this._updateSelectedRadioFromValue();
      this._checkSelectedRadioButton();
    }
  }

  _checkSelectedRadioButton(): void {
    if (this._selected && !this._selected.checked) {
      this._selected.checked = true;
    }
  }

  /**
   * The currently selected radio button. If set to a new radio button, the radio group value
   * will be updated to match the new selected button.
   */
  @Input()
  get selected(): T | null {
    return this._selected;
  }

  set selected(selected: T | null) {
    this._selected = selected;
    this.value = selected ? selected.value : null;
    this._checkSelectedRadioButton();
  }

  /** Whether the radio group is disabled */
  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = value;
    this._markRadiosForCheck();
  }

  /** Whether the radio group is required */
  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean) {
    this._required = value;
    this._markRadiosForCheck();
  }

  constructor(private _changeDetector: ChangeDetectorRef) {}

  /**
   * Initialize properties once content children are available.
   * This allows us to propagate relevant attributes to associated buttons.
   */
  ngAfterContentInit(): void {
    // Mark this component as initialized in AfterContentInit because the initial value can
    // possibly be set by NgModel on MatRadioGroup, and it is possible that the OnInit of the
    // NgModel occurs *after* the OnInit of the MatRadioGroup.
    this._isInitialized = true;
    this._updateRadioButtonNames();
    this._updateSelectedRadioFromValue();
    this._checkSelectedRadioButton();
  }

  /**
   * Mark this group as being "touched" (for ngModel). Meant to be called by the contained
   * radio buttons upon their blur.
   */
  _touch(): void {
    if (this.onTouched) {
      this.onTouched();
    }
  }

  private _updateRadioButtonNames(): void {
    if (this._radios) {
      this._radios.forEach(radio => {
        radio.name = this.name;
        radio._markForCheck();
      });
    }
  }

  /** Updates the `selected` radio button from the internal _value state. */
  private _updateSelectedRadioFromValue(): void {
    // If the value already matches the selected radio, do nothing.
    const isAlreadySelected = this._selected !== null && this._selected.value === this._value;
    if (this._radios && !isAlreadySelected) {
      this._selected = null;
      this._radios.forEach(radio => {
        radio.checked = this.value === radio.value;
        if (radio.checked) {
          this._selected = radio;
        }
      });
    }
  }

  /** Dispatch change event with current selection and group value. */
  _emitChangeEvent(): void {
    if (this._isInitialized) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      this.change.emit(new CuiRadioChange(this._selected!, this._value));
    }
  }

  _markRadiosForCheck(): void {
    if (this._radios) {
      this._radios.forEach(radio => radio._markForCheck());
    }
  }

  /**
   * Sets the model value. Implemented as part of ControlValueAccessor.
   *
   * @param value the new value
   */
  writeValue(value: unknown): void {
    this.value = value;
    this._changeDetector.markForCheck();
  }

  /**
   * Registers a callback to be triggered when the model value changes.
   * Implemented as part of ControlValueAccessor.
   *
   * @param fn Callback to be registered.
   */
  registerOnChange(fn: (value: unknown) => void): void {
    this._controlValueAccessorChangeFn = fn;
  }

  /**
   * Registers a callback to be triggered when the control is touched.
   * Implemented as part of ControlValueAccessor.
   *
   * @param fn Callback to be registered.
   */
  registerOnTouched(fn: () => unknown): void {
    this.onTouched = fn;
  }

  /**
   * Sets the disabled state of the control. Implemented as a part of ControlValueAccessor.
   *
   * @param isDisabled Whether the control should be disabled.
   */
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this._changeDetector.markForCheck();
  }
}

/**
 * A group of radio buttons. May contain one or more `<cui-radio-button>` elements.
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'cui-radio-group',
  exportAs: 'cuiRadioGroup',
  providers: [
    CUI_RADIO_GROUP_CONTROL_VALUE_ACCESSOR,
    { provide: CUI_RADIO_GROUP, useExisting: RadioGroupDirective }
  ],
  host: {
    role: 'radiogroup',
    class: 'cui-radio-group'
  }
})
export class RadioGroupDirective extends _CuiRadioGroupBase<RadioButtonComponent> {
  @ContentChildren(forwardRef(() => RadioButtonComponent), { descendants: true })
  _radios!: QueryList<RadioButtonComponent>;
}
