import { ChangeDetectorRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { noop } from '@coyoapp/ui/cdk';
import { CUI_RADIO_GROUP_CONTROL_VALUE_ACCESSOR, RadioGroupDirective } from './radio-group.directive';

describe('RadioGroupDirective', () => {
  let directive: RadioGroupDirective;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    directive = new RadioGroupDirective(changeDetectorRef);

    TestBed.configureTestingModule({
      providers: [{
        provide: CUI_RADIO_GROUP_CONTROL_VALUE_ACCESSOR,
        useValue: RadioGroupDirective
      }]
    });
  });

  it('should create an instance', () => {
    directive = new RadioGroupDirective(changeDetectorRef);

    expect(directive).toBeTruthy();
  });

  it('should get correct CUI_RADIO_GROUP_CONTROL_VALUE_ACCESSOR provider of type RadioGroupDirective', () => {
    const directiveDecoraters = (<any>RadioGroupDirective).__annotations__;
    const useExistingClass = directiveDecoraters[0].providers[0].useExisting();

    expect(directiveDecoraters[0].providers[0].useExisting).toBeTruthy();
    expect(useExistingClass).toEqual(RadioGroupDirective);
  });

  it('should set correct name value', () => {
    spyOn(directive as any, '_updateRadioButtonNames');
    directive.name = 'test';

    expect(directive.name).toEqual('test');
    expect((directive as any)._updateRadioButtonNames).toHaveBeenCalled();
  });

  it('should set disabled name value', () => {
    spyOn(directive as any, '_markRadiosForCheck');
    directive.disabled = true;

    expect(directive.disabled).toEqual(true);
    expect((directive as any)._markRadiosForCheck).toHaveBeenCalled();
  });

  it('should set initialized and call methods ngAfterContentInit call', () => {
    (directive as any)._isInitialized = false;
    spyOn(directive as any, '_updateRadioButtonNames');
    spyOn(directive as any, '_updateSelectedRadioFromValue');
    spyOn(directive as any, '_checkSelectedRadioButton');
    directive.ngAfterContentInit();

    expect((directive as any)._isInitialized).toEqual(true);
    expect((directive as any)._updateRadioButtonNames).toHaveBeenCalled();
    expect((directive as any)._updateSelectedRadioFromValue).toHaveBeenCalled();
    expect((directive as any)._checkSelectedRadioButton).toHaveBeenCalled();
  });

  it('should call onTouched on _touch method call', () => {
    spyOn(directive, 'onTouched');
    (directive as any)._touch();

    expect(directive.onTouched).toHaveBeenCalled();
  });

  it('should call markForCheck and set disabled on setDisabledState', () => {
    (directive as any).setDisabledState(false);

    expect(directive.disabled).toBeFalse();
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should register function on registerOnTouched method call', () => {
    let funcCall = false;
    const func = () => {
      funcCall = true;
    };
    directive.registerOnTouched(func);

    expect(directive.onTouched).toEqual(func);
    directive.onTouched();

    expect(funcCall).toBeTrue();
  });

  it('should set value on writeValue method call', () => {
    directive.writeValue(false);

    expect(directive.value).toBeFalse();
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should emit change event on _emitChangeEvent call', () => {
    (directive as any)._isInitialized = true;
    spyOn(directive.change, 'emit');
    (directive as any)._emitChangeEvent();

    expect(directive.change.emit).toHaveBeenCalled();
  });

  it('should set radio names on _updateRadioButtonNames call', () => {
    (directive as any)._radios = [
      {
        name: '',
        _markForCheck: noop
      }];
    directive.name = 'test';
    spyOn((directive as any)._radios[0], '_markForCheck');
    (directive as any)._updateRadioButtonNames();

    expect((directive as any)._radios[0].name).toEqual('test');
    expect((directive as any)._radios[0]._markForCheck).toHaveBeenCalled();
  });

  it('should set radio names on _markRadiosForCheck call', () => {
    (directive as any)._radios = [
      {
        _markForCheck: noop
      }];
    spyOn((directive as any)._radios[0], '_markForCheck');
    (directive as any)._markRadiosForCheck();

    expect((directive as any)._radios[0]._markForCheck).toHaveBeenCalled();
  });

  it('should register function on registerOnChange method call', () => {
    let funcCall = false;
    const func = () => {
      funcCall = true;
    };
    directive.registerOnChange(func);

    expect((directive as any)._controlValueAccessorChangeFn).toEqual(func);
    (directive as any)._controlValueAccessorChangeFn();

    expect(funcCall).toBeTrue();
  });

  it('should set radio names on _updateSelectedRadioFromValue call', () => {
    (directive as any)._radios = [
      {
        checked: true,
        value: true
      }];
    (directive as any).value = true;
    (directive as any)._updateSelectedRadioFromValue();

    expect((directive as any)._radios[0].checked).toBeTrue();
    expect((directive as any)._selected).toEqual((directive as any)._radios[0]);
  });

});
