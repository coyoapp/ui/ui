import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import {
  AfterViewInit,
  Attribute,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnDestroy,
  OnInit, Optional,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { noop } from '@coyoapp/ui/cdk';
import { CUI_RADIO_GROUP, RadioGroupDirective, _CuiRadioGroupBase } from '../radio-group/radio-group.directive';

let nextUniqueId = 0;

/** Change event for Radio Component */
export class CuiRadioChange {
  constructor(
    public source: _CuiRadioButtonBase,
    public value: unknown
  ) {}
}

/**
 * Base class with all of the `RadioButton` functionality.
 */
@Directive()
export abstract class _CuiRadioButtonBase implements OnInit, AfterViewInit, OnDestroy {

  private _uniqueId = `cui-radio-${++nextUniqueId}`;

  private _tabIndex = 0;

  /** The unique ID for the radio button. */
  @Input() id: string = this._uniqueId;

  /** Analog to HTML 'name' attribute used to group radios for unique selection. */
  @Input() name!: string;

  /** Used to set the 'aria-label' attribute on the underlying input element. */
  @Input('aria-label') ariaLabel?: string;

  /** The 'aria-labelledby' attribute takes precedence as the element's text alternative. */
  @Input('aria-labelledby') ariaLabelledby?: string;

  /** The 'aria-describedby' attribute is read after the element's label and field type. */
  @Input('aria-describedby') ariaDescribedby?: string;

  /**
   * The appearance mode of the checkbox.
   */
  @Input()
  @HostBinding('attr.data-cui-mode')
  mode: 'primary' | 'secondary' = 'primary';

  /**
   * The size of the checkbox.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size: 's' | 'm' | 'l' = 'm';

  /** Whether this radio button is checked. */
  @Input()
  get checked(): boolean {
    return this._checked;
  }

  set checked(value: boolean) {
    if (this._checked !== value) {
      this._checked = value;
      if (value && this.radioGroup && this.radioGroup.value !== this.value) {
        this.radioGroup.selected = this;
      } else if (!value && this.radioGroup && this.radioGroup.value === this.value) {

        // When unchecking the selected radio button, update the selected radio
        // property on the group.
        this.radioGroup.selected = null;
      }
      if (value) {
        // Notify all radio buttons with the same name to un-check.
        this._radioDispatcher.notify(this.id, this.name);
      }
      this._changeDetector.markForCheck();
    }
  }

  /** The value of this radio button. */
  @Input()
  get value(): unknown {
    return this._value;
  }

  set value(value: unknown) {
    if (this._value !== value) {
      this._value = value;
      if (this.radioGroup !== null) {
        if (!this.checked) {
          // Update checked when the value changed to match the radio group's value
          this.checked = this.radioGroup.value === value;
        }
        if (this.checked) {
          this.radioGroup.selected = this;
        }
      }
    }
  }

  get tabIndex(): number {
    return this.disabled ? -1 : this._tabIndex;
  }

  set tabIndex(value: number) {
    this._tabIndex = value != null ? value : 0;
  }

  /** Whether the label should appear after or before the radio button. Defaults to 'left' */
  @Input()
  get align(): 'left' | 'right' | 'left-spread' | 'right-spread' {
    return this._align || (this.radioGroup && this.radioGroup.align) || 'left';
  }

  set align(value: 'left' | 'right' | 'left-spread' | 'right-spread') {
    this._align = value;
  }

  private _align: 'left' | 'right' | 'left-spread' | 'right-spread' = 'left';

  /** Whether the radio button is disabled. */
  @Input()
  get disabled(): boolean {
    return this._disabled || (this.radioGroup !== null && this.radioGroup.disabled);
  }

  set disabled(value: boolean) {
    this._setDisabled(value);
  }

  /** Whether the radio button is required. */
  @Input()
  get required(): boolean {
    return this._required || (this.radioGroup && this.radioGroup.required);
  }

  set required(value: boolean) {
    this._required = value;
  }

  /**
   * Event emitted when the checked state of this radio button changes.
   * Change events are only emitted when the value changes due to user interaction with
   * the radio button (the same behavior as `<input type-"radio">`).
   */
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() readonly change: EventEmitter<CuiRadioChange> = new EventEmitter<CuiRadioChange>();

  /** The parent radio group. May or may not be present. */
  radioGroup: _CuiRadioGroupBase<_CuiRadioButtonBase>;

  /**
   * ID of the native input element inside `<cui-radio>`, in order to attach an external Label
   * you have to suffix the ID used in the FOR attribute with '-input'
   * */
  get inputId(): string {
    return `${this.id || this._uniqueId}-input`;
  }

  /** Whether this radio is checked. */
  private _checked = false;

  /** Whether this radio is disabled. */
  private _disabled?: boolean;

  /** Whether this radio is required. */
  private _required?: boolean;

  /** Value assigned to this radio. */
  private _value: unknown = null;

  /** Unregister function for _radioDispatcher */
  private _removeUniqueSelectionListener: () => void = noop;

  /** The native `<input type=radio>` element */
  @ViewChild('input') _inputElement!: ElementRef<HTMLInputElement>;

  constructor(
    radioGroup: _CuiRadioGroupBase<_CuiRadioButtonBase>,
    public _elementRef: ElementRef,
    protected _changeDetector: ChangeDetectorRef,
    private _focusMonitor: FocusMonitor,
    private _radioDispatcher: UniqueSelectionDispatcher,
    tabIndex?: number
  ) {
    this.radioGroup = radioGroup;
    this.name = this.radioGroup?.name;

    if (tabIndex) {
      this.tabIndex = tabIndex;
    }

    this._removeUniqueSelectionListener = _radioDispatcher.listen((id: string, name: string) => {
      if (id !== this.id && name === this.name) {
        this.checked = false;
      }
    });
  }

  /** Focuses the radio button. */
  focus(options?: FocusOptions, origin?: FocusOrigin): void {
    if (origin) {
      this._focusMonitor.focusVia(this._inputElement, origin, options);
    } else {
      this._inputElement.nativeElement.focus(options);
    }
  }

  clickFocused(): void {
    this._focusMonitor.focusVia(this._elementRef, 'mouse');
  }

  /**
   * Marks the radio button as needing checking for change detection.
   * This method is exposed because the parent radio group will directly
   * update bound properties of the radio button.
   */
  _markForCheck(): void {
    // When group value changes, the button will not be notified. Use `markForCheck` to explicit
    // update radio button's status
    this._changeDetector.markForCheck();
  }

  ngOnInit(): void {
    if (this.radioGroup) {
      // If the radio is inside a radio group, determine if it should be checked
      this.checked = this.radioGroup.value === this._value;

      if (this.checked) {
        this.radioGroup.selected = this;
      }

      // Copy name from parent radio group
      this.name = this.radioGroup.name;
    }
  }

  ngAfterViewInit(): void {
    this._focusMonitor
      .monitor(this._elementRef, true)
      .subscribe(focusOrigin => {
        if (!focusOrigin && this.radioGroup) {
          this.radioGroup._touch();
        }
      });
  }

  ngOnDestroy(): void {
    this._focusMonitor.stopMonitoring(this._elementRef);
    this._removeUniqueSelectionListener();
  }

  /** Dispatch change event with current value. */
  private _emitChangeEvent(): void {
    this.change.emit(new CuiRadioChange(this, this._value));
  }

  inputClicked(event: Event): void {
    event.stopPropagation();
  }

  /** Triggered when the radio button receives an interaction from the user. */
  changed(event: Event): void {
    // We always have to stop propagation on the change event.
    // Otherwise the change event, from the input element, will bubble up and
    // emit its event object to the `change` output.
    event.stopPropagation();

    if (!this.checked && !this.disabled) {
      const groupValueChanged = this.radioGroup && this.value !== this.radioGroup.value;
      this.checked = true;
      this._emitChangeEvent();

      if (this.radioGroup) {
        this.radioGroup._controlValueAccessorChangeFn(this.value);
        if (groupValueChanged) {
          this.radioGroup._emitChangeEvent();
        }
      }
    }
  }

  /** Sets the disabled state and marks for check if a change occurred. */
  protected _setDisabled(value: boolean): void {
    if (this._disabled !== value) {
      this._disabled = value;
      this._changeDetector.markForCheck();
    }
  }
}

/**
 * The cui-radio-button component extends the visual style of a radio button.
 * Typically placed inside of `<cui-radio-group>` elements.
 */
@Component({
  selector: 'cui-radio-button',
  templateUrl: 'radio-button.component.html',
  styleUrls: ['radio-button.component.scss'],
  // eslint-disable-next-line @angular-eslint/no-inputs-metadata-property
  inputs: ['tabIndex'],
  encapsulation: ViewEncapsulation.None,
  exportAs: 'cuiRadioButton',
  host: {
    'class': 'cui-radio-button',
    '[class.cui-radio-checked]': 'checked',
    '[class.cui-radio-disabled]': 'disabled',
    '[class.cui-radio-spread]': 'align === "left-spread" || align === "right-spread"',
    '[attr.tabindex]': 'null',
    '[attr.id]': 'id',
    '[attr.aria-label]': 'null',
    '[attr.aria-labelledby]': 'null',
    '[attr.aria-describedby]': 'null',
    '(focus)': '_inputElement.nativeElement.focus()'
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadioButtonComponent extends _CuiRadioButtonBase {
  constructor(@Optional() @Inject(CUI_RADIO_GROUP) radioGroup: RadioGroupDirective,
    elementRef: ElementRef,
    changeDetector: ChangeDetectorRef,
    focusMonitor: FocusMonitor,
    radioDispatcher: UniqueSelectionDispatcher,
    @Attribute('tabindex') tabIndex?: number) {
    super(radioGroup, elementRef, changeDetector, focusMonitor, radioDispatcher, tabIndex);
  }
}
