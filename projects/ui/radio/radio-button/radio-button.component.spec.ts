import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { noop } from '@coyoapp/ui/cdk';
import { RadioGroupDirective } from '@coyoapp/ui/radio';
import { Subject } from 'rxjs';
import { CuiRadioChange, RadioButtonComponent } from './radio-button.component';

describe('RadioComponent', () => {
  let component: RadioButtonComponent;
  let fixture: ComponentFixture<RadioButtonComponent>;
  let radioGroup: RadioGroupDirective;
  let changeDetectorRef: jasmine.SpyObj<ChangeDetectorRef>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RadioButtonComponent]
    }).overrideTemplate(RadioButtonComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioButtonComponent);
    component = fixture.componentInstance;
    changeDetectorRef = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    (component as any)._changeDetector = changeDetectorRef;
    radioGroup = new RadioGroupDirective(changeDetectorRef);
    component.radioGroup = radioGroup;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct change event class', () => {
    const changeEvent = new CuiRadioChange(component, true);

    expect(changeEvent.value).toEqual(true);
    expect(changeEvent.source).toEqual(component);
  });

  it('should return and set correct checked values', () => {
    (component as any)._checked = false;
    spyOn((component as any)._radioDispatcher, 'notify');

    expect(component.checked).toBeFalse();
    component.radioGroup.value = false;
    component.value = false;
    component.checked = true;

    expect(component.checked).toBeTrue();
    expect(component.radioGroup.selected).toEqual(component);
    expect((component as any)._radioDispatcher.notify).toHaveBeenCalled();
    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
    component.checked = false;

    expect(component.radioGroup.selected).toBeNull();
  });

  it('should set checked when radio value equals group value', () => {
    (component as any)._value = true;

    expect(component.value).toBeTrue();
    component.radioGroup.value = false;
    component.checked = false;
    component.value = false;

    expect(component.checked).toBeTrue();
  });

  it('should return correct tabindex value', () => {
    expect(component.tabIndex).toEqual(0);
    (component as any)._tabIndex = 1;
    component.disabled = true;

    expect(component.tabIndex).toEqual(-1);
    component.disabled = false;

    expect(component.tabIndex).toEqual(1);
    component.tabIndex = null as any;

    expect(component.tabIndex).toEqual(0);
    component.tabIndex = 2;

    expect(component.tabIndex).toEqual(2);
  });

  it('should return correct align value', () => {
    expect(component.align).toEqual('left');
    component.radioGroup.align = 'right';
    (component as any)._align = null;

    expect(component.align).toEqual('right');
    (component.radioGroup as any).align = null;
    component.align = 'right';

    expect(component.align).toEqual('right');
  });

  it('should return correct required value', () => {
    expect(component.required).toBeFalsy();
    (component as any)._required = true;

    expect(component.required).toBeTrue();
    (component as any)._required = null;
    component.radioGroup.required = true;

    expect(component.required).toBeTrue();
    component.radioGroup.required = false;
    component.required = false;

    expect(component.required).toBeFalse();
  });

  it('should return correct inputId value', () => {
    expect(component.inputId).toEqual(component.id + '-input');
    expect((component as any)._uniqueId).toContain('cui-radio-');
    expect((component as any).id).toContain('cui-radio-');
  });

  it('should call focusVia when calling focus method', () => {
    (component as any)._inputElement = {
      nativeElement: {
        focus: noop
      }
    };
    spyOn((component as any)._inputElement.nativeElement, 'focus');
    (component as any)._focusMonitor = jasmine.createSpyObj('FocusMonitor', ['focusVia', 'stopMonitoring']);
    component.focus();

    expect((component as any)._inputElement.nativeElement.focus).toHaveBeenCalled();
    component.focus({}, 'keyboard');

    expect((component as any)._focusMonitor.focusVia).toHaveBeenCalled();
  });

  it('should call focusVia when calling clickFocused method', () => {
    (component as any)._focusMonitor = jasmine.createSpyObj('FocusMonitor', ['focusVia', 'stopMonitoring']);
    component.clickFocused();

    expect((component as any)._focusMonitor.focusVia).toHaveBeenCalled();
  });

  it('should call call markForCheck when calling _markForCheck method', () => {
    (component as any)._markForCheck();

    expect(changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

  it('should call stopPropagation and setting checked when calling changed method', () => {
    const event = new Event('keydown', { bubbles: true });
    spyOn(component as any, '_emitChangeEvent');
    component.checked = false;
    component.disabled = false;
    component.value = false;
    spyOn((component.radioGroup as any), '_controlValueAccessorChangeFn');
    spyOn((component.radioGroup as any), '_emitChangeEvent');
    component.radioGroup.value = true;
    spyOn(event, 'stopPropagation');
    component.changed(event);

    expect(component.checked).toBeTrue();
    expect((component.radioGroup as any)._controlValueAccessorChangeFn).toHaveBeenCalled();
    expect((component.radioGroup as any)._emitChangeEvent).toHaveBeenCalled();
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('should call stopPropagation when calling inputClicked method', () => {
    const event = new Event('click', { bubbles: true });
    spyOn(event, 'stopPropagation');
    component.inputClicked(event);

    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('should emit change event when calling _emitChangeEvent method', () => {
    (component as any).change = jasmine.createSpyObj('EventEmitter', ['emit']);
    (component as any)._emitChangeEvent();

    expect((component as any).change.emit).toHaveBeenCalled();
  });

  it('should start monitoring after view init', fakeAsync(() => {
    let touchFnCalled = false;
    (component as any).radioGroup._touch = () => {
      touchFnCalled = true;
    };
    const subj = new Subject();
    let monitorWasCalled = false;
    (component as any)._focusMonitor = {
      monitor: () => {
        monitorWasCalled = true;
        return subj.asObservable();
      },
      stopMonitoring: noop
    };
    component.ngAfterViewInit();
    subj.next(false);
    tick();

    expect(monitorWasCalled).toBeTruthy();
    expect(touchFnCalled).toBeTruthy();
    expect((component as any)._changeDetector.markForCheck).toHaveBeenCalled();
  }));
});
