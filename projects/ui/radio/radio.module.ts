import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RadioButtonComponent } from './radio-button/radio-button.component';
import { RadioGroupDirective } from './radio-group/radio-group.directive';

@NgModule({
  declarations: [RadioButtonComponent, RadioGroupDirective],
  imports: [
    CommonModule
  ],
  exports: [RadioButtonComponent, RadioGroupDirective]
})
export class RadioModule {}
