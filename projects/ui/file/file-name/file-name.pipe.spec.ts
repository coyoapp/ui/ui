import { FileNamePipe } from './file-name.pipe';

describe('AlertIconPipe', () => {
  let pipe: FileNamePipe;

  beforeEach(() => {
    pipe = new FileNamePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('return correct file name from path on transform', () => {
    const result1 = pipe.transform('www.test.com/test.jpg');
    const result2 = pipe.transform('www.test.com/test.jpg', { stripPath: true, stripExt: true });
    const result3 = pipe.transform('www.test.com/test.jpg', { stripPath: true, stripExt: false });
    const result4 = pipe.transform('www.test.com/test.jpg', { stripPath: false, stripExt: false });

    expect(result1).toBe('www.test.com/test');
    expect(result2).toBe('test');
    expect(result3).toBe('test.jpg');
    expect(result4).toBe('www.test.com/test.jpg');
  });
});
