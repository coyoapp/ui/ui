import { Pipe, PipeTransform } from '@angular/core';

export interface FileNamePipeOptions {
  stripPath: boolean;
  stripExt: boolean;
}

/**
 * Pipe that returns file name without extension or path from a full path
 */
@Pipe({
  name: 'fileName'
})
export class FileNamePipe implements PipeTransform {
  private static readonly DEFAULTS: FileNamePipeOptions = {
    stripPath: false,
    stripExt: true
  };

  transform(file: string, options?: Partial<FileNamePipeOptions>): string {
    const opts = { ...FileNamePipe.DEFAULTS, ...options };
    const fullPathParts = file.split('/');
    const fileNameParts = fullPathParts[fullPathParts.length - 1].split('.');
    const returnedFileName = [];
    returnedFileName[0] = opts.stripPath ? '' : fullPathParts.slice(0, -1).join('/');
    if (returnedFileName[0]) {
      returnedFileName[0] += '/';
    }
    returnedFileName[1] = opts.stripExt ? fileNameParts[0] : fileNameParts.join('.');
    return `${returnedFileName[0]}${returnedFileName[1]}`;
  }
}
