import { Inject, Pipe, PipeTransform } from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';
import { CUI_FILE_ICON_CONFIG, FileIconConfig } from '../file.config';

/**
 * Pipe that transforms a MIME type into a corresponding icon name.
 */
@Pipe({
  name: 'fileIcon'
})
export class FileIconPipe implements PipeTransform {
  private static readonly DEFAULT_ICON_SET_KEY = 'default';
  private static readonly DEFAULT_ICON_KEY = 'default';
  private static readonly DEFAULT_ICON: coyoIcon = 'generic-file';

  constructor(
    @Inject(CUI_FILE_ICON_CONFIG) readonly config: FileIconConfig
  ) {}

  transform(mimeType: string, setName = 'default'): coyoIcon {
    return this.getIconFromSet(setName, mimeType)
      || this.getIconFromSet(FileIconPipe.DEFAULT_ICON_SET_KEY, mimeType)
      || this.config[FileIconPipe.DEFAULT_ICON_SET_KEY]?.[FileIconPipe.DEFAULT_ICON_KEY]
      || FileIconPipe.DEFAULT_ICON;
  }

  private getIconFromSet(setName: string, mimeType: string): coyoIcon | undefined {
    const set = this.config[setName] || {};
    return Object.entries(set).find(([regex]) => new RegExp(regex).test(mimeType))?.[1];
  }
}
