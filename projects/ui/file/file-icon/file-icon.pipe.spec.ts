import { CUI_FILE_ICON_DEFAULT_CONFIG } from '../file.config';
import { FileIconPipe } from './file-icon.pipe';

describe('AlertIconPipe', () => {
  let pipe: FileIconPipe;

  beforeEach(() => {
    pipe = new FileIconPipe(null as any);
    (pipe as any).config = {
      ...CUI_FILE_ICON_DEFAULT_CONFIG,
      alternative: {
        '^video\\/': 'video-alt',
        '^application\\/json$': 'json-alt'
      }
    };
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('retrieve the correct icons value', () => {
    const result1 = pipe.transform('application/pdf');
    const result2 = pipe.transform('application/zip');
    const result3 = pipe.transform('text/plain');
    const result4 = pipe.transform('image/jpeg');
    const result5 = pipe.transform('video/mp4');
    const result6 = pipe.transform('application/json');
    const result7 = pipe.transform('video/mp4', 'alternative');
    const result8 = pipe.transform('application/json', 'alternative');
    const result9 = pipe.transform('image/jpeg', 'alternative');
    const result10 = pipe.transform('application/rtf', 'alternative');

    expect(result1).toBe('pdf');
    expect(result2).toBe('mc-zip-archive');
    expect(result3).toBe('text');
    expect(result4).toBe('mc-image');
    expect(result5).toBe('video');
    expect(result6).toBe('generic-file');
    expect(result7).toBe('video-alt');
    expect(result8).toBe('json-alt');
    expect(result9).toBe('mc-image');
    expect(result10).toBe('generic-file');
  });
});
