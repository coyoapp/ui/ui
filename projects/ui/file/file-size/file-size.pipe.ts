import { Pipe, PipeTransform } from '@angular/core';
import { clamp } from '@coyoapp/ui/cdk';

/**
 * Pipe that displays sizes in bytes in human readable format
 */
@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {
  private static readonly SYMBOLS = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  transform(size: string | number, separator = ''): string {
    const result = [];
    const num = Math.abs(+size);
    const e = clamp(Math.floor(Math.log(num) / Math.log(1000)), 0, 8);

    if (num === 0) {
      result[0] = 0;
      result[1] = FileSizePipe.SYMBOLS[e];
    } else {
      const val = num / Math.pow(1000, e);
      const p = Math.pow(10, e > 0 ? 1 : 0);
      result[0] = Math.round(val * p) / p;
      result[1] = FileSizePipe.SYMBOLS[e];
    }

    return `${Math.abs(result[0])}${separator}${result[1]}`;
  }
}
