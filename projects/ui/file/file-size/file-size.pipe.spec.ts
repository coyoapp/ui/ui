import { FileSizePipe } from './file-size.pipe';

describe('AlertIconPipe', () => {
  let pipe: FileSizePipe;

  beforeEach(() => {
    pipe = new FileSizePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('return correct readable file size values from bytes value', () => {
    const result1 = pipe.transform('1024');
    const result2 = pipe.transform('1024000');
    const result3 = pipe.transform('1024000000');
    const result4 = pipe.transform('1024000000000');
    const result5 = pipe.transform('5700000', ' ');
    const result6 = pipe.transform(27000000000, ' ');
    const result7 = pipe.transform('170', '  ');
    const result8 = pipe.transform(0, '');

    expect(result1).toBe('1KB');
    expect(result2).toBe('1MB');
    expect(result3).toBe('1GB');
    expect(result4).toBe('1TB');
    expect(result5).toBe('5.7 MB');
    expect(result6).toBe('27 GB');
    expect(result7).toBe('170  B');
    expect(result8).toBe('0B');
  });
});
