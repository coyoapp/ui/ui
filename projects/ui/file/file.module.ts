import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileIconPipe } from './file-icon/file-icon.pipe';
import { FileNamePipe } from './file-name/file-name.pipe';
import { FileSizePipe } from './file-size/file-size.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [FileIconPipe, FileNamePipe, FileSizePipe],
  exports: [FileIconPipe, FileNamePipe, FileSizePipe]
})
export class FileModule {}
