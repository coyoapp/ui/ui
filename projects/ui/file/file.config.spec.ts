import { inject } from '@angular/core/testing';
import { FileIconConfig, CUI_FILE_ICON_CONFIG, CUI_FILE_ICON_DEFAULT_CONFIG } from './file.config';

describe('FileConfig', () => {
  it('should provide a default config', inject([CUI_FILE_ICON_CONFIG], (config: FileIconConfig) => {
    expect(config).toBe(CUI_FILE_ICON_DEFAULT_CONFIG);
  }));
});
