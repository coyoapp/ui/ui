import { InjectionToken } from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

export interface FileIconConfig {
  readonly [setName: string]: {
    [mimeType: string]: coyoIcon
  }
}

export const CUI_FILE_ICON_DEFAULT_CONFIG: FileIconConfig = {
  default: {
    'default': 'generic-file',
    '^application\\/pdf$': 'pdf',
    '^application\\/zip$': 'mc-zip-archive',
    '^text\\/plain$': 'text',
    '^image\\/': 'mc-image',
    '^video\\/': 'video'
  }
};

export const CUI_FILE_ICON_CONFIG = new InjectionToken<FileIconConfig>('CUI_FILE_ICON_CONFIG', {
  factory: () => CUI_FILE_ICON_DEFAULT_CONFIG
});
