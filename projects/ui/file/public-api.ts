export * from './file.module';
export * from './file.config';
export * from './file-icon/file-icon.pipe';
export * from './file-name/file-name.pipe';
export * from './file-size/file-size.pipe';
