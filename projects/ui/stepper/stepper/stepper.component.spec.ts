import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperComponent } from './stepper.component';

describe('StepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StepperComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set and get active tab index', () => {
    component.active = 0;

    expect(component.active).toBe(0);

    (component as any).steps.length = 3;
    component.active = 1;

    expect((component.active)).toBe(1);

    (component as any).steps.length = 3;
    component.active = 4;

    expect(component.active).toBe(2);
  });

  it('should return undefined active tab when no tabs are loaded', () => {
    expect(component.activeTab).toBe(undefined);
  });
});
