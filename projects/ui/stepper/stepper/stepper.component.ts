import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, QueryList, ContentChildren, Input } from '@angular/core';
import { clamp } from '@coyoapp/ui/cdk';
import { borderAnimation } from './stepper.animations';
import { StepComponent } from '../step/step.component';

@Component({
  selector: 'cui-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-stepper'
  },
  animations: [borderAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepperComponent {
  @ContentChildren(StepComponent, { descendants: true })
  steps!: QueryList<StepComponent>;

  private _active = 0;

  get active(): number {
    return this._active;
  }

  set active(active: number) {
    const newActive = clamp(active, 0, this.steps.length - 1);
    if (this._active !== newActive) {
      this._active = newActive;
      // Manual change detection needed due to nesting in ng-container
      this.ref.markForCheck();
    }
  }

  get activeTab(): StepComponent | undefined {
    return this.steps.get(this.active);
  }

  constructor(private readonly ref: ChangeDetectorRef) {}

  @Input()
  vertical = false;

  @Input()
  activeHead = true;
}
