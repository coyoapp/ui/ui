import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BackButtonDirective } from './back-button.directive';
import { StepperComponent } from '..';

@Component({
  template: '<button cuiStepBack>back</button>'
})
class TestBackButtonComponent {}

describe('BackButtonDirective', () => {
  let fixture: ComponentFixture<TestBackButtonComponent>;
  let component: TestBackButtonComponent;
  let button: DebugElement;
  let stepper: jasmine.SpyObj<StepperComponent>;

  beforeEach(() => {
    stepper = jasmine.createSpyObj(StepperComponent, ['active']);
    stepper.active = 0;

    TestBed.configureTestingModule({
      declarations: [TestBackButtonComponent, BackButtonDirective],
      providers: [
        {
          provide: StepperComponent,
          useValue: stepper
        }
      ]
    });
    fixture = TestBed.createComponent(TestBackButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button'));
  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('it should decrease active´s value by one when clicked', () => {
    stepper.active = 2;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(1);
  });

  it('when clicked it should decrease active´s value by one if no steps are disabled', () => {
    stepper.active = 1;
    const queryList = jasmine.createSpyObj('QueryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: false });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(0);
  });

  it('when clicked it should not decrease active´s value if active is zero', () => {
    stepper.active = 0;
    const queryList = jasmine.createSpyObj('QueryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: false });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(0);
  });

  it('when clicked it should decrease active´s value by two if the previous step is disabled', () => {
    stepper.active = 4;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });

  it('when clicked it should decrease active´s value by three if two previous steps in a row are disabled', () => {
    stepper.active = 5;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true }, { disabled: true }, { disabled: false });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });

  it('when clicked it should not decrease active´s value if the previous step is the first one and disabled', () => {
    stepper.active = 1;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(1);
  });

  it('when clicked it should not decrease active´s value if the previous steps are the first ones and disabled', () => {
    stepper.active = 2;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true }, { disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });
});
