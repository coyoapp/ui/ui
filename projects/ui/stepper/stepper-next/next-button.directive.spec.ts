import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { StepperComponent } from '..';
import { NextButtonDirective } from './next-button.directive';

@Component({
  template: '<button cuiStepNext>back</button>'
})
class TestNextButtonComponent {}

describe('NextButtonDirective', () => {
  let fixture: ComponentFixture<TestNextButtonComponent>;
  let component: TestNextButtonComponent;
  let button: DebugElement;
  let stepper: jasmine.SpyObj<StepperComponent>;

  beforeEach(() => {
    stepper = jasmine.createSpyObj(StepperComponent, ['active']);
    stepper.active = 0;

    TestBed.configureTestingModule({
      declarations: [TestNextButtonComponent, NextButtonDirective],
      providers: [
        {
          provide: StepperComponent,
          useValue: stepper
        }
      ]
    });
    fixture = TestBed.createComponent(TestNextButtonComponent);
    component = fixture.componentInstance;
    button = fixture.debugElement.query(By.css('button'));

  });

  it('should create component', () => {
    expect(component).toBeDefined();
  });

  it('it should increase active´s value by one when clicked', () => {
    stepper.active = 0;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(1);
  });

  it('when clicked it should increase active´s value by two if the following step is disabled', () => {
    stepper.active = 0;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });

  it('when clicked it should increase active´s value by one if no steps are disabled', () => {
    stepper.active = 0;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: false });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(1);
  });

  it('when clicked it should increase active´s value by three if the two following steps are disabled', () => {
    stepper.active = 2;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.get.and.returnValues({ disabled: true }, { disabled: true }, { disabled: false });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(5);
  });

  it('when clicked the active´s value should not change if active is the last step', () => {
    stepper.active = 4;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.length = 5;
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(4);
  });

  it('when clicked the active´s value should not change if the following step is the last one and disabled', () => {
    stepper.active = 2;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.length = 4;
    queryList.get.and.returnValues({ disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });

  it('when clicked the active´s value should not change if the following two steps are the last one and disabled', () => {
    stepper.active = 2;
    const queryList = jasmine.createSpyObj('queryList', ['get']);
    stepper.steps = queryList;
    queryList.length = 5;
    queryList.get.and.returnValues({ disabled: true }, { disabled: true });
    button.triggerEventHandler('click', null);

    expect(stepper.active).toEqual(2);
  });
});
