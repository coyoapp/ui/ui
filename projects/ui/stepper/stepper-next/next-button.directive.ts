import { Directive, HostListener } from '@angular/core';
import { StepperComponent } from '../stepper/stepper.component';

/**
 *  Directive to turn a button into a stepper button.
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'button[cuiStepNext]'
})
export class NextButtonDirective {

  constructor(private readonly stepper: StepperComponent) {}

  @HostListener('click') changeStep(): void {

    let current = this.stepper.active;
    let counter = 0;
    while (this.stepper.steps.get(current + 1)?.disabled && current !== this.stepper.steps.length - 1) {
      current += 1;
      counter += 1;
    }

    this.stepper.active = current === this.stepper.steps.length - 1 ? current - counter : current + 1;
  }
}
