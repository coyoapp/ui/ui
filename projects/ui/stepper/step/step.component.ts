import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, ViewChild, TemplateRef } from '@angular/core';
import type { coyoIcon } from '@coyoapp/icons';

@Component({
  selector: 'cui-step',
  templateUrl: './step.component.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-step'
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepComponent {
  @Input()
  label = '';

  @Input()
  stepNumber?: number;

  @Input()
  prefixIcon?: coyoIcon;

  @Input()
  suffixIcon?: coyoIcon;

  @Input()
  disabled = false;

  @ViewChild(TemplateRef)
  bodyContent!: TemplateRef<unknown>;
}
