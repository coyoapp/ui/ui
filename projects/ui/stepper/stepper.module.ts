import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { StepperComponent } from './stepper/stepper.component';
import { StepComponent } from './step/step.component';
import { BackButtonDirective } from './stepper-back/back-button.directive';
import { NextButtonDirective } from './stepper-next/next-button.directive';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule
  ],
  declarations: [
    StepComponent,
    StepperComponent,
    BackButtonDirective,
    NextButtonDirective
  ],
  exports: [
    StepComponent,
    StepperComponent,
    BackButtonDirective,
    NextButtonDirective
  ]
})
export class StepperModule {}
