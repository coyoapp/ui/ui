import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  imports: [CommonModule],
  declarations: [SpinnerComponent, SkeletonComponent],
  exports: [SpinnerComponent, SkeletonComponent]
})
export class LoadingModule {}
