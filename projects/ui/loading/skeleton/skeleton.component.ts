import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Skeleton } from './skeleton';

/**
 * Skeletons are used to indicate the loading state of a component. The usage of
 * **spinners is discouraged**. Loading skeletons should aim for the exact
 * height and width of their respective component to avoid shifting and
 * flickering while loading.
 *
 * Skeletons are built to work on a white background by default. But their color
 * and highlight color can be adjsted by their respective inputs. The definition
 * of a height is not necessary. Skeletons will automatically scale to fit their
 * content. The component will automatically harmonize top and bottom margins.
 * So if the first skeleton element is placed at `y=10rem`, the bottom margin
 * will also be set to `10rem`.
 */
@Component({
  selector: 'cui-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-skeleton'
  }
})
export class SkeletonComponent implements AfterViewChecked {

  /**
   * An array of skeleton definitions. A skeleton is defined as:
   * - **top:** the *y* coordinate of the skeleton as a value in pixels
   * - **left:** the *x* coordinate of the skeleton as a value in pixels or a
   * string value, e.g. *"50%"* or *"calc(75% - 1.5rem)"* or  *"[10,30]%"* or  *"calc([10,20]% - [3,10]rem)"*
   * - **width:** the width of the skeleton as a value in pixels or a string
   * value, e.g. *"50%"* or *"calc(75% - 1.5rem)"*
   * - **height:** the height of the skeleton as a value in pixels
   * - **radius?:** a custom border radius for the skeleton as a value in pixels
   * or a string value, e.g. *"50%"*
   * - **color?:** a custom color for the skeleton
   */
  @Input()
  skeletons: Skeleton[] = [];

  /**
   * The total height of the skeleton. If it is not provided, the component will
   * try to automatically determine the total height of the skeleton based on
   * the skeleton definitions.
   */
  @Input()
  height: number | null = null;

  /**
   * A custom color for the skeleton elements.
   */
  @Input()
  color: string | null = null;

  /**
   * A custom color for the skeleton's highlight.
   */
  @Input()
  highlight: string | null = null;

  @ViewChild('skeletonHighlight', { static: true })
  skeletonHighlight!: ElementRef<HTMLDivElement>;

  constructor(
    private readonly sanitizer: DomSanitizer,
    private readonly elem: ElementRef,
    private readonly renderer: Renderer2
  ) {}

  get highlightColor(): SafeStyle | null {
    if (!this.highlight) {
      return null;
    }

    const rgb = this.parseRgb(this.highlight) || this.hexToRgb(this.highlight);
    return rgb ? this.sanitizer.bypassSecurityTrustStyle(
      'linear-gradient(90deg,'
      + ` rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0) 0%,`
      + ` rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a * 0.25}) 25%,`
      + ` rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a * 0.5}) 50%,`
      + ` rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a * 0.25}) 75%,`
      + ` rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0) 100%)`
    ) : null;
  }

  ngAfterViewChecked(): void {
    const host = this.elem.nativeElement;
    const hostHeight = Math.min(...this.skeletons.map(skeleton => skeleton.top))
      + Math.max(...this.skeletons.map(skeleton => skeleton.top + skeleton.height));
    const highlight = this.skeletonHighlight.nativeElement;
    const highlightDuration = 4 * highlight.offsetWidth;
    this.renderer.setStyle(host, 'height', (this.height || hostHeight) + 'rem');
    this.renderer.setStyle(highlight, 'animation-duration', highlightDuration + 'ms');
  }

  unit(value: number | string): SafeStyle {
    let style = '';
    if (typeof value === 'number') {
      style = `${value}rem`;
    } else if (typeof value === 'string') {
      // check if the string contains [min,max] pairs
      if (/\[\d{1,4},\s*\d{1,4}]/g.test(value)) {
        let newValue: string = value;
        // extract all occurrences of [min,max]
        const minMaxOccurrences = value.match(/\[\d{1,4},\s*\d{1,4}]/g);
        minMaxOccurrences?.forEach(range => {
          const minMaxValues = range.match(/\d{1,4}/g);
          // generate a random number between MIN and MAX
          const min = minMaxValues ? Number.parseInt(minMaxValues[0], 10) : 0;
          const max = minMaxValues ? Number.parseInt(minMaxValues[1], 10) : 100;
          const randomValue = this.randomNumberFromInterval(min, max);
          // replace the [min,max] occurrence with the random number
          newValue = newValue.replace(range.toString(), randomValue.toString());
        });
        style = newValue;
      } else {
        style = value;
      }
    }
    return this.sanitizer.bypassSecurityTrustStyle(style);
  }

  private randomNumberFromInterval(min: number, max: number): number { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  private parseRgb(rgb: string): { r: number; g: number; b: number; a: number } | null {
    const result = /^rgba?\((\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3})(?:,\s?(1|0|0?\.\d+))?\)$/i.exec(rgb);
    return result ? {
      r: Number.parseInt(result[1], 10),
      g: Number.parseInt(result[2], 10),
      b: Number.parseInt(result[3], 10),
      a: Number.parseFloat(result[4])
    } : null;
  }

  private hexToRgb(hex: string): { r: number; g: number; b: number; a: number } | null {
    const result = /^#?([\da-f]{2})([\da-f]{2})([\da-f]{2})$/i.exec(hex);
    return result ? {
      r: Number.parseInt(result[1], 16),
      g: Number.parseInt(result[2], 16),
      b: Number.parseInt(result[3], 16),
      a: 1
    } : null;
  }
}
