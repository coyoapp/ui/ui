import {
  ComponentFixture, inject, TestBed, waitForAsync
} from '@angular/core/testing';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { SkeletonComponent } from './skeleton.component';

describe('SkeletonComponent', () => {
  let component: SkeletonComponent;
  let fixture: ComponentFixture<SkeletonComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SkeletonComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should get the highlight color if it is null', () => {
    fixture.detectChanges();

    expect(component.highlightColor).toBe(null);
  });

  it('should get the highlight color if it is invalid', () => {
    component.highlight = 'Hello World';
    fixture.detectChanges();

    expect(component.highlightColor).toBe(null);
  });

  it('should get the highlight color if it is valid', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
    component.highlight = '#CCFFAA';
    fixture.detectChanges();

    expect(component.highlightColor).toEqual(
      sanitizer.bypassSecurityTrustStyle(
        'linear-gradient(90deg, rgba(204, 255, 170, 0) 0%,'
        + ' rgba(204, 255, 170, 0.25) 25%,'
        + ' rgba(204, 255, 170, 0.5) 50%,'
        + ' rgba(204, 255, 170, 0.25) 75%,'
        + ' rgba(204, 255, 170, 0) 100%)'
      )
    );
  }));

  it('should suffix numbers with "rem" using unit()', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
    fixture.detectChanges();

    expect(component.unit(8)).toEqual(sanitizer.bypassSecurityTrustStyle('8rem'));
  }));

  it('should not suffix strings with "rem" using unit()', inject([DomSanitizer], (sanitizer: DomSanitizer) => {
    fixture.detectChanges();

    expect(component.unit('50%')).toEqual(sanitizer.bypassSecurityTrustStyle('50%'));
  }));

  it('should set the total height', () => {
    component.skeletons = [
      {
        top: 8, left: 16, width: 100, height: 32
      },
      {
        top: 24, left: 16, width: 100, height: 8
      }
    ];
    fixture.detectChanges();

    expect(fixture.nativeElement.style.height).toBe('48rem');
  });

  it('should use a defined total height', () => {
    component.height = 64;
    component.skeletons = [
      {
        top: 8, left: 16, width: 100, height: 32
      },
      {
        top: 24, left: 16, width: 100, height: 8
      }
    ];
    fixture.detectChanges();

    expect(fixture.nativeElement.style.height).toBe('64rem');
  });

  it('should return number between min and max when calling randomNumberFromInterval', () => {
    const randomNumber = (component as any).randomNumberFromInterval(10, 40);
    const isNumberBetweenMinAndMax = randomNumber >= 10 && randomNumber <= 40;

    expect(isNumberBetweenMinAndMax).toBeTrue();
  });

  it('should calculate correct element style with random values', () => {
    const style: SafeStyle = component.unit('calc([10,20]% - 5px)');
    const calcValue = Number.parseInt(style.toString().split('(')[1].split('%')[0], 10);
    const isNumberBetweenMinAndMax = calcValue >= 10 && calcValue <= 20;

    expect(isNumberBetweenMinAndMax).toBeTrue();
  });
});
