export interface Skeleton {
  top: number;
  left: number | string;
  width: number | string;
  height: number;
  radius?: number | string;
  color?: string;
}
