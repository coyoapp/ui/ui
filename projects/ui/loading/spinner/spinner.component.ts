import {
  ChangeDetectionStrategy, Component, HostBinding, Input, ViewEncapsulation
} from '@angular/core';

/**
 * Use spinners to let users know that content is being loaded and will take
 * an indeterminate amount of time. However, there should only be a single
 * spinner on a page at one time. **Skeletons should be used in most
 * situations** when a loading indication is needed.
 *
 * The spinner inherits the current **font color** and **size** by default.
 * Thus, it can be styled using the `font-size` and `color` properties. The
 * `size` input can also be used to quickly adjust the size of the spinner.
 */
@Component({
  selector: 'cui-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-spinner'
  }
})
export class SpinnerComponent {

  /**
   * The size of the spinner.
   */
  @Input()
  @HostBinding('attr.data-cui-size')
  size?: 'xs' | 's' | 'm' | 'l' | 'xl';
}
