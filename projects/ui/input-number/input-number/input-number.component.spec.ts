import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputNumberComponent } from './input-number.component';

describe('InputNumberComponent', () => {
  let component: InputNumberComponent;
  let fixture: ComponentFixture<InputNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InputNumberComponent]
    }).overrideTemplate(InputNumberComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should increment', () => {
    // given
    component.value = 3;
    fixture.detectChanges();

    // when
    component.increment();

    // then
    expect(component.value).toBe(4);
  });

  it('should decrement', () => {
    // given
    component.value = 2;
    fixture.detectChanges();

    // when
    component.decrement();

    // then
    expect(component.value).toBe(1);
  });

  it('should check if one can increment', () => {
    // given
    component.value = 3;
    component.max = 3;
    fixture.detectChanges();

    // when
    component.increment();

    // then
    expect(component.value).toBe(3);
    expect(component.canIncrement).toBe(false);
  });

  it('should check if one can decrement', () => {
    // given
    component.value = 3;
    component.min = 3;
    fixture.detectChanges();

    // when
    component.decrement();

    // then
    expect(component.value).toBe(3);
    expect(component.canDecrement).toBe(false);
  });
});
