import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { clamp } from '@coyoapp/ui/cdk';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';

@Component({
  selector: 'cui-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputNumberComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-number'
  }
})
export class InputNumberComponent extends FormFieldControlComponent<number> {

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  /**
   * The minimum value for this input.
   */
  @Input()
  min: number | null = null;

  /**
   * The maximum value for this input.
   */
  @Input()
  max: number | null = null;

  /**
   * The interval between legal numbers in this input.
   */
  @Input()
  step = 1;

  private get safeValue(): number {
    return this.value || 0;
  }

  get canIncrement(): boolean {
    return this.max == null || this.safeValue < this.max;
  }

  get canDecrement(): boolean {
    return this.min == null || this.safeValue > this.min;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  increment(): void {
    this.value = clamp(this.safeValue + this.step, this.min, this.max);
  }

  decrement(): void {
    this.value = clamp(this.safeValue - this.step, this.min, this.max);
  }
}
