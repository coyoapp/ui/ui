import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from '@coyoapp/ui/button';
import { InputNumberComponent } from './input-number/input-number.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, ButtonModule],
  declarations: [InputNumberComponent],
  exports: [InputNumberComponent]
})
export class InputNumberModule {}
