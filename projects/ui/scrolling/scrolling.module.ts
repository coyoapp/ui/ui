import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ScrollableComponent } from './scrollable/scrollable.component';
import { ScrolledBottomDirective } from './scrolled-bottom/scrolled-bottom.directive';
import { ScrolledLeftDirective } from './scrolled-left/scrolled-left.directive';
import { ScrolledRightDirective } from './scrolled-right/scrolled-right.directive';
import { ScrolledTopDirective } from './scrolled-top/scrolled-top.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ScrollableComponent, ScrolledTopDirective, ScrolledBottomDirective, ScrolledLeftDirective, ScrolledRightDirective],
  exports: [ScrollableComponent, ScrolledTopDirective, ScrolledBottomDirective, ScrolledLeftDirective, ScrolledRightDirective]
})
export class ScrollingModule {}
