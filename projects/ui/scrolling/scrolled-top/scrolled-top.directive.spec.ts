import { noop } from '@coyoapp/ui/cdk';
import { NgZone, Renderer2 } from '@angular/core';
import { Scrollable } from '@coyoapp/ui/scrolling/scrollable/scrollable';
import { ScrolledTopDirective } from './scrolled-top.directive';

describe('ScrolledTopDirective', () => {
  let directive: ScrolledTopDirective;
  let zone: jasmine.SpyObj<NgZone>;
  let elementRef: any;
  let renderer2: jasmine.SpyObj<Renderer2>;

  beforeEach(() => {
    zone = jasmine.createSpyObj('NgZone', ['runOutsideAngular']);
    zone.runOutsideAngular.and.callFake(<T>(): T => ({} as any));
    elementRef = {
      nativeElement: {
        focus: noop
      }
    };
    renderer2 = jasmine.createSpyObj('Renderer2', ['addClass', 'removeClass']);
    directive = new ScrolledTopDirective(zone, elementRef, renderer2);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call attachEmitter onInit', () => {
    spyOn(Scrollable.prototype as any, 'attachEmitter');
    directive.ngOnInit();

    expect((Scrollable.prototype as any).attachEmitter).toHaveBeenCalledWith('top', directive.cuiScrolledTop, Scrollable.SCROLLED_BUFFER);
  });

  it('should call checkInit on ngAfterContentInit', () => {
    spyOn(Scrollable.prototype as any, 'checkInit');
    directive.ngAfterContentInit();

    expect((Scrollable.prototype as any).checkInit).toHaveBeenCalledWith(Scrollable.SCROLLED_INIT);
  });
});
