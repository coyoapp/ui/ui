import { Renderer2 } from '@angular/core';
import { fakeAsync, tick } from '@angular/core/testing';
import { Scrollable } from '@coyoapp/ui/scrolling/scrollable/scrollable';
import { Subject } from 'rxjs';
import { ScrollableComponent } from './scrollable.component';
import createSpyObj = jasmine.createSpyObj;

describe('ScrollableComponent', () => {
  let component: ScrollableComponent;
  const elementRef = {
    nativeElement: {
      scrollTop: 10,
      scrollLeft: 10,
      scrollWidth: 20,
      scrollHeight: 20,
      clientWidth: 5,
      clientHeight: 5
    }
  };
  const renderer2: jasmine.SpyObj<Renderer2> = jasmine.createSpyObj('Renderer2', ['addClass', 'removeClass']);
  const subj: Subject<boolean> = new Subject();
  const ngZone = createSpyObj('mockNgZone', ['runOutsideAngular', 'run']);
  ngZone.runOutsideAngular.and.callFake(() => subj);
  (ScrollableComponent.prototype as any).scrollElementRef = elementRef;

  beforeEach(() => {
    component = new ScrollableComponent(ngZone, elementRef as any, renderer2);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get scroll element', () => {
    expect((component as any).scrollElement).toBeTruthy();
  });

  it('should call checkInit on AfterContentInit ', () => {
    spyOn(Scrollable.prototype as any, 'checkInit');
    component.ngAfterContentInit();

    expect((Scrollable.prototype as any).checkInit).toHaveBeenCalled();
  });

  it('should call toggle class after onInit ', fakeAsync(() => {
    spyOn(Scrollable.prototype as any, 'toggleClass');
    (component as any).scrolled = subj.asObservable();
    (component as any).init = subj.asObservable();
    component.ngOnInit();
    subj.next(false);
    tick();

    expect((Scrollable.prototype as any).toggleClass).toHaveBeenCalled();
  }));
});
