import { noop } from '@coyoapp/ui/cdk';
import { NgZone, Renderer2 } from '@angular/core';
import { Scrollable } from '@coyoapp/ui/scrolling/scrollable/scrollable';
import { fakeAsync, tick } from '@angular/core/testing';
import { Subject } from 'rxjs';

class ScrollableTest extends Scrollable {}

describe('Scrollable', () => {
  let scrollable: ScrollableTest;
  let zone: jasmine.SpyObj<NgZone>;
  let elementRef: any;
  let renderer2: jasmine.SpyObj<Renderer2>;

  beforeEach(() => {
    zone = jasmine.createSpyObj('NgZone', ['runOutsideAngular']);
    zone.runOutsideAngular.and.callFake(<T>(): T => ({} as any));
    elementRef = {
      nativeElement: {
        focus: noop,
        scrollTop: 10,
        scrollLeft: 10,
        scrollWidth: 20,
        scrollHeight: 20,
        clientWidth: 5,
        clientHeight: 5
      }
    };
    renderer2 = jasmine.createSpyObj('Renderer2', ['addClass', 'removeClass']);
    scrollable = new ScrollableTest(zone, elementRef, renderer2);
  });

  it('should create an instance and trigger runOutsideAngular', fakeAsync(() => {
    expect(scrollable).toBeTruthy();
    (scrollable as any).scrolled.subscribe();
    tick();

    expect(zone.runOutsideAngular).toHaveBeenCalled();
  }));

  it('should complete observables onDestroy', () => {
    spyOn((scrollable as any).init, 'complete');
    spyOn((scrollable as any).destroyed, 'complete');
    spyOn((scrollable as any).destroyed, 'next');
    scrollable.ngOnDestroy();

    expect((scrollable as any).init.complete).toHaveBeenCalled();
    expect((scrollable as any).destroyed.complete).toHaveBeenCalled();
    expect((scrollable as any).destroyed.next).toHaveBeenCalled();
  });

  it('should call init next on checkInit', () => {
    spyOn((scrollable as any).init, 'next');
    (scrollable as any).checkInit(true);

    expect((scrollable as any).init.next).toHaveBeenCalled();
  });

  it('should call appropriate methods on attachEmitter', fakeAsync(() => {
    const subj = new Subject();
    const subj2 = new Subject();
    const obs = subj2.asObservable();
    const emitter = jasmine.createSpyObj('EventEmitter', ['emit']);

    spyOn(scrollable as any, 'getScrollOffset').and.returnValue(10);
    (scrollable as any).scrolled = obs;
    (scrollable as any).init = subj;
    subj.next(subj2.asObservable());
    tick();
    (scrollable as any).attachEmitter('top', emitter, 20);
    subj2.next(true);
    tick();

    expect((scrollable as any).getScrollOffset).toHaveBeenCalled();
    expect(emitter.emit).toHaveBeenCalled();
  }));

  it('should return correct scroll value on getScrollOffset call', () => {
    const resTop = (scrollable as any).getScrollOffset('top');
    const resLeft = (scrollable as any).getScrollOffset('left');
    const resRight = (scrollable as any).getScrollOffset('right');
    const resBottom = (scrollable as any).getScrollOffset('bottom');

    expect(resTop).toEqual(10);
    expect(resLeft).toEqual(10);
    expect(resRight).toEqual(5);
    expect(resBottom).toEqual(5);
  });

  it('should toggle the class on toggleClass call', () => {
    (scrollable as any).toggleClass('top', true);

    expect(renderer2.addClass).toHaveBeenCalledWith(elementRef.nativeElement, 'top');
    (scrollable as any).toggleClass('top', false);

    expect(renderer2.removeClass).toHaveBeenCalledWith(elementRef.nativeElement, 'top');
  });
});
