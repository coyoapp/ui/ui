import { Directive, ElementRef, EventEmitter, NgZone, OnDestroy, Renderer2 } from '@angular/core';
import { fromEvent, merge, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';

/**
 * Base class to implement scrolling directives.
 */
@Directive()
export abstract class Scrollable implements OnDestroy {
  static readonly SCROLLED_INIT = true;
  static readonly SCROLLED_BUFFER = 0;

  protected readonly init = new Subject<void>();
  protected readonly destroyed = new Subject<void>();
  protected readonly scrolled: Observable<Event> = new Observable(
    observer => this.ngZone.runOutsideAngular(
      () => fromEvent(this.scrollElement, 'scroll')
        .pipe(takeUntil(this.destroyed))
        .subscribe(observer)
    )
  );

  get scrollElement(): HTMLElement {
    return this.elementRef.nativeElement;
  }

  constructor(
    protected readonly ngZone: NgZone,
    protected readonly elementRef: ElementRef<HTMLElement>,
    protected readonly renderer2: Renderer2
  ) {}

  ngOnDestroy() {
    this.init.complete();
    this.destroyed.next();
    this.destroyed.complete();
  }

  protected attachEmitter(from: 'top' | 'left' | 'right' | 'bottom', emitter: EventEmitter<void>, buffer: number) {
    merge(this.init, this.scrolled)
      .pipe(map(() => this.getScrollOffset(from)))
      .pipe(map(offset => offset <= buffer))
      .pipe(distinctUntilChanged())
      .pipe(filter(isLower => isLower))
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => emitter.emit());
  }

  protected checkInit(init = true): void {
    if (init) {
      this.init.next();
    }
  }

  protected getScrollOffset(from: 'top' | 'left' | 'right' | 'bottom'): number {
    switch (from) {
      case 'top': return this.scrollElement.scrollTop;
      case 'left': return this.scrollElement.scrollLeft;
      case 'right': return this.scrollElement.scrollWidth - this.scrollElement.clientWidth - this.scrollElement.scrollLeft;
      case 'bottom': return this.scrollElement.scrollHeight - this.scrollElement.clientHeight - this.scrollElement.scrollTop;
      default: return 0;
    }
  }

  protected toggleClass(name: string, value: boolean, el: any = this.elementRef.nativeElement): void {
    if (value) {
      this.renderer2.addClass(el, name);
    } else {
      this.renderer2.removeClass(el, name);
    }
  }
}
