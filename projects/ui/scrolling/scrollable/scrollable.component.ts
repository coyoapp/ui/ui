import {
  AfterContentInit, ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation
} from '@angular/core';
import { merge } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { Scrollable } from './scrollable';

/**
 * Component to add scroll shadows to a container.
 */
@Component({
  selector: 'cui-scrollable',
  templateUrl: './scrollable.component.html',
  styleUrls: ['./scrollable.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    class: 'cui-scrollable'
  }
})
export class ScrollableComponent extends Scrollable implements OnInit, AfterContentInit {

  /** Flags to enable/disable scroll shadows. */
  @Input()
  shadow: { x?: boolean; y?: boolean; } = { x: true, y: true };

  /** Flags to enable/disable overflow. */
  @Input()
  overflow: { x?: boolean; y?: boolean; } = { y: true };

  /** Flag to enable/disable overscroll behavior. */
  @Input()
  overscroll = true;

  @ViewChild('scrollElement', { static: true })
  protected scrollElementRef!: ElementRef<HTMLElement>;

  get scrollElement(): HTMLElement {
    return this.scrollElementRef.nativeElement;
  }

  ngOnInit(): void {
    merge(this.init, this.scrolled)
      .pipe(map(() => ({
        top: this.getScrollOffset('top') > 0,
        left: this.getScrollOffset('left') > 0,
        right: this.getScrollOffset('right') > 0,
        bottom: this.getScrollOffset('bottom') > 0
      })))
      .pipe(distinctUntilChanged())
      .pipe(takeUntil(this.destroyed))
      .subscribe(({ top, left, right, bottom }) => {
        this.toggleClass('cui-scrollable-top', top);
        this.toggleClass('cui-scrollable-left', left);
        this.toggleClass('cui-scrollable-right', right);
        this.toggleClass('cui-scrollable-bottom', bottom);
      });
  }

  ngAfterContentInit(): void {
    this.checkInit();
  }
}
