import { noop } from '@coyoapp/ui/cdk';
import { NgZone, Renderer2 } from '@angular/core';
import { Scrollable } from '@coyoapp/ui/scrolling/scrollable/scrollable';
import { ScrolledRightDirective } from './scrolled-right.directive';

describe('ScrolledRightDirective', () => {
  let directive: ScrolledRightDirective;
  let zone: jasmine.SpyObj<NgZone>;
  let elementRef: any;
  let renderer2: jasmine.SpyObj<Renderer2>;

  beforeEach(() => {
    zone = jasmine.createSpyObj('NgZone', ['runOutsideAngular']);
    zone.runOutsideAngular.and.callFake(<T>(): T => ({} as any));
    elementRef = {
      nativeElement: {
        focus: noop
      }
    };
    renderer2 = jasmine.createSpyObj('Renderer2', ['addClass', 'removeClass']);
    directive = new ScrolledRightDirective(zone, elementRef, renderer2);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call attachEmitter onInit', () => {
    spyOn(Scrollable.prototype as any, 'attachEmitter');
    directive.ngOnInit();

    expect((Scrollable.prototype as any).attachEmitter).toHaveBeenCalledWith('right', directive.cuiScrolledRight, Scrollable.SCROLLED_BUFFER);
  });

  it('should call checkInit on ngAfterContentInit', () => {
    spyOn(Scrollable.prototype as any, 'checkInit');
    directive.ngAfterContentInit();

    expect((Scrollable.prototype as any).checkInit).toHaveBeenCalledWith(Scrollable.SCROLLED_INIT);
  });
});
