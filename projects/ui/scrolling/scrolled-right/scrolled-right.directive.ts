import {
  AfterContentInit, Directive, ElementRef, EventEmitter, Input, NgZone, OnInit, Optional, Output, Renderer2, Self
} from '@angular/core';
import { Scrollable } from '../scrollable/scrollable';
import { ScrollableComponent } from '../scrollable/scrollable.component';

/**
 * Directive to get notified when a srollable container has been scrolled to the
 * right. An initial event will be triggered after content initialization and can
 * be disabled via the `cuiScrolledInit` input. To adjust the buffer value that
 * is used to fire the event, use the `cuiScrolledBuffer` input.
 *
 * @example
 * ```
 * <div (cuiScrolledRight)="onScrolledToRight()"
 *      [cuiScrolledInit]="false"
 *      [cuiScrolledBuffer]="250">
 *   <!-- function called every time the right scroll distance undercuts 250px -->
 * </div>
 * ```
 */
@Directive({
  selector: '[cuiScrolledRight]'
})
export class ScrolledRightDirective extends Scrollable implements OnInit, AfterContentInit {

  /**
   * Flag to fire an initial event after content initialization.
   */
  @Input()
  cuiScrolledInit = Scrollable.SCROLLED_INIT;

  /**
   * Buffer to be used to calculate the scroll distance.
   */
  @Input()
  cuiScrolledBuffer = Scrollable.SCROLLED_BUFFER;

  /**
   * Event emitter firing every time the scroll distance undercuts the buffer.
   */
  @Output()
  readonly cuiScrolledRight: EventEmitter<void> = new EventEmitter();

  get scrollElement(): HTMLElement {
    return this.scrollable?.scrollElement || this.elementRef.nativeElement;
  }

  constructor(
    ngZone: NgZone,
    elementRef: ElementRef<HTMLElement>,
    renderer2: Renderer2,
    @Optional() @Self() private readonly scrollable?: ScrollableComponent
  ) {
    super(ngZone, elementRef, renderer2);
  }

  ngOnInit() {
    this.attachEmitter('right', this.cuiScrolledRight, this.cuiScrolledBuffer);
  }

  ngAfterContentInit(): void {
    this.checkInit(this.cuiScrolledInit);
  }
}
