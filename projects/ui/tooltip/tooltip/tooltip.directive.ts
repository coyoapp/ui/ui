import {
  Directive, ElementRef, Input, OnChanges, OnDestroy, SimpleChanges
} from '@angular/core';
import { TippyInstance, TippyService } from '@ngneat/helipopper';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Platform } from '@angular/cdk/platform';

/**
 * The preferred placement of a tooltip.
 */
export type TooltipPlacement = 'top' | 'top-start' | 'top-end'
| 'right' | 'right-start' | 'right-end'
| 'bottom' | 'bottom-start' | 'bottom-end'
| 'left' | 'left-start' | 'left-end'
| 'auto' | 'auto-start' | 'auto-end';

export type TooltipDelay = number | [number | null, number | null];

/**
 * A text label that is displayed when the user hovers over an element. The
 * tooltip will be displayed below the element but this can be configured
 * using the `cuiTooltipPlacement` input. The tooltip can be displayed above
 * (top), below (bottom), left, or right of the element. By default the position
 * will be above (top).
 *
 * By default, the tooltip will be immediately shown when the user's mouse
 * hovers over the tooltip's trigger element and immediately hides when the
 * user's mouse leaves. To add a delay before showing or hiding the tooltip,
 * you can use the `cuiTooltipDelay` and provide a delay time in milliseconds
 * or a tuple of delay times in milliseconds (e.g. `[250, 1500]`).
 *
 * Elements with the `cuiTooltip` will add an `aria-describedby` label that
 * provides a reference to a visually hidden element containing the tooltip's
 * message. This provides screenreaders the information needed to read out the
 * tooltip's contents when the end-user focuses on the element triggering the
 * tooltip.
 */
@Directive({
  selector: '[cuiTooltip]'
})
export class TooltipDirective implements OnChanges, OnDestroy {
  private tippy?: TippyInstance;

  /**
   * The content of the tooltip.
   */
  @Input()
  cuiTooltip = '';

  /**
   * The screenreader content of the tooltip.
   */
  @Input()
  cuiTooltipScreenreader: string | boolean = false;

  /**
   * The preferred placement of the tooltip.
   */
  @Input()
  cuiTooltipPlacement: TooltipPlacement = 'top';

  /**
   * Delay in ms once a trigger event is fired before a tooltip shows or hides.
   */
  @Input()
  cuiTooltipDelay: TooltipDelay = 0;

  /**
   * Event(s) that cause the tippy to show. Multiple event names are separated by spaces.
   */
  @Input()
  cuiTooltipTrigger?: string;

  /**
   * Allow tooltip to contain HTML.
   */
  @Input()
  cuiTooltipAllowHtml = false;

  /**
   * Temporarily prevent a tooltip from showing or hiding.
   */
  @Input()
  cuiTooltipDisabled = false;

  private onShown = () => {
    if (this.cuiTooltipScreenreader) {
      this.liveAnnouncer.announce(this.cuiTooltipScreenreader === true ? this.cuiTooltip : this.cuiTooltipScreenreader);
    }
  };

  constructor(
    private readonly elementRef: ElementRef,
    private readonly tippyService: TippyService,
    private readonly liveAnnouncer: LiveAnnouncer,
    private platform: Platform
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.cuiTooltipDisabled || this.platform.IOS || this.platform.ANDROID) {
      this.tippy?.disable();
      return;
    } else if (!this.cuiTooltip) {
      this.tearDown();
      return;
    } else if (changes.cuiTooltip && !this.tippy?.state.isShown) {
      // workaround as tippy does not update content correctly while hidden
      this.tearDown();
    }
    const allowHTML = this.cuiTooltipAllowHtml;
    const placement = this.cuiTooltipPlacement;
    const delay = this.cuiTooltipDelay;
    const trigger = this.cuiTooltipTrigger;

    this.tippy?.setContent(this.cuiTooltip);
    this.tippy?.setProps({
      placement,
      delay,
      trigger,
      allowHTML
    });
    this.tippy?.enable();
    this.tippy ??= this.tippyService.create(this.elementRef.nativeElement, this.cuiTooltip, {
      variation: 'tooltip',
      theme: 'tooltip',
      placement,
      delay,
      trigger,
      allowHTML,
      onShown: this.onShown,
      aria: {
        content: 'describedby'
      }
    });

  }

  ngOnDestroy(): void {
    this.tearDown();
  }

  private tearDown(): void {
    this.tippy?.destroy();
    this.tippy = undefined;
  }
}
