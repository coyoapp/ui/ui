import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ElementRef, SimpleChanges } from '@angular/core';
import { noop } from '@coyoapp/ui/cdk';
import { ExtendedTippyInstance, TippyService } from '@ngneat/helipopper';
import { TooltipDirective } from './tooltip.directive';

describe('TooltipDirective', () => {
  let directive: TooltipDirective;
  let nativeElement: HTMLElement;
  let elementRef: ElementRef;
  let tippyService: jasmine.SpyObj<TippyService>;
  let tippyInstance: jasmine.SpyObj<ExtendedTippyInstance<any>>;
  let announcer: jasmine.SpyObj<LiveAnnouncer>;
  const platform = { ANDROID: false, IOS: false };

  beforeEach(() => {
    nativeElement = {} as HTMLElement;
    elementRef = { nativeElement } as ElementRef;
    tippyService = jasmine.createSpyObj('TippyService', ['create']);
    tippyInstance = jasmine.createSpyObj('TippyInstance', ['enable', 'disable', 'setContent', 'setProps', 'destroy']);
    tippyService.create.and.returnValue(tippyInstance);
    announcer = jasmine.createSpyObj('LiveAnnouncer', ['announce']);
    directive = new TooltipDirective(elementRef, tippyService, announcer, platform as any);
    directive.cuiTooltipAllowHtml = false;
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should not create the tooltip if empty', () => {
    // when
    directive.ngOnChanges({});

    // then
    expect(tippyService.create).not.toHaveBeenCalled();
  });

  it('should create the tooltip', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.cuiTooltipPlacement = 'bottom';
    directive.cuiTooltipDelay = 500;
    directive.cuiTooltipTrigger = 'click';

    // when
    directive.ngOnChanges({});

    // then
    expect(tippyService.create).toHaveBeenCalledWith(nativeElement, 'Tooltip!', {
      variation: 'tooltip',
      theme: 'tooltip',
      placement: 'bottom',
      delay: 500,
      trigger: 'click',
      allowHTML: false,
      onShown: (directive as any).onShown,
      aria: { content: 'describedby' }
    });
  });

  it('should update the tooltip', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.cuiTooltipPlacement = 'bottom';
    directive.cuiTooltipDelay = 500;
    directive.cuiTooltipTrigger = 'click';
    directive.ngOnChanges({});

    // when
    directive.cuiTooltip = 'Another Tooltip!';
    directive.cuiTooltipPlacement = 'left';
    directive.cuiTooltipDelay = 250;
    directive.ngOnChanges({});

    // then
    expect(tippyInstance.setContent).toHaveBeenCalledWith('Another Tooltip!');
    expect(tippyInstance.setProps).toHaveBeenCalledWith({
      placement: 'left',
      delay: 250,
      trigger: 'click',
      allowHTML: false
    });

    expect(tippyInstance.enable).toHaveBeenCalled();
  });

  it('should allow HTML', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.cuiTooltipAllowHtml = false;
    directive.cuiTooltipTrigger = 'click';
    directive.ngOnChanges({});

    // when
    directive.cuiTooltip = 'Tooltip<br/>Another Line';
    directive.cuiTooltipAllowHtml = true;
    directive.cuiTooltipTrigger = 'click';
    directive.ngOnChanges({});

    // then
    expect(tippyInstance.setContent).toHaveBeenCalledWith('Tooltip<br/>Another Line');
    expect(tippyInstance.setProps).toHaveBeenCalledWith({
      placement: 'top',
      delay: 0,
      trigger: 'click',
      allowHTML: true
    });

    expect(tippyInstance.enable).toHaveBeenCalled();
  });

  it('should disable the tooltip', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.ngOnChanges({});

    // when
    directive.cuiTooltipDisabled = true;
    directive.ngOnChanges({});

    // then
    expect(tippyInstance.disable).toHaveBeenCalled();
  });

  it('should remove the tooltip if empty', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.ngOnChanges({});

    // when
    directive.cuiTooltip = '';
    directive.ngOnChanges({});

    // then
    expect(tippyInstance.destroy).toHaveBeenCalled();
  });

  it('should tear down', () => {
    // given
    directive.cuiTooltip = 'Tooltip!';
    directive.ngOnChanges({});

    // when
    directive.ngOnDestroy();

    // then
    expect(tippyInstance.destroy).toHaveBeenCalled();
  });

  it('should tear down when calling onChanges', () => {
    directive.cuiTooltip = 'Tooltip!';
    spyOn(directive as any, 'tearDown');
    (directive as any).tippy = {
      state: {
        isShown: false
      },
      setContent: noop,
      setProps: noop,
      enable: noop
    };
    directive.ngOnChanges({ cuiTooltip: true } as unknown as SimpleChanges);

    expect((directive as any).tearDown).toHaveBeenCalled();
  });
});
