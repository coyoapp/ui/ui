import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { popperVariation, provideTippyConfig, tooltipVariation } from '@ngneat/helipopper';
import { PlatformModule } from '@angular/cdk/platform';
import { IsEllipsedDirective } from './is-ellipsed/is-ellipsed.directive';
import { TooltipDirective } from './tooltip/tooltip.directive';

@NgModule({
  imports: [
    CommonModule,
    PlatformModule
  ],
  declarations: [
    TooltipDirective,
    IsEllipsedDirective
  ],
  exports: [
    TooltipDirective,
    IsEllipsedDirective
  ]
})
export class TooltipModule {

  static forRoot(): ModuleWithProviders<TooltipModule> {
    return {
      ngModule: TooltipModule,
      providers: [provideTippyConfig({
        variations: {
          tooltip: tooltipVariation,
          popover: {
            ...popperVariation,
            placement: 'auto',
            appendTo: 'parent',
            arrow: false,
            animation: 'shift-away-subtle',
            offset: [0, 8],
            maxWidth: 'unset'
          }
        }
      })]
    };
  }
}
