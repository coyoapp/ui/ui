import { ElementRef } from '@angular/core';
import { ResizeObserverService } from '@ng-web-apis/resize-observer';
import { Subject } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';
import { IsEllipsedDirective } from './is-ellipsed.directive';

describe('IsEllipsedDirective', () => {
  let entries: Subject<ResizeObserverEntry[]>;
  let entries$: ResizeObserverService;
  let elementRef: ElementRef;

  beforeEach(() => {
    entries = new Subject();
    entries$ = entries.asObservable();
    elementRef = {
      nativeElement: {
        offsetWidth: 0,
        offsetHeight: 0,
        scrollWidth: 0,
        scrollHeight: 0
      }
    };
  });

  it('should create an instance', () => {
    const directive = new IsEllipsedDirective(elementRef, entries$);

    expect(directive).toBeTruthy();
  });

  it('should calculate correct isEllipsed value', fakeAsync(() => {
    const directive = new IsEllipsedDirective(elementRef, entries$);
    spyOn((directive as any).isEllipsed, 'next');
    directive.ngOnInit();

    expect((directive as any).subscription).toBeTruthy();
    entries.next(true as any);
    tick();

    expect((directive as any).isEllipsed.next).toHaveBeenCalled();
  }));

  it('should unsubscribe onDestroy', () => {
    const directive = new IsEllipsedDirective(elementRef, entries$);
    directive.ngOnInit();

    expect((directive as any).subscription).toBeTruthy();
    spyOn((directive as any).subscription, 'unsubscribe');
    directive.ngOnDestroy();

    expect((directive as any).subscription.unsubscribe).toHaveBeenCalled();
  });

});
