import {
  Directive, ElementRef, EventEmitter, Inject, OnDestroy, OnInit, Output
} from '@angular/core';
import { ResizeObserverService, RESIZE_OPTION_BOX } from '@ng-web-apis/resize-observer';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

/**
 * A helper directive to check if text has been ellipsed via CSS.
 */
@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[isEllipsed]',
  providers: [ResizeObserverService, {
    provide: RESIZE_OPTION_BOX,
    useValue: 'border-box'
  }]
})
export class IsEllipsedDirective implements OnInit, OnDestroy {
  private subscription?: Subscription;

  @Output()
  readonly isEllipsed = new EventEmitter<boolean>();

  constructor(
    private readonly elementRef: ElementRef,
    @Inject(ResizeObserverService) private readonly entries$: ResizeObserverService
  ) {}

  ngOnInit(): void {
    this.subscription = this.entries$.pipe(map(() => this.elementRef.nativeElement.offsetWidth < this.elementRef.nativeElement.scrollWidth
      || this.elementRef.nativeElement.offsetHeight < this.elementRef.nativeElement.scrollHeight)).pipe(distinctUntilChanged()).subscribe(v => this.isEllipsed.next(v));
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
