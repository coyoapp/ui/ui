import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Injector,
  Input,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormFieldControlComponent, provideFormFieldControl } from '@coyoapp/ui/form-field';

@Component({
  selector: 'cui-input-timepicker',
  templateUrl: './input-timepicker.component.html',
  styleUrls: ['./input-timepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [provideFormFieldControl(InputTimepickerComponent)],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'cui-input-timepicker'
  }
})
export class InputTimepickerComponent extends FormFieldControlComponent<string> {
  readonly mask = 'Hh:m0';
  readonly maskWithSeconds = 'Hh:m0:s0';

  /**
   * Has seconds flag
   */
  @Input() seconds = false;

  @ViewChild('focus', { static: true })
  protected focusElementRef!: ElementRef<HTMLElement>;

  constructor(injector: Injector) {
    super(injector);
  }
}
