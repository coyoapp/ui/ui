import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask';
import { InputTimepickerComponent } from './input-timepicker/input-timepicker.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgxMaskPipe, NgxMaskDirective],
  declarations: [InputTimepickerComponent],
  exports: [InputTimepickerComponent]
})

export class InputTimepickerModule {

  static forRoot(): ModuleWithProviders<InputTimepickerModule> {
    return {
      ngModule: InputTimepickerModule,
      providers: [provideNgxMask()]
    };
  }
}
