variables:
  CLI_VERSION: 17.3.6

stages:
  - init
  - build
  - test
  - deploy
  - release

workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

default:
  image: trion/ng-cli:${CLI_VERSION}
  before_script:
    - NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
    - NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
    - npm ci --cache .npm --prefer-offline

cache:
  key:
    files:
      - package-lock.json
  paths:
    - .npm
  policy: pull

install:
  stage: init
  script:
    - echo 'Warming the cache'
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - .npm
    policy: push
  only:
    changes:
      - package-lock.json

build:
  stage: build
  script:
    - npm run build:ci
  except:
    - tags

lint:
  stage: test
  script:
    - npm run lint:src
  except:
    - tags

lint-style:
  stage: test
  script:
    - npm run lint:style
  except:
    - tags

test:
  stage: test
  image: trion/ng-cli-karma:${CLI_VERSION}
  script:
    - npm run test:ci
  coverage: '/Lines \W+: (\d+(?:\.\d+)?)%.*/'
  artifacts:
    when: always
    paths:
      - coverage/ui
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/ui/cobertura.xml
      junit: coverage/ui/junit.xml
    expire_in: 21 days
  except:
    - tags

pages:
  stage: deploy
  script:
    - npm run build:storybook
    - mv -f dist/storybook public
  artifacts:
    paths:
      - public
    expire_in: 7 days
  only:
    - master

publish:
  stage: release
  script:
    - npm run build:ci
    - cd dist/ui
    - |
      if [[ ! -f .npmrc ]]; then
        echo 'No .npmrc found! Creating one now. Please review the following link for more information: https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#authenticating-with-a-ci-job-token'

        {
          echo '//registry.npmjs.org/:_authToken=${NPM_TOKEN}'
        } >> .npmrc

      fi
    - npm publish --access public
  only:
    - tags

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  before_script:
    - ''
  script:
    - echo 'Creating release...'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: './CHANGELOG.md'
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_SHA
  only:
    - tags
