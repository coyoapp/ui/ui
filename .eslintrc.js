const OFF = 0, WARN = 1, ERROR = 2;

module.exports = {
  "root": true,
  "overrides": [{
    files: "*.ts",
    parserOptions: {
      project: "tsconfig.eslint.json",
      ecmaVersion: 2020
    },
    env: {
      browser: true,
      es6: true
    },
    plugins: [
      "eslint-plugin-tsdoc",
      "import"
    ],
    extends: [
      "airbnb-base",
      "airbnb-typescript/base",
      "plugin:@typescript-eslint/recommended",
      "plugin:@typescript-eslint/recommended-requiring-type-checking",
      "plugin:@angular-eslint/recommended",
      "plugin:unicorn/recommended",
      "plugin:import/recommended",
      "plugin:import/typescript",
      "plugin:jsdoc/recommended"
    ],
    rules: {
      "@angular-eslint/no-host-metadata-property": OFF,
      "@typescript-eslint/comma-dangle": [ERROR, "never"],
      "arrow-parens": [ERROR, "as-needed"],
      "implicit-arrow-linebreak": OFF,
      "jsdoc/require-param-type": OFF,
      "jsdoc/require-returns-type": OFF,
      "object-curly-newline": [ERROR, {"consistent": true, "minProperties": 8}],
      "quote-props": [ERROR, "consistent-as-needed"],
      "spaced-comment": [ERROR, "always", {exceptions: ["-", "+", "*"]}],
      "unicorn/numeric-separators-style": OFF,
      "unicorn/no-null": OFF,
      "unicorn/prevent-abbreviations": OFF,
      // ------
      "@typescript-eslint/dot-notation": OFF,
      "@typescript-eslint/explicit-module-boundary-types": OFF,
      "@typescript-eslint/lines-between-class-members": OFF,
      "@typescript-eslint/naming-convention": OFF,
      "@typescript-eslint/no-explicit-any": OFF,
      "@typescript-eslint/no-floating-promises": OFF,
      "@typescript-eslint/no-shadow": OFF,
      "@typescript-eslint/no-unsafe-assignment": OFF,
      "@typescript-eslint/no-unsafe-call": OFF,
      "@typescript-eslint/no-unsafe-member-access": OFF,
      "@typescript-eslint/no-unsafe-return": OFF,
      "@typescript-eslint/no-use-before-define": OFF,
      "@typescript-eslint/no-useless-constructor": OFF,
      "@typescript-eslint/require-await": OFF,
      "@typescript-eslint/restrict-plus-operands": OFF,
      "@typescript-eslint/restrict-template-expressions": OFF,
      "@typescript-eslint/unbound-method": OFF,
      "class-methods-use-this": OFF,
      "default-case": OFF,
      "import/no-cycle": OFF,
      "import/no-extraneous-dependencies": OFF,
      "import/no-unresolved": OFF,
      "import/prefer-default-export": OFF,
      "jsdoc/check-param-names": OFF,
      "jsdoc/no-multi-asterisks": OFF,
      "jsdoc/require-jsdoc": OFF,
      "jsdoc/require-param": OFF,
      "jsdoc/tag-lines": OFF,
      "max-classes-per-file": OFF,
      "max-len": OFF,
      "no-bitwise": OFF,
      "no-confusing-arrow": OFF,
      "no-console": OFF,
      "no-else-return": OFF,
      "no-param-reassign": OFF,
      "no-plusplus": OFF,
      "no-restricted-properties": OFF,
      "no-underscore-dangle": OFF,
      "no-void": OFF,
      "padded-blocks": OFF,
      "prefer-template": OFF,
      "unicorn/new-for-builtins": OFF,
      "unicorn/no-array-callback-reference": OFF,
      "unicorn/no-array-for-each": OFF,
      "unicorn/no-array-reduce": OFF,
      "unicorn/no-lonely-if": OFF,
      "unicorn/no-nested-ternary": OFF,
      "@typescript-eslint/indent": OFF,
      "@typescript-eslint/no-unsafe-argument": OFF,
      "prefer-exponentiation-operator": OFF,
      "unicorn/no-negated-condition": OFF,
      "unicorn/prefer-logical-operator-over-ternary": OFF,
      "unicorn/switch-case-braces": OFF,
      "function-call-argument-newline": OFF,
      "function-paren-newline": OFF,
      "jasmine/no-unsafe-spy": OFF,
      "unicorn/consistent-function-scoping": OFF,
      "@angular-eslint/no-output-native": OFF,
      "jsdoc/require-param-description": OFF,
      "jsdoc/require-returns": OFF,
      "jsdoc/no-undefined-types": OFF,
      "@typescript-eslint/no-redundant-type-constituents": OFF,
      "unicorn/prefer-at": OFF,
      "@typescript-eslint/no-base-to-string": OFF,
      "@typescript-eslint/object-curly-spacing": OFF,
      "object-property-newline": OFF,
      "@angular-eslint/no-output-on-prefix": OFF,
      "unicorn/prefer-native-coercion-functions": OFF
    }
  }, {
    files: "*.spec.ts",
    env: {
      jasmine: true
    },
    plugins: [
      "jasmine"
    ],
    extends: [
      "plugin:jasmine/recommended"
    ],
    rules: {
      "jasmine/no-spec-dupes": OFF,
      "jasmine/prefer-toHaveBeenCalledWith": OFF
    }
  }, {
    files: "*.html",
    extends: [
      "plugin:@angular-eslint/template/recommended"
    ]
  }]
};
