# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.4](https://gitlab.com/coyoapp/ui/ui/compare/v1.0.3...v1.0.4) (2024-11-20)


### Bug Fixes

* remove console log ([3c91606](https://gitlab.com/coyoapp/ui/ui/commit/3c91606c3f2222e1340a0817f8589e74f822a694))

### [1.0.3](https://gitlab.com/coyoapp/ui/ui/compare/v1.0.2...v1.0.3) (2024-05-08)

### [1.0.2](https://gitlab.com/coyoapp/ui/ui/compare/v1.0.1...v1.0.2) (2024-05-07)

### [1.0.1](https://gitlab.com/coyoapp/ui/ui/compare/v1.0.0...v1.0.1) (2024-05-07)

## [1.0.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.18.4...v1.0.0) (2024-05-06)

### [0.18.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.18.2...v0.18.4) (2024-02-13)

### [0.18.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.18.2...v0.18.3) (2024-02-13)

### [0.18.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.18.1...v0.18.2) (2023-10-05)

## [0.18.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.27...v0.18.0) (2023-04-21)


### ⚠ BREAKING CHANGES

* cui-cards do not enforce cui typography styles anymore

### Bug Fixes

* cui-cards do not enforce cui typography styles anymore ([fcf48de](https://gitlab.com/coyoapp/ui/ui/commit/fcf48de1f5f2dd8a231f02b736f2014ea10d96cd))

### [0.17.27](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.26...v0.17.27) (2023-01-12)

### [0.17.26](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.25...v0.17.26) (2022-11-25)

### [0.17.25](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.24...v0.17.25) (2022-11-23)

### [0.17.24](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.23...v0.17.24) (2022-11-08)

### [0.17.23](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.22...v0.17.23) (2022-11-07)


### Features

* **dialog:** COYOFOUR-19627 add possibilitie to remove padding in dialog ([5f043b5](https://gitlab.com/coyoapp/ui/ui/commit/5f043b559b552b4505cc31ffa75c3fff8b4c50ad))

### [0.17.22](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.21...v0.17.22) (2022-10-21)


### Bug Fixes

* **icon:** update svg-icon library to work within angular 14.2 ([f3e9b8e](https://gitlab.com/coyoapp/ui/ui/commit/f3e9b8e3409a45b2c85c27fc7a949e2d56ad7e5a))

### [0.17.21](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.20...v0.17.21) (2022-10-20)

### [0.17.20](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.19...v0.17.20) (2022-10-19)

### [0.17.19](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.18...v0.17.19) (2022-10-12)

### [0.17.18](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.17...v0.17.18) (2022-10-11)

### [0.17.17](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.16...v0.17.17) (2022-10-11)

### [0.17.16](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.15...v0.17.16) (2022-10-05)

### [0.17.15](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.14...v0.17.15) (2022-10-05)

### [0.17.14](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.13...v0.17.14) (2022-09-30)

### [0.17.13](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.12...v0.17.13) (2022-09-23)


### Bug Fixes

* **labels:** COYOFOUR-19638 fix label a11y and autocomplete default ([146c6a8](https://gitlab.com/coyoapp/ui/ui/commit/146c6a870f0d52db0225f773ca2de2902c242cf4))

### [0.17.12](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.11...v0.17.12) (2022-09-08)

### [0.17.11](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.10...v0.17.11) (2022-09-05)

### [0.17.10](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.9...v0.17.10) (2022-08-22)

### [0.17.9](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.8...v0.17.9) (2022-08-08)

### [0.17.8](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.6...v0.17.8) (2022-08-01)

### [0.17.7](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.6...v0.17.7) (2022-07-27)

### [0.17.6](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.5...v0.17.6) (2022-05-19)

### [0.17.5](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.3...v0.17.4) (2022-03-24)

### Bug Fixes

* **date:** fix date picker styles in firefox ([06fbbf7e](https://gitlab.com/coyoapp/ui/ui/-/commit/06fbbf7e022b796b3360d4ce106ccfeb799a2b77))
* **form:** fix checkbox multi click ([a96fe427](https://gitlab.com/coyoapp/ui/ui/-/commit/a96fe4278e1804c53ae05c4f24d12c5dad8f3d0c))
* **date:** fix placeholder replacement on validation ([0656cddd](https://gitlab.com/coyoapp/ui/ui/-/commit/0656cddd4796298cc491941c9bf95fca9c8b9f7b))


### [0.17.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.3...v0.17.4) (2022-03-24)


### Features

* **SELECT:** COYOFOUR-18457 Add clear even to cui-select ([f256808](https://gitlab.com/coyoapp/ui/ui/commit/f256808dafc2f47c7c8e0a199154e326ad4b0c4d))

### [0.17.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.2...v0.17.3) (2022-03-21)


### Features

* **select:** adjust dropdownPanel positioning after onChange ([2386a34](https://gitlab.com/coyoapp/ui/ui/commit/2386a3480694582dbbbb9aa1cc50fa6c1d3c0053))

### [0.17.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.1...v0.17.2) (2022-03-21)


### Features

* **menu:** add onHidden and onShown output ([77a2ef4](https://gitlab.com/coyoapp/ui/ui/commit/77a2ef4baeac1314894f6836c67ab05f1ba0cd3c))

### [0.17.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.17.0...v0.17.1) (2022-03-14)

## [0.17.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.16.1...v0.17.0) (2022-03-11)

### [0.16.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.16.0...v0.16.1) (2022-03-04)

## [0.16.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.6...v0.16.0) (2022-02-08)


### ⚠ BREAKING CHANGES

* **tabs:** fully rework tabs component, allow menus, remove dynamic more tab handling
* Update to Angular 13
* **icons:** change default size for all icons to M

### Features

* **card:** implement card navigation ([0f0d1bf](https://gitlab.com/coyoapp/ui/ui/commit/0f0d1bf6d21e8acba00b38da86b70fdfbb65cf3b))
* **dialog:** setup focus trap for dialogs and support autoFocus ([bb22d15](https://gitlab.com/coyoapp/ui/ui/commit/bb22d1593b061a491c1df8876d35d6963b7cc26b))
* **formly-field:** implement formly wrappers ([65a31da](https://gitlab.com/coyoapp/ui/ui/commit/65a31da0d1cad2c877a5d427ad3b24145556f28f))
* **forms:** improve focus state for primary and ghost inputs ([486796e](https://gitlab.com/coyoapp/ui/ui/commit/486796ed2de5d485d1790ef9a13f9b0f77569270))
* **icons:** change default size for all icons to M ([46008b2](https://gitlab.com/coyoapp/ui/ui/commit/46008b2aa12d5fadaf002b5f8090bc6864b0388b))
* **tabs:** fully rework tabs component, allow menus, remove dynamic more tab handling ([3a29b8c](https://gitlab.com/coyoapp/ui/ui/commit/3a29b8c7c5d0ab6ff0b8202d10ffa4a841658057))
* **typo:** add cui styling for kbd element ([6c4e3dd](https://gitlab.com/coyoapp/ui/ui/commit/6c4e3dd63c0fe3c7f13eef8a446e5cf8fee9bc00))
* Update to Angular 13 ([cadeb51](https://gitlab.com/coyoapp/ui/ui/commit/cadeb516626e2bf747bb575a563cb599631c5cd1))

### [0.15.6](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.5...v0.15.6) (2021-12-10)


### Features

* **card:** add aria label for card menu ([2d10457](https://gitlab.com/coyoapp/ui/ui/commit/2d10457df099cf1d3710ae10cb24330fded09025))

### [0.15.5](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.4...v0.15.5) (2021-12-08)


### Features

* **css:** add media queries and hover styles ([f53a10f](https://gitlab.com/coyoapp/ui/ui/commit/f53a10fa720894f303fbbaebc5188fb46a12baa9))

### [0.15.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.3...v0.15.4) (2021-12-06)


### Features

* **pagination:** add link style to pagination ([73ae0b2](https://gitlab.com/coyoapp/ui/ui/commit/73ae0b2273ec3a11dd63e329cc170c629ff70793))

### [0.15.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.2...v0.15.3) (2021-12-02)


### Bug Fixes

* **select:** removed selectable clear button on select token ([6a20d32](https://gitlab.com/coyoapp/ui/ui/commit/6a20d32b25a5a2e0d4d52c7f08ce9f6a6aeb24c2))

### [0.15.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.1...v0.15.2) (2021-11-30)


### Bug Fixes

* **form:** fix input overflow ([0082ccd](https://gitlab.com/coyoapp/ui/ui/commit/0082ccda8ecefbd7dfe9143db7d189a674f1ff85))

### [0.15.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.15.0...v0.15.1) (2021-11-30)


### Features

* **menu:** add closeOnClick option to menu items ([0732106](https://gitlab.com/coyoapp/ui/ui/commit/0732106268c05fad554e7d661dff995dd908b476))


### Bug Fixes

* **button:** added focus text color for cui-buttons to prevent overriding by default a rules ([c0f9bee](https://gitlab.com/coyoapp/ui/ui/commit/c0f9beefdd19709b09c94f994d2725181a85c1f9))
* **menu:** buffer added items until the menu is rendered ([bf3653f](https://gitlab.com/coyoapp/ui/ui/commit/bf3653f9a60ffa0af7a56a2a4331e3a602d03ac6))
* **sortable:** add default sorting ([7d5f7a5](https://gitlab.com/coyoapp/ui/ui/commit/7d5f7a5502d326a7dfe390a609c4350db2f97403))
* **typo:** don't add border to last table row ([1d747a4](https://gitlab.com/coyoapp/ui/ui/commit/1d747a4f79162e8ac25b6768ba603856d5fe9bff))

## [0.15.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.14.1...v0.15.0) (2021-11-25)


### Features

* **tooltip:** disable tooltips on mobile ([6038ffc](https://gitlab.com/coyoapp/ui/ui/commit/6038ffcdd170c2908ff54278ad4b48d93c5d4372))

### [0.14.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.14.0...v0.14.1) (2021-11-22)


### Features

* **checkbox:** added prefix to ID so external labels work ([6df601b](https://gitlab.com/coyoapp/ui/ui/commit/6df601b4d2c53e4f99aee6ce33feedefe3173d20))


### Bug Fixes

* **select:** fix broken multiselect template ([5612306](https://gitlab.com/coyoapp/ui/ui/commit/56123064fa652ea425ffb5e7ea317b86aabc5338))

## [0.14.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.6...v0.14.0) (2021-11-18)


### Bug Fixes

* **pagination:** center pagination in block element ([57dd553](https://gitlab.com/coyoapp/ui/ui/commit/57dd553b811ab900a5decb06ded6ae793b4f1e33))
* **scrollable:** Fix scrollable observers on scrollable component itself ([8dbec5d](https://gitlab.com/coyoapp/ui/ui/commit/8dbec5d299d3ff551253093cfd1a3a02d4ad7381))
* **scrollable:** Fix size of scrollable container ([1117862](https://gitlab.com/coyoapp/ui/ui/commit/1117862c830f75253295dd10cc80c610776ede89))
* **select:** fix multi select default label template ([0281941](https://gitlab.com/coyoapp/ui/ui/commit/0281941391ed3e7438aa75e225b8b3757d6944b9))
* **toggle:** adjust toggle styles ([97d5ae6](https://gitlab.com/coyoapp/ui/ui/commit/97d5ae6b15dda4534ac0dca9a5b6a80a06e28205))

### [0.13.6](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.5...v0.13.6) (2021-11-12)


### Bug Fixes

* **menu:** menu title padding changed ([059e823](https://gitlab.com/coyoapp/ui/ui/commit/059e823916408000cabb2870dd3fd9237a543e06))
* **typo:** fix anchor selector ([073ae21](https://gitlab.com/coyoapp/ui/ui/commit/073ae21b6dfb6c1f3bd6084555dbe5c49a108eb1))

### [0.13.5](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.4...v0.13.5) (2021-11-11)


### Bug Fixes

* **menu:** correctly render menus wrapped in subcomponents ([f44924f](https://gitlab.com/coyoapp/ui/ui/commit/f44924f916371eb0292bb0cb0c0b81db20106e7d))

### [0.13.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.3...v0.13.4) (2021-11-10)


### Features

* **tooltip:** added screen reader support ([de01e6b](https://gitlab.com/coyoapp/ui/ui/commit/de01e6b9eee18688a8f92c2cefbfc9fbf9ba078d))

### [0.13.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.2...v0.13.3) (2021-11-09)


### Bug Fixes

* **typo:** adjust typography selector ([7f82955](https://gitlab.com/coyoapp/ui/ui/commit/7f82955dc5c030cc8746b3ecd27bc3bf6d7961bf))
* **typo:** adjust typography selectors ([b1c7d05](https://gitlab.com/coyoapp/ui/ui/commit/b1c7d05e667a145cfee78ec8ca4e82eb9389fb99))

### [0.13.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.1...v0.13.2) (2021-11-05)


### Bug Fixes

* **scrolling:** fix tests ([9790e3e](https://gitlab.com/coyoapp/ui/ui/commit/9790e3ef765c2b2577efbe8c59d508fd0e4212db))

### [0.13.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.13.0...v0.13.1) (2021-11-01)


### ⚠ BREAKING CHANGES

* **scrolling:** fix scrollable shadows and converted directive into compontent

### Bug Fixes

* **scrolling:** fix scrollable shadows and converted directive into compontent ([4439b60](https://gitlab.com/coyoapp/ui/ui/commit/4439b60479136126a2c158dc7cd817b0a4f95f91))

## [0.13.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.12.0...v0.13.0) (2021-10-29)


### Features

* **popover:** implement lazy initialization for popover content ([c42ac07](https://gitlab.com/coyoapp/ui/ui/commit/c42ac07d8381f575a9038a2f9305ceced6f74f19))
* **scroll:** implement cuiScrolledToEnd directive ([d17f494](https://gitlab.com/coyoapp/ui/ui/commit/d17f494a757060d803a67150d9b59aa57b5ffeab))
* **scrolling:** added scrolling helper directives ([b742ffb](https://gitlab.com/coyoapp/ui/ui/commit/b742ffb6358abf610d45cb2cc7e994e159a5d45a))
* **sidebar:** improve & cleanup implementation ([7751958](https://gitlab.com/coyoapp/ui/ui/commit/775195886b30b28119fefb9175734a836e5b6067))


### Bug Fixes

* **COYOFOUR-17189:** renamed toast classes to avoid overlapping with existing classes ([79f341f](https://gitlab.com/coyoapp/ui/ui/commit/79f341f279bd42790c1d84e33761ad95a1fd6028))
* **table:** improve & cleanup implementation ([9b5687e](https://gitlab.com/coyoapp/ui/ui/commit/9b5687eed541a56c9c27fb8ee293800b1263c08b))

## [0.12.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.11.1...v0.12.0) (2021-10-21)


### ⚠ BREAKING CHANGES

* **tabs:** rename align input to tabAlign

### Features

* **table:** implement select rows in table story ([22ef665](https://gitlab.com/coyoapp/ui/ui/commit/22ef665f809033e07afc77cc007055be1d7b925a))
* **tabs:** rename align input to tabAlign ([30e2dca](https://gitlab.com/coyoapp/ui/ui/commit/30e2dca4c50e36a956dbb00db2ce92890e089d01))


### Bug Fixes

* **form:** input hint shouldn't take any space if it's empty ([c837a7c](https://gitlab.com/coyoapp/ui/ui/commit/c837a7c70783ed01578169df00dc5c56e6fb8b3e))
* **tab:** trigger CD on tab group when tab title changes ([7a58af4](https://gitlab.com/coyoapp/ui/ui/commit/7a58af414b1bc03d217ce3226539a10ca0c9c482))

### [0.11.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.11.0...v0.11.1) (2021-10-08)


### Bug Fixes

* **file:** use correct default icons for file pipe ([88302dd](https://gitlab.com/coyoapp/ui/ui/commit/88302dd64bdbbe425fab7c59460beae04f496960))

## [0.11.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.10.4...v0.11.0) (2021-10-08)


### ⚠ BREAKING CHANGES

* **tabs:** remove margin on tab content pane. Add margin on your own
* **badge:** update badge component to new specifications

### Features

* **badge:** update badge component to new specifications ([d6498a5](https://gitlab.com/coyoapp/ui/ui/commit/d6498a535cdfb9780c336f368cb740743610a1b0))
* **collapse:** implement collapse component ([356b513](https://gitlab.com/coyoapp/ui/ui/commit/356b5137847947a13ebba551b1b344ff859b9ff3))
* **popover:** implement popover component and unify tippy configs ([13b081f](https://gitlab.com/coyoapp/ui/ui/commit/13b081fef623eb8f4ff5828428e6d71620925a4a))
* **tabs:** add justified tab alignment ([238701c](https://gitlab.com/coyoapp/ui/ui/commit/238701c132ca80770873387b1ab54f1f7d1716f8))
* **theme:** add cui-plain for unstyled links ([b708fdd](https://gitlab.com/coyoapp/ui/ui/commit/b708fddd977520513cc7aa2a912fdb2a65160459))
* **tooltip:** added support for HTML content inside tooltips ([9350ca3](https://gitlab.com/coyoapp/ui/ui/commit/9350ca3e4c644825b0d65862048569eb5922fad1))


### Bug Fixes

* **tabs:** remove margin on tab content pane. Add margin on your own ([c4e7a3f](https://gitlab.com/coyoapp/ui/ui/commit/c4e7a3ffcbe58ec9f68e281559d4c344469a0bd1))

### [0.10.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.10.3...v0.10.4) (2021-10-04)


### Features

* update to storybook 6.4.0-beta.4 ([12aa645](https://gitlab.com/coyoapp/ui/ui/commit/12aa645b785254ceb57b858d0bb5cc505c8483c1))
* **theme:** adjust green color ([0f520a8](https://gitlab.com/coyoapp/ui/ui/commit/0f520a8c5a8759c7175e43ff62349b9b089ad3e7))


### Bug Fixes

* ReferenceError: TypoDemoComponent is not defined ([b77ad15](https://gitlab.com/coyoapp/ui/ui/commit/b77ad15d4207858040b01b0a2070953a4207e670))

### [0.10.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.10.2...v0.10.3) (2021-09-30)

### [0.10.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.10.1...v0.10.2) (2021-09-30)

### [0.10.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.10.0...v0.10.1) (2021-09-30)

## [0.10.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.9.0...v0.10.0) (2021-09-30)


### Features

* **file:** extend default config of fileicon pipe ([b579ada](https://gitlab.com/coyoapp/ui/ui/commit/b579ada813c40000754db0fef42042a841f25f2e))
* **file:** implement file icon pipe ([9c682ad](https://gitlab.com/coyoapp/ui/ui/commit/9c682ad46a3223d48b3b6080990a705b2b496426))
* **file:** implement file name pipe ([300dd0c](https://gitlab.com/coyoapp/ui/ui/commit/300dd0c702cc599f5f2ae9b2b3feba9c3a0341e0))
* **file:** implement file size pipe ([52aa21a](https://gitlab.com/coyoapp/ui/ui/commit/52aa21afbca99ef1dfcd4b7b72ae8196fad154b7))
* **menu:** implement submenus ([dee85a5](https://gitlab.com/coyoapp/ui/ui/commit/dee85a5d3db8fe1a13264013c96e2d06c1f8c538))
* **theme:** implement custom color palette directive ([03b9fbe](https://gitlab.com/coyoapp/ui/ui/commit/03b9fbe9d8d79dbbaeeca9f544749ca44de4cfcd))
* **typo:** implement table styles ([84a9b25](https://gitlab.com/coyoapp/ui/ui/commit/84a9b250c54e5fc321abb157afd85187298f7ec4))


### Bug Fixes

* **card:** wrong colors in card menus ([70a052a](https://gitlab.com/coyoapp/ui/ui/commit/70a052a99930c292527ac1ab810bab457413579d))
* **datepicker:** fix datepicker init errors ([05d0f35](https://gitlab.com/coyoapp/ui/ui/commit/05d0f352af079384a310748918ffbe376b365487))
* **form:** fix disabled state on form fields ([9198f57](https://gitlab.com/coyoapp/ui/ui/commit/9198f57bc052b296a5f8c0c3a5a581fee9098da4))

## [0.9.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.8.3...v0.9.0) (2021-09-22)


### Features

* **card:** rework card component ([153e550](https://gitlab.com/coyoapp/ui/ui/commit/153e5503ede46504f4034d2262419dad2d8d715b))
* **theme:** add modes to checkbox, radio & toggle ([92af4d8](https://gitlab.com/coyoapp/ui/ui/commit/92af4d82574ac8951e3b866454b06b5ec0d0418f))
* **tooltip:** support tooltip trigger ([8b265b3](https://gitlab.com/coyoapp/ui/ui/commit/8b265b3b4052059810642998c35b093337df93fe))
* implement css utilities ([91bae95](https://gitlab.com/coyoapp/ui/ui/commit/91bae9565153fcad981c0607ecebfa515c6806a2))

### [0.8.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.8.2...v0.8.3) (2021-09-15)


### Bug Fixes

* **card:** Fix misaligned card action buttons ([a16e03c](https://gitlab.com/coyoapp/ui/ui/commit/a16e03c91062ee74caea99a957d0e273803f5384))
* **cards:** nested cards shadow ([034d09c](https://gitlab.com/coyoapp/ui/ui/commit/034d09c3d9188811db6f4c83168bb0b4f0c9cd71))

### [0.8.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.8.1...v0.8.2) (2021-09-13)


### Features

* added additional helper classes ([3fbfa2a](https://gitlab.com/coyoapp/ui/ui/commit/3fbfa2ae50fd7d5a5c182040935dec8ec2a4ab1d))
* **radio:** implement component ([2cdee45](https://gitlab.com/coyoapp/ui/ui/commit/2cdee45e8bcaf1a78bd49f93b37f01d774e171eb))
* **tooltip:** added directive to check for ellipsed text ([7db1c01](https://gitlab.com/coyoapp/ui/ui/commit/7db1c01d9b042ce0b9cd73ebf3cff519b30dcc2e))


### Bug Fixes

* **button:** misplaced loading spinner ([e2be28e](https://gitlab.com/coyoapp/ui/ui/commit/e2be28e08b7e86eb4d45c142a611c693166a91be))
* **docs:** import icon module in all stories ([59b1166](https://gitlab.com/coyoapp/ui/ui/commit/59b1166987c53370af01d193ccc9103127b82203))

### [0.8.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.8.0...v0.8.1) (2021-09-06)


### Bug Fixes

* **dialog:** make it possible to return false as dialog result ([fb668eb](https://gitlab.com/coyoapp/ui/ui/commit/fb668ebcfba881cdf152ef65a4943f466e15e654))
* **dialog:** not working correctly on small screens ([9e526da](https://gitlab.com/coyoapp/ui/ui/commit/9e526da24bd9092fb4bddd389de6300272e2b9ce))

## [0.8.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.7.0...v0.8.0) (2021-08-18)


### ⚠ BREAKING CHANGES

* **skeleton:** skeletons use REM units now - definitions must be adjusted

### Features

* **skeleton:** move from PX to REM units. ([a694838](https://gitlab.com/coyoapp/ui/ui/commit/a69483808c2181da0ba4f54eaf2bf1436b82184a))
* **skeleton:** support random dimensions in skeleton widths ([09e22fb](https://gitlab.com/coyoapp/ui/ui/commit/09e22fbb07049bac2465c7b72ff47d7873cc1442))

## [0.7.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.6.1...v0.7.0) (2021-08-17)


### Features

* Deactivate view encapsulation in CUI ([fa92cd7](https://gitlab.com/coyoapp/ui/ui/commit/fa92cd76c4669ed33038199ebd6ffa11f1e665d9))
* **forms:** added form label component ([de193be](https://gitlab.com/coyoapp/ui/ui/commit/de193be35d33944e9440befdec7a15a5cd815821))
* **select:** improve select component API (incl. search) ([0ed4056](https://gitlab.com/coyoapp/ui/ui/commit/0ed40568d53d89f58e9db8dc1fb7f38c25093c72))


### Bug Fixes

* **tooltip:** wrap overlong words in tootips ([2c1dcb6](https://gitlab.com/coyoapp/ui/ui/commit/2c1dcb6c3cb9d36250fd4ee7a0043cc16d39322d))

### [0.6.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.6.0...v0.6.1) (2021-08-06)

## [0.6.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.5.1...v0.6.0) (2021-08-06)


### ⚠ BREAKING CHANGES

* @ng-select/ng-select has been moved to peerDependencies

### Features

* **dialog:** make it possible to append selects to dialog overlays ([b8da96a](https://gitlab.com/coyoapp/ui/ui/commit/b8da96a4d1c20cbaa5ac885df7501cf4b31652be))
* **toast:** Add additional configuration possibilities ([e039b22](https://gitlab.com/coyoapp/ui/ui/commit/e039b2273d8f4af20241a4bd7d396f4e6aa9fa3d))
* enable ivy compiler ([2d35852](https://gitlab.com/coyoapp/ui/ui/commit/2d3585218e63924433340c0a4f00571507a54715))
* Update @ngneat/helipopper, @ngneat/overview, @ngneat/svg-icon and @ng-select/ng-select ([54396b6](https://gitlab.com/coyoapp/ui/ui/commit/54396b6b9eea615b2c6b282875f82aa12484e6ad))
* Upgraded to Angular 12 ([e9de407](https://gitlab.com/coyoapp/ui/ui/commit/e9de407b32f209a600026c570fd982751cfe4902))


### Bug Fixes

* **checkbox:** Improve implementation and visuals ([5f3d5c3](https://gitlab.com/coyoapp/ui/ui/commit/5f3d5c33658e271ae0d6b9c440a42eac1ed4ddbd))
* **toggle:** Improve implementation and visuals ([5d59df5](https://gitlab.com/coyoapp/ui/ui/commit/5d59df5b165e161139a9d8ad341f5b5d25496074))
* remove deep imports into '@angular/cdk/portal/portal'. ([7892e0c](https://gitlab.com/coyoapp/ui/ui/commit/7892e0c30de48b8cbb4861c75bf0b52694b0921d))

### [0.5.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.5.0...v0.5.1) (2021-07-28)

## [0.5.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.4.1...v0.5.0) (2021-07-28)


### Features

* **tabs:** added tab(s) component ([2885f07](https://gitlab.com/coyoapp/ui/ui/commit/2885f075b5008499c7c8e5b5bb65e38440dc6302))


### Bug Fixes

* **forms:** add missing config file ([d171d06](https://gitlab.com/coyoapp/ui/ui/commit/d171d066e8c906f398e59cd8cec958444d6d5e05))
* **test:** Fix coverage values ([0651036](https://gitlab.com/coyoapp/ui/ui/commit/0651036a6c63473e8dcc48e7868455dbad8ac4e2))
* **toast:** close toasts after 8s (4s extended timeout) ([2aee04e](https://gitlab.com/coyoapp/ui/ui/commit/2aee04ed733724fd582b4c2ceb80d3e57f2d3eaf))

### [0.4.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.4.0...v0.4.1) (2021-07-16)

## [0.4.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.3.3...v0.4.0) (2021-07-16)


### Features

* **cards:** simplify and adjust card API ([c5c02ce](https://gitlab.com/coyoapp/ui/ui/commit/c5c02ce182fd8a9333d3ef1e2951027b73f80a04))
* **checkbox:** finalize checkbox implementation ([165be8f](https://gitlab.com/coyoapp/ui/ui/commit/165be8f855c0af35b4ea2365b1e4462654336aa4))
* **dialog:** Added dialogs ([6621846](https://gitlab.com/coyoapp/ui/ui/commit/6621846465f08f858d12ad0f0ab6493e4ea31982))
* **select:** Implement select fields via ngSelect ([6eec77a](https://gitlab.com/coyoapp/ui/ui/commit/6eec77a3f01f658cf8491ccd0d914b6f93ba06a4))
* **storybook:** updated storybook version to fix circular structure bug ([8794915](https://gitlab.com/coyoapp/ui/ui/commit/87949153a8d09b9f997a8d6d4531047654a0bc27))
* update to storybook 6.3 ([c41fa67](https://gitlab.com/coyoapp/ui/ui/commit/c41fa67a4bc7f6fd7bba684529c8cb1fd9e3f758))


### Bug Fixes

* **form:** Update form field styles ([1281498](https://gitlab.com/coyoapp/ui/ui/commit/1281498798158d7963d9b6c5c4e6a90e50c11a20))
* **submodule-paths:** fixed import path for sub modules to be evailable under @coyoapp/ui/module-xyz ([05ab678](https://gitlab.com/coyoapp/ui/ui/commit/05ab6781af44aa2a0090a48cf041ff26422f73e6))

### [0.3.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.3.2...v0.3.3) (2021-06-23)

### [0.3.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.3.1...v0.3.2) (2021-06-23)


### Bug Fixes

* **typo:** set h5 to 15px ([810ab7c](https://gitlab.com/coyoapp/ui/ui/commit/810ab7c04de9f2f72e174dcc26fec65e40e03480))

### [0.3.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.3.0...v0.3.1) (2021-06-16)


### Bug Fixes

* **select:** remove interim styles ([466fe69](https://gitlab.com/coyoapp/ui/ui/commit/466fe69085c84f57bb0d53853f1777bd26061627))

## [0.3.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.2.4...v0.3.0) (2021-06-14)


### Features

* **forms:** Added form field validations ([7956b14](https://gitlab.com/coyoapp/ui/ui/commit/7956b14c893e7981bfac7c67d75ce88629c9e6c0))

### [0.2.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.2.3...v0.2.4) (2021-06-11)


### Features

* added bootstrap helper classes to CUI ([d37e423](https://gitlab.com/coyoapp/ui/ui/commit/d37e4236783b5f1175a411056f6fad32e75c0f90))
* improve icon typings ([6db89a9](https://gitlab.com/coyoapp/ui/ui/commit/6db89a944adc6818a3cc5d173c6b18be6afc9c1a))

### [0.2.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.2.2...v0.2.3) (2021-05-16)


### Features

* **button:** add button link styles and sizes xs and xl ([fa4b124](https://gitlab.com/coyoapp/ui/ui/commit/fa4b12433042b3a3151ed72df4c57b802264c4c6))

### [0.2.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.2.1...v0.2.2) (2021-05-14)


### Features

* **typography:** add typo helper classes ([56792e3](https://gitlab.com/coyoapp/ui/ui/commit/56792e39ee745cc7e180a359110547b33a857f45))

### [0.2.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.2.0...v0.2.1) (2021-05-14)


### Bug Fixes

* **buttons:** adjust button paddings ([ebe3fbf](https://gitlab.com/coyoapp/ui/ui/commit/ebe3fbf08a66aa8198406bd586d055fd42a3076c))
* **icons:** use inline flex for SVG element ([a71a9b3](https://gitlab.com/coyoapp/ui/ui/commit/a71a9b3d64dec0197972a4cd4c0f2e65778d7ad1))
* **pagination:** use round buttons ([4351d12](https://gitlab.com/coyoapp/ui/ui/commit/4351d129aba11ff4bbc019c9963553678dc6f549))

## [0.2.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.1.1...v0.2.0) (2021-05-14)


### Bug Fixes

* **ci:** move back to SCSS import due to missing bundler options ([caa021e](https://gitlab.com/coyoapp/ui/ui/commit/caa021eeb2380e73c6d57067a48fdaa68a62756d))

### [0.1.1](https://gitlab.com/coyoapp/ui/ui/compare/v0.1.0...v0.1.1) (2021-05-12)

## [0.1.0](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.7...v0.1.0) (2021-05-12)


### Features

* **checkbox:** implement proper color handling ([63a9de6](https://gitlab.com/coyoapp/ui/ui/commit/63a9de6005fd26fb10c850c2a9bb5a9dd7df192b))
* **input:** support autocomplete and tab index ([321cfdf](https://gitlab.com/coyoapp/ui/ui/commit/321cfdf56397588e4794f9bdefa42a804a962798))
* **menu:** fully implement menu ([0cc54ee](https://gitlab.com/coyoapp/ui/ui/commit/0cc54ee39efdca28a7d174b39c3c41eb3878951c))
* added demo application ([3727abd](https://gitlab.com/coyoapp/ui/ui/commit/3727abde93f5e8e12b0739807f94aa4d1a88205b))
* **button:** Add inputs to adjust text and icon classes ([0feb4dd](https://gitlab.com/coyoapp/ui/ui/commit/0feb4dd96f215d3f062cf66a99df9c3c01e61c77))
* **card:** COYOFOUR-15296 - adjusting cards, resolving lint issues ([36bf04b](https://gitlab.com/coyoapp/ui/ui/commit/36bf04b010ced86c3810def16bbfe62b581fae6f))
* **COYOFOUR-15507:** implement color & palette service ([4cd982e](https://gitlab.com/coyoapp/ui/ui/commit/4cd982eb8b98f352d765bc9a984e1f25301fc60e))


### Bug Fixes

* remove stylelint errors ([76b61ea](https://gitlab.com/coyoapp/ui/ui/commit/76b61ea68b5d45ed5c55e1789d845a1abb84439a))
* **checkbox:** fix checkbox story ([556d2fb](https://gitlab.com/coyoapp/ui/ui/commit/556d2fb04fcf831ce8048c8beef4f60feec4af63))
* **menu:** remove circular dependency ([104a7a5](https://gitlab.com/coyoapp/ui/ui/commit/104a7a54bd2ed9837d4b8933d5925a6513c6f479))

### [0.0.7](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.6...v0.0.7) (2021-04-27)

### [0.0.6](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.5...v0.0.6) (2021-04-27)


### Features

* added color helpers ([a039c27](https://gitlab.com/coyoapp/ui/ui/commit/a039c273ce5ce4fe9d7fb7539c0903ae3c1f8453))
* **COYOFOUR-15307:** Added elevations ([77f350f](https://gitlab.com/coyoapp/ui/ui/commit/77f350f5595eb3d80654f7a536ff9f30fa966bc0))
* **COYOFOUR-15482:** add button ([f6923c5](https://gitlab.com/coyoapp/ui/ui/commit/f6923c56810f5fd39d94c5eb4c30e0d4161ea919))
* **COYOFOUR-15484:** Add spinner ([f58fd00](https://gitlab.com/coyoapp/ui/ui/commit/f58fd007fafbabf4847e76f9b09990c690980249))
* **COYOFOUR-15485:** Add skeleton ([f150180](https://gitlab.com/coyoapp/ui/ui/commit/f150180c860e3e73a4f7a3f475e09ff7c588165c))
* **COYOFOUR-15486:** Add tooltip ([fa5613d](https://gitlab.com/coyoapp/ui/ui/commit/fa5613d6cd06fbc851eec4086f52e94bbf4e539a))
* **COYOFOUR-15487:** Add notification ([8b5fafb](https://gitlab.com/coyoapp/ui/ui/commit/8b5fafbf13e10aab8994b16f242c5f836ffbfe4a))
* **COYOFOUR-15489:** add alert ([1b2a710](https://gitlab.com/coyoapp/ui/ui/commit/1b2a710a6cb5e7cbd6a06bd80736dcd211410c92))
* **COYOFOUR-15490:** add badges ([4db5be7](https://gitlab.com/coyoapp/ui/ui/commit/4db5be725f8b273076117f750be39207530f5d14))
* **COYOFOUR-15491:** Add pagination ([e9e8212](https://gitlab.com/coyoapp/ui/ui/commit/e9e8212a7911d41844ca4352ef996559d383f48f))
* add status badges for components ([bfe8933](https://gitlab.com/coyoapp/ui/ui/commit/bfe89335b9ad1f08689b0e0b25f2bacc35620423))

### [0.0.5](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.4...v0.0.5) (2021-03-11)

### [0.0.4](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.3...v0.0.4) (2021-03-11)

### [0.0.3](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.2...v0.0.3) (2021-03-11)


### Features

* add theme switch to storybook ([b16f8a3](https://gitlab.com/coyoapp/ui/ui/commit/b16f8a3f736e18f7d4d76ff965a310cbe9323a89))
* move from yarn to npm ([793c069](https://gitlab.com/coyoapp/ui/ui/commit/793c0695df155b4f94f2a5e59e362bc73d216216))

### [0.0.2](https://gitlab.com/coyoapp/ui/ui/compare/v0.0.1...v0.0.2) (2021-03-10)

* Setup CI/CD

### 0.0.1 (2021-03-10)

* Initial project setup
