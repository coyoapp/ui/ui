import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import { demoKbd1 } from "./util/typography";

export default {
  title: "Foundation/Typography/KBD",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const CodeInline = {
  render: () => ({
    template: cuiDemo(demoKbd1),
  }),

  name: "Code (inline)",
  height: "80px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoKbd1,
      },
    },
  },
};
