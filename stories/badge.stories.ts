import { BadgeComponent, BadgeModule } from '@coyoapp/ui/badge';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { IconModule } from '@coyoapp/ui/icon';
import { inputArgs } from './util/arg-inject';
import { boolean, iconOptional, modeAll, text } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'UI/Badge',
  component: BadgeComponent,
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), BadgeModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=80%3A194')
  },
  argTypes: {
    mode: modeAll,
    iconClass: text(),
    round: boolean(),
    prefixIcon: iconOptional,
    suffixIcon: iconOptional
  }
} as Meta;

export const Badge: StoryObj<BadgeComponent> = (args: any) => ({
  props: args,
  template: `<cui-badge ${inputArgs('mode', 'round', 'iconClass', 'prefixIcon', 'suffixIcon')}>{{ngContent}}</cui-badge>`
});

Badge.args = {
  ngContent: 'New',
  mode: 'primary'
} as Partial<BadgeComponent>;
