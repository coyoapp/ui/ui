import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import { demoCode1, demoCode2 } from "./util/typography";

export default {
  title: "Foundation/Typography/Code",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const CodeInline = {
  render: () => ({
    template: cuiDemo(demoCode1),
  }),

  name: "Code (inline)",
  height: "80px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoCode1,
      },
    },
  },
};

export const CodeBlock = {
  render: () => ({
    template: cuiDemo(demoCode2),
  }),

  name: "Code (block)",
  height: "138px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoCode2,
      },
    },
  },
};
