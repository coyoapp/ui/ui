import { ButtonModule } from '@coyoapp/ui/button';
import { CardComponent, CardModule } from '@coyoapp/ui/card';
import { IconModule } from '@coyoapp/ui/icon';
import { MenuModule } from '@coyoapp/ui/menu';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'UI/Card',
  component: CardComponent,
  decorators: [
    width(360),
    moduleMetadata({
      imports: [CardModule, ButtonModule, IconModule.forRoot(), MenuModule.forRoot()]
    })
  ],
  parameters: {
    badges: ['live'],
    backgrounds: { default: 'light' },
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=61%3A50')
  }
} as Meta;

const h = 'Lorem ipsum dolor sit ametLorem ipsum dolor sit amet';
const p1 = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.';
const p2 = 'Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.';

export const Card: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <cui-card-header>
        <h5>${h}</h5>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
        <p>${p2}</p>
      </cui-card-body>
    </cui-card>`
});
Card.args = {};

export const CardMenu: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <cui-card-header>
        <h5>${h}</h5>
        <cui-card-menu>
          <a href="#" cui-menu-item>Action 1</a>
          <a href="#" cui-menu-item>Action 2</a>
          <hr>
          <a href="#" cui-menu-item>Action 3</a>
        </cui-card-menu>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
        <p>${p2}</p>
      </cui-card-body>
    </cui-card>`
});
CardMenu.args = {};

export const CardFooter: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <cui-card-header>
        <h5>${h}</h5>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
      </cui-card-body>
      <cui-card-footer>
        <button cui-button mode="primary">Action 1</button>
        <button cui-button mode="ghost">Action 2</button>
        <button cui-button mode="error" class="cui-mr-auto">Action 3</button>
      </cui-card-footer>
    </cui-card>`
});
CardFooter.args = {};

export const CardMedia: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <img src="https://dummyimage.com/360x148/9bbf29/FFFFFF.png">
      <cui-card-header>
        <h5>${h}</h5>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
        <img src="https://dummyimage.com/360x148/9bbf29/FFFFFF.png" class="cui-card-pull-out">
        <p>${p2}</p>
      </cui-card-body>
    </cui-card>`
});
CardMedia.args = {};

export const CardSections: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <cui-card-header>
        <h5>${h}</h5>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
        <hr>
        <p>${p2}</p>
        <hr class="cui-card-pull-out">
        <p>${p1}</p>
      </cui-card-body>
    </cui-card>`
});
CardSections.args = {};

export const StackedCards: StoryObj<CardComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-card>
      <cui-card-header>
        <h5>${h}</h5>
      </cui-card-header>
      <cui-card-body>
        <p>${p1}</p>
        <cui-card>
          <cui-card-header>
            <h5>${h}</h5>
          </cui-card-header>
          <cui-card-body>
            <p>${p1}</p>
            <p>${p2}</p>
          </cui-card-body>
        </cui-card>
      </cui-card-body>
    </cui-card>`
});
StackedCards.args = {};
