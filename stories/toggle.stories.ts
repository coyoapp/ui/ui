import { FormsModule } from '@angular/forms';
import { ToggleComponent, ToggleModule } from '@coyoapp/ui/toggle';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { outputArgs } from './util/arg-inject';
import { action } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Toggle',
  component: ToggleComponent,
  decorators: [
    width(360),
    moduleMetadata({
      imports: [ToggleModule, FormsModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  argTypes: {
    change: action('change')
  }
} as Meta;

export const Toggle: StoryObj<ToggleComponent & {ngContent: string}> = (args: any) => ({
  props: args,
  template: `<cui-toggle [(ngModel)]="value"
    ${outputArgs('change')}
  >{{ngContent}}</cui-toggle>
  <pre>Value: {{ value }}</pre>`
});

Toggle.args = {
  ngContent: 'Toggle label'
};
