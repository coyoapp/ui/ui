import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { MenuModule } from '@coyoapp/ui/menu';
import { TabsComponent, TabsModule } from '@coyoapp/ui/tabs';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, radio, text } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Tabs',
  component: TabsComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserAnimationsModule, IconModule.forRoot(), ButtonModule, MenuModule.forRoot(), TabsModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=76%3A178')
  },
  argTypes: {
    active: text(),
    tabAlign: radio(['left', 'justify', 'center']),
    tabChange: action('tabChange')
  }
} as Meta;

export const Tabs: StoryObj<TabsComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-tabs ${inputArgs('active', 'tabAlign')} ${outputArgs('tabChange')}>
      <cui-tab label="First" prefixIcon="globe">This is the <b>first</b> tab.</cui-tab>
      <cui-tab label="Second" id="tab2">This is the <b>second</b> tab.</cui-tab>
      <cui-tab label="Third">This is the <b>third</b> tab.</cui-tab>
      <cui-tab label="Fourth" [disabled]="true">This is the <b>fourth</b> tab.</cui-tab>
      <cui-tab label="Fifth" suffixIcon="shield">This is the <b>fifth</b> tab.</cui-tab>
    </cui-tabs>`
});

Tabs.args = {
  active: 'tab2',
  tabAlign: 'left'
};
