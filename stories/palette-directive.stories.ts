import { CommonModule } from '@angular/common';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { ThemeModule } from '@coyoapp/ui/theme';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { PaletteDirectiveDemoComponent } from './demo/palette-directive-demo/palette-directive-demo.component';
import { color } from './util/arg-types';

export default {
  title: 'Interaction/Palette/Directive',
  component: PaletteDirectiveDemoComponent,
  decorators: [
    moduleMetadata({
      imports: [CommonModule, IconModule.forRoot(), ButtonModule, ThemeModule],
      declarations: [PaletteDirectiveDemoComponent]
    })
  ],
  parameters: {
    badges: ['beta']
  },
  argTypes: {
    color: color(),
    lightFill: color(),
    darkFill: color()
  }
} as Meta;

export const Directive: StoryObj<PaletteDirectiveDemoComponent> = (args: any) => ({
  props: {
    color: args.color,
    lightFill: args.lightFill,
    darkFill: args.darkFill
  }
});

Directive.args = {
  color: '#0073e6',
  lightFill: '#ffffff',
  darkFill: '#000000'
};
