import { A11yModule } from "@angular/cdk/a11y";
import { OverlayModule } from "@angular/cdk/overlay";
import { PortalModule } from "@angular/cdk/portal";
import { ButtonModule } from "@coyoapp/ui/button";
import { DialogModule } from "@coyoapp/ui/dialog";
import { IconModule } from "@coyoapp/ui/icon";
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DialogDemoComponent } from "./demo/dialog-demo/dialog-demo.component";
import { DialogComponent } from "./demo/dialog-demo/dialog/dialog.component";
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Dialog',
  component: DialogDemoComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=739%3A1237'),
    docs: {
      description: {
        component: `
Dialogs are opened via the \`DialogService\`.
You can inject the dialog reference via \`DialogReference\` injection token into your dialog component.
The \`DialogReference\` is providing functionality to return the result of the dialog and also to access the provided
data given to the service call.
The result of the dialog is delivered via the returned observable of the service.

For the implementation your dialog component needs to use the \`cui-dialog\` component and the corresponding components
\`cui-dialog-title\` and \`cui-dialog-actions\` for a consistent look and feel. See the following example for a
confirmation dialog:

\`\`\`
<cui-dialog>
 <cui-dialog-title title="Test title" [closable]="true" closeAriaLabel="Close Dialog"></cui-dialog-title>
 Are you sure you want to do this?
 <cui-dialog-actions>
   <button cui-button>No</button>
   <button cui-button>Yes</button>
 </cui-dialog-actions>
</cui-dialog>
\`\`\`
`
      }
    }
  },
  decorators: [
    moduleMetadata({
      imports: [
        OverlayModule,
        PortalModule,
        IconModule.forRoot(),
        ButtonModule,
        A11yModule,
        DialogModule
      ],
      declarations: [DialogDemoComponent, DialogComponent],
      entryComponents: [DialogDemoComponent, DialogComponent]
    })
  ]
} as Meta;

export const Dialog: StoryObj<DialogDemoComponent> = (args: any) => ({
  props: args
});

Dialog.args = {
  content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'
};
