import { moduleMetadata } from "@storybook/angular";
import { ButtonComponent, ButtonModule } from "@coyoapp/ui/button";
import { IconModule } from "@coyoapp/ui/icon";
import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import { demoSpacing, demoMargin } from "./util/utilities";

export default {
  title: "Foundation/Utilities/Spacings",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Spacings = {
  render: () => ({
    template: cuiDemo(demoSpacing),
  }),

  name: "Spacings",
  height: "176px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoSpacing,
      },
    },
  },
};

export const NegativeButtonMargins = {
  render: () => ({
    template: cuiDemo(demoMargin),
  }),

  name: "Negative button margins",
  height: "152px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoMargin,
      },
    },
  },

  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule],
    }),
  ],
};
