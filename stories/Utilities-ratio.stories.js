import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import { demoRatio } from "./util/utilities";

export default {
  title: "Foundation/Utilities/Aspect ratio",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const AspectRatio = {
  render: () => ({
    template: cuiDemo(demoRatio),
  }),

  name: "Aspect ratio",
  height: "190px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoRatio,
      },
    },
  },
};
