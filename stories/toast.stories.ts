import { importProvidersFrom } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { ToastModule } from '@coyoapp/ui/toast';
import { applicationConfig, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { ToastDemoComponent } from './demo/toast-demo/toast-demo.component';
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Toast',
  component: ToastDemoComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserAnimationsModule, IconModule.forRoot(), ButtonModule, ToastModule.forRoot()],
      declarations: [ToastDemoComponent]
    }),
    applicationConfig({
      providers: [importProvidersFrom(ToastModule.forRoot())]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=734%3A926'),
    controls: {
      hideNoControlsWarning: true
    }
  }
} as Meta;

export const Toast: StoryObj<ToastDemoComponent> = (args: any) => ({
  props: args
});

Toast.args = {};
