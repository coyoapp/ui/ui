import { IconModule } from '@coyoapp/ui/icon';
import { PaginationComponent, PaginationModule } from '@coyoapp/ui/pagination';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, look, number, size } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Pagination',
  component: PaginationComponent,
  decorators: [
    width(600),
    moduleMetadata({
      imports: [IconModule.forRoot(), PaginationModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=68%3A165')
  },
  argTypes: {
    page: number(0),
    pageCount: number(0),
    activePadding: number(0),
    sidePadding: number(0),
    pageChange: action('pageChange'),
    look,
    size
  }
} as Meta;

export const Pagination: StoryObj<PaginationComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-pagination
      ${inputArgs('page', 'pageCount', 'activePadding', 'sidePadding', 'size', 'look')}
      ${outputArgs('pageChange')}
    ></cui-pagination>`
});

Pagination.args = {
  page: 4,
  pageCount: 10,
  activePadding: 1,
  sidePadding: 1,
  look: 'button',
  size: 'm'
};
