import { ButtonComponent, ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, iconOptional, look, mode, sizeAll, text } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'UI/Button',
  component: ButtonComponent,
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A8')
  },
  argTypes: {
    mode,
    look,
    size: sizeAll,
    round: boolean(),
    loading: boolean(),
    disabled: boolean(),
    prefixIcon: iconOptional,
    suffixIcon: iconOptional,
    textClass: text(),
    iconClass: text(),
    click: action('click')
  }
} as Meta;

export const Button: StoryObj<ButtonComponent & {ngContent: string}> = (args: any) => ({
  props: args,
  template: `<button cui-button
    ${inputArgs('mode', 'look', 'size', 'round', 'loading', 'disabled', 'prefixIcon', 'suffixIcon', 'textClass', 'iconClass')}
    ${outputArgs('click')}
  >{{ngContent}}</button>`
});

Button.args = {
  ngContent: 'Home',
  mode: 'primary',
  look: 'button',
  size: 'm'
};
