import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent, AlertModule } from '@coyoapp/ui/alert';
import { IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs } from './util/arg-inject';
import { action, iconOptional, modeAll, size } from './util/arg-types';
import { maxWidth } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'UI/Alert',
  component: AlertComponent,
  decorators: [
    maxWidth(480),
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        IconModule.forRoot(),
        AlertModule
      ]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=716%3A905')
  },
  argTypes: {
    mode: modeAll,
    size,
    icon: iconOptional,
    closed: action('closed')
  }
} as Meta;

export const AlertTemplate: StoryObj<AlertComponent & {ngContent: string}> = {
  args: {
    ngContent: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
    mode: 'primary',
    size: 'm'
  },
  render: args => ({
    props: args,
    template: `
    <cui-alert *ngIf="!hidden"
      ${ inputArgs('mode', 'size', 'icon', 'closeable', 'transition') }
      (closed)="hidden = !hidden; closed($event)"
    >{{ngContent}}</cui-alert>`
  })
};
