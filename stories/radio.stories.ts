import { FormsModule } from '@angular/forms';
import { RadioButtonComponent, RadioModule } from '@coyoapp/ui/radio';
import { action } from '@storybook/addon-actions';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { boolean, radio } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Radio',
  component: RadioButtonComponent,
  parameters: {
    badges: ['beta'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7'),
    actions: {
      handles: ['click']
    }
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [RadioModule, FormsModule]
    })
  ],
  argTypes: {
    disabled: boolean(),
    mode: radio(['primary', 'secondary']),
    size: radio(['s', 'm', 'l']),
    align: radio(['left', 'right', 'left-spread', 'right-spread'])
  }
} as Meta;

export const Radio: StoryObj<RadioButtonComponent> = (args: any) => ({
  props: {
    ...args,
    valueChange: action('Value changed', { depth: 1 })
  },
  styles: ['input { display: block; margin: .5rem }'],
  template: `
    <cui-radio-group [(ngModel)]="value" (change)="valueChange($event)">
      <cui-radio-button
          value="true"
          [mode]="mode"
          [size]="size"
          [align]="align"
          [checked]="checked"
          [disabled]="disabled">Value True</cui-radio-button>
        <cui-radio-button
          value="false"
          [mode]="mode"
          [size]="size"
          [align]="align"
          [checked]="checked"
          [disabled]="disabled">Value False</cui-radio-button>
    </cui-radio-group>
    <pre>Value: {{ value }}</pre>`
});

Radio.args = {
  size: 'm',
  align: 'left',
  disabled: false,
  mode: 'primary'
};
