import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { CollapseComponent, CollapseModule } from '@coyoapp/ui/collapse';
import { figma } from './util/parameters';
import { boolean } from './util/arg-types';

export default {
  title: 'Interaction/Collapse',
  component: CollapseComponent,
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule, CollapseModule, BrowserAnimationsModule]
    })
  ],
  parameters: {
    badges: ['beta'],
    design: figma('')
  },
  argTypes: {
    collapsed: boolean()
  }
} as Meta;

export const Collapse: StoryObj<CollapseComponent> = (args: any) => ({
  props: args,
  template: `
    <button cui-button
            (click)="collapse.toggle()" [suffixIcon]="!collapsed ? 'eye-solid' : 'eye-outlined'">
      Toggle Collapse
    </button>
    <div class="collapsable-container"
         [(cuiCollapse)]="collapsed"
         #collapse="cuiCollapse">
      <div class="collapsable-content"  style="padding: 1rem; margin-top: 1rem; border: 1px solid black">
        Collapsible content
      </div>
    </div>`
});

Collapse.args = {
  collapsed: false
} as any;
