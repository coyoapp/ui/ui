import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import { demoVisibility } from "./util/utilities";

export default {
  title: "Foundation/Utilities/Visibility",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Visibility = {
  render: () => ({
    template: cuiDemo(demoVisibility),
  }),

  name: "Visibility",
  height: "80px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoVisibility,
      },
    },
  },
};
