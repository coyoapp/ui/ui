import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { SidebarModule } from '@coyoapp/ui/sidebar';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { SidebarDemoComponent } from './demo/sidebar-demo/sidebar-demo.component';
import { ToastDemoComponent } from './demo/toast-demo/toast-demo.component';
import { number, radio } from './util/arg-types';

export default {
  title: 'Interaction/Sidebar',
  component: SidebarDemoComponent,
  decorators: [
    moduleMetadata({
      imports: [BrowserAnimationsModule, IconModule.forRoot(), ButtonModule, SidebarModule],
      declarations: [ToastDemoComponent]
    })
  ],
  parameters: {
    badges: ['beta'],
    controls: {
      hideNoControlsWarning: true
    }
  },
  argTypes: {
    direction: radio(['left', 'right']),
    width: number()
  }
} as Meta;

export const Sidebar: StoryObj<SidebarDemoComponent> = (args: any) => ({
  props: args
});

Sidebar.args = {
  direction: 'left',
  width: 25
};
