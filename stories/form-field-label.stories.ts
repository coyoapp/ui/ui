import { FormsModule } from '@angular/forms';
import { FormFieldLabelComponent, FormFieldModule } from '@coyoapp/ui/form-field';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs } from './util/arg-inject';
import { width } from './util/decorator';
import { figma } from './util/parameters';
import { IconModule } from "@coyoapp/ui/icon";

export default {
  title: 'Forms/Form Field Label',
  component: FormFieldLabelComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule, FormFieldModule, IconModule.forRoot()]
    })
  ]
} as Meta;

export const FormFieldLabel: StoryObj<FormFieldLabelComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field-label ${inputArgs('for', 'form')}>{{ngContent}}</cui-form-field-label>`
});
FormFieldLabel.args = {
  ngContent: 'Label'
} as Partial<FormFieldLabelComponent>;
