import { IconComponent, IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { icon, sizeAll } from './util/arg-types';

export default {
  title: 'UI/Icon',
  component: IconComponent,
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot()]
    })
  ],
  parameters: {
    badges: ['live']
  },
  argTypes: {
    name: icon,
    size: sizeAll
  }
} as Meta;

export const Icon: StoryObj<IconComponent> = (args: any) => ({
  props: args
});

Icon.args = {
  name: 'user'
};
