import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { IconModule } from '@coyoapp/ui/icon';
import { InputNumberComponent, InputNumberModule } from '@coyoapp/ui/input-number';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, number, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Input Number',
  component: InputNumberComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule, IconModule.forRoot(), FormFieldModule, InputNumberModule]
    })
  ],
  argTypes: {
    required: boolean(),
    placeholder: text(),
    min: number(),
    max: number(),
    step: number(),
    change: action('change')
  }
} as Meta;

export const InputNumber: StoryObj<InputNumberComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-number [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'min', 'max', 'step')}
        ${outputArgs('change')}
      ></cui-input-number>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputNumber.args = {
  placeholder: ''
}
