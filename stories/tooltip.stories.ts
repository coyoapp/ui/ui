import { importProvidersFrom } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { TooltipDirective, TooltipModule } from '@coyoapp/ui/tooltip';
import { applicationConfig, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs } from './util/arg-inject';
import { number } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Tooltip',
  component: TooltipDirective,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=718%3A950')
  },
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule, TooltipModule.forRoot()],
    }),
    applicationConfig({
      providers: [importProvidersFrom(TooltipModule.forRoot())]
    })
  ],
  argTypes: {
    cuiTooltipDelay: number(0)
  }
} as Meta;

export const Tooltip: StoryObj<TooltipDirective> = (args: any) => ({
  props: args,
  template: `<button cui-icon-button
    mode="primary" icon="globe" aria-label="Sample button"
    ${inputArgs('cuiTooltip', 'cuiTooltipPlacement', 'cuiTooltipDelay', 'cuiTooltipTrigger', 'cuiTooltipDisabled', 'cuiTooltipAllowHtml')}
  ></button>`
});

Tooltip.args = {
  cuiTooltip: 'I have a tooltip'
};
