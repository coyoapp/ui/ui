import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { InputTimepickerComponent, InputTimepickerModule } from '@coyoapp/ui/input-timepicker';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';
import { IconModule } from "@coyoapp/ui/icon";

export default {
  title: 'Forms/Input Timepicker',
  component: InputTimepickerComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule, FormFieldModule, IconModule.forRoot(), InputTimepickerModule.forRoot()]
    })
  ],
  argTypes: {
    required: boolean(),
    placeholder: text(),
    seconds: boolean(),
    change: action('change')
  }
} as Meta;

export const InputTimepicker: StoryObj<InputTimepickerComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-timepicker [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'seconds')}
        ${outputArgs('change')}
      ></cui-input-timepicker>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputTimepicker.args = {
  placeholder: '',
  seconds: false,
  required: true
}
