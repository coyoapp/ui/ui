import { FormFieldModule } from '@coyoapp/ui/form-field';
import { IconModule } from '@coyoapp/ui/icon';
import { SelectComponent, SelectModule } from '@coyoapp/ui/select';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { ArgsStoryFn } from '@storybook/types';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, array, boolean, number, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Select',
  component: SelectComponent,
  decorators: [
    width(360),
    moduleMetadata({
      imports: [IconModule.forRoot(), FormFieldModule, SelectModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  argTypes: {
    addTag: boolean(),
    placeholder: text(),
    bindValue: text(),
    bindLabel: text(),
    clearable: boolean(),
    loading: boolean(),
    maxSelectedItems: number(),
    multiple: boolean(),
    searchable: boolean(),
    submitKeys: array(),
    defaultPrefix: text(),
    change: action('change'),
    scroll: action('scroll'),
    scrollToEnd: action('scrollToEnd')
  }
} as Meta;

const Template: ArgsStoryFn<any> = (args)=> ({
  props: args,
  template: `<cui-form-field>
    <cui-select
      ${inputArgs('placeholder', 'addTag', 'submitKeys', 'searchPrefix', 'bindValue', 'bindLabel', 'clearable', 'items', 'loading', 'maxSelectedItems', 'multiple', 'searchable')}
      ${outputArgs('change', 'scroll', 'scrollToEnd')}
      bindLabel="name"
    ></cui-select>
  </cui-form-field>`
});

const args = {
  addTag: false,
  placeholder: 'Placeholder',
  bindValue: '',
  bindLabel: 'label',
  clearable: false,
  items: [
    { id: 1, label: 'Carmen Kuphal' },
    { id: 2, label: 'Rick Will' },
    { id: 3, label: 'Wanda Berge' },
    { id: 4, label: 'Dr. Bridget Glover' },
    { id: 5, label: 'Brad Raynor' },
    { id: 6, label: 'Rosie Hoppe' },
    { id: 7, label: 'Kellie Bradtke' },
    { id: 8, label: 'Santiago Torphy' },
    { id: 9, label: 'Julia Beier' },
    { id: 10, label: 'Faye O\'Conner' }
  ],
  loading: false,
  multiple: false,
  searchable: true,
  submitKeys: [],
  defaultPrefix: ''
};

export const SingleSelect = {
  args: { ...args },
  render: Template
};

export const MultiSelect = {
  args: { ...args, multiple: true, clearable: true },
  render: Template
};

export const TagInput = {
  args: {
    ...args,
    items: [],
    searchPrefix: '#',
    selectOnTab: true,
    submitKeys: [' ', ':', ',', 'Enter'],
    addTag: true,
    multiple: true,
    clearable: true,
    bindLabel: ''
  },
  render: Template
};
