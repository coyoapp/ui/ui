import { ScrolledLeftDirective, ScrollingModule } from '@coyoapp/ui/scrolling';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { boolean, number, action } from './util/arg-types';

export default {
  title: 'Interaction/Scrolling/Scrolled Left',
  decorators: [
    moduleMetadata({
      imports: [ScrollingModule]
    })
  ],
  parameters: {
    badges: ['live']
  },
  argTypes: {
    cuiScrolledInit: boolean(),
    cuiScrolledBuffer: number(),
    cuiScrolledLeft: action('cuiScrolledLeft')
  }
} as Meta;

export const ScrolledLeft: StoryObj<ScrolledLeftDirective> = (args: any) => ({
  props: args,
  template: `
    <div style="max-width: 200px; max-height: 400px; overflow: auto; white-space: pre" ${inputArgs()} ${outputArgs('cuiScrolledLeft')}>
      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
      <p>At vero eos et accusam et justo duo dolores et ea rebum.</p>
      <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
      <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
      <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
      <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
    </div>`
});

ScrolledLeft.args = {
  cuiScrolledInit: true,
  cuiScrolledBuffer: 0
};
