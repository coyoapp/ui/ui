import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule, NoopAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { ButtonModule } from '@coyoapp/ui/button';
import { CheckboxModule } from '@coyoapp/ui/checkbox';
import { IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { CardModule } from '@coyoapp/ui/card';
import { InputTextModule } from '@coyoapp/ui/input-text';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { SelectModule } from '@coyoapp/ui/select';
import { action } from '@storybook/addon-actions';
import { RadioModule } from '@coyoapp/ui/radio';
import { ToggleModule } from '@coyoapp/ui/toggle';
import { InputDatepickerModule } from '@coyoapp/ui/input-datepicker';
import { MenuModule } from '@coyoapp/ui/menu';
import { BadgeModule } from '@coyoapp/ui/badge';
import { CollapseModule } from '@coyoapp/ui/collapse';
import { addClass, width } from './util/decorator';
import { figma } from './util/parameters';
import { FilterBoxDemoComponent } from './demo/filter-box-demo/filter-box-demo.component';

export default {
  title: 'UI/Filter Box',
  component: FilterBoxDemoComponent,
  decorators: [
    width(300),
    addClass('cui-typo'),
    moduleMetadata({
      imports: [CommonModule,
        CardModule,
        InputTextModule,
        FormFieldModule,
        FormsModule,
        RadioModule,
        ReactiveFormsModule,
        SelectModule,
        MenuModule.forRoot(),
        IconModule.forRoot(),
        InputDatepickerModule.forRoot(),
        BadgeModule,
        CollapseModule,
        ButtonModule,
        ToggleModule,
        BrowserAnimationsModule,
        CheckboxModule],
      declarations: [FilterBoxDemoComponent]
    })
  ],
  parameters: {
    badges: ['live'],
    backgrounds: { default: 'light' },
    design: figma('https://www.figma.com/file/lI9CWPuean5zGraTVVQqWZ/Overview(s)?node-id=487%3A5667'),
    controls: {
      hideNoControlsWarning: true
    }
  },
  argTypes: {
    filtersChanged: action('change')
  }
} as Meta;

export const FilterBox: StoryObj<FilterBoxDemoComponent> = () => ({
  props: {
    valueChange: action('Filters changed', { depth: 1 })
  },
  template: `
    <cui-filter-box-demo (filtersChanged)="valueChange($event)"></cui-filter-box-demo>`
});

FilterBox.args = {};
