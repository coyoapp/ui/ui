import { ButtonModule, IconButtonDirective } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, icon, inlineRadio, radio, text } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'UI/Button (Icon)',
  component: IconButtonDirective,
  decorators: [
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=27%3A88')
  },
  argTypes: {
    mode: radio(['primary', 'secondary', 'ghost', 'success', 'error']),
    size: inlineRadio(['xs', 's', 'm', 'l', 'xl']),
    iconClass: text(),
    loading: boolean(),
    disabled: boolean(),
    icon,
    click: action('click')
  }
} as Meta;

export const IconButton: StoryObj<IconButtonDirective> = (args: any) => ({
  props: args,
  template: `<button cui-icon-button
    ${inputArgs('mode', 'size', 'icon', 'loading', 'disabled', 'iconClass')}
    ${outputArgs('click')}
  ></button>`
});

IconButton.storyName = 'Button (Icon)';
IconButton.args = {
  mode: 'primary',
  size: 'm',
  disabled: false,
  loading: false
} as Partial<IconButtonDirective>;
