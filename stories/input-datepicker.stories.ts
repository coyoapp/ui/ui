import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { IconModule } from '@coyoapp/ui/icon';
import { InputDatepickerComponent, InputDatepickerModule } from '@coyoapp/ui/input-datepicker';
import { InputDatepickerInlineComponent } from '@coyoapp/ui/input-datepicker/input-datepicker-inline/input-datepicker-inline.component';
import { MenuModule } from '@coyoapp/ui/menu';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { AngularMyDatePickerModule } from '@nodro7/angular-mydatepicker';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Input Datepicker',
  component: InputDatepickerComponent,
  parameters: {
    badges: ['beta'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(320),
    moduleMetadata({
      imports: [FormsModule, FormFieldModule, MenuModule.forRoot(), IconModule.forRoot(), AngularMyDatePickerModule, InputDatepickerModule.forRoot()]
    })
  ],
  argTypes: {
    required: boolean(),
    placeholder: text(),
    locale: text(),
    isRange: boolean(),
    change: action('change')
  }
} as Meta;

export const InputDatepicker: StoryObj<InputDatepickerComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-datepicker [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'isRange', 'dateFormat', 'locale')}
        ${outputArgs('change')}
      ></cui-input-datepicker>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputDatepicker.args = {
  locale: 'en',
  placeholder: '',
  isRange: false,
  required: true,
  dateFormat: 'mm/dd/yyyy',
  value: '02/02/2021'
};

export const InputDatepickerRange: StoryObj<InputDatepickerComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-datepicker [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'isRange', 'dateFormat', 'locale')}
        ${outputArgs('change')}
      ></cui-input-datepicker>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputDatepickerRange.args = {
  locale: 'en',
  placeholder: '',
  isRange: true,
  required: true,
  dateFormat: 'mm/dd/yyyy',
  value: '02/02/2021-02/05/2021'
};

export const InputDatepickerInline: StoryObj<InputDatepickerInlineComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-datepicker-inline [(ngModel)]="value"
        ${inputArgs('required', 'isRange', 'dateFormat', 'locale')}
        ${outputArgs('change')}
      ></cui-input-datepicker-inline>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputDatepickerInline.args = {
  locale: 'en',
  required: true,
  isRange: false,
  dateFormat: 'mm/dd/yyyy',
  value: '02/02/2021'
};

export const InputDatepickerInlineRange: StoryObj<InputDatepickerInlineComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-datepicker-inline [(ngModel)]="value"
        ${inputArgs('required', 'isRange', 'dateFormat', 'locale')}
        ${outputArgs('change')}
      ></cui-input-datepicker-inline>
    </cui-form-field>
    <div>Value: {{ value }}</div>`
});
InputDatepickerInlineRange.args = {
  locale: 'en',
  required: true,
  isRange: true,
  dateFormat: 'mm/dd/yyyy',
  value: '02/02/2021-02/05/2021'
};
