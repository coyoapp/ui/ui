import { CommonModule } from '@angular/common';
import { ThemeModule } from '@coyoapp/ui/theme';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { PaletteServiceDemoComponent } from './demo/palette-service-demo/palette-service-demo.component';
import { color } from './util/arg-types';

export default {
  title: 'Interaction/Palette/Service',
  component: PaletteServiceDemoComponent,
  decorators: [
    moduleMetadata({
      imports: [CommonModule, ThemeModule],
      declarations: [PaletteServiceDemoComponent]
    })
  ],
  parameters: {
    badges: ['beta']
  },
  argTypes: {
    color: color()
  }
} as Meta;

export const Service: StoryObj<PaletteServiceDemoComponent> = (args: any) => ({
  props: {
    color: args.color
  }
});

Service.args = {
  color: '#0073e6'
};
