import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule, provideAnimations } from '@angular/platform-browser/animations';
import { BadgeModule } from '@coyoapp/ui/badge';
import { ButtonModule } from '@coyoapp/ui/button';
import { CheckboxModule } from '@coyoapp/ui/checkbox';
import { CollapseModule } from '@coyoapp/ui/collapse';
import { IconModule } from '@coyoapp/ui/icon';
import { ScrollingModule } from '@coyoapp/ui/scrolling';
import { TableModule } from '@coyoapp/ui/table';
import { applicationConfig, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import {
  TableCollapsableRowsDemoComponent
} from './demo/table-collapsable-rows-demo/table-collapsable-rows-demo.component';
import { TableResponsiveRowsDemoComponent } from './demo/table-responsive-demo/table-responsive-demo.component';
import { TableSelectRowsDemoComponent } from './demo/table-select-rows-demo/table-select-rows-demo.component';
import { TableSortableRowsDemoComponent } from './demo/table-sortable-rows-demo/table-sortable-rows-demo.component';
import { TableStickyRowsDemoComponent } from './demo/table-sticky-demo/table-sticky-demo.component';
import { addClass, width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'UI/Table',
  component: TableSelectRowsDemoComponent,
  decorators: [
    width(600),
    addClass('cui-typo'),
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        CollapseModule,
        FormsModule,
        IconModule.forRoot(),
        ButtonModule,
        CheckboxModule,
        BadgeModule,
        TableModule,
        ScrollingModule
      ],
      declarations: [
        TableStickyRowsDemoComponent,
        TableResponsiveRowsDemoComponent,
        TableSelectRowsDemoComponent,
        TableSortableRowsDemoComponent,
        TableCollapsableRowsDemoComponent
      ]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Web-Components?node-id=371%3A859'),
    controls: {
      hideNoControlsWarning: true
    }
  }
} as Meta;

export const TableSelectable: StoryObj<TableSelectRowsDemoComponent> = {
  args: {},
  render:
    () => ({
      template: `
    <cui-table-select-rows-demo></cui-table-select-rows-demo>`
    })
};

export const TableSorting: StoryObj<TableSortableRowsDemoComponent> = {
  args: {},
  render: () => ({
    template: `
    <cui-table-sortable-rows-demo></cui-table-sortable-rows-demo>`
  })
};


export const TableCollapse: StoryObj<TableCollapsableRowsDemoComponent> = {
  args: {},
  render: () => ({
    template: `
    <cui-table-collapsable-rows-demo></cui-table-collapsable-rows-demo>`
  })
};

export const TableResponsive: StoryObj<TableResponsiveRowsDemoComponent> = {
  args: {},
  render: () => ({
    template: `
    <cui-table-responsive-demo></cui-table-responsive-demo>`
  })
};

export const TableSticky: StoryObj<TableStickyRowsDemoComponent> = {
  args: {},
  render: () => ({
    template: `
    <cui-table-sticky-demo></cui-table-sticky-demo>`
  })
};
