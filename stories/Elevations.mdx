import { Meta, Canvas, Source } from '@storybook/blocks';
import dedent from 'ts-dedent';

<Meta title="Foundation/Elevations" />

# Elevations

Shadows are used to create a visual hierarchy and add separation between
elements along the z-axis.

### Predefined CSS classes

The easiest way to add elevation to an element is to simply add one of the
predefined CSS classes `cui-elevation-#` where `#` is the elevation number you
want, 1-4. Dynamic elevation can be achieved by switching elevation classes.

<div class="canvas canvas-source" style={{display: 'flex', justifyContent: 'space-around', background: '#f2f3f4'}}>
  <pre class="cui-elevation-1" style={{padding: '1.25rem', background: '#fff'}}>Elevation 1</pre>
  <pre class="cui-elevation-2" style={{padding: '1.25rem', background: '#fff'}}>Elevation 2</pre>
  <pre class="cui-elevation-3" style={{padding: '1.25rem', background: '#fff'}}>Elevation 3</pre>
  <pre class="cui-elevation-4" style={{padding: '1.25rem', background: '#fff'}}>Elevation 4</pre>
</div>

<Source
  language="html"
  code={dedent`
    <pre class="cui-elevation-1">Elevation 1</pre>
    <pre class="cui-elevation-2">Elevation 2</pre>
    <pre class="cui-elevation-3">Elevation 3</pre>
    <pre class="cui-elevation-4">Elevation 4</pre>
  `}
/>

To easily switch elevations on hover, you can simply add one of the predefined
CSS classes `cui-elevation-hover-#` where `#` is the elevation number you want,
1-4. A transition between elevation changes can be applied using the predefined
CSS class `cui-elevation-transition`.

<div class="canvas canvas-source" style={{display: 'flex', justifyContent: 'space-around', background: '#f2f3f4'}}>
  <pre className="cui-elevation-hover-1 cui-elevation-transition" style={{padding: '1.25rem', background: '#fff', textAlign: 'center'}}>Elevation 1<br />(hover)</pre>
  <pre className="cui-elevation-hover-2 cui-elevation-transition" style={{padding: '1.25rem', background: '#fff', textAlign: 'center'}}>Elevation 2<br />(hover)</pre>
  <pre className="cui-elevation-hover-3 cui-elevation-transition" style={{padding: '1.25rem', background: '#fff', textAlign: 'center'}}>Elevation 3<br />(hover)</pre>
  <pre className="cui-elevation-hover-4 cui-elevation-transition" style={{padding: '1.25rem', background: '#fff', textAlign: 'center'}}>Elevation 4<br />(hover)</pre>
</div>

<Source
  language="html"
  code={dedent`
    <pre class="cui-elevation-hover-1 cui-elevation-transition">Elevation 1 (hover)</pre>
    <pre class="cui-elevation-hover-2 cui-elevation-transition">Elevation 2 (hover)</pre>
    <pre class="cui-elevation-hover-3 cui-elevation-transition">Elevation 3 (hover)</pre>
    <pre class="cui-elevation-hover-4 cui-elevation-transition">Elevation 4 (hover)</pre>
  `}
/>

### Mixins

Elevations can also be added in CSS via the `elevation` mixin, which takes a
number 1-4 indicating the elevation of the element.

In order to use the mixin, you must import `~@coyoapp/ui/mixins`.

<Source
  language="css"
  code={dedent`
    @use 'mixins';
    .my-class {
      // Adds a shadow for elevation level 2:
      @include mixins.elevation(2);
    }
  `}
/>

For convenience, you can use the `elevation-transition` mixin to add a
transition when the elevation changes:

<Source
  language="css"
  code={dedent`
    @use 'mixins';
    .my-class {
      @include mixins.elevation-transition;
      @include mixins.elevation(2);
      &:active {
        @include mixins.elevation(4);
      }
    }
  `}
/>
