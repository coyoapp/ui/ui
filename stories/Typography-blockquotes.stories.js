import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoHead1,
  demoHead2,
  demoInline,
  demoBlockquote1,
  demoBlockquote2,
  demoList1,
  demoList2,
  demoTable,
} from "./util/typography";

export default {
  title: "Foundation/Typography/Blockquotes",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Blockquotes = {
  render: () => ({
    template: cuiDemo(demoBlockquote1),
  }),

  name: "Blockquotes",
  height: "84px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoBlockquote1,
      },
    },
  },
};

export const BlockquotesWithSource = {
  render: () => ({
    template: cuiDemo(demoBlockquote2),
  }),

  name: "Blockquotes (with source)",
  height: "100px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoBlockquote2,
      },
    },
  },
};
