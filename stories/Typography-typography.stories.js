import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoHead1,
  demoHead2,
  demoInline,
  demoBlockquote1,
  demoBlockquote2,
  demoList1,
  demoList2,
  demoTable,
  demoText,
} from "./util/typography";

export default {
  title: "Foundation/Typography/Typography",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Headings = {
  render: () => ({
    template: cuiDemo(demoHead1),
  }),

  name: "Headings",
  height: "244px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoHead1,
      },
    },
  },
};

export const HeadingsCss = {
  render: () => ({
    template: cuiDemo(demoHead2),
  }),

  name: "Headings (CSS)",
  height: "284px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoHead2,
      },
    },
  },
};

export const Text = {
  render: () => ({
    template: cuiDemo(demoText),
  }),

  name: "Text",
  height: "224px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoText,
      },
    },
  },
};

export const InlineTextElements = {
  render: () => ({
    template: cuiDemo(demoInline),
  }),

  name: "Inline text elements",
  height: "405px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoInline,
      },
    },
  },
};
