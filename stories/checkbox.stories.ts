import { FormsModule } from '@angular/forms';
import { CheckboxComponent, CheckboxModule } from '@coyoapp/ui/checkbox';
import { action } from '@storybook/addon-actions';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { radio } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Checkbox',
  component: CheckboxComponent,
  decorators: [
    width(360),
    moduleMetadata({
      imports: [CheckboxModule, FormsModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7'),
    actions: {
      handles: ['click', 'change']
    }
  },
  argTypes: {
    mode: radio(['primary', 'secondary'])
  }
} as Meta;

export const Checkbox: StoryObj<CheckboxComponent & {ngContent: string}> = (args: any) => ({
  props: {
    ...args,
    valueChange: action('Value changed', { depth: 1 })
  },
  template: `<cui-checkbox [(ngModel)]="value" [mode]="mode" (change)="valueChange($event)">{{ngContent}}</cui-checkbox>
  <pre>Value: {{ value }}</pre>`
});

Checkbox.args = {
  ngContent: 'Checkbox label',
  mode: 'primary',
} as any;
