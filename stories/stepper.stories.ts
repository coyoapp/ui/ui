import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconModule } from '@coyoapp/ui/icon';
import { MenuModule } from '@coyoapp/ui/menu';
import { StepperComponent, StepperModule } from '@coyoapp/ui/stepper';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { ButtonModule } from '@coyoapp/ui/button';
import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { InputTextModule } from '@coyoapp/ui/input-text';
import { figma } from './util/parameters';

export default {
  title: 'Interaction/Stepper',
  component: StepperComponent,
  decorators: [
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        IconModule.forRoot(),
        MenuModule.forRoot(),
        StepperModule,
        InputTextModule,
        FormFieldModule,
        FormsModule,
        ButtonModule
      ]
    })
  ],
  parameters: {
    badges: ['beta'],
    design: figma(
      'https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=76%3A178'
    )
  },
  argTypes: {}
} as Meta;

export const Stepper: StoryObj<StepperComponent> = {
  args: {
    active: 0
  },
  render: (args: any) => ({
    props: args,
    template: `
  <div style="width:85vw">
    <cui-stepper [vertical]="false" [activeHead]="true">
      <cui-step label="Name" prefixIcon="contacts" >
        <label class="step-input-label" for="input">
          <p class="step-input-heading">Please enter your name</p>
        </label>
        <cui-form-field style="width:fit-content">
          <cui-input-text
            id="input"
            [(ngModel)]="value"
            placeholder="Firstname, Lastname">
          </cui-input-text>
        </cui-form-field>
        <div class="navigation">
          <button
            cui-button
            cuiStepNext
            mode="ghost"
            size="s"
          >
            Next
          </button>
        </div>
      </cui-step>
      <cui-step label="Address" suffixIcon="location-pin">
        <label class="step-input-label" for="input">
          <p class="step-input-heading">Please enter your address</p>
        </label>
        <cui-form-field style="width:fit-content">
          <cui-input-text
            id="input"
            [(ngModel)]="value"
            placeholder="Street, Nr., Zip, City">
          </cui-input-text>
        </cui-form-field>
        <div class="navigation">
          <button
            cui-button
            cuiStepBack
            mode="ghost"
            size="s"
          >
            Back
          </button>
          <button
            cui-button
            cuiStepNext
            mode="ghost"
            size="s"
          >
            Next
          </button>
        </div>
      </cui-step>
      <cui-step label="Feddback" prefixIcon="emblem-coyo" [disabled]="true">
        <label class="step-input-label" for="input">
          <p class="step-input-heading">What do you like about COYO?</p>
        </label>
        <cui-form-field style="width:fit-content">
          <cui-input-text
            id="input"
            [(ngModel)]="value"
            placeholder="Your Feedback">
          </cui-input-text>
        </cui-form-field>
        <div class="navigation">
        <button
            cui-button
            cuiStepBack
            mode="ghost"
            size="s"
          >
            Back
          </button>
          <button
            cui-button
            cuiStepNext
            mode="ghost"
            size="s"
          >
            Next
          </button>
        </div>
      </cui-step>
      <cui-step label="Summary" prefixIcon="check" stepNumber="10" >
        <p class="step-input-heading">All set and done!</p>
        <div class="navigation">
          <button
            cui-button
            cuiStepBack
            mode="ghost"
            size="s"
          >
            Back
          </button>
        </div>
      </cui-step>
    </cui-stepper>
  </div>`
  })
};
