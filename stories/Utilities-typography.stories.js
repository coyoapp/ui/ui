import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoSize,
  demoAlign,
  demoBreak,
  demoSelect,
  demoTruncate1,
  demoTruncate2,
} from "./util/utilities";

export default {
  title: "Foundation/Utilities/Typography",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Size = {
  render: () => ({
    template: cuiDemo(demoSize),
  }),

  name: "Size",
  height: "224px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoSize,
      },
    },
  },
};

export const Alignment = {
  render: () => ({
    template: cuiDemo(demoAlign),
  }),

  name: "Alignment",
  height: "152px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoAlign,
      },
    },
  },
};

export const WordBreak = {
  render: () => ({
    template: cuiDemo(demoBreak),
  }),

  name: "Word break",
  height: "100px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoBreak,
      },
    },
  },
};

export const Selection = {
  render: () => ({
    template: cuiDemo(demoSelect),
  }),

  name: "Selection",
  height: "116px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoSelect,
      },
    },
  },
};

export const TextTruncation = {
  render: () => ({
    template: cuiDemo(demoTruncate1),
  }),

  name: "Text truncation",
  height: "80px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTruncate1,
      },
    },
  },
};

export const TextTruncationMultiLine = {
  render: () => ({
    template: cuiDemo(demoTruncate2),
  }),

  name: "Text truncation (multi-line)",
  height: "100px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTruncate2,
      },
    },
  },
};
