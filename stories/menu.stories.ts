import { importProvidersFrom } from '@angular/core';
import { ButtonModule } from '@coyoapp/ui/button';
import { IconModule } from '@coyoapp/ui/icon';
import { MenuComponent, MenuModule } from '@coyoapp/ui/menu';
import { applicationConfig, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { figma } from './util/parameters';
import { addClass, width } from './util/decorator';
import { MenuTestDemoComponent } from './demo/menu-test-demo/menu-test-demo.component';

export default {
  title: 'Interaction/Menu',
  component: MenuComponent,
  decorators: [
    width(400),
    addClass('cui-typo'),
    moduleMetadata({
      imports: [IconModule.forRoot(), ButtonModule, MenuModule.forRoot()],
      declarations: [MenuTestDemoComponent],
    }),
    applicationConfig({
      providers: [importProvidersFrom(MenuModule.forRoot())]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=29%3A100')
  }
} as Meta;

export const Menu: StoryObj<MenuComponent> = (args: any) => ({
  props: args,
  template: `
    <button cui-button mode="primary" [cui-menu-trigger]="menu">Show menu</button>
    <cui-menu #menu="menu">
      <cui-menu-test-demo></cui-menu-test-demo>
      <hr>
      <button cui-menu-item [active]="true">Active</button>
      <span cui-menu-title>Options</span>
      <button cui-menu-item>Edit</button>
      <a href="#" cui-menu-item>Link</a>
      <button cui-menu-item [disabled]="true">Share</button>
      <hr>
      <button cui-menu-item prefixIcon="delete">Delete</button>
    </cui-menu>`
});

Menu.args = {};

export const SubMenu: StoryObj<MenuComponent> = (args: any) => ({
  props: args,
  template: `
    <button cui-button mode="primary" [cui-menu-trigger]="menu">Show menu</button>
    <cui-menu #menu="menu">
      <button cui-menu-item>Action 1</button>
      <button cui-menu-item [cui-menu-trigger]="submenu">Show submenu</button>
      <button cui-menu-item [disabled]="true" [cui-menu-trigger]="submenu">Show submenu (inactive)</button>
      <button cui-menu-item>Action 2</button>
    </cui-menu>
    <cui-menu #submenu="menu">
      <button cui-menu-item>Action 1</button>
      <button cui-menu-item>Action 2</button>
      <button cui-menu-item>Action 3</button>
    </cui-menu>`
});

SubMenu.args = {};
