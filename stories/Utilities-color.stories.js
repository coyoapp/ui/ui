import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoTextColor,
  demoBackgroundColor,
  demoHoverColor,
  demoLinkColor,
  demoResetColor,
} from "./util/utilities";

export default {
  title: "Foundation/Utilities/Colors",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const TextColor = {
  render: () => ({
    template: cuiDemo(demoTextColor),
  }),

  name: "Text color",
  height: "112px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTextColor,
      },
    },
  },
};

export const BackgroundColor = {
  render: () => ({
    template: cuiDemo(demoBackgroundColor),
  }),

  name: "Background color",
  height: "112px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoBackgroundColor,
      },
    },
  },
};

export const HoverColor = {
  render: () => ({
    template: cuiDemo(demoHoverColor),
  }),

  name: "Hover color",
  height: "164px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoHoverColor,
      },
    },
  },
};

export const LinkColor = {
  render: () => ({
    template: cuiDemo(demoLinkColor),
  }),

  name: "Link color",
  height: "112px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoLinkColor,
      },
    },
  },
};

export const ResetColor = {
  render: () => ({
    template: cuiDemo(demoResetColor),
  }),

  name: "Reset color",
  height: "80px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoResetColor,
      },
    },
  },
};
