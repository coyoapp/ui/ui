import { ci } from '@coyoapp/icons';

export const mode = radio(['primary', 'secondary', 'ghost', 'success', 'error']);
export const modeAll = radio(['primary', 'secondary', 'ghost', 'note', 'info', 'success', 'warning', 'error']);
export const size = inlineRadio(['s', 'm', 'l']);
export const look = radio(['button', 'link']);
export const radius = inlineRadio(['s', 'm', 'l']);
export const sizeAll = inlineRadio(['xs', 's', 'm', 'l', 'xl']);
export const icon = select(Object.keys(ci));
export const iconOptional = select([null, ...Object.keys(ci)]);

/*
 * See: https://storybook.js.org/docs/react/essentials/controls
 */

export function array(separator = ',') {
  return control('array', undefined, separator);
}

export function boolean() {
  return control('boolean');
}

export function number(min?: number, max?: number, step?: number) {
  return control('number', undefined, undefined, min, max, step);
}
export function range(min?: number, max?: number, step?: number) {
  return control('range', undefined, undefined, min, max, step);
}

export function object() {
  return control('object');
}

export function radio(options?: (string | null)[]) {
  return control('radio', options);
}
export function inlineRadio(options?: (string | null)[]) {
  return control('inline-radio', options);
}
export function check(options?: (string | null)[]) {
  return control('check', options);
}
export function inlineCheck(options?: (string | null)[]) {
  return control('inline-check', options);
}
export function select(options?: (string | null)[]) {
  return control('select', options);
}
export function multiSelect(options?: (string | null)[]) {
  return control('multi-select', options);
}

export function text() {
  return control('text');
}
export function color() {
  return control('color');
}
export function date() {
  return control('date');
}

export function action(name: string) {
  return ({ action: name });
}

type controlType = 'array'
  | 'boolean'
  | 'number'
  | 'range'
  | 'object'
  | 'radio'
  | 'inline-radio'
  | 'check'
  | 'inline-check'
  | 'select'
  | 'multi-select'
  | 'text'
  | 'color'
  | 'date';

function control(
  type: controlType,
  options?: (string | null)[],
  separator?: string,
  min?: number,
  max?: number,
  step?: number
) {
  return ({
    options,
    control: { type, separator, min, max, step }
  });
}
