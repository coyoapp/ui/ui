/*
 * See: https://github.com/pocka/storybook-addon-designs/blob/master/README.md
 */

export function figma(url: string) {
  return ({
    type: 'figma',
    url
  });
}
