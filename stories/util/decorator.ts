export const width = (width: number) => (storyFunc: any) => {
  const story = storyFunc();
  return {
    ...story,
    template: `<div style="width: ${width}px;">${story.template}</div>`
  };
};

export const maxWidth = (width: number) => (storyFunc: any) => {
  const story = storyFunc();
  return {
    ...story,
    template: `<div style="max-width: ${width}px;">${story.template}</div>`
  };
};

export const addClass = (...classes: string[]) => (storyFunc: any) => {
  const story = storyFunc();
  return {
    ...story,
    template: `<div class="${classes.join(' ')}">${story.template}</div>`
  };
};
