export function inputArgs(...inputs: string[]) {
  return inputs.map(input => buildInputArg(input)).join(' ');
}

function buildInputArg(input: string) {
  let result = `[${input}]="${input}`;

  switch (input) {
    case 'mode':
      result += " || 'primary'";
      break;
    case 'size':
      result += " || 'm'";
      break;
    case 'look':
      result += " || 'button'";
      break;
  }

  return result + '"';
}

export function outputArgs(...outputs: string[]) {
  return outputs.map(output => `(${output})="${output}($event)"`).join(' ');
}
