import dedent from 'ts-dedent';

// ----- typography

export const demoHead1 = dedent(`
  <h1>h1. CUI heading element</h1>
  <h2>h2. CUI heading element</h2>
  <h3>h3. CUI heading element</h3>
  <h4>h4. CUI heading element</h4>
  <h5>h5. CUI heading element</h5>
  <h6>h6. CUI heading element</h6>
`);

export const demoHead2 = dedent(`
  <p class="cui-h1">cui-h1. CUI heading element</p>
  <p class="cui-h2">cui-h2. CUI heading element</p>
  <p class="cui-h3">cui-h3. CUI heading element</p>
  <p class="cui-h4">cui-h4. CUI heading element</p>
  <p class="cui-h5">cui-h5. CUI heading element</p>
  <p class="cui-h6">cui-h6. CUI heading element</p>
`);

export const demoText = dedent(`
  <p class="cui-text-xl">cui-text-xl. CUI text element</p>
  <p class="cui-text-l">cui-text-l. CUI text element</p>
  <p class="cui-text-m">cui-text-m. CUI text element (default)</p>
  <p class="cui-text-s">cui-text-s. CUI text element</p>
  <p class="cui-text-xs">cui-text-xs. CUI text element</p>
`);

export const demoInline = dedent(`
  <p>You can click <a href="#">this highlighted</a> text.</p>
  <p>You can use the mark tag to <mark>highlight</mark> text.</p>
  <p><del>This line of text is meant to be treated as deleted text.</del></p>
  <p><s>This line of text is meant to be treated as no longer accurate.</s></p>
  <p><ins>This line of text is meant to be treated as an addition to the document.</ins></p>
  <p><u>This line of text will render as underlined.</u></p>
  <p><small>This line of text is meant to be treated as fine print.</small></p>
  <p><strong>This line rendered as bold text.</strong></p>
  <p><em>This line rendered as italicized text.</em></p>
`);

// ----- blockquotes

export const demoBlockquote1 = dedent(`
  <blockquote>
    <p>A well-known quote, contained in a blockquote element.</p>
  </blockquote>
`);

export const demoBlockquote2 = dedent(`
  <figure>
    <blockquote>
      <p>A well-known quote, contained in a blockquote element.</p>
    </blockquote>
    <figcaption>
      Someone famous in <cite title="Source Title">Source Title</cite>
    </figcaption>
  </figure>
`);

// ----- code

export const demoCode1 = dedent(`
  Use <code>&lt;code&gt;&lt;/code&gt;</code> to mark inline code.
`);

export const demoCode2 = dedent(`
  <pre>
    <code>&lt;script&gt;
      alert( 'Hello, world!' );
    &lt;/script&gt;</code>
  </pre>
`);

// ----- KBD

export const demoKbd1 = dedent(`
  Press <kbd>Ctrl</kbd> + <kbd class="symbol">↑</kbd> key to submit
  or <kbd class="symbol">↓</kbd> + <kbd class="symbol">←</kbd> + <kbd class="symbol">→</kbd> to Cancel
`);

// ----- lists

export const demoList1 = dedent(`
  <ol>
    <li>Buy milk</li>
    <li>Pour glass</li>
    <li>Drink it</li>
  </ol>
  <ul>
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
  </ul>
`);

export const demoList2 = dedent(`
  <ul class="cui-plain">
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
  </ul>
`);

// ----- tables

export function demoTable(...classes: string[]) {
  return dedent(`
    <table${classes.length > 0 ? ' class="' + classes.join(' ') + '"' : ''}>
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">First</th>
          <th scope="col">Last</th>
          <th scope="col">Handle</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>&#64;mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>&#64;fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td colspan="2">Larry the Bird</td>
          <td>&#64;twitter</td>
        </tr>
      </tbody>
    </table>
  `);
}
