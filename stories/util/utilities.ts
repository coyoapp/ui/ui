import dedent from 'ts-dedent';

// ----- Color

export const demoTextColor = dedent(`
  <div class="cui-demo-grid cui-demo-panels">
    <div class="cui-text-primary">Primary</div>
    <div class="cui-text-secondary">Secondary</div>
    <div class="cui-text-note">Note</div>
    <div class="cui-text-info">Info</div>
    <div class="cui-text-success">Success</div>
    <div class="cui-text-warning">Warning</div>
    <div class="cui-text-error">Error</div>
  </div>
`);

export const demoBackgroundColor = dedent(`
  <div class="cui-demo-grid cui-demo-panels">
    <div class="cui-bg-primary">Primary</div>
    <div class="cui-bg-secondary">Secondary</div>
    <div class="cui-bg-note">Note</div>
    <div class="cui-bg-info">Info</div>
    <div class="cui-bg-success">Success</div>
    <div class="cui-bg-warning">Warning</div>
    <div class="cui-bg-error">Error</div>
  </div>
`);

export const demoHoverColor = dedent(`
  <div class="cui-demo-grid cui-demo-panels">
    <div class="cui-text-primary-hover">Primary</div>
    <div class="cui-text-secondary-hover">Secondary</div>
    <div class="cui-text-note-hover">Note</div>
    <div class="cui-text-info-hover">Info</div>
    <div class="cui-text-success-hover">Success</div>
    <div class="cui-text-warning-hover">Warning</div>
    <div class="cui-text-error-hover">Error</div>
  </div>
  <div class="cui-demo-grid cui-demo-panels">
    <div class="cui-bg-primary-hover">Primary</div>
    <div class="cui-bg-secondary-hover">Secondary</div>
    <div class="cui-bg-note-hover">Note</div>
    <div class="cui-bg-info-hover">Info</div>
    <div class="cui-bg-success-hover">Success</div>
    <div class="cui-bg-warning-hover">Warning</div>
    <div class="cui-bg-error-hover">Error</div>
  </div>
`);

export const demoLinkColor = dedent(`
  <div class="cui-demo-grid cui-demo-panels">
    <a href="#" class="cui-link-primary">Primary</a>
    <a href="#" class="cui-link-secondary">Secondary</a>
    <a href="#" class="cui-link-note">Note</a>
    <a href="#" class="cui-link-info">Info</a>
    <a href="#" class="cui-link-success">Success</a>
    <a href="#" class="cui-link-warning">Warning</a>
    <a href="#" class="cui-link-error">Error</a>
  </div>
`);

export const demoResetColor = dedent(`
  <p class="cui-text-secondary">
    Muted text with a <a href="#" class="cui-text-reset">reset link</a>.
  </p>
`);

// ----- spacings

export const demoSpacing = dedent(`
  <div class="cui-demo-spacing">
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-xl cui-p-xl">xl</div>
    </div>
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-l cui-p-l">l</div>
    </div>
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-m cui-p-m">m</div>
    </div>
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-s cui-p-s">s</div>
    </div>
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-xs cui-p-xs">xs</div>
    </div>
    <div class="cui-bg-secondary">
      <div class="cui-bg-primary cui-m-0 cui-p-0">0</div>
    </div>
  </div>
`);

export const demoMargin = dedent(`
  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>
  <p><button cui-button mode="primary" look="link" class="cui-button-pull-out">A correctly aligned link button</button></p>
  <p>At vero eos et accusam et justo duo dolores et ea rebum.</p>
`);

// ----- typography

export const demoSize = dedent(`
  <p class="cui-text-xl">.cui-text-xl Extra large text</p>
  <p class="cui-text-l">.cui-text-l Large text</p>
  <p class="cui-text-m">.cui-text-m Normal text</p>
  <p class="cui-text-s">.cui-text-s Small text</p>
  <p class="cui-text-xs">.cui-text-xs Extra small text</p>
`);

export const demoAlign = dedent(`
  <p class="cui-text-left">Left aligned text.</p>
  <p class="cui-text-center">Center aligned text.</p>
  <p class="cui-text-right">Right aligned text.</p>
`);

export const demoBreak = dedent(`
  <p class="cui-break-word">mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm</p>
`);

export const demoSelect = dedent(`
  <p class="cui-select">This paragraph has default select behavior.</p>
  <p class="cui-no-select">This paragraph will not be selectable when clicked by the user.</p>
`);

export const demoTruncate1 = dedent(`
  <p class="cui-ellipsis" style="max-width: 400px;">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
  </p>
`);

export const demoTruncate2 = dedent(`
  <p class="cui-ellipsis-2" style="max-width: 400px; max-height: 40px;">
    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
  </p>
`);

// ----- ratio

export const demoRatio = dedent(`
  <div class="cui-demo-ratio">
    <div class="cui-ratio cui-ratio-1x1 cui-bg-secondary">
      <div>1x1</div>
    </div>
    <div class="cui-ratio cui-ratio-4x3 cui-bg-secondary">
      <div>4x3</div>
    </div>
    <div class="cui-ratio cui-ratio-16x9 cui-bg-secondary">
      <div>16x9</div>
    </div>
    <div class="cui-ratio cui-ratio-21x9 cui-bg-secondary">
      <div>21x9</div>
    </div>
    <div class="cui-ratio cui-ratio-3x1 cui-bg-secondary">
      <div>3x1</div>
    </div>
    <div class="cui-ratio cui-ratio-6x1 cui-bg-secondary">
      <div>6x1</div>
    </div>
  </div>
`);

// ----- visibility

export const demoVisibility = dedent(`
  <h2 class="cui-visually-hidden">Title for screen readers</h2>
  <a class="cui-visually-hidden-focusable" href="#content">Skip to main content</a>
  <div class="cui-visually-hidden-focusable">A container with a <a href="#">focusable element</a>.</div>
`);
