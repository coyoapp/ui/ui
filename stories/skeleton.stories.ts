import { LoadingModule, SkeletonComponent } from '@coyoapp/ui/loading';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import dedent from 'ts-dedent';
import { inputArgs } from './util/arg-inject';
import { color, number } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Loaders/Skeleton',
  component: SkeletonComponent,
  decorators: [
    width(360),
    moduleMetadata({
      imports: [LoadingModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=739%3A1222')
  },
  argTypes: {
    color: color(),
    highlight: color(),
    height: number(0)
  }
} as Meta;

export const Skeleton: StoryObj<SkeletonComponent> = {
  args: {
    skeletons: [
      { top: 0.5, left: 0.5, width: 2.5, height: 2.5, radius: '50%' },
      { top: 0.5, left: 4, width: '20%', height: 1 },
      { top: 0.5, left: 'calc(4.5rem + 20%)', width: '20%', height: 1 },
      { top: 0.5, left: 'calc(5.5rem + 40%)', width: 'calc([20,40]% - [5, 10]px)', height: 1 },
      { top: 2, left: 4, width: 4, height: 1 },
      { top: 2, left: 8.5, width: '[20,40]%', height: 1 }
    ]
  },
  parameters: {
    docs: {
      source: {
        code: dedent`
        <cui-skeleton [skeletons]="[
          { top: 8, left: 8, width: 40, height: 40, radius: '50%' },
          { top: 8, left: 64, width: '70%', height: 16 },
          { top: 32, left: 64, width: 64, height: 16 },
          { top: 32, left: 136, width: 'calc(100% - 154px)', height: 16 }
        ]"></cui-skeleton>
      `
      }
    }
  },
  render: (args: any) => ({
    template: `<cui-skeleton
    ${ inputArgs('skeletons', 'color', 'height', 'highlight') }
  ></cui-skeleton>`,
    props: args
  })
};
