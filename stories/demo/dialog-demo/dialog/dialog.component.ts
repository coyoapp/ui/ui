import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {CuiDialogReference, DialogReference} from "@coyoapp/ui/dialog";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent {

  constructor(@Inject(DialogReference) public dialogRef: CuiDialogReference<string, number>) {}

  closeOne(): void {
    this.dialogRef.close(1);
  }

  closeTwo(): void {
    this.dialogRef.close(2);
  }
}
