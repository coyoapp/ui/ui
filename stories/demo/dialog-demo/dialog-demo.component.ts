import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {DialogService} from "@coyoapp/ui/dialog/dialog/dialog.service";
import {DialogComponent} from "./dialog/dialog.component";
import {DialogWidth} from "@coyoapp/ui/dialog";

@Component({
  selector: 'cui-toast-demo',
  templateUrl: './dialog-demo.component.html',
  styleUrls: ['./dialog-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogDemoComponent {

  @Input() content?: string;

  constructor(private dialogService: DialogService) {}

  openLg(): void {
    this.dialogService.open<number, string>(DialogComponent, {width: DialogWidth.LARGE, data: this.content});
  }

  openSm(): void {
    this.dialogService.open<number, string>(DialogComponent, {width: DialogWidth.SMALL, data: this.content});
  }

  openMd(): void {
    this.dialogService.open<number, string>(DialogComponent, {width: DialogWidth.MEDIUM, data: this.content});
  }
}
