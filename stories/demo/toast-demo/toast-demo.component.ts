import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ToastService } from '@coyoapp/ui/toast/toast/toast.service';

@Component({
  selector: 'cui-toast-demo',
  templateUrl: './toast-demo.component.html',
  styleUrls: ['./toast-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastDemoComponent {

  constructor(private toastService: ToastService) {}

  show() {
    this.toastService.show('Hi there!', 'Default toast');
  }

  success() {
    this.toastService.success('Yeah!', 'Success toast');
  }

  error() {
    this.toastService.error('Oh no!', 'Error toast');
  }

  showPersisting() {
    this.toastService.show('Here to stay.', 'Default toast', true);
  }

  showAction() {
    this.toastService.show('Click me.', 'Default toast', false, {
      button: {
        ariaLabel: 'Button-Label',
        id: 'button-id',
        title: 'Button Title'
      },
      link: {
        ariaLabel: 'Link-Label',
        id: 'link-id',
        title: 'Link Title'
      }
    }).onAction().subscribe(trigger => {
      alert(trigger.id + ' was clicked.');
    });
  }

  showButton(message: string) {
    this.toastService.show(message, 'Default toast with button', false, {
      button: {
        ariaLabel: 'Button-Label',
        id: 'button-id',
        title: 'Button Title'
      }
    }).onAction().subscribe(trigger => {
      alert(trigger.id + ' was clicked.');
    });
  }

  showLink(message: string) {
    this.toastService.show(message, 'Default toast with link', false, {
      link: {
        ariaLabel: 'Link-Label',
        id: 'link-id',
        title: 'Link Title'
      }
    }).onAction().subscribe(trigger => {
      alert(trigger.id + ' was clicked.');
    });
  }

  showHtmlContent() {
    this.toastService.show(
      'Custom content <a href="http://haiilo.com" target="_blank">link</a>',
      'Default toast',
      false,
      {
        enableHtml: true
      }
    );
  }
}
