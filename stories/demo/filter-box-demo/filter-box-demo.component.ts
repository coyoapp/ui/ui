import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';

@Component({
  selector: 'cui-filter-box-demo',
  templateUrl: './filter-box-demo.component.html',
  styleUrls: ['./filter-box-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterBoxDemoComponent {
  formGroup = this.fb.group({
    search: [''],
    selectFilter1: [],
    selectFilter2: [],
    checkboxFilter: [false],
    checkboxFilter2: [false],
    radioFilter: ['ASC'],
    toggleFilter: [false],
    dateFilter: ['']
  });

  selectFilter = [
    { id: 1, label: 'Carmen Kuphal' },
    { id: 2, label: 'Rick Will' },
    { id: 3, label: 'Wanda Berge' }
  ];
  selectFilter2 = [
    { id: 4, label: 'Dr. Bridget Glover' },
    { id: 5, label: 'Brad Raynor' },
    { id: 6, label: 'Rosie Hoppe' }
  ];
  collapsed = false;

  @Output() readonly filtersChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor(private fb: UntypedFormBuilder) {
    this.formGroup.valueChanges.subscribe(() => this.filtersChanged.emit(this.formGroup.getRawValue()));
  }

  resetUserFilters() {
    this.formGroup.get('selectFilter1')?.reset();
    this.formGroup.get('selectFilter2')?.reset();
  }
}
