import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import type { HEX, Palette } from '@coyoapp/ui/theme';

@Component({
  selector: 'cui-palette-directive-demo',
  templateUrl: './palette-directive-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaletteDirectiveDemoComponent {

  @Input()
  color: HEX = '#0073e6';

  @Input()
  overrides?: Partial<Palette>;

  @Input()
  lightFill: HEX = '#ffffff';

  @Input()
  darkFill: HEX = '#000000';
}
