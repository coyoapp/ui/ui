import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { SortDirection } from '@coyoapp/ui/table';

export interface Links {
  id: number;
  name: string;
  url: string;
  priority: number;
}

export const links: Links[] = [
  {
    id: 9,
    name: 'Link2',
    url: '/home/link2',
    priority: 7
  },
  {
    id: 1,
    name: 'Welcome',
    url: '/home/welcome',
    priority: 2
  },
  {
    id: 2,
    name: 'Onboarding',
    url: '/home/onboarding',
    priority: 5
  },
  {
    id: 10,
    name: 'Link3',
    url: '/home/link3',
    priority: 12
  },
  {
    id: 12,
    name: 'Link4',
    url: '/home/link4',
    priority: 15
  },
  {
    id: 3,
    name: 'Org-Chart',
    url: '/home/org-chart',
    priority: 1
  },
  {
    id: 4,
    name: 'Startseite',
    url: '/home/startseite',
    priority: 0
  }
];

@Component({
  selector: 'cui-table-sortable-rows-demo',
  templateUrl: './table-sortable-rows-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableSortableRowsDemoComponent {
  links: Links[] = links;
  sortedLinks: Links[] = links;

  private sortOptions?: {
    column: keyof Links,
    direction: SortDirection
  };

  constructor(private cdRef: ChangeDetectorRef) {}

  sort(column: keyof Links, direction: SortDirection) {
    this.sortOptions = { column, direction };
    this.sortedLinks = this.sortArrayOfObjects(
      this.links,
      this.sortOptions.column,
      this.sortOptions.direction
    );
    this.cdRef.markForCheck();
  }

  rowById(index: number, row: Links) {
    return row.id;
  }

  private sortArrayOfObjects<T>(
    data: T[],
    keyToSort: keyof T,
    direction: SortDirection
  ) {
    if (direction === 'NONE') {
      return data;
    }
    return [...data].sort((objectA: T, objectB: T) => {
      const valueA = objectA[keyToSort];
      const valueB = objectB[keyToSort];

      if (typeof valueA === 'number' && typeof valueB === 'number') {
        return direction === 'ASC' ? (valueA - valueB) : valueB - valueA;
      } else if (typeof valueA === 'string' && typeof valueB === 'string') {
        return direction === 'ASC' ? (valueA).localeCompare(valueB) : (valueB).localeCompare(valueA);
      } else {
        return -1;
      }
    });
  }
}
