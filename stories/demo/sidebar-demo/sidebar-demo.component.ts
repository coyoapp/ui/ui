import { Component, Input } from '@angular/core';

@Component({
  selector: 'cui-sidebar-demo',
  templateUrl: './sidebar-demo.component.html'
})
export class SidebarDemoComponent {
  @Input() direction = 'left';
  @Input() width = 25;
}
