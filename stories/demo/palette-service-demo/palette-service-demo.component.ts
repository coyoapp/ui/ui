import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ColorService } from '@coyoapp/ui/theme/color/color.service';
import { Palette } from '@coyoapp/ui/theme/palette/palette';
import { PaletteService } from '@coyoapp/ui/theme/palette/palette.service';

@Component({
  selector: 'cui-palette-demo',
  templateUrl: './palette-service-demo.component.html',
  styleUrls: ['./palette-service-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaletteServiceDemoComponent implements OnChanges {
  palette?: Palette;

  @Input()
  color = '#ffffff';

  constructor(
    private colorService: ColorService,
    private paletteService: PaletteService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    const color = changes.color?.currentValue || '';
    this.palette = this.build(color);
  }

  private build(color: string): Palette | undefined {
    const hex = /^#([\dA-Fa-f]{6}|[\dA-Fa-f]{3})$/.exec(color);
    if (hex) {
      return this.paletteService.build(color);
    }

    const rgba = /^rgba\((\d+),\s*(\d+),\s*(\d+),\s*(\d+(?:\.\d+)?)\)$/.exec(color);
    if (rgba) {
      const [_, r, g, b] = rgba;
      return this.paletteService.build(this.fromRgb(r, g, b));
    }

    const hsla = /^hsla\((\d+),\s*(\d+)%,\s*(\d+)%,\s*(\d+(?:\.\d+)?)\)$/.exec(color);
    if (hsla) {
      const [_, h, s, l] = hsla;
      return this.paletteService.build(this.fromHsl(h, s, l));
    }

    return undefined;
  }

  private fromRgb(r: string, g: string, b: string) {
    return this.colorService.rgbToHex({
      r: Number.parseInt(r, 10),
      g: Number.parseInt(g, 10),
      b: Number.parseInt(b, 10)
    });
  }

  private fromHsl(h: string, s: string, l: string) {
    return this.colorService.rgbToHex(
      this.colorService.hslToRgb({
        h: Number.parseInt(h, 10) / 360,
        s: Number.parseInt(s, 10) / 100,
        l: Number.parseInt(l, 10) / 100
      })
    );
  }
}
