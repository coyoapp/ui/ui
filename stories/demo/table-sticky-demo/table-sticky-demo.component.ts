import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'cui-table-sticky-demo',
  templateUrl: './table-sticky-demo.component.html',
  styleUrls: ['./table-sticky-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableStickyRowsDemoComponent {}
