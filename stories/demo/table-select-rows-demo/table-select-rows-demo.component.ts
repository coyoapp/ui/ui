import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { CheckboxChangeEvent } from '@coyoapp/ui/checkbox';

interface Homepage {
  id: number;
  name: string;
  url: string;
  webApp: boolean;
  coyoApp: boolean;
}

const homepages: Homepage[] = [
  {
    id: 1,
    name: 'Welcome',
    url: '/home/welcome',
    webApp: false,
    coyoApp: true
  },
  {
    id: 2,
    name: 'Onboarding',
    url: '/home/onboarding',
    webApp: false,
    coyoApp: true
  },
  {
    id: 3,
    name: 'Org-Chart',
    url: '/home/org-chart',
    webApp: true,
    coyoApp: false
  },
  {
    id: 4,
    name: 'Startseite',
    url: '/home/startseite',
    webApp: true,
    coyoApp: false
  }
];

@Component({
  selector: 'cui-table-select-rows-demo',
  templateUrl: './table-select-rows-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableSelectRowsDemoComponent {
  homepages: Homepage[] = homepages;
  selection: { [key: string]: boolean } = {};
  selectAllValue = false;
  isIndeterminate = false;

  constructor(private cdRef: ChangeDetectorRef) {
    this.homepages?.forEach(homepage => {
      this.selection[homepage.id] = false;
    });
  }

  selectAll(state: CheckboxChangeEvent) {
    this.homepages?.forEach(homepage => {
      this.selection[homepage.id] = state.checked;
    });
    this.checkIfIndeterminate();
  }

  rowById(index: number, row: Homepage) {
    return row.id;
  }

  checkIfIndeterminate(): void {
    const checkedCount = this.homepages.filter(link => this.selection[link.id]).length;
    const indeterminate = checkedCount > 0 && checkedCount < this.homepages.length;
    this.selectAllValue = !!indeterminate || this.homepages.length === checkedCount;
    this.isIndeterminate = indeterminate;
    this.cdRef.markForCheck();
  }

  selectionChanged(event: any) {
    this.checkIfIndeterminate();
  }
}
