import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'cui-table-collapsable-rows-demo',
  templateUrl: './table-collapsable-rows-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableCollapsableRowsDemoComponent {
  collapsed = false;
}
