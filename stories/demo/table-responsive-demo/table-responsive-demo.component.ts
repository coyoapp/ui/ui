import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'cui-table-responsive-demo',
  templateUrl: './table-responsive-demo.component.html',
  styleUrls: ['./table-responsive-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableResponsiveRowsDemoComponent {}
