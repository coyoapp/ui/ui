import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { IconModule } from '@coyoapp/ui/icon';
import { InputTextComponent, InputTextModule } from '@coyoapp/ui/input-text';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, number, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Input Text',
  component: InputTextComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule, FormFieldModule, IconModule.forRoot(), InputTextModule]
    })
  ],
  argTypes: {
    required: boolean(),
    placeholder: text(),
    minlength: number(),
    maxlength: number(),
    pattern: text(),
    clearable: boolean(),
    change: action('change')
  }
} as Meta;

export const InputText: StoryObj<InputTextComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-text [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'minlength', 'maxlength', 'pattern', 'clearable')}
        ${outputArgs('change')}
      ></cui-input-text>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputText.args = {
  placeholder: ''
};
