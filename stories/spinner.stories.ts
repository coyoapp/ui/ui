import { LoadingModule, SpinnerComponent } from '@coyoapp/ui/loading';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { size } from './util/arg-types';
import { figma } from './util/parameters';

export default {
  title: 'Loaders/Spinner',
  component: SpinnerComponent,
  decorators: [
    moduleMetadata({
      imports: [LoadingModule]
    })
  ],
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=754%3A1228')
  },
  argTypes: {
    size
  }
} as Meta;

export const Template: StoryObj<SpinnerComponent> = {
  args: {},
  render: (args: any) => ({
    props: args
  })
};
