import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoHead1,
  demoHead2,
  demoInline,
  demoBlockquote1,
  demoBlockquote2,
  demoList1,
  demoList2,
  demoTable,
} from "./util/typography";

export default {
  title: "Foundation/Typography/Tables",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Tables = {
  render: () => ({
    template: cuiDemo(demoTable()),
  }),

  name: "Tables",
  height: "285px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTable(),
      },
    },
  },
};

export const TablesStriped = {
  render: () => ({
    template: cuiDemo(demoTable("cui-table-striped")),
  }),

  name: "Tables (striped)",
  height: "285px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTable("cui-table-striped"),
      },
    },
  },
};

export const TablesHover = {
  render: () => ({
    template: cuiDemo(demoTable("cui-table-hover")),
  }),

  name: "Tables (hover)",
  height: "285px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTable("cui-table-hover"),
      },
    },
  },
};

export const TablesBordered = {
  render: () => ({
    template: cuiDemo(demoTable("cui-table-bordered")),
  }),

  name: "Tables (bordered)",
  height: "286px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoTable("cui-table-bordered"),
      },
    },
  },
};
