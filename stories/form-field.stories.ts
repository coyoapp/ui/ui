import { FormsModule } from '@angular/forms';
import { FormFieldComponent, FormFieldModule } from '@coyoapp/ui/form-field';
import { IconModule } from '@coyoapp/ui/icon';
import { InputTextModule } from '@coyoapp/ui/input-text';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { InputPasswordModule } from '@coyoapp/ui/input-password';
import { InputNumberModule } from '@coyoapp/ui/input-number';
import { InputTextareaModule } from '@coyoapp/ui/input-textarea';
import { InputTimepickerModule } from '@coyoapp/ui/input-timepicker';
import { InputDatepickerModule } from '@coyoapp/ui/input-datepicker';
import { MenuModule } from '@coyoapp/ui/menu';
import { AngularMyDatePickerModule } from '@nodro7/angular-mydatepicker';
import { boolean, radio, size } from './util/arg-types';
import { figma } from './util/parameters';
import { width } from './util/decorator';
import { inputArgs } from './util/arg-inject';

export default {
  title: 'Forms/Form Field',
  component: FormFieldComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule,
        IconModule.forRoot(),
        InputTextModule,
        InputTextareaModule,
        InputDatepickerModule.forRoot(),
        InputTimepickerModule,
        InputNumberModule,
        FormFieldModule,
        InputPasswordModule,
        MenuModule.forRoot(),
        AngularMyDatePickerModule
      ]
    })
  ],
  argTypes: {
    mode: radio(['primary', 'secondary', 'ghost']),
    size,
    count: boolean(),
    disabled: boolean(),
    clearable: boolean()
  }
} as Meta;

export const TextFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('label', 'size', 'count', 'mode')}>
      <cui-icon name="user" [size]="size" cuiFormPrefix></cui-icon>
      <cui-input-text [(ngModel)]="value" maxlength="20" [disabled]="disabled" [clearable]="clearable"></cui-input-text>
      <span cuiFormSuffix>Suffix</span>
    </cui-form-field>`
});
TextFormField.args = {
  label: 'Label',
  size: 'm',
  count: true,
  mode: 'primary'
};

export const PasswordFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('size', 'mode')}>
     <cui-input-password [(ngModel)]="value" [disabled]="disabled"></cui-input-password>
    </cui-form-field>`
});
PasswordFormField.args = {
  size: 'm',
  mode: 'primary'
};

export const NumberFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('size', 'mode')}>
     <cui-input-number [(ngModel)]="value" [disabled]="disabled"></cui-input-number>
    </cui-form-field>`
});
NumberFormField.args = {
  size: 'm',
  mode: 'primary'
};

export const TextareaFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('size', 'mode')}>
     <cui-input-textarea [(ngModel)]="value" [disabled]="disabled"></cui-input-textarea>
    </cui-form-field>`
});
TextareaFormField.args = {
  size: 'm',
  mode: 'primary'
};

export const TimepickerFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('size', 'mode')}>
      <cui-input-timepicker [(ngModel)]="value" [disabled]="disabled"></cui-input-timepicker>
    </cui-form-field>`
});
TimepickerFormField.args = {
  size: 'm',
  mode: 'primary'
};

export const DatepickerFormField: StoryObj<FormFieldComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field ${inputArgs('size', 'mode')}>
      <cui-input-datepicker [(ngModel)]="value" [disabled]="disabled"></cui-input-datepicker>
    </cui-form-field>`
});
DatepickerFormField.args = {
  size: 'm',
  mode: 'primary'
};
