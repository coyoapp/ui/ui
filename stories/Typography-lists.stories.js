import { width } from "./util/decorator";
import { cuiDemo } from "./util/demo";
import {
  demoHead1,
  demoHead2,
  demoInline,
  demoBlockquote1,
  demoBlockquote2,
  demoList1,
  demoList2,
  demoTable,
} from "./util/typography";

export default {
  title: "Foundation/Typography/Lists",

  parameters: {
    viewMode: "docs",

    previewTabs: {
      canvas: {
        hidden: true,
      },
    },
  },
};

export const Lists = {
  render: () => ({
    template: cuiDemo(demoList1),
  }),

  name: "Lists",
  height: "196px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoList1,
      },
    },
  },
};

export const ListsPlain = {
  render: () => ({
    template: cuiDemo(demoList2),
  }),

  name: "Lists (plain)",
  height: "120px",

  parameters: {
    layout: "fullscreen",

    docs: {
      source: {
        code: demoList2,
      },
    },
  },
};
