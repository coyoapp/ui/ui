import { FormsModule } from '@angular/forms';
import { FormFieldModule } from '@coyoapp/ui/form-field';
import { InputTextareaComponent, InputTextareaModule } from '@coyoapp/ui/input-textarea';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { IconModule } from '@coyoapp/ui/icon';
import { inputArgs, outputArgs } from './util/arg-inject';
import { action, boolean, number, text } from './util/arg-types';
import { width } from './util/decorator';
import { figma } from './util/parameters';

export default {
  title: 'Forms/Input Textarea',
  component: InputTextareaComponent,
  parameters: {
    badges: ['live'],
    design: figma('https://www.figma.com/file/EUBHqbtZaPcQaPDdEJ3r88/Components?node-id=8%3A7')
  },
  decorators: [
    width(360),
    moduleMetadata({
      imports: [FormsModule, IconModule.forRoot(), FormFieldModule, InputTextareaModule]
    })
  ],
  argTypes: {
    required: boolean(),
    placeholder: text(),
    minlength: number(),
    maxlength: number(),
    pattern: text(),
    change: action('change')
  }
} as Meta;

export const InputTextarea: StoryObj<InputTextareaComponent> = (args: any) => ({
  props: args,
  template: `
    <cui-form-field>
      <cui-input-textarea [(ngModel)]="value"
        ${inputArgs('required', 'placeholder', 'minlength', 'maxlength', 'pattern')}
        ${outputArgs('change')}
      ></cui-input-textarea>
    </cui-form-field>
    <pre>Value: {{ value }}</pre>`
});
InputTextarea.args = {
  placeholder: ''
};
