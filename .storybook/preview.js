import { setCompodocJson } from '@storybook/addon-docs/angular';
import docJson from '../dist/compodoc/documentation.json';
setCompodocJson(docJson);

const themes = {
  'Theme: Blue': {
    'cui-primary-bg': '#0073e6',
    'cui-primary-bg-hover': '#2693ff',
    'cui-primary-bg-active': '#0060bf',
    'cui-primary-fill': '#ffffff',
    'cui-primary-fill-hover': '#ffffff',
    'cui-primary-fill-active': '#ffffff',
    'cui-primary-text': '#0073e6',
    'cui-primary-text-hover': '#0073e6',
    'cui-primary-text-active': '#0073e6'
  },
  'Theme: Cyan': {
    'cui-primary-bg': '#b2ebf2',
    'cui-primary-bg-hover': '#e0f7fa',
    'cui-primary-bg-active': '#80deea',
    'cui-primary-fill': '#000000',
    'cui-primary-fill-hover': '#000000',
    'cui-primary-fill-active': '#000000',
    'cui-primary-text': '#00838f',
    'cui-primary-text-hover': '#00838f',
    'cui-primary-text-active': '#00838f'
  },
  'Theme: Pink': {
    'cui-primary-bg': '#c2185b',
    'cui-primary-bg-hover': '#d81b60',
    'cui-primary-bg-active': '#ad1457',
    'cui-primary-fill': '#ffffff',
    'cui-primary-fill-hover': '#ffffff',
    'cui-primary-fill-active': '#ffffff',
    'cui-primary-text': '#ad1457',
    'cui-primary-text-hover': '#ad1457',
    'cui-primary-text-active': '#ad1457'
  }
}

export const globalTypes = {
  rem: {
    name: 'REM',
    description: 'Document root font size',
    defaultValue: '16px',
    toolbar: {
      icon: 'paragraph',
      items: ['12px', '16px', '20px', '24px']
    }
  },
  theme: {
    name: 'Theme',
    description: 'Theme color',
    defaultValue: 'Primary: Dark Blue',
    toolbar: {
      icon: 'globe',
      items: Object.keys(themes)
    }
  },
  border: {
    name: 'Border radius',
    description: 'Border radius',
    defaultValue: 'medium',
    toolbar: {
      icon: 'grow',
      items: ['none', 'small', 'medium', 'large']
    }
  }
};

export const decorators = [
  (storyFunc, context) => {
    document.documentElement.style.fontSize = context.globals.rem;
    return storyFunc();
  },
  (storyFunc, context) => {
    const theme = themes[context.globals.theme];
    for (var prop in theme) {
      document.documentElement.style.setProperty(`--${prop}`, theme[prop]);
    }
    return storyFunc();
  },
  (storyFunc, context) => {
    const setProperty = (s, m, l) => {
      document.documentElement.style.setProperty('--cui-border-radius-s', `${s}rem`);
      document.documentElement.style.setProperty('--cui-border-radius-m', `${m}rem`);
      document.documentElement.style.setProperty('--cui-border-radius-l', `${l}rem`);
    };
    switch (context.globals.border) {
      case 'small': setProperty(.0625, .125, .25); break;
      case 'medium': setProperty(.125, .25, .5); break;
      case 'large': setProperty(.25, .5, 1); break;
      default: setProperty(0, 0, 0); break;
    }
    return storyFunc();
  }
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  layout: 'centered',
  options: {
    storySort: {
      order: [
        'Foundation', ['Usage', 'Colors', 'Elevations',
          'Typography',
            ['Typography', 'Blockquotes', 'Code', 'Lists', 'Tables'],
          'Utilities',
            ['Colors', 'Spacings', 'Typography', 'Aspect ratio', 'Visibility']
        ],
        'Loaders', ['Spinner', 'Skeleton'],
        'UI',
        'Forms',
        'Interaction'
      ]
    }
  },
  backgrounds: {
    default: null,
    values: [{
      name: 'light',
      value: '#f2f3f4'
    }, {
      name: 'dark',
      value: '#101d30'
    }]
  },
  a11y: {
    options: {
      runOnly: {
        type: 'tag',
        values: ['wcag2a', 'wcag2aa']
      }
    }
  },
  badgesConfig: {
    live: {
      contrast: '#FFF',
      color: '#789c16',
      title: 'Live'
    },
    beta: {
      contrast: '#FFF',
      color: '#8627e6',
      title: 'Beta'
    },
    deprecated: {
      contrast: '#FFF',
      color: '#bf2600',
      title: 'Deprecated'
    }
  }
}
