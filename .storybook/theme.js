import { create } from '@storybook/theming/create';
import logo from './public/coyo.png';

export default create({
  base: 'light',
  colorPrimary: '#101D30',
  colorSecondary: '#0073E6',
  fontBase: '"Source Sans Pro", sans-serif',
  fontCode: '"Source Code Pro", monospace',
  brandTitle: 'COYO UI',
  brandUrl: 'https://coyoapp.atlassian.net/wiki/spaces/PL',
  brandImage: logo,
});
