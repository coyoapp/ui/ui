module.exports = {
  "stories": ["../stories/**/*.mdx", "../stories/**/*.stories.@(js|jsx|ts|tsx)"],

  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-actions",
    '@storybook/addon-a11y',
    "@storybook/addon-mdx-gfm"
  ],

  docs: {
    autodocs: true
  },

  framework: {
    name: "@storybook/angular",
    options: {}
  }
}
