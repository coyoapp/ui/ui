# COYO UI Framework (CUI)

## Development Guidelines

### Code contributions and publication

* Use **npm** as your package manager.
* The *master* branch is protected. All features and bugfixes are to be developed via **feature branches**.
* Branches should correspond to JIRA tickets and must be merged using a **squash commit**.
* We are using [Standard Version](https://github.com/conventional-changelog/standard-version) for versioning using [semver](https://semver.org/) and CHANGELOG generation powered by [Conventional Commits](https://conventionalcommits.org/). Please make sure that **every commit follows the *Conventional Commits* specification**.
* Use *Standard Version* and the CI/CD to **publish a new version**. You can use the following commands to
  * bump the version in *projects/ui/package.json*
  * generate a new *CHANGELOG.md* based on commit messages
  * commit *projects/ui/package.json* and *CHANGELOG.md*
  * create a new release tag

<Source
  language='bash'
  code={dedent`
     $ npm run release:{patch|minor|major}
     $ git push --follow-tags origin master
  `}
/>

### Writing stories

* Every component must be **documented in code** and have a corresponding **storybook story**.
* Mark your component with appropriate **status badges**. The following badges are available:
  * **ready**
  * **beta**
  * **deprecated**

<Source
  language='js'
  code={dedent`
    export default {
      title: 'UI/My Component',
      component: MyComponent,
      parameters: {
        badges: ['ready']
      },
      ...
    } as Meta;
  `}
/>

### Modules & Imports

* CUI is built tree-shakeable using **one module per component**. Each module must have:
  * `index.ts`
  * `package.json`
  * `public-api.ts`
* Imports within one module should use **relative imports**, e.g. `import { MenuItemComponent } from '../menu-item/menu-item.component';`.
* Imports from other modules should use **absolute imports**, e.g. `import { FocusKeyManagerProvider } from '@coyoapp/ui/cdk';`.

### Local Setup to link the local Pattern Library to the Coyoapp

* In the Coyoapp repository package.json (/coyoapp/coyo-frontend/ngx/package.json)
  replace the @coyoapp/ui library version with the local path of the Pattern Library dist folder:  
  **From:** `"@coyoapp/ui": "x.y.z"`  
  **To:** `"@coyoapp/ui": "/path/to/local/coyo/dist/ui"`  
* In the PL run the build command: `npm run build:ci`
* In the Coyoapp repository recompile the _packages.json_ file running: `yarn upgrade @coyoapp/ui`
* After the packages are recompiled start the Coyoapp: `yarn start`
* Don't commit the _packages.json_ changes

## Publishing

The library can be published by running `npm publish`. However, the preferred way to publish a new version of the
library is via the CI setup. A new version of the library will automatically be published to our
[npmjs.com](https://www.npmjs.com/package/@coyoapp/ui) when pushing a new tag on the master branch. To simplyfy
this process, you can use the following npm commands:

 * **npm run release:major** releases a new *major* version
 * **npm run release:minor** releases a new *minor* version
 * **npm run release:patch** releases a new *patch* version

These commands will automatically increase the version number in the `package.json` and `package-lock.json`, update
the [CHANGELOG.md](https://gitlab.com/coyoapp/ui/ui/-/blob/master/CHANGELOG.md), commit the changed files and create a
corresponding git tag. Follow the instructions printed in your terminal to push everything to the remote branch. This
will then automatically publish a new library version at both [npmjs.com](https://www.npmjs.com/package/@coyoapp/ui)
as well as on [GitLab](https://gitlab.com/coyoapp/ui/ui/-/releases).
